// Copyright 2017 Tomasz Kosek. All rights reserved.

#ifndef MULTI_THREADED_RENDERER_ENGINE_DEBUG_DEBUG_INFO_HPP_
#define MULTI_THREADED_RENDERER_ENGINE_DEBUG_DEBUG_INFO_HPP_

#include <thread>  // NOLINT

namespace engine {
namespace debug {
class Debug_info final {
 public:
    INLINE Debug_info() = default;
    INLINE ~Debug_info();
    INLINE std::thread::id get_thread_id() const;
    INLINE void set_thread_id_if_empty();
    INLINE void unsetup();

 private:
    std::thread::id thread_id_;
    const std::thread::id empty_thread_id_;
};
}  // namespace debug
}  // namespace engine

#if !defined _DEBUG
#include <engine\debug\Debug_info.inl>
#endif

#endif  // MULTI_THREADED_RENDERER_ENGINE_DEBUG_DEBUG_INFO_HPP_
