// Copyright 2017 Tomasz Kosek. All rights reserved.

#include <engine\debug\Assert.hpp>

namespace engine {
namespace debug {
INLINE void Debug_info::set_thread_id_if_empty() {
    if (thread_id_ == empty_thread_id_) {
        thread_id_ = std::this_thread::get_id();
    }
    ENGINE_ASSERT(thread_id_ == std::this_thread::get_id(), "Thread must not change!");
}
INLINE Debug_info::~Debug_info() {
    unsetup();
}
INLINE void Debug_info::unsetup() {
    thread_id_ = empty_thread_id_;
}
INLINE std::thread::id Debug_info::get_thread_id() const {
    return thread_id_;
}
}  // namespace debug
}  // namespace engine
