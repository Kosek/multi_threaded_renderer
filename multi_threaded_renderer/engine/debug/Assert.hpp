// Copyright 2017 Tomasz Kosek. All rights reserved.

#ifndef MULTI_THREADED_RENDERER_ENGINE_DEBUG_ASSERT_HPP_
#define MULTI_THREADED_RENDERER_ENGINE_DEBUG_ASSERT_HPP_

#include <engine\logging\Logger.hpp>

#if defined _DEBUG
#define ENGINE_ASSERT_ON
#endif

#if defined _DEBUG
#define ENGINE_ASSERT_IN_DEBUG_CONFIGURATION_ONLY(condition, ____message____) {\
if (!(condition)) {\
engine::logging::Logger::error(__FILE__, __LINE__, __FUNCTION__, ____message____);\
int zero = 0;\
int crash = 1 / zero;}\
}
#else
#define ENGINE_ASSERT_IN_DEBUG_CONFIGURATION_ONLY(condition, ____message____)
#endif

#if defined ENGINE_ASSERT_ON
#define ENGINE_ASSERT(condition, ____message____) {\
if (!(condition)) {\
engine::logging::Logger::error(__FILE__, __LINE__, __FUNCTION__, ____message____);\
int zero = 0;\
int crash = 1 / zero;}\
}
#else
#define ENGINE_ASSERT(condition, ____message____)
#endif

#endif  // MULTI_THREADED_RENDERER_ENGINE_DEBUG_ASSERT_HPP_
