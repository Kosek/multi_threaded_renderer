// Copyright 2017 Tomasz Kosek. All rights reserved.

#include <stdafx.h>

#include <engine\debug\Debug_info.hpp>

#if defined _DEBUG
#include <engine\debug\Debug_info.inl>
#endif
