// Copyright 2017 Tomasz Kosek. All rights reserved.

#ifndef MULTI_THREADED_RENDERER_ENGINE_GLOBAL_DEFS_NONCOPYABLE_HPP_
#define MULTI_THREADED_RENDERER_ENGINE_GLOBAL_DEFS_NONCOPYABLE_HPP_

namespace engine {
class Noncopyable {
 public:
    INLINE Noncopyable(const Noncopyable&) = delete;
    INLINE const Noncopyable& operator=(const Noncopyable&) = delete;
    INLINE Noncopyable(Noncopyable&&) = default;
    INLINE Noncopyable& operator=(Noncopyable&&) = default;

 protected:
    INLINE Noncopyable() = default;
};
}  // namespace engine

#endif  // MULTI_THREADED_RENDERER_ENGINE_GLOBAL_DEFS_NONCOPYABLE_HPP_
