// Copyright 2017 Tomasz Kosek. All rights reserved.

#ifndef MULTI_THREADED_RENDERER_ENGINE_GLOBAL_DEFS_INLINE_HPP_
#define MULTI_THREADED_RENDERER_ENGINE_GLOBAL_DEFS_INLINE_HPP_

#include <xnamath.h>

#if defined _DEBUG
#define INLINE
#else
#define INLINE XMINLINE
#endif


#endif  // MULTI_THREADED_RENDERER_ENGINE_GLOBAL_DEFS_INLINE_HPP_
