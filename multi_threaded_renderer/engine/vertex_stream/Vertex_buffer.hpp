// Copyright 2017 Tomasz Kosek. All rights reserved.

#ifndef MULTI_THREADED_RENDERER_ENGINE_VERTEX_STREAM_VERTEX_BUFFER_HPP_
#define MULTI_THREADED_RENDERER_ENGINE_VERTEX_STREAM_VERTEX_BUFFER_HPP_

#include <d3d11.h>
#include <memory>
#include <vector>

#include <engine\naming\Unique_name.hpp>
#include <engine\utilities\memory\Raw_ptr.hpp>
#include <engine\vertex_stream\Stride_schema.hpp>

namespace engine {
namespace vertex_stream {
class Vertex_buffer final : public Noncopyable {
 public:
    class Name : public naming::Unique_name {
        using naming::Unique_name::Unique_name;
    };

 public:
    INLINE Vertex_buffer(const Name& name);
    INLINE ~Vertex_buffer();
    INLINE void release();
    template <typename VERTEX_STRIDE_SCHEMA>
    INLINE void setup(ID3D11Device* device, const std::vector<VERTEX_STRIDE_SCHEMA>& vertices);
    INLINE void setup(ID3D11Device* device, const Stride_schema* vertex_stride_schema,
        const std::vector<uint8_t>& buffer_blob);
    INLINE ID3D11Buffer* get_buffer_ptr() const;
    INLINE const Name& get_name() const;
    INLINE const Stride_schema* get_vertex_stream_stride_schema() const;

 private:
    const Name name_;
    engine::utilities::memory::Raw_ptr<const Stride_schema> element_schema_;
    engine::utilities::memory::Raw_ptr<ID3D11Buffer> vertex_buffer_;
};
}  // namespace vertex_stream
}  // namespace engine

#include <engine\vertex_stream\Vertex_buffer_header.inl>

#if !defined _DEBUG
#include <engine\vertex_stream\Vertex_buffer.inl>
#endif


#endif  // MULTI_THREADED_RENDERER_ENGINE_VERTEX_STREAM_VERTEX_BUFFER_HPP_
