// Copyright 2017 Tomasz Kosek. All rights reserved.

#include <vector>

namespace engine {
namespace vertex_stream {
INLINE Vertex_buffer::Vertex_buffer(const Name& name)
    : name_(name) {
}
INLINE Vertex_buffer::~Vertex_buffer() {
    release();
}
INLINE void Vertex_buffer::release() {
    if (vertex_buffer_) {
        vertex_buffer_->Release();
    }
    vertex_buffer_ = nullptr;
}
INLINE ID3D11Buffer* Vertex_buffer::get_buffer_ptr() const {
    return vertex_buffer_;
}
INLINE const Vertex_buffer::Name& Vertex_buffer::get_name() const {
    return name_;
}
INLINE void Vertex_buffer::setup(ID3D11Device* device, const Stride_schema* vertex_stride_schema,
    const std::vector<uint8_t>& buffer_blob) {
    ENGINE_ASSERT(device, "Device must not be null!");
    ENGINE_ASSERT(vertex_stride_schema, "Vertex stride schema must not be null!");

    release();

    element_schema_ = vertex_stride_schema;

    D3D11_BUFFER_DESC bd;
    std::memset(&bd, 0, sizeof(bd));
    bd.Usage = D3D11_USAGE_IMMUTABLE;
    bd.ByteWidth = static_cast<unsigned int>(buffer_blob.size());
    bd.BindFlags = D3D11_BIND_VERTEX_BUFFER;

    const UINT UNUSED = 0;
    D3D11_SUBRESOURCE_DATA srd;
    srd.pSysMem = static_cast<const void*>(buffer_blob.data());
    srd.SysMemPitch = UNUSED;
    srd.SysMemSlicePitch = UNUSED;

    HRESULT r = device->CreateBuffer(&bd, &srd, &vertex_buffer_);
    ENGINE_ASSERT(r == NOERROR, "Error while creating vertex buffer!");
}
INLINE const Stride_schema* Vertex_buffer::get_vertex_stream_stride_schema() const {
    return element_schema_;
}
}  // namespace vertex_stream
}  // namespace engine
