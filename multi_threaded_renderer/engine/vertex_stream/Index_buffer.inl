// Copyright 2017 Tomasz Kosek. All rights reserved.

#include <vector>

#include <engine\debug\Assert.hpp>

namespace engine {
namespace vertex_stream {
INLINE Index_buffer::Index_buffer(const Id& name)
    : name_(name) {
}
INLINE Index_buffer::~Index_buffer() {
    release();
}
INLINE void Index_buffer::setup(ID3D11Device* device,
    const std::vector<unsigned int>& indices) {
    ENGINE_ASSERT(device, "Device must not be null!");

    release();

    indices_count_ = static_cast<unsigned int>(indices.size());

    D3D11_BUFFER_DESC bd;
    std::memset(&bd, 0, sizeof(bd));
    bd.Usage = D3D11_USAGE_IMMUTABLE;
    bd.ByteWidth = sizeof(std::vector<unsigned int>::value_type) * indices_count_;
    bd.BindFlags = D3D11_BIND_INDEX_BUFFER;

    const UINT UNUSED = 0;
    D3D11_SUBRESOURCE_DATA srd;
    srd.pSysMem = static_cast<const void*>(indices.data());
    srd.SysMemPitch = UNUSED;
    srd.SysMemSlicePitch = UNUSED;

    HRESULT r = device->CreateBuffer(&bd, &srd, &index_buffer_);
    ENGINE_ASSERT(r == NOERROR, "Error while creating index buffer!");
}
INLINE void Index_buffer::release() {
    if (index_buffer_) {
        index_buffer_->Release();
    }
    index_buffer_ = nullptr;
    indices_count_ = 0;
}
INLINE ID3D11Buffer* Index_buffer::get_buffer_ptr() const {
    return index_buffer_;
}
INLINE const Index_buffer::Id& Index_buffer::get_name() const {
    return name_;
}
INLINE unsigned int Index_buffer::get_indices_count() const {
    return indices_count_;
}
}  // namespace vertex_stream
}  // namespace engine
