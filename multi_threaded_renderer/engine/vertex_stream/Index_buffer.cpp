// Copyright 2017 Tomasz Kosek. All rights reserved.

#include <stdafx.h>

#include <engine\vertex_stream\Index_buffer.hpp>

#if defined _DEBUG
#include <engine\vertex_stream\Index_buffer.inl>
#endif
