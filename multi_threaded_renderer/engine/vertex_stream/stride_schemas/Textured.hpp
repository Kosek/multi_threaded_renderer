// Copyright 2017 Tomasz Kosek. All rights reserved.

#ifndef MULTI_THREADED_RENDERER_ENGINE_VERTEX_STREAM_STRIDE_SCHEMAS_TEXTURED_HPP_
#define MULTI_THREADED_RENDERER_ENGINE_VERTEX_STREAM_STRIDE_SCHEMAS_TEXTURED_HPP_

#include <D3DX10math.h>

#include <engine\assets3\Material.hpp>
#include <engine\vertex_stream\Stride_schema.hpp>

namespace engine {
namespace vertex_stream {
namespace stride_schemas {
class Textured final : public Stride_schema {
 public:
    struct Stride {
        XMFLOAT3 position;
        XMFLOAT2 texcoord;
    };
    INLINE Textured();
    INLINE static const Textured* get_instance();
    INLINE static bool is_compatible_with(const engine::naming::Unique_id& material_id);

 private:
    static const Textured instance_;
    static const unsigned int INPUT_ELEMENT_DESCRIPTION_ELEMENTS_COUNT = 3;
    D3D11_INPUT_ELEMENT_DESC input_element_description
        [INPUT_ELEMENT_DESCRIPTION_ELEMENTS_COUNT] = {
        { "POSITION", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, 0,
            D3D11_INPUT_PER_VERTEX_DATA, 0 },
        { "TEXCOORD", 0, DXGI_FORMAT_R32G32_FLOAT, 0, D3D11_APPEND_ALIGNED_ELEMENT,
            D3D11_INPUT_PER_VERTEX_DATA, 0 },
        { "SV_InstanceID", 0, DXGI_FORMAT_R32_UINT, 1, 0,
            D3D11_INPUT_PER_INSTANCE_DATA, 1 }
    };
};
}  // namespace stride_schemas
}  // namespace vertex_stream
}  // namespace engine

#if !defined _DEBUG
#include <engine\vertex_stream\stride_schemas\Textured.inl>
#endif

#endif  // MULTI_THREADED_RENDERER_ENGINE_VERTEX_STREAM_STRIDE_SCHEMAS_TEXTURED_HPP_
