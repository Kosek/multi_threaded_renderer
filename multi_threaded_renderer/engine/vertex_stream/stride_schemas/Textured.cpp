// Copyright 2017 Tomasz Kosek. All rights reserved.

#include <stdafx.h>

#include <engine\vertex_stream\stride_schemas\Textured.hpp>

#if defined _DEBUG
#include <engine\vertex_stream\stride_schemas\Textured.inl>
#endif

namespace engine {
namespace vertex_stream {
namespace stride_schemas {
const Textured Textured::instance_;
}  // namespace stride_schemas
}  // namespace vertex_stream
}  // namespace engine
