// Copyright 2017 Tomasz Kosek. All rights reserved.

#include <engine\assets3\materials\textured\Textured_material.hpp>

namespace engine {
namespace vertex_stream {
namespace stride_schemas {
INLINE Textured::Textured()
    : Stride_schema(Stride_schema::Id(__FUNCTION__), sizeof(Stride), input_element_description,
        INPUT_ELEMENT_DESCRIPTION_ELEMENTS_COUNT) {
}

INLINE const Textured* Textured::get_instance() {
    return &instance_;
}
INLINE bool Textured::is_compatible_with(const engine::naming::Unique_id& material_id) {
    return material_id.compare(engine::assets3::materials::Textured::ID) == 0;
}
}  // namespace stride_schemas
}  // namespace vertex_stream
}  // namespace engine
