// Copyright 2017 Tomasz Kosek. All rights reserved.

#ifndef MULTI_THREADED_RENDERER_ENGINE_VERTEX_STREAM_INDEX_BUFFER_HPP_
#define MULTI_THREADED_RENDERER_ENGINE_VERTEX_STREAM_INDEX_BUFFER_HPP_

#include <d3d11.h>
#include <memory>
#include <vector>

#include <engine\naming\Unique_name.hpp>
#include <engine\utilities\memory\Raw_ptr.hpp>

namespace engine {
namespace vertex_stream {
class Index_buffer final : public Noncopyable {
 public:
    class Id : public naming::Unique_name {
        using naming::Unique_name::Unique_name;
    };

 public:
    INLINE Index_buffer(const Id& name);
    INLINE ~Index_buffer();
    INLINE void release();
    INLINE void setup(ID3D11Device* device, const std::vector<unsigned int>& indices);
    INLINE ID3D11Buffer* get_buffer_ptr() const;
    INLINE const Id& get_name() const;
    INLINE unsigned int get_indices_count() const;

 private:
    const Id name_;
    engine::utilities::memory::Raw_ptr<ID3D11Buffer> index_buffer_;
    unsigned int indices_count_ = 0;
};
}  // namespace vertex_stream
}  // namespace engine

#if !defined _DEBUG
#include <engine\vertex_stream\Index_buffer.inl>
#endif


#endif  // MULTI_THREADED_RENDERER_ENGINE_VERTEX_STREAM_INDEX_BUFFER_HPP_
