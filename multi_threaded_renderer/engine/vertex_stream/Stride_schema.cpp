// Copyright 2017 Tomasz Kosek. All rights reserved.

#include <stdafx.h>

#include <engine\vertex_stream\Stride_schema.hpp>

#if defined _DEBUG
#include <engine\vertex_stream\Stride_schema.inl>
#endif
