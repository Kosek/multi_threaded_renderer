// Copyright 2017 Tomasz Kosek. All rights reserved.

#include <engine\debug\Assert.hpp>

namespace engine {
namespace vertex_stream {
INLINE Stride_schema::Stride_schema(
    const Id& name,
    const unsigned int stride_size,
    const D3D11_INPUT_ELEMENT_DESC* input_element_descritpion,
    const unsigned int input_element_description_elements_count)
    : name_(name),
    stride_size_(stride_size),
    input_element_descritpion_(input_element_descritpion),
    input_element_description_elements_count_(input_element_description_elements_count) {
    ENGINE_ASSERT(input_element_descritpion_, "Input element description must not be null!");
    ENGINE_ASSERT(input_element_description_elements_count > 0,
        "Input element descritption elements count must not be 0!");
}
INLINE const Stride_schema::Id& Stride_schema::get_name() const {
    return name_;
}
INLINE const unsigned int Stride_schema::get_stride_size() const {
    return stride_size_;
}
INLINE const D3D11_INPUT_ELEMENT_DESC* Stride_schema::get_input_element_descritpion() const {
    return input_element_descritpion_;
}
INLINE const unsigned int Stride_schema::get_input_element_description_elements_count() const {
    return input_element_description_elements_count_;
}
}  // namespace vertex_stream
}  // namespace engine
