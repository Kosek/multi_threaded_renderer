// Copyright 2017 Tomasz Kosek. All rights reserved.

#include <stdafx.h>

#include <engine\vertex_stream\Vertex_buffer.hpp>

#if defined _DEBUG
#include <engine\vertex_stream\Vertex_buffer.inl>
#endif
