// Copyright 2017 Tomasz Kosek. All rights reserved.

#ifndef MULTI_THREADED_RENDERER_ENGINE_VERTEX_STREAM_STRIDE_SCHEMA_HPP_
#define MULTI_THREADED_RENDERER_ENGINE_VERTEX_STREAM_STRIDE_SCHEMA_HPP_

#include <d3dx11.h>

#include <engine\naming\Unique_name.hpp>
#include <engine\utilities\memory\Raw_ptr.hpp>

namespace engine {
namespace vertex_stream {
class Stride_schema : public Noncopyable {
 public:
    class Id : public naming::Unique_name {
        using naming::Unique_name::Unique_name;
    };
    INLINE Stride_schema(
        const Id& name,
        const unsigned int stride_size,
        const D3D11_INPUT_ELEMENT_DESC* input_element_descritpion,
        const unsigned int input_element_description_elements_count);
    INLINE const Id& get_name() const;
    INLINE const unsigned int get_stride_size() const;
    INLINE const D3D11_INPUT_ELEMENT_DESC* get_input_element_descritpion() const;
    INLINE const unsigned int get_input_element_description_elements_count() const;

 private:
    const Id name_;
    const unsigned int stride_size_ = 0;
    engine::utilities::memory::Raw_ptr<const D3D11_INPUT_ELEMENT_DESC> input_element_descritpion_;
    const unsigned int input_element_description_elements_count_ = 0;
};
}  // namespace vertex_stream
}  // namespace engine

#if !defined _DEBUG
#include <engine\vertex_stream\Stride_schema.inl>
#endif

#endif  // MULTI_THREADED_RENDERER_ENGINE_VERTEX_STREAM_STRIDE_SCHEMA_HPP_
