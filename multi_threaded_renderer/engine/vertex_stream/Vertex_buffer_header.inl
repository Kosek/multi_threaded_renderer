// Copyright 2017 Tomasz Kosek. All rights reserved.

#include <vector>

#include <engine\debug\Assert.hpp>

namespace engine {
namespace vertex_stream {
template <typename VERTEX_STRIDE_SCHEMA>
INLINE void Vertex_buffer::setup(ID3D11Device* device, const std::vector<VERTEX_STRIDE_SCHEMA>&
    vertices) {
    setup(device, VERTEX_STRIDE_SCHEMA::get_instance(), static_cast<const std::vector<uint8_t>>(
        vertices));
}
}  // namespace vertex_stream
}  // namespace engine
