// Copyright 2017 Tomasz Kosek. All rights reserved.

namespace engine {
namespace assets {
INLINE const Script::Name& Script::get_name() const {
    return static_cast<const Script::Name&>(Asset::get_name());
}
}  // namespace assets
}  // namespace engine
