// Copyright 2017 Tomasz Kosek. All rights reserved.

#include <memory>
#include <utility>

namespace engine {
namespace assets {
template <class ASSET_TYPE>
INLINE Asset::Ptr<ASSET_TYPE>::Ptr(ASSET_TYPE* asset)
    : asset_(asset) {
    if (asset_) {
        asset_->inc_refs_count();
    }
}
template <class ASSET_TYPE>
INLINE Asset::Ptr<ASSET_TYPE>::Ptr(const Ptr<ASSET_TYPE>& asset_ptr) {
    (*this) = asset_ptr;
}
template <class ASSET_TYPE>
template<class U>
INLINE Asset::Ptr<ASSET_TYPE>::Ptr(const Ptr<U>& ptr) {
    attach(ptr.get());
}
template <class ASSET_TYPE>
INLINE Asset::Ptr<ASSET_TYPE>::~Ptr() {
    detach();
}
template <class ASSET_TYPE>
INLINE Asset::Ptr<ASSET_TYPE>& Asset::Ptr<ASSET_TYPE>::operator=(
    const Ptr<ASSET_TYPE>& asset_ptr) {
    attach(asset_ptr.get());
    return *this;
}
template <class ASSET_TYPE>
INLINE void Asset::Ptr<ASSET_TYPE>::detach() {
    if (asset_) {
        asset_->dec_refs_count();
        if (asset_->get_refs_count() == 0) {
            Asset::add_asset_to_destroy(asset_);
        }
        asset_ = nullptr;
    }
}
template <class ASSET_TYPE>
INLINE void Asset::Ptr<ASSET_TYPE>::attach(ASSET_TYPE* asset) {
    detach();
    if (asset) {
        asset_ = asset;
        asset_->inc_refs_count();
    }
}
template <class ASSET_TYPE>
INLINE void Asset::Ptr<ASSET_TYPE>::reset() {
    detach();
}
template <class ASSET_TYPE>
INLINE ASSET_TYPE* Asset::Ptr<ASSET_TYPE>::get() const {
    return asset_;
}
template <class ASSET_TYPE>
INLINE Asset::Ptr<ASSET_TYPE>::operator bool() const {
    return asset_ != nullptr;
}
template <class ASSET_TYPE>
INLINE ASSET_TYPE* Asset::Ptr<ASSET_TYPE>::operator->() const {
    return asset_;
}
template <class ASSET_TYPE>
template<typename... Args>
INLINE Asset::Ptr<ASSET_TYPE> Asset::Ptr<ASSET_TYPE>::make_asset(Args&&... args) {
    return Asset::Ptr<ASSET_TYPE>(new ASSET_TYPE(std::forward<Args>(args)...));
}
template <class ASSET_TYPE>
INLINE Asset::Ptr<ASSET_TYPE> Asset::Ptr<ASSET_TYPE>::make_from_pointer(
    std::unique_ptr<ASSET_TYPE>&& asset) {
    return Asset::Ptr<ASSET_TYPE>(asset.release());
}
template <class ASSET_TYPE>
INLINE Asset::Ptr<ASSET_TYPE> Asset::Ptr<ASSET_TYPE>::make_from_pointer(ASSET_TYPE* asset) {
    return Asset::Ptr<ASSET_TYPE>(asset);
}
}  // namespace assets
}  // namespace engine
