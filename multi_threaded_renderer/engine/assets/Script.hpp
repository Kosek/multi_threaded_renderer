// Copyright 2017 Tomasz Kosek. All rights reserved.

#ifndef MULTI_THREADED_RENDERER_ENGINE_ASSETS_SCRIPT_HPP_
#define MULTI_THREADED_RENDERER_ENGINE_ASSETS_SCRIPT_HPP_

#include <engine\assets\Asset.hpp>
#include <engine\frame\update\Receiver.hpp>

namespace engine {
namespace assets {

class Script : public Asset,  public engine::frame::update::Receiver {
 public:
    class Name : public Asset::Name {
        using Asset::Name::Name;
    };

    INLINE const Script::Name& get_name() const;

 protected:
    using Asset::Asset;
};
}  // namespace assets
}  // namespace engine

#if !defined _DEBUG
#include <engine\assets\Script.inl>
#endif

#endif  // MULTI_THREADED_RENDERER_ENGINE_ASSETS_SCRIPT_HPP_
