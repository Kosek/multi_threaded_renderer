// Copyright 2017 Tomasz Kosek. All rights reserved.

#ifndef MULTI_THREADED_RENDERER_ENGINE_ASSETS_ASSET_HPP_
#define MULTI_THREADED_RENDERER_ENGINE_ASSETS_ASSET_HPP_

#include <memory>
#include <utility>

#include <engine\data_structures\Embedded_list.hpp>
#include <engine\debug\Debug_info.hpp>
#include <engine\debug\Engine_debug.hpp>
#include <engine\naming\Unique_name.hpp>
#include <engine\utilities\memory\Raw_ptr.hpp>

namespace engine {
namespace assets {

extern const char ASSET_TO_REMOVE_NULL_NOT_ALLOWED[];
extern const char ASSET_TO_REMOVE_ELEMENT_ALREADY_IN_LIST[];
extern const char ASSET_TO_REMOVE_ELEMENT_NOT_IN_CURRENT_LIST[];

class Asset : public Noncopyable {
 public:
    class Name : public naming::Unique_name {
        using naming::Unique_name::Unique_name;
    };
    template <class ASSET_TYPE>
    class Ptr final {
     public:
        INLINE Ptr() = default;
        INLINE Ptr(const Ptr<ASSET_TYPE>& asset_ptr);
        template<class U>
        INLINE Ptr(const Ptr<U>& ptr);
        INLINE ~Ptr();
        INLINE Ptr<ASSET_TYPE>& operator=(const Ptr<ASSET_TYPE>& asset_ptr);
        INLINE ASSET_TYPE* get() const;
        INLINE operator bool() const;
        INLINE void reset();
        INLINE ASSET_TYPE* operator->() const;
        template<typename... Args>
        INLINE static Ptr<ASSET_TYPE> make_asset(Args&&... args);
        INLINE static Ptr<ASSET_TYPE> make_from_pointer(std::unique_ptr<ASSET_TYPE>&& asset);
        INLINE static Ptr<ASSET_TYPE> make_from_pointer(ASSET_TYPE* asset);

     private:
        INLINE Ptr(ASSET_TYPE* asset);
        INLINE void detach();
        INLINE void attach(ASSET_TYPE* asset_ptr);

        engine::utilities::memory::Raw_ptr<ASSET_TYPE> asset_;
    };

    INLINE const Name& get_name() const;
    virtual ~Asset();

 private:
    class Assets_to_destroy_list_element;
 public:
    class Assets_to_destroy_list final : public engine::data_structures::Embedded_list<
        Assets_to_destroy_list,
        Asset,
        Assets_to_destroy_list_element,
        ASSET_TO_REMOVE_NULL_NOT_ALLOWED,
        ASSET_TO_REMOVE_ELEMENT_ALREADY_IN_LIST,
        ASSET_TO_REMOVE_ELEMENT_NOT_IN_CURRENT_LIST> {
    };

 protected:
    INLINE Asset(const Name& name);
    INLINE unsigned int get_refs_count() const;

 private:
    class Assets_to_destroy_list_element final :
         public engine::data_structures::Embedded_list<
        Assets_to_destroy_list,
        Asset,
        Assets_to_destroy_list_element,
        ASSET_TO_REMOVE_NULL_NOT_ALLOWED,
        ASSET_TO_REMOVE_ELEMENT_ALREADY_IN_LIST,
        ASSET_TO_REMOVE_ELEMENT_NOT_IN_CURRENT_LIST>::Element {
     public:
        using Type = engine::data_structures::Embedded_list<
            Assets_to_destroy_list,
            Asset,
            Assets_to_destroy_list_element,
            ASSET_TO_REMOVE_NULL_NOT_ALLOWED,
            ASSET_TO_REMOVE_ELEMENT_ALREADY_IN_LIST,
            ASSET_TO_REMOVE_ELEMENT_NOT_IN_CURRENT_LIST>::Element;

        INLINE static Type& get_embedded_list_element(Asset* e);
        INLINE static const Type& get_embedded_list_element(const Asset* e);
    };

    INLINE void inc_refs_count();
    INLINE void dec_refs_count();
    static void add_asset_to_destroy(Asset* asset);  // Implementation moved to .cpp not to make
                                                     // circular dependencies with Context.hpp

    const Name name_;
    unsigned int refs_count_ = 0;
    Assets_to_destroy_list_element assets_to_destroy_list_element_;
    ENGINE_DEBUG(engine::debug::Debug_info debug_info_;)
};
}  // namespace assets
}  // namespace engine

#include <engine\assets\Asset_header.inl>

#if !defined _DEBUG
#include <engine\assets\Asset.inl>
#endif

#endif  // MULTI_THREADED_RENDERER_ENGINE_ASSETS_ASSET_HPP_
