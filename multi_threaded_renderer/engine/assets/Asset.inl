// Copyright 2017 Tomasz Kosek. All rights reserved.

#include <engine\data_structures\Embedded_list.hpp>
#include <engine\debug\Assert.hpp>
#include <engine\threading\Local_storage.hpp>

namespace engine {
namespace assets {
INLINE Asset::Asset(const Name& name)
    : name_(name) {
    ENGINE_DEBUG(debug_info_.set_thread_id_if_empty());
    ENGINE_ASSERT_IN_DEBUG_CONFIGURATION_ONLY(debug_info_.get_thread_id() ==
        engine::threading::Local_storage::get_main_context()->get_debug_info().get_thread_id(),
        "Asset may only be created on main game context thread!");
}
INLINE const Asset::Name& Asset::get_name() const {
    return name_;
}
INLINE void Asset::inc_refs_count() {
    ENGINE_ASSERT(std::this_thread::get_id() == debug_info_.get_thread_id(),
        "Reference count incrementation may only be called on main game context thread that asset was created on!");  // NOLINT
    ++refs_count_;
}
INLINE void Asset::dec_refs_count() {
    ENGINE_ASSERT(std::this_thread::get_id() == debug_info_.get_thread_id(),
        "Reference count decrementation may only be called on main game context thread that asset was created on!");  // NOLINT
    --refs_count_;
}
INLINE unsigned int Asset::get_refs_count() const {
    return refs_count_;
}

}  // namespace assets
}  // namespace engine

namespace engine {
namespace assets {
INLINE Asset::Assets_to_destroy_list_element::Type&
Asset::Assets_to_destroy_list_element::get_embedded_list_element(Asset* a) {
    return a->assets_to_destroy_list_element_;
}
INLINE const Asset::Assets_to_destroy_list_element::Type&
Asset::Assets_to_destroy_list_element::get_embedded_list_element(const Asset* a) {
    return a->assets_to_destroy_list_element_;
}
}  // namespace assets
}  // namespace engine
