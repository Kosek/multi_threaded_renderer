// Copyright 2017 Tomasz Kosek. All rights reserved.

#include <stdafx.h>

#include <engine\assets\Asset.hpp>
#include <engine\game\Context.hpp>
#include <engine\threading\Local_storage.hpp>

#if defined _DEBUG
#include <engine\assets\Asset.inl>
#endif

namespace engine {
namespace assets {

extern const char ASSET_TO_REMOVE_NULL_NOT_ALLOWED[] = { "Asset must not be null!" };
extern const char ASSET_TO_REMOVE_ELEMENT_ALREADY_IN_LIST[] = { "Asset already added to list!" };
extern const char ASSET_TO_REMOVE_ELEMENT_NOT_IN_CURRENT_LIST[] = {
    "Asset is not added to current list!" };

Asset::~Asset() {
}

void Asset::add_asset_to_destroy(Asset* asset) {
    engine::threading::Local_storage::get_main_context()->add_asset_to_destroy(asset);
}

}  // namespace assets
}  // namespace engine
