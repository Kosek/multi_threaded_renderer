// Copyright 2017 Tomasz Kosek. All rights reserved.

#ifndef MULTI_THREADED_RENDERER_ENGINE_GAME_PLAYER_HPP_
#define MULTI_THREADED_RENDERER_ENGINE_GAME_PLAYER_HPP_

#include <engine\game\player\input\Controllers_state.hpp>
#include <engine\rendering\Window_fwd.hpp>

namespace engine {
namespace game {

class Player final : public Noncopyable {
 public:
    INLINE const player::input::Controllers_state& get_input_controllers_state() const;
    INLINE void set_window_that_mouse_coordinates_will_be_calculated_relative_to(
        const engine::rendering::Window* window);

 private:
    player::input::Controllers_state input_controllers_state_;
};

}  // namespace game
}  // namespace engine

#if !defined _DEBUG
#include <engine\game\Player.inl>
#endif

#endif  // MULTI_THREADED_RENDERER_ENGINE_GAME_PLAYER_HPP_
