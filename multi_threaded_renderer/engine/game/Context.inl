// Copyright 2017 Tomasz Kosek. All rights reserved.

#include <memory>
#include <utility>

#include <engine\debug\Assert.hpp>
#include <engine\rendering\Renderer.hpp>

namespace engine {
namespace game {
INLINE Context::Context(const Name& name)
    : name_(name), scenes_(this),
    renderer_synchronization_(*this) {
}
INLINE Context::~Context() {
    ENGINE_ASSERT(!is_renderer_synchronized(),
        "Context may not be destroyed when there is a renderer synchronized to it!");
    destroy_assets_that_are_pending_for_destruction();
}
INLINE const Context::Name& Context::get_name() const {
    return name_;
}
INLINE void Context::add(engine::scene3::Scene* scene) {
    scenes_.add(scene);
    scenes_index_.add(scene);
}
INLINE void Context::remove(engine::scene3::Scene* scene) {
    ENGINE_ASSERT(scene, "Scene must not be null!");
    scenes_.remove(scene);
    scenes_index_.remove(scene->get_name());
    if (scene->is_receive_update_enabled()) {
        updater_.remove(scene);
    }
}
INLINE void Context::remove(const engine::scene3::Scene::Name& name) {
    remove(get(name));
}
INLINE engine::scene3::Scene* Context::get(const engine::scene3::Scene::Name& name) {
    return scenes_index_.get(name);
}
INLINE const engine::scene3::Scene* Context::get(const engine::scene3::Scene::Name& name) const {
    return scenes_index_.get(name);
}
INLINE void Context::add(engine::assets::Script* s) {
    scripts_.add(s);
}
INLINE void Context::remove(const engine::assets::Script* s) {
    remove(s->get_name());
}
INLINE void Context::remove(const engine::assets::Script::Name& name) {
    scripts_.remove(name);
}
INLINE const engine::assets::Script* Context::get(const engine::assets::Script::Name& name) const {
    return static_cast<const engine::assets::Script*>(scripts_.get(name));
}
INLINE engine::assets::Script* Context::get(const engine::assets::Script::Name& name) {
    return static_cast<engine::assets::Script*>(scripts_.get(name));
}
INLINE void Context::enable_update(const engine::scene3::Scene::Name& name) {
    updater_.add(get(name));
}
INLINE void Context::disable_update(const engine::scene3::Scene::Name& name) {
    updater_.remove(get(name));
}
INLINE void Context::enable_update(const engine::assets::Script::Name& name) {
    updater_.add(get(name));
}
INLINE void Context::disable_update(const engine::assets::Script::Name& name) {
    updater_.remove(get(name));
}
INLINE void Context::start() {
    current_frame_.update_frame_time();
}
INLINE void Context::stop() {
    ENGINE_ASSERT(!is_renderer_synchronized(),
        "Context should have no renderers synchronized before stopping!");
    destroy_assets_that_are_pending_for_destruction();
}
INLINE void Context::update() {
    if (is_main_context()) {
        current_frame_.update_frame_time();
        process_renderer_synchronization();
        if (is_renderer_synchronized()) {
#if !defined SINGLE_THREADED_RENDERING
            engine::threading::sync::Binary_point::Producer_guard producer_guard(
                get_sync_point_for_frame_simulation(), [this]() {
                process_renderer_synchronization();
                return is_renderer_synchronized();
            });
#endif
            destroy_assets_that_are_pending_for_destruction();
            updater_.update(this);
        } else {
            destroy_assets_that_are_pending_for_destruction();
            updater_.update(this);
        }
    } else {
        ENGINE_ASSERT(false, "Sub contexts are not supported!");
    }
}
INLINE bool Context::is_renderer_synchronized() const {
    return renderer_synchronization_.get_state() == Renderer_synchronization::State::SYNCHRONIZED;
}
INLINE void Context::process_renderer_synchronization() {
    switch (renderer_synchronization_.get_state()) {
    case Renderer_synchronization::State::SYNCHRONIZATION_REQ:
        renderer_synchronization_.set_synchronized();
        break;
    case Renderer_synchronization::State::DESYNCHRONIZATION_REQ:
        renderer_synchronization_.set_desynchronized();
        break;
    }
}
INLINE const Context* Context::get_main_context() const {
    // sub contexts are not supported yet
    return this;
}
INLINE Context* Context::get_main_context() {
    // sub contexts are not supported yet
    return this;
}
INLINE bool Context::is_main_context() const {
    return true;
}
INLINE engine::threading::sync::Binary_point& Context::get_sync_point_for_frame_simulation() {
    return sync_point_for_end_of_frame_simulation_;
}
INLINE const engine::threading::sync::Binary_point&
Context::get_sync_point_for_frame_simulation() const {
    return sync_point_for_end_of_frame_simulation_;
}
INLINE void Context::synchronize_renderer(const engine::rendering::Renderer* r) {
    renderer_synchronization_.request_synchronization(r);
    _ReadWriteBarrier();
    while (
        renderer_synchronization_.get_state() !=
        Renderer_synchronization::State::SYNCHRONIZED) {
        _ReadBarrier();
    }
}
INLINE void Context::desynchronize_renderer(const engine::rendering::Renderer* r) {
    renderer_synchronization_.request_desynchronization(r);
    _ReadWriteBarrier();
    while (
        renderer_synchronization_.get_state() !=
        Renderer_synchronization::State::DESYNCHRONIZED) {
        _ReadBarrier();
    }
}
INLINE engine::game::Player* Context::get_player() {
    return &player_;
}
INLINE const engine::game::Player* Context::get_player() const {
    return &player_;
}
INLINE std::unique_ptr<Context::Renderer_synchronizator> Context::create_renderer_synchronizator(
    const engine::rendering::Renderer* renderer) {
    return std::make_unique<Context::Renderer_synchronizator>(
        Context::Renderer_synchronizator::Construction_key(), this, renderer);
}
INLINE void Context::add_asset_to_destroy(engine::assets::Asset* asset) {
    assets_to_destroy_.add(asset);
}
INLINE void Context::destroy_assets_that_are_pending_for_destruction() {
    assets_to_destroy_.remove_all([](engine::assets::Asset* a) {
        delete a;
    });
}
INLINE const Context::iFrame& Context::get_current_frame() const {
    return current_frame_;
}
}  // namespace game
}  // namespace engine

namespace engine {
namespace game {
INLINE float Context::iFrame::get_frame_time_millis() const {
    return frame_time_;
}
}  // namespace game
}  // namespace engine

namespace engine {
namespace game {
INLINE void Context::Frame::update_frame_time() {
    std::chrono::system_clock::time_point now = std::chrono::system_clock::now();
    frame_time_ = std::chrono::duration<float, std::milli>(now - prev_frame_start_).count();
    prev_frame_start_ = now;
}
}  // namespace game
}  // namespace engine

namespace engine {
namespace game {
INLINE void Context::Updater::add(engine::scene3::Scene* s) {
    scenes_.add(s);
}
INLINE void Context::Updater::remove(engine::scene3::Scene* s) {
    scenes_.remove(s);
}
INLINE void Context::Updater::add(engine::frame::update::Receiver* r) {
    update_receivers_.add(r);
}
INLINE void Context::Updater::remove(engine::frame::update::Receiver* r) {
    update_receivers_.remove(r);
}
INLINE void Context::Updater::update(Context* c) {
    update_receivers_.get_iterator().visit_all([&c](engine::frame::update::Receiver* r) {
        r->update(c);
    });
    scenes_.get_iterator().visit_all([&c](engine::scene3::Scene* s) {
        s->update(c);
    });
}
}  // namespace game
}  // namespace engine
namespace engine {
namespace game {
INLINE int Context::Scripts_comparator::cmp(
    const engine::assets::Script::Name& key,
    const engine::assets::Script* s) const {
    return key.compare(get_key(s));
}
INLINE const engine::assets::Script::Name& Context::Scripts_comparator::get_key(
    const engine::assets::Script* s) const {
    return s->get_name();
}
}  // namespace game
}  // namespace engine

namespace engine {
namespace game {
INLINE Context::Renderer_synchronizator::Renderer_synchronizator(const Construction_key& key,
    Context* c, const engine::rendering::Renderer* r) : context_(c), renderer_(r) {
    ENGINE_ASSERT(context_, "Context must not be null!");
    ENGINE_ASSERT(renderer_, "Renderer must not be null!");
    context_->synchronize_renderer(renderer_);
}
INLINE Context::Renderer_synchronizator::~Renderer_synchronizator() {
    desynchronize_renderer();
}
INLINE void Context::Renderer_synchronizator::desynchronize_renderer() {
    if (!was_desynchronize_called_) {
        context_->desynchronize_renderer(renderer_);
    }
    was_desynchronize_called_ = true;
}
}  // namespace game
}  // namespace engine

namespace engine {
namespace game {
INLINE Context::Renderer_synchronization::Renderer_synchronization(const Context& context) :
    context_(context) {
}
INLINE void Context::Renderer_synchronization::request_synchronization(
    const engine::rendering::Renderer* renderer) {
    ENGINE_DEBUG(_ReadBarrier();)
    ENGINE_ASSERT_IN_DEBUG_CONFIGURATION_ONLY(
        std::this_thread::get_id() == renderer->get_debug_info().get_thread_id(),
        "Synchronization request was called not from requestor renderer's thread!");
    ENGINE_ASSERT(!renderer_, "Only one renderer may be synchronized to main context!");
    renderer_ = renderer;
    state_ = State::SYNCHRONIZATION_REQ;
    _WriteBarrier();
}
INLINE void Context::Renderer_synchronization::set_synchronized() {
    ENGINE_DEBUG(_ReadBarrier();)
    ENGINE_ASSERT(std::this_thread::get_id() == context_.get_debug_info().get_thread_id(),
        "Synchronization finish up (set_synchronized) was called not from owning context thread!");
    state_ = State::SYNCHRONIZED;
    _WriteBarrier();
}
INLINE void Context::Renderer_synchronization::request_desynchronization(
    const engine::rendering::Renderer* renderer) {
    ENGINE_DEBUG(_ReadBarrier();)
    ENGINE_ASSERT(renderer, "Renderer must not be null!");
    ENGINE_ASSERT(renderer_, "No renderer is synchronized!");
    ENGINE_ASSERT(renderer == renderer_,
        "Renderer requested for desynchronization is not the one being synchronized!");
    ENGINE_ASSERT_IN_DEBUG_CONFIGURATION_ONLY(
        std::this_thread::get_id() == renderer->get_debug_info().get_thread_id(),
        "Desynchronization request was called not from requestor renderer's thread!");
    state_ = State::DESYNCHRONIZATION_REQ;
    _WriteBarrier();
}
INLINE void Context::Renderer_synchronization::set_desynchronized() {
    ENGINE_DEBUG(_ReadBarrier();)
    ENGINE_ASSERT(std::this_thread::get_id() == context_.get_debug_info().get_thread_id(),
    "Desynchronization finish up (set_desynchronized) was called not from owning context thread!");
    state_ = State::DESYNCHRONIZED;
    renderer_ = nullptr;
    _WriteBarrier();
}
INLINE const Context::Renderer_synchronization::State
    Context::Renderer_synchronization::get_state() const {
    _ReadBarrier();
    return state_;
}
}  // namespace game
}  // namespace engine
