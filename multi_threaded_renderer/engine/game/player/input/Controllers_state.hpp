// Copyright 2017 Tomasz Kosek. All rights reserved.

#ifndef MULTI_THREADED_RENDERER_ENGINE_GAME_PLAYER_INPUT_CONTROLLERS_STATE_HPP_
#define MULTI_THREADED_RENDERER_ENGINE_GAME_PLAYER_INPUT_CONTROLLERS_STATE_HPP_

#include <memory>

#include <engine\game\player\input\Source.hpp>
#include <engine\rendering\Window_fwd.hpp>
#include <engine\utilities\memory\Raw_ptr.hpp>

namespace engine {
namespace game {
namespace player {
namespace input {

class Controllers_state final : public Noncopyable {
 public:
    INLINE Controllers_state() = default;
    template <class T> INLINE T get_state() const;
    template <source::mouse::key::Type> INLINE source::mouse::key::State get_state() const;
    template <source::keyboard::key::Type> INLINE source::keyboard::key::State get_state() const;
    INLINE void set_window_that_mouse_coordinates_will_be_calculated_relative_to(
        const engine::rendering::Window* window);

 private:
    template <source::mouse::key::Type> INLINE int get_mouse_key_code() const;
    template <source::keyboard::key::Type> INLINE int get_keyboard_key_code() const;

    engine::utilities::memory::Raw_ptr<const engine::rendering::Window>
        window_that_mouse_coordinates_will_be_calculated_relative_to_;
};

}  // namespace input
}  // namespace player
}  // namespace game
}  // namespace engine

#include <engine\game\player\input\Controllers_state_header.inl>

#if !defined _DEBUG
#include <engine\game\player\input\Controllers_state.inl>
#endif

#endif  // MULTI_THREADED_RENDERER_ENGINE_GAME_PLAYER_INPUT_CONTROLLERS_STATE_HPP_
