// Copyright 2017 Tomasz Kosek. All rights reserved.

namespace engine {
namespace game {
namespace player {
namespace input {
namespace source {
namespace mouse {
namespace analog {
namespace state {
INLINE Ball::Ball(float x, float y)
    : x_(x), y_(y) {
}

INLINE Wheel::Wheel(float wheel_delta)
    : wheel_delta_(wheel_delta) {
}
}  // namespace state
}  // namespace analog
}  // namespace mouse
}  // namespace source
}  // namespace input
}  // namespace player
}  // namespace game
}  // namespace engine
