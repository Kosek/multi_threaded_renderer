// Copyright 2017 Tomasz Kosek. All rights reserved.

#include <engine\debug\Assert.hpp>
#include <engine\rendering\Window.hpp>

namespace engine {
namespace game {
namespace player {
namespace input {

template <source::mouse::key::Type KEY>
INLINE source::mouse::key::State Controllers_state::get_state() const {
    return GetAsyncKeyState(get_mouse_key_code<KEY>()) ?
        source::mouse::key::State::PRESSED :
        source::mouse::key::State::IDLE;
}

template <source::keyboard::key::Type KEY>
INLINE source::keyboard::key::State Controllers_state::get_state() const {
    return GetAsyncKeyState(get_keyboard_key_code<KEY>()) ?
        source::keyboard::key::State::PRESSED :
        source::keyboard::key::State::IDLE;
}

}  // namespace input
}  // namespace player
}  // namespace game
}  // namespace engine
