// Copyright 2017 Tomasz Kosek. All rights reserved.

#ifndef MULTI_THREADED_RENDERER_ENGINE_GAME_PLAYER_INPUT_SOURCE_HPP_
#define MULTI_THREADED_RENDERER_ENGINE_GAME_PLAYER_INPUT_SOURCE_HPP_

namespace engine {
namespace game {
namespace player {
namespace input {
namespace source {
namespace keyboard {
namespace key {
enum class Type {
    W, S, A, D, SPACE, LCTRL
};
enum class State {
    IDLE,
    PRESSED
};
}  // namespace key
}  // namespace keyboard
namespace pad {
namespace key {
enum class Type {
    R1, R2, L1, L2
};
enum class State {
    IDLE,
    PRESSED
};
}  // namespace key
namespace analog {
enum class Type {
    ANALOG_LEFT,
    ANALOG_RIGHT
};
}  // namespace analog
}  // namespace pad
namespace mouse {
namespace key {
enum class Type {
    LEFT,
    RIGHT
};
enum class State {
    IDLE,
    PRESSED
};
}  // namespace key
namespace analog {
namespace state {
class Ball final {
 public:
    INLINE Ball(float x, float y);

 private:
    const float x_ = 0;
    const float y_ = 0;
};
class Wheel final {
 public:
    INLINE Wheel(float wheel_delta);

 private:
    const float wheel_delta_ = 0;
};
}  // namespace state
}  // namespace analog
}  // namespace mouse
}  // namespace source
}  // namespace input
}  // namespace player
}  // namespace game
}  // namespace engine

#if !defined _DEBUG

#include <engine\game\player\input\Source.inl>

#endif

#endif  // MULTI_THREADED_RENDERER_ENGINE_GAME_PLAYER_INPUT_SOURCE_HPP_
