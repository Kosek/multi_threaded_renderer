// Copyright 2017 Tomasz Kosek. All rights reserved.

#include <engine\debug\Assert.hpp>

namespace engine {
namespace game {
namespace player {
namespace input {

template <>
INLINE int Controllers_state::get_mouse_key_code<source::mouse::key::Type::LEFT>() const {
    return VK_LBUTTON;
}

template <>
INLINE int Controllers_state::get_mouse_key_code<source::mouse::key::Type::RIGHT>() const {
    return VK_RBUTTON;
}

INLINE void Controllers_state::set_window_that_mouse_coordinates_will_be_calculated_relative_to(
    const engine::rendering::Window* window) {
    window_that_mouse_coordinates_will_be_calculated_relative_to_ = window;
}

template <>
INLINE source::mouse::analog::state::Ball
Controllers_state::get_state<source::mouse::analog::state::Ball>() const {
    POINT p;
    if (GetCursorPos(&p) && window_that_mouse_coordinates_will_be_calculated_relative_to_) {
        if (ScreenToClient(
            window_that_mouse_coordinates_will_be_calculated_relative_to_->get_hwnd(), &p)) {
            return source::mouse::analog::state::Ball(
                static_cast<float>(p.x), static_cast<float>(p.y));
        }
    }
    return source::mouse::analog::state::Ball(0, 0);
}

template <>
INLINE source::mouse::analog::state::Wheel
Controllers_state::get_state<source::mouse::analog::state::Wheel>() const {
    ENGINE_ASSERT(false, "todo");
    return source::mouse::analog::state::Wheel(0);
}

template <>
INLINE int Controllers_state::get_keyboard_key_code<source::keyboard::key::Type::W>() const {
    return 0x57;
}

template <>
INLINE int Controllers_state::get_keyboard_key_code<source::keyboard::key::Type::S>() const {
    return 0x53;
}

template <>
INLINE int Controllers_state::get_keyboard_key_code<source::keyboard::key::Type::A>() const {
    return 0x41;
}

template <>
INLINE int Controllers_state::get_keyboard_key_code<source::keyboard::key::Type::D>() const {
    return 0x44;
}

template <>
INLINE int Controllers_state::get_keyboard_key_code<source::keyboard::key::Type::SPACE>() const {
    return 0x20;
}

template <>
INLINE int Controllers_state::get_keyboard_key_code<source::keyboard::key::Type::LCTRL>() const {
    return 0xA2;
}

}  // namespace input
}  // namespace player
}  // namespace game
}  // namespace engine
