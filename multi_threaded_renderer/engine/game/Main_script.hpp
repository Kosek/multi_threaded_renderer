// Copyright 2017 Tomasz Kosek. All rights reserved.

#ifndef MULTI_THREADED_RENDERER_ENGINE_GAME_MAIN_SCRIPT_HPP_
#define MULTI_THREADED_RENDERER_ENGINE_GAME_MAIN_SCRIPT_HPP_

#include <engine\game\Context_fwd.hpp>

namespace engine {
namespace game {

class Main_script {
 public:
    virtual void start(engine::game::Context* context) = 0;
    virtual void stop(engine::game::Context* context) = 0;
};
}  // namespace game
}  // namespace engine

#if !defined _DEBUG
#include <engine\game\Main_script.inl>
#endif

#endif  // MULTI_THREADED_RENDERER_ENGINE_GAME_MAIN_SCRIPT_HPP_
