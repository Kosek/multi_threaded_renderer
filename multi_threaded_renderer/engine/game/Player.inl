// Copyright 2017 Tomasz Kosek. All rights reserved.

namespace engine {
namespace game {
INLINE const player::input::Controllers_state& Player::get_input_controllers_state() const {
    return input_controllers_state_;
}
INLINE void Player::set_window_that_mouse_coordinates_will_be_calculated_relative_to(
    const engine::rendering::Window* window) {
    input_controllers_state_.set_window_that_mouse_coordinates_will_be_calculated_relative_to(
        window);
}
}  // namespace game
}  // namespace engine
