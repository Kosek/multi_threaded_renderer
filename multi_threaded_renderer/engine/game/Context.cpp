// Copyright 2017 Tomasz Kosek. All rights reserved.

#include <stdafx.h>

#include <engine\game\Context.hpp>

#if defined _DEBUG
#include <engine\game\Context.inl>
#endif

namespace engine {
namespace game {

#if defined ENGINE_DEBUG_ON
const engine::debug::Debug_info& Context::get_debug_info() const {
    return debug_info_;
}
void Context::on_context_set_to_thread_local_storage() {
    ENGINE_DEBUG(debug_info_.set_thread_id_if_empty();)
}
#endif

}  // namespace game
}  // namespace engine
