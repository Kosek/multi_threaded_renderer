// Copyright 2017 Tomasz Kosek. All rights reserved.

#ifndef MULTI_THREADED_RENDERER_ENGINE_GAME_CONTEXT_HPP_
#define MULTI_THREADED_RENDERER_ENGINE_GAME_CONTEXT_HPP_

#include <chrono>  // NOLINT
#include <memory>
#include <utility>
#include <vector>

#include <engine\assets\Asset.hpp>
#include <engine\assets\Script.hpp>
#include <engine\data_structures\Name_index.hpp>
#include <engine\data_structures\Sorted_vector.hpp>
#include <engine\debug\Debug_info.hpp>
#include <engine\debug\Engine_debug.hpp>
#include <engine\frame\update\Receiver.hpp>
#include <engine\game\Player.hpp>
#include <engine\naming\Unique_name.hpp>
#include <engine\rendering\Renderer_fwd.hpp>
#include <engine\scene3\Scene.hpp>
#include <engine\threading\sync\Binary_point.hpp>
#include <engine\utilities\memory\Raw_ptr.hpp>

namespace engine {
namespace game {
class Context final : public Noncopyable {
 public:
    class Name : public engine::naming::Unique_name {
     public:
        using engine::naming::Unique_name::Unique_name;
    };

    INLINE Context(const Name& name);
    INLINE ~Context();
    INLINE const Name& get_name() const;
    INLINE void add(engine::scene3::Scene* scene);
    INLINE void remove(engine::scene3::Scene* scene);
    INLINE void remove(const engine::scene3::Scene::Name& name);
    INLINE engine::scene3::Scene* get(const engine::scene3::Scene::Name& name);
    INLINE const engine::scene3::Scene* get(const engine::scene3::Scene::Name& name) const;
    INLINE void add(engine::assets::Script* s);
    INLINE void remove(const engine::assets::Script* s);
    INLINE void remove(const engine::assets::Script::Name& name);
    INLINE engine::assets::Script* get(const engine::assets::Script::Name& name);
    INLINE const engine::assets::Script* get(const engine::assets::Script::Name& name) const;
    INLINE void enable_update(const engine::scene3::Scene::Name& name);
    INLINE void disable_update(const engine::scene3::Scene::Name& name);
    INLINE void enable_update(const engine::assets::Script::Name& name);
    INLINE void disable_update(const engine::assets::Script::Name& name);
    INLINE void start();
    INLINE void stop();
    INLINE void update();
    INLINE engine::threading::sync::Binary_point& get_sync_point_for_frame_simulation();
    INLINE const engine::threading::sync::Binary_point&
        get_sync_point_for_frame_simulation() const;
    INLINE const Context* get_main_context() const;
    INLINE Context* get_main_context();
    INLINE void add_asset_to_destroy(engine::assets::Asset* asset);
    INLINE engine::game::Player* get_player();
    INLINE const engine::game::Player* get_player() const;
    ENGINE_DEBUG(const engine::debug::Debug_info& get_debug_info() const;)

    class Renderer_synchronizator final : public Noncopyable {
        friend Context;

     private:
        class Construction_key final : public Noncopyable {};
     public:
        INLINE Renderer_synchronizator(const Construction_key& key, Context* c,
            const engine::rendering::Renderer* r);
        INLINE ~Renderer_synchronizator();
        INLINE void desynchronize_renderer();

     private:
        engine::utilities::memory::Raw_ptr<Context> context_;
        engine::utilities::memory::Raw_ptr<const engine::rendering::Renderer> renderer_;
        bool was_desynchronize_called_ = false;
    };

    INLINE std::unique_ptr<Renderer_synchronizator> create_renderer_synchronizator(
        const engine::rendering::Renderer* renderer);

    ENGINE_DEBUG(void on_context_set_to_thread_local_storage();)

    class iFrame {
     public:
        INLINE iFrame() = default;
        INLINE float get_frame_time_millis() const;

     protected:
        float frame_time_ = 0;
        std::chrono::system_clock::time_point prev_frame_start_;
    };

    INLINE const iFrame& get_current_frame() const;

 private:
    class Updater final : public Noncopyable {
     public:
        INLINE void add(engine::scene3::Scene* s);
        INLINE void remove(engine::scene3::Scene* s);
        INLINE void add(engine::frame::update::Receiver* r);
        INLINE void remove(engine::frame::update::Receiver* r);
        INLINE void update(Context* c);

     private:
        engine::frame::update::Receiver::Receivers_to_update_list update_receivers_;
        engine::scene3::Scene::Scenes_to_update_in_context_list scenes_;
    };

    class Scripts_comparator final : public Noncopyable {
     public:
        INLINE int cmp(
            const engine::assets::Script::Name& key,
            const engine::assets::Script* s) const;
        INLINE const engine::assets::Script::Name& get_key(
            const engine::assets::Script* s) const;
    };

    class Renderer_synchronization final : public Noncopyable {
     public:
        enum class State {
            DESYNCHRONIZED,
            SYNCHRONIZATION_REQ,
            SYNCHRONIZED,
            DESYNCHRONIZATION_REQ
        };

        INLINE Renderer_synchronization(const Context& context);
        INLINE const State get_state() const;
        INLINE void request_synchronization(const engine::rendering::Renderer* renderer);
        INLINE void set_synchronized();
        INLINE void request_desynchronization(const engine::rendering::Renderer* renderer);
        INLINE void set_desynchronized();

     private:
        const Context& context_;
        State state_ = State::DESYNCHRONIZED;
        engine::utilities::memory::Raw_ptr<const engine::rendering::Renderer> renderer_;
    };

    class Frame final : public iFrame {
     public:
        INLINE void update_frame_time();
    };

    INLINE bool is_main_context() const;
    INLINE void synchronize_renderer(const engine::rendering::Renderer* r);
    INLINE void desynchronize_renderer(const engine::rendering::Renderer* r);
    INLINE void process_renderer_synchronization();
    INLINE void destroy_assets_that_are_pending_for_destruction();
    INLINE bool is_renderer_synchronized() const;

    const Name name_;
    Renderer_synchronization renderer_synchronization_;
    engine::scene3::Scene::Scenes_in_context_list scenes_;
    engine::data_structures::Sorted_vector<
        const engine::assets::Script::Name,
        engine::utilities::memory::Raw_ptr<engine::assets::Script>,
        Scripts_comparator> scripts_;
    engine::data_structures::Name_index<engine::scene3::Scene> scenes_index_;
    Updater updater_;
    engine::threading::sync::Binary_point sync_point_for_end_of_frame_simulation_;
    engine::assets::Asset::Assets_to_destroy_list assets_to_destroy_;
    engine::game::Player player_;  // in case of more players at a time,
                                   // this is the place to add them
    Frame current_frame_;
    ENGINE_DEBUG(engine::debug::Debug_info debug_info_;)
};
}  // namespace game
}  // namespace engine

#if !defined _DEBUG
#include <engine\game\Context.inl>
#endif

#endif  // MULTI_THREADED_RENDERER_ENGINE_GAME_CONTEXT_HPP_
