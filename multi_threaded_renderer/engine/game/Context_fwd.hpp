// Copyright 2017 Tomasz Kosek. All rights reserved.

#ifndef MULTI_THREADED_RENDERER_ENGINE_GAME_CONTEXT_FWD_HPP_
#define MULTI_THREADED_RENDERER_ENGINE_GAME_CONTEXT_FWD_HPP_

namespace engine {
namespace game {

class Context;

}  // namespace game
}  // namespace engine

#endif  // MULTI_THREADED_RENDERER_ENGINE_GAME_CONTEXT_FWD_HPP_
