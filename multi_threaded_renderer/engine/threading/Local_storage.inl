// Copyright 2017 Tomasz Kosek. All rights reserved.

#include <engine\debug\Assert.hpp>

namespace engine {
namespace threading {

INLINE engine::game::Context* Local_storage::get_main_context() {
    return Local_storage::main_context_;
}

}  // namespace threading
}  // namespace engine
