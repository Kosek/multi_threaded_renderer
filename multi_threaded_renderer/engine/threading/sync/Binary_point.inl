// Copyright 2017 Tomasz Kosek. All rights reserved.

#include <intrin.h>

namespace engine {
namespace threading {
namespace sync {
INLINE Binary_point::Producer_guard::~Producer_guard() {
    binary_point_.state_ = Production_state::PRODUCT_READY;
    _WriteBarrier();
}

INLINE Binary_point::Consumer_guard::Consumer_guard(Binary_point& binary_point)
    : binary_point_(binary_point) {
    _ReadBarrier();
    while (binary_point_.state_ != Production_state::PRODUCT_READY) {
        _ReadBarrier();
    }
}
INLINE Binary_point::Consumer_guard::~Consumer_guard() {
    binary_point_.state_ = Production_state::PRODUCT_CONSUMED;
    _WriteBarrier();
}
}  // namespace sync
}  // namespace threading
}  // namespace engine
