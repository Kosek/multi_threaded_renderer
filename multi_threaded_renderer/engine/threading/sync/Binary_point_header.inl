// Copyright 2017 Tomasz Kosek. All rights reserved.

#include <intrin.h>

namespace engine {
namespace threading {
namespace sync {
template <class KEEP_WAITING_PREDICATE>
INLINE Binary_point::Producer_guard::Producer_guard(Binary_point& binary_point,
    const KEEP_WAITING_PREDICATE& keep_waiting_predicate)
    : binary_point_(binary_point) {
    _ReadBarrier();
    while (binary_point_.state_ != Production_state::PRODUCT_CONSUMED
        && keep_waiting_predicate()) {
        _ReadBarrier();
    }
}
}  // namespace sync
}  // namespace threading
}  // namespace engine
