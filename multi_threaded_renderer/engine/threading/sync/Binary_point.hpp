// Copyright 2017 Tomasz Kosek. All rights reserved.

#ifndef MULTI_THREADED_RENDERER_ENGINE_THREADING_SYNC_BINARY_POINT_HPP_
#define MULTI_THREADED_RENDERER_ENGINE_THREADING_SYNC_BINARY_POINT_HPP_

namespace engine {
namespace threading {
namespace sync {
class Binary_point final : public Noncopyable {
 public:
    INLINE Binary_point() = default;

    class Producer_guard final : public Noncopyable {
     public:
        template <class KEEP_WAITING_PREDICATE>
        INLINE Producer_guard(Binary_point& binary_point,  // NOLINT
            const KEEP_WAITING_PREDICATE& keep_waiting_predicate);
        INLINE ~Producer_guard();

     private:
        Binary_point& binary_point_;
    };

    class Consumer_guard final : public Noncopyable {
     public:
        INLINE Consumer_guard(Binary_point& binary_point);  // NOLINT
        INLINE ~Consumer_guard();

     private:
        Binary_point& binary_point_;
    };

 private:
    enum class Production_state {
        PRODUCT_CONSUMED,
        PRODUCT_READY
    };

    Production_state state_ = Production_state::PRODUCT_CONSUMED;
};
}  // namespace sync
}  // namespace threading
}  // namespace engine

#include <engine\threading\sync\Binary_point_header.inl>

#if !defined _DEBUG

#include <engine\threading\sync\Binary_point.inl>

#endif

#endif  // MULTI_THREADED_RENDERER_ENGINE_THREADING_SYNC_BINARY_POINT_HPP_
