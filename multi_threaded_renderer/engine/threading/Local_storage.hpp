// Copyright 2017 Tomasz Kosek. All rights reserved.

#ifndef MULTI_THREADED_RENDERER_ENGINE_THREADING_LOCAL_STORAGE_HPP_
#define MULTI_THREADED_RENDERER_ENGINE_THREADING_LOCAL_STORAGE_HPP_

#include <engine\game\Context_fwd.hpp>
#include <engine\utilities\memory\Raw_ptr.hpp>

namespace engine {
namespace threading {

class Local_storage final {
 public:
    class Main_context_setter final {
     public:
        Main_context_setter(engine::game::Context& main_context);  // NOLINT
        ~Main_context_setter();

     private:
        engine::game::Context& main_context_;
    };

    INLINE static engine::game::Context* get_main_context();

 private:
    thread_local static engine::utilities::memory::Raw_ptr<engine::game::Context> main_context_;
};

}  // namespace threading
}  // namespace engine

#if !defined _DEBUG
#include <engine\threading\Local_storage.inl>

#endif
#endif  // MULTI_THREADED_RENDERER_ENGINE_THREADING_LOCAL_STORAGE_HPP_
