// Copyright 2017 Tomasz Kosek. All rights reserved.

#include <stdafx.h>

#include <engine\game\Context.hpp>
#include <engine\threading\Local_storage.hpp>

#if defined _DEBUG
#include <engine\threading\Local_storage.inl>
#endif

namespace engine {
namespace threading {

thread_local engine::utilities::memory::Raw_ptr<engine::game::Context>
engine::threading::Local_storage::main_context_;

Local_storage::Main_context_setter::Main_context_setter(
    engine::game::Context& main_context) : main_context_(main_context) {
    Local_storage::main_context_ = &main_context_;
    ENGINE_DEBUG(main_context_.on_context_set_to_thread_local_storage();)
}

Local_storage::Main_context_setter::~Main_context_setter() {
    Local_storage::main_context_ = nullptr;
}

}  // namespace threading
}  // namespace engine
