// Copyright 2017 Tomasz Kosek. All rights reserved.

#include <stdafx.h>

#include <engine\rendering\Renderer.hpp>

#if defined _DEBUG
#include <engine\rendering\Renderer.inl>
#endif

namespace engine {
namespace rendering {
const char PARAMETERS_INSTANCED_RENDERING_NULL_NOT_ALLOWED[] = {
    "Parameters must not be null!" };
const char PARAMETERS_INSTANCED_RENDERING_ELEMENT_ALREADY_IN_LIST[] = {
    "Parameters already added to list!" };
const char PARAMETERS_INSTANCED_RENDERING_ELEMENT_NOT_IN_CURRENT_LIST[] = {
    "Parameters are not in current list!" };
}  // namespace rendering
}  // namespace engine
