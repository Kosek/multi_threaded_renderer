// Copyright 2017 Tomasz Kosek. All rights reserved.

#ifndef MULTI_THREADED_RENDERER_ENGINE_RENDERING_WINDOW_FWD_HPP_
#define MULTI_THREADED_RENDERER_ENGINE_RENDERING_WINDOW_FWD_HPP_

namespace engine {
namespace rendering {

class Window;

}  // namespace rendering
}  // namespace engine

#endif  // MULTI_THREADED_RENDERER_ENGINE_RENDERING_WINDOW_FWD_HPP_
