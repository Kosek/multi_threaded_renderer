// Copyright 2017 Tomasz Kosek. All rights reserved.

#include <D3DX10math.h>
#include <d3dx11.h>
#include <memory>
#include <thread>  // NOLINT
#include <tuple>
#include <utility>

#include <engine\assets3\materials\Material_pixel_shader.hpp>
#include <engine\assets3\materials\Material_vertex_shader.hpp>
#include <engine\global_defs\Load_simulation.hpp>
#include <engine\hlsl\Instances_defs.hlsl>  // NOLINT
#include <engine\rendering\Target.hpp>
#include <engine\rendering\Window.hpp>
#include <engine\scene3\Scene.hpp>
#include <engine\scene3\Space.hpp>
#include <engine\scene3\view\Camera.hpp>
#include <engine\threading\sync\Binary_point.hpp>

namespace engine {
namespace rendering {
INLINE Renderer::Renderer(const engine::rendering::Target* t)
    : target_(t), geometry_drawer_(statistics_) {
    ENGINE_ASSERT(target_, "Target must not be null!");
    ENGINE_ASSERT(target_->get_window(), "Target's window must not be null!")
}

INLINE Renderer::~Renderer() {
    if (registrator_to_main_context_) {
        registrator_to_main_context_->desynchronize_renderer();  // It's called implicitly (despite RAII of engine::game::Context::Renderer_synchronizator) // NOLINT
                                                                 // because there is an assert, in Debug configuration, in // NOLINT
                                                                 // Context::Renderer_synchronization::request_desynchronization method: // NOLINT
                                                                 // "std::this_thread::get_id() == renderer->get_debug_info().get_thread_id()". // NOLINT
                                                                 // It might be so that, engine::debug::Debug_info debug_info_ d-tor (that will nullify thread id) // NOLINT
                                                                 // will be called before d-tor // NOLINT
                                                                 // of td::unique_ptr<engine::game::Context::Renderer_synchronizator> registrator_to_main_context_ // NOLINT
                                                                 // and renderer->get_debug_info().get_thread_id() will return 0 thread id and assert will fail. // NOLINT
    }
    unsetup();
}

INLINE ID3D11Device* Renderer::get_device() const {
    return device_;
}

INLINE void Renderer::setup() {
    unsetup();
    setup_device_and_swap_chain_and_device_context_depth_stencil(target_->get_window()->get_hwnd(),
        target_->get_window()->get_width(), target_->get_window()->get_height());
}

INLINE void Renderer::render() {
    ENGINE_DEBUG(debug_info_.set_thread_id_if_empty();)

    device_context_->ClearRenderTargetView(render_target_view_, D3DXCOLOR(0.0f, 0.2f, 0.4f, 1.0f));
    device_context_->ClearDepthStencilView(depth_stencil_view_, D3D11_CLEAR_DEPTH, 1.0f, 0);

    if (!target_->get_camera()) {
        return;
    }

    engine::game::Context* main_context =
        target_->get_camera()->get_entry()->get_space()->get_scene()->get_context()->
        get_main_context();

    statistics_.update(main_context->get_current_frame().get_frame_time_millis());
    statistics_.get_cumulative_counters().increment_frame_number();

#if !defined SINGLE_THREADED_RENDERING
    synchronize_with_context_if_needed(main_context);
#endif

    XMMATRIX projection_matrix;

    // sync point 1 - simulation thread waits
    {
#if !defined SINGLE_THREADED_RENDERING
        engine::threading::sync::Binary_point::Consumer_guard consumer_guard(
            main_context->get_sync_point_for_frame_simulation());
#endif

        projection_matrix = calculate_projection_matrix(target_->get_camera());

        geometry_to_render_selector_.
            store_material_parameter_values_and_transformation_matrices(
                target_->get_camera(), projection_matrix);
    }

    // sync point 2 - simulation thread runs to simulate next frame

    geometry_to_render_grouper_.perform_grouping(
        geometry_to_render_selector_.get_mesh_materials_to_render());

    geometry_drawer_.draw(device_context_,
        geometry_to_render_grouper_.get_grouped_mesh_materials_to_render(),
        target_->get_camera()->get_view_matrix(), projection_matrix);

    // switch the back buffer and the front buffer
    swap_chain_->Present(0, 0);
}

INLINE void Renderer::synchronize_with_context_if_needed(engine::game::Context* main_context) {
    if (main_context_renderer_is_registered_to_ != main_context) {
        registrator_to_main_context_ = main_context->create_renderer_synchronizator(this);
        main_context_renderer_is_registered_to_ = main_context;
    }
}

INLINE XMMATRIX Renderer::calculate_projection_matrix(
    const engine::scene3::view::Camera* camera) const {
    float aspect_ratio =
        static_cast<float>(target_->get_window()->get_width()) /
        static_cast<float>(target_->get_window()->get_height());
    return XMMatrixPerspectiveFovLH(
        camera->get_field_of_view(),
        aspect_ratio,
        camera->get_near_clipping_plane(),
        camera->get_far_clipping_plane());
}

INLINE void Renderer::setup_device_and_swap_chain_and_device_context_depth_stencil(
    HWND hWnd, int screen_width, int screen_height) {

    DXGI_SWAP_CHAIN_DESC scd;
    std::memset(&scd, 0, sizeof(scd));
    scd.BufferCount = 1;
    scd.BufferDesc.Width = screen_width;
    scd.BufferDesc.Height = screen_height;
    scd.BufferDesc.Format = DXGI_FORMAT_R8G8B8A8_UNORM;
    scd.BufferUsage = DXGI_USAGE_RENDER_TARGET_OUTPUT;
    scd.OutputWindow = hWnd;
    scd.SampleDesc.Count = 1;
    scd.Windowed = false;
    scd.Flags = 0;
    HRESULT res = D3D11CreateDeviceAndSwapChain(
        nullptr,
        D3D_DRIVER_TYPE_HARDWARE,
        nullptr,
        0,
        nullptr,
        0,
        D3D11_SDK_VERSION,
        &scd,
        &swap_chain_,
        &device_,
        nullptr,
        &device_context_);
    ENGINE_ASSERT(!FAILED(res), "Failed to create device and swap chain!");

    ID3D11Texture2D* back_buffer = nullptr;
    res = swap_chain_->GetBuffer(0, __uuidof(ID3D11Texture2D), reinterpret_cast<void**>(
        &back_buffer));
    ENGINE_ASSERT(!FAILED(res), "Failed to get back buffer!");
    res = device_->CreateRenderTargetView(back_buffer, nullptr, &render_target_view_);
    ENGINE_ASSERT(!FAILED(res), "Failed to create render target view!");
    back_buffer->Release();
    back_buffer = nullptr;

    D3D11_TEXTURE2D_DESC t2dd;
    std::memset(&t2dd, 0, sizeof(t2dd));
    t2dd.Width = screen_width;
    t2dd.Height = screen_height;
    t2dd.MipLevels = 1;
    t2dd.ArraySize = 1;
    t2dd.Format = DXGI_FORMAT_D24_UNORM_S8_UINT;
    t2dd.SampleDesc.Count = 1;
    t2dd.SampleDesc.Quality = 0;
    t2dd.Usage = D3D11_USAGE_DEFAULT;
    t2dd.BindFlags = D3D11_BIND_DEPTH_STENCIL;
    t2dd.CPUAccessFlags = 0;
    t2dd.MiscFlags = 0;
    res = device_->CreateTexture2D(&t2dd, nullptr, &depth_stencil_texture_);
    ENGINE_ASSERT(!FAILED(res), "Failed to create texture for depth stencil!");

    D3D11_DEPTH_STENCIL_DESC dsd;
    std::memset(&dsd, 0, sizeof(dsd));
    dsd.DepthEnable = true;
    dsd.DepthWriteMask = D3D11_DEPTH_WRITE_MASK_ALL;
    dsd.DepthFunc = D3D11_COMPARISON_LESS;
    dsd.StencilEnable = true;
    dsd.StencilReadMask = D3D10_DEFAULT_STENCIL_READ_MASK;
    dsd.StencilWriteMask = D3D10_DEFAULT_STENCIL_WRITE_MASK;
    dsd.FrontFace.StencilFailOp = D3D11_STENCIL_OP_KEEP;
    dsd.FrontFace.StencilDepthFailOp = D3D11_STENCIL_OP_KEEP;
    dsd.FrontFace.StencilPassOp = D3D11_STENCIL_OP_KEEP;
    dsd.FrontFace.StencilFunc = D3D11_COMPARISON_ALWAYS;
    dsd.BackFace.StencilFailOp = D3D11_STENCIL_OP_KEEP;
    dsd.BackFace.StencilDepthFailOp = D3D11_STENCIL_OP_KEEP;
    dsd.BackFace.StencilPassOp = D3D11_STENCIL_OP_KEEP;
    dsd.BackFace.StencilFunc = D3D11_COMPARISON_ALWAYS;
    res = device_->CreateDepthStencilState(&dsd, &depth_stencil_state_);
    ENGINE_ASSERT(!FAILED(res), "Failed to create depth stencil state!");
    device_context_->OMSetDepthStencilState(depth_stencil_state_, 1);

    D3D11_DEPTH_STENCIL_VIEW_DESC dsvd;
    std::memset(&dsvd, 0, sizeof(dsvd));
    dsvd.Format = DXGI_FORMAT_D24_UNORM_S8_UINT;
    dsvd.ViewDimension = D3D11_DSV_DIMENSION_TEXTURE2D;
    dsvd.Texture2D.MipSlice = 0;
    res = device_->CreateDepthStencilView(depth_stencil_texture_, &dsvd, &depth_stencil_view_);
    ENGINE_ASSERT(!FAILED(res), "Failed to create depth stencil view!");

    device_context_->OMSetRenderTargets(1, &render_target_view_, depth_stencil_view_);

    D3D11_RASTERIZER_DESC rd;
    std::memset(&rd, 0, sizeof(rd));
    rd.AntialiasedLineEnable = false;
    rd.CullMode = D3D11_CULL_BACK;
    rd.DepthBias = 0;
    rd.DepthBiasClamp = 0.0f;
    rd.DepthClipEnable = true;
    rd.FillMode = D3D11_FILL_SOLID;
    rd.FrontCounterClockwise = false;
    rd.MultisampleEnable = false;
    rd.ScissorEnable = false;
    rd.SlopeScaledDepthBias = 0.0f;
    res = device_->CreateRasterizerState(&rd, &rasterizer_state_);
    ENGINE_ASSERT(!FAILED(res), "Failed to create rasterizer state!");
    device_context_->RSSetState(rasterizer_state_);

    D3D11_VIEWPORT viewport;
    std::memset(&viewport, 0, sizeof(D3D11_VIEWPORT));
    viewport.TopLeftX = 0;
    viewport.TopLeftY = 0;
    viewport.Width = static_cast<float>(screen_width);
    viewport.Height = static_cast<float>(screen_height);
    viewport.MinDepth = 0.0f;
    viewport.MaxDepth = 1.0f;
    device_context_->RSSetViewports(1, &viewport);

    geometry_drawer_.setup(device_);
}

INLINE void Renderer::unsetup() {
    if (swap_chain_) {
        swap_chain_->SetFullscreenState(false, nullptr);
    }

    if (rasterizer_state_) {
        rasterizer_state_->Release();
    }
    rasterizer_state_ = nullptr;

    if (depth_stencil_state_) {
        depth_stencil_state_->Release();
    }
    depth_stencil_state_ = nullptr;

    if (depth_stencil_view_) {
        depth_stencil_view_->Release();
    }
    depth_stencil_view_ = nullptr;

    if (depth_stencil_texture_) {
        depth_stencil_texture_->Release();
    }
    depth_stencil_texture_ = nullptr;

    if (render_target_view_) {
        render_target_view_->Release();
    }
    render_target_view_ = nullptr;

    if (device_context_) {
        device_context_->Release();
    }
    device_context_ = nullptr;

    if (device_) {
        device_->Release();
    }
    device_ = nullptr;

    if (swap_chain_) {
        swap_chain_->Release();
    }
    swap_chain_ = nullptr;

    ENGINE_DEBUG(debug_info_.unsetup();)
}

#if defined _DEBUG
INLINE const engine::debug::Debug_info& Renderer::get_debug_info() const {
    return debug_info_;
}
#endif

}  // namespace rendering
}  // namespace engine

namespace engine {
namespace rendering {
INLINE void Renderer::Geometry_to_render_selector::
store_material_parameter_values_and_transformation_matrices(
    const engine::scene3::view::Camera* camera, CXMMATRIX projection_matrix) {

    ENGINE_ASSERT(camera, "Camera must not be null!");

    clear();

    scene3::Scene::Query::Nodes_iterator it = camera->get_entry()
        ->get_space()->get_scene()->create_query()
        .select_nodes(/*calculate_frustum(projection_matrix)*/);

    it.visit_all([this](const engine::scene3::Node* n) {
        const engine::assets3::Model::Instance* mi = n->get_entry()->get_model_instance();
        if (mi) {
            XMMATRIX world_matrix = n->get_world_matrix();
            mi->get_iterator().visit_all_mesh_materials([this, n, &world_matrix](
                const engine::assets3::Model::Mesh_material* mm,
                const engine::assets3::Material::Parameters& p) {
                mesh_materials_to_render_.push_back(std::make_pair(
                    engine::utilities::memory::Raw_ptr<
                        const engine::assets3::Model::Mesh_material>(mm),
                    Geometry_to_render_selector::Parameters(world_matrix, p)));
            });
        }
    });
}
INLINE void Renderer::Geometry_to_render_selector::clear() {
    mesh_materials_to_render_.reset_elements_count();
}
INLINE Renderer::Geometry_to_render_selector::Mesh_materials_to_render&
    Renderer::Geometry_to_render_selector::get_mesh_materials_to_render() {
    return mesh_materials_to_render_;
}
INLINE const Renderer::Geometry_to_render_selector::Mesh_materials_to_render&
Renderer::Geometry_to_render_selector::get_mesh_materials_to_render() const {
    return mesh_materials_to_render_;
}
INLINE Renderer::Geometry_to_render_selector::Parameters::
Parameters_for_instanced_rendering_list_element::Type&
Renderer::Geometry_to_render_selector::Parameters::
Parameters_for_instanced_rendering_list_element::get_embedded_list_element(
    Renderer::Geometry_to_render_selector::Parameters* p) {
    return p->parameters_for_instanced_rendering_list_element_;
}
INLINE const Renderer::Geometry_to_render_selector::Parameters::
Parameters_for_instanced_rendering_list_element::Type&
Renderer::Geometry_to_render_selector::Parameters::
Parameters_for_instanced_rendering_list_element::get_embedded_list_element(
    const Renderer::Geometry_to_render_selector::Parameters* p) {
    return p->parameters_for_instanced_rendering_list_element_;
}
INLINE Renderer::Geometry_to_render_selector::Parameters::Parameters(
    CXMMATRIX world_matrix, const engine::assets3::Material::Parameters& parameters) :
    world_matrix_(world_matrix), parameters_(parameters) {
}
INLINE CXMMATRIX Renderer::Geometry_to_render_selector::Parameters::get_world_matrix() const {
    return world_matrix_;
}
INLINE const engine::assets3::Material::Parameters&
Renderer::Geometry_to_render_selector::Parameters::get_parameters() const {
    return parameters_;
}
}  // namespace rendering
}  // namespace engine

namespace engine {
namespace rendering {
INLINE void Renderer::Geometry_to_render_grouper::perform_grouping(
    Geometry_to_render_selector::Mesh_materials_to_render& geometry_to_render) {

    clear();

    geometry_to_render.get_iterator().visit_all([this](
        Geometry_to_render_selector::Mesh_material_to_render& mm) {
        Geometry_to_render_grouper::Key key = std::make_tuple(
            mm.first->get_mesh_chunk()->get_vertex_buffer(),
            mm.first->get_vertices_start_index(),
            mm.second.get_parameters().get_instances_group(),
            mm.first);
        grouped_mesh_materials_to_render_[std::move(key)].add(&mm.second);
    });
}
INLINE void Renderer::Geometry_to_render_grouper::clear() {
    grouped_mesh_materials_to_render_.clear();
}
INLINE const Renderer::Geometry_to_render_grouper::Grouped_mesh_materials_to_render&
Renderer::Geometry_to_render_grouper::get_grouped_mesh_materials_to_render() const {
    return grouped_mesh_materials_to_render_;
}
}  // namespace rendering
}  // namespace engine

namespace engine {
namespace rendering {
INLINE Renderer::Geometry_drawer::Geometry_drawer(Statistics& statistics) :
    statistics_(statistics) {
}

INLINE Renderer::Geometry_drawer::~Geometry_drawer() {
    unsetup();
}

INLINE void Renderer::Geometry_drawer::setup(ID3D11Device* device) {
    ENGINE_ASSERT(device, "Device must not be null!");

    instances_world_matrices_.setup(device, HLSL_MAX_INSTANCES_COUNT);
}
INLINE void Renderer::Geometry_drawer::unsetup() {
    instances_world_matrices_.unsetup();
}
INLINE void Renderer::Geometry_drawer::draw(ID3D11DeviceContext* device_context,
    const Geometry_to_render_grouper::Grouped_mesh_materials_to_render&
    grouped_geometry_to_render, CXMMATRIX view_matrix, CXMMATRIX projection_matrix) {
    ENGINE_ASSERT(device_context, "Device context must not be null!");

    device_context->IASetPrimitiveTopology(D3D10_PRIMITIVE_TOPOLOGY_TRIANGLELIST);

    XMMATRIX view_projection_matrix = XMMatrixMultiply(view_matrix, projection_matrix);

    unsigned int geometry_chunks_count = 0;
    for (auto it = grouped_geometry_to_render.begin(); it != grouped_geometry_to_render.end();
        ++it) {
        const Renderer::Geometry_to_render_grouper::Key& key = it->first;
        const engine::assets3::Model::Mesh_material* mesh_material = std::get<3>(key);
        engine::assets3::Material* material = mesh_material->get_material().get();
        const engine::vertex_stream::Vertex_buffer* vertex_buffer = std::get<0>(key);
        ID3D11Buffer* vertex_buffer_ptr = vertex_buffer->get_buffer_ptr();
        const engine::vertex_stream::Stride_schema* vertex_stream_stride_schema =
            vertex_buffer->get_vertex_stream_stride_schema();
        const unsigned int vertex_stream_stride_schema_size =
            vertex_stream_stride_schema->get_stride_size();
        const unsigned int offset = 0;
        const engine::vertex_stream::Index_buffer* index_buffer =
            mesh_material->get_mesh_chunk()->get_index_buffer();

        device_context->IASetVertexBuffers(0, 1, &vertex_buffer_ptr,
            &vertex_stream_stride_schema_size, &offset);
        device_context->IASetIndexBuffer(index_buffer->get_buffer_ptr(), DXGI_FORMAT_R32_UINT, 0);
        device_context->VSSetShader(
            material->get_parameters().get_material_vertex_shader()->get_vertex_shader(),
            nullptr, 0);
        device_context->IASetInputLayout(
            material->get_parameters().get_material_vertex_shader()->get_input_layout());
        device_context->PSSetShader(
            material->get_parameters().get_material_pixel_shader()->get_pixel_shader(),
            nullptr, 0);

        const Geometry_to_render_selector::Parameters::
            Parameters_for_instanced_rendering_list& instances_list = it->second;
        const engine::assets3::Model::Mesh_material* mm = std::get<3>(key);

        draw(device_context, instances_list, mm, view_projection_matrix);

        ++geometry_chunks_count;
    }
    statistics_.get_per_frame_counters().set_geometry_chunks_count(geometry_chunks_count);
}
INLINE void Renderer::Geometry_drawer::draw(ID3D11DeviceContext* device_context,
    const Geometry_to_render_selector::Parameters::Parameters_for_instanced_rendering_list&
    instances_list, const engine::assets3::Model::Mesh_material* mm,
    CXMMATRIX view_projection_matrix) {
    engine::utilities::buffers::Gpu_buffer::Writer w(device_context, instances_world_matrices_);
    unsigned int instance_no = 0;

    unsigned int material_max_instances_count = HLSL_MAX_INSTANCES_COUNT;
    if (mm->get_material()->get_parameters().get_max_textures_count()) {
        material_max_instances_count = HLSL_MAX_INSTANCES_COUNT /
            mm->get_material()->get_parameters().get_max_textures_count();
    }

    textures_.remove_all();

    instances_list.get_iterator().visit_all([this, &w, &instance_no, device_context, mm,
        view_projection_matrix, material_max_instances_count](
            const Geometry_to_render_selector::Parameters* p) {
        w.write<XMMATRIX>(XMMatrixMultiplyTranspose(p->get_world_matrix(),
            view_projection_matrix));
        p->get_parameters().get_textures().get_iterator().visit_all([this](
            const engine::utilities::memory::Raw_ptr<ID3D11ShaderResourceView>& texture) {
            textures_.push_back(texture);
        });
        if (++instance_no == material_max_instances_count) {
            draw(device_context, mm, material_max_instances_count);
            instance_no = 0;
            w.reset();
            textures_.remove_all();
        }
    });
    if (instance_no) {
        draw(device_context, mm, instance_no);
    }
}
INLINE void Renderer::Geometry_drawer::draw(ID3D11DeviceContext* device_context,
    const engine::assets3::Model::Mesh_material* mm, unsigned int instances_count) {
    device_context->VSSetConstantBuffers(0, 1, instances_world_matrices_.get_buffer_ptr_ptr());
    if (textures_.get_count()) {
        device_context->PSSetShaderResources(0, textures_.get_count(), &(*textures_.get_data()));
    }
    device_context->DrawIndexedInstanced(
        mm->get_indices_count(),  // IndexCountPerInstance
        instances_count,  // InstanceCount
        mm->get_indices_start_index(),  // StartIndexLocation
        0,  // BaseVertexLocation
        0  // StartInstanceLocation
    );  // NOLINT
    statistics_.get_per_frame_counters().increment_draw_indexed_instanced_calls_count();
    statistics_.get_per_frame_counters().increment_instances_count(instances_count);
    ENGINE_ASSERT(mm->get_indices_count() % 3 == 0, "Indices count is not divisible by 3!");
    statistics_.get_cumulative_counters().increment_triangles_count(
        (mm->get_indices_count() / 3) * instances_count);
    statistics_.get_cumulative_counters().increment_instances_count(instances_count);
}
}  // namespace rendering
}  // namespace engine

namespace engine {
namespace rendering {
INLINE Renderer::Statistics::~Statistics() {
    append_entries_to_log_file();
}
INLINE void Renderer::Statistics::update(float frame_time) {
    sum_of_frame_time_ += frame_time;
    if (sum_of_frame_time_ >= 1000.0f) {
        float seconds_count = std::floor(sum_of_frame_time_ / 1000.0f);
        sum_of_frame_time_ -= seconds_count * 1000.0f;

        entries_.emplace_back(logging::Time::now(), get_cumulative_counters().get_frame_number(),
            get_per_frame_counters().get_draw_indexed_instanced_calls_count(),
            get_cumulative_counters().get_triangles_count() / seconds_count,
            get_cumulative_counters().get_instances_count() / seconds_count,
            get_per_frame_counters().get_instances_count(),
            get_per_frame_counters().get_geometry_chunks_count());

        cumulative_counters_.reset();
    }
    per_frame_counters_.reset();
}
INLINE Renderer::Statistics::iCumulative_counters&
Renderer::Statistics::get_cumulative_counters() {
    return cumulative_counters_;
}
INLINE const Renderer::Statistics::iCumulative_counters&
Renderer::Statistics::get_cumulative_counters() const {
    return cumulative_counters_;
}
INLINE Renderer::Statistics::iPer_frame_counters& Renderer::Statistics::get_per_frame_counters() {
    return per_frame_counters_;
}
INLINE const Renderer::Statistics::iPer_frame_counters&
Renderer::Statistics::get_per_frame_counters() const {
    return per_frame_counters_;
}
INLINE void Renderer::Statistics::append_entries_to_log_file() {
    std::ofstream f;
    f.open("renderer.log", std::ios_base::app);
    if (f.is_open()) {
        f << "==================================================================================="
            << std::endl;
        f << "Rendering mode: ";
#if defined SINGLE_THREADED_RENDERING
        f << "singlethreaded";
#else
        f << "multithreaded";
#endif
        f << std::endl;
        f << "Time stamp; Frame number; DrawIndexedInstanced calls";
        f << "; Triangles count / second; Instances count / second; Instances count";
        f << "; Geometry chunks count";
        f << std::endl;
        for (const auto& e : entries_) {
            Entry copy = e;
            std::get<0>(copy).get_writer().write(f);
            f << ";" << std::get<1>(e);
            f << ";" << std::get<2>(e);
            f << ";" << static_cast<unsigned int>(std::get<3>(e));
            f << ";" << static_cast<unsigned int>(std::get<4>(e));
            f << ";" << std::get<5>(e);
            f << ";" << std::get<6>(e);
            f << std::endl;
        }
    }
}
}  // namespace rendering
}  // namespace engine

namespace engine {
namespace rendering {
INLINE void Renderer::Statistics::Cumulative_counters::reset() {
    triangles_count_ = 0;
    instances_count_ = 0;
}
}  // namespace rendering
}  // namespace engine

namespace engine {
namespace rendering {
INLINE void Renderer::Statistics::Per_frame_counters::reset() {
    geometry_chunks_count_ = 0;
    draw_indexed_instanced_calls_count_ = 0;
    instances_count_ = 0;
}
}  // namespace rendering
}  // namespace engine

namespace engine {
namespace rendering {
INLINE void Renderer::Statistics::iCumulative_counters::increment_frame_number() {
    ++frame_number_;
}
INLINE void Renderer::Statistics::iCumulative_counters::increment_triangles_count(unsigned int v) {
    triangles_count_ += v;
}
INLINE void Renderer::Statistics::iCumulative_counters::increment_instances_count(unsigned int v) {
    instances_count_ += v;
}
INLINE const Renderer::Statistics::Frame_number&
Renderer::Statistics::iCumulative_counters::get_frame_number() const {
    return frame_number_;
}
INLINE unsigned int Renderer::Statistics::iCumulative_counters::get_triangles_count() const {
    return triangles_count_;
}
INLINE unsigned int Renderer::Statistics::iCumulative_counters::get_instances_count() const {
    return instances_count_;
}
}  // namespace rendering
}  // namespace engine

namespace engine {
namespace rendering {
INLINE void Renderer::Statistics::iPer_frame_counters::set_geometry_chunks_count(
    const Geometry_chunks_count& v) {
    geometry_chunks_count_ = v;
}
INLINE const Renderer::Statistics::Geometry_chunks_count&
Renderer::Statistics::iPer_frame_counters::get_geometry_chunks_count() const {
    return geometry_chunks_count_;
}
INLINE void Renderer::Statistics::iPer_frame_counters::
increment_draw_indexed_instanced_calls_count() {
    ++draw_indexed_instanced_calls_count_;
}
INLINE unsigned int
Renderer::Statistics::iPer_frame_counters::get_draw_indexed_instanced_calls_count() const {
    return draw_indexed_instanced_calls_count_;
}
INLINE void Renderer::Statistics::iPer_frame_counters::increment_instances_count(
    unsigned int v) {
    instances_count_ += v;
}
INLINE unsigned int Renderer::Statistics::iPer_frame_counters::get_instances_count() const {
    return instances_count_;
}
}  // namespace rendering
}  // namespace engine
