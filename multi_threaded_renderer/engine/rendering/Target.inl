// Copyright 2017 Tomasz Kosek. All rights reserved.

#include <engine\debug\Assert.hpp>

namespace engine {
namespace rendering {
INLINE Target::Target(const Name& name, const Window* w)
    : name_(name), window_(w) {
    ENGINE_ASSERT(window_, "Window must not be null!");
}
INLINE const Target::Name& Target::get_name() const {
    return name_;
}
INLINE const Window* Target::get_window() const {
    return window_;
}
INLINE void Target::set_camera(const engine::scene3::view::Camera* camera) {
    camera_ = camera;
}
INLINE const engine::scene3::view::Camera* Target::get_camera() const {
    return camera_;
}
}  // namespace rendering
}  // namespace engine
