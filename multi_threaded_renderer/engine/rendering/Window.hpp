// Copyright 2017 Tomasz Kosek. All rights reserved.

#ifndef MULTI_THREADED_RENDERER_ENGINE_RENDERING_WINDOW_HPP_
#define MULTI_THREADED_RENDERER_ENGINE_RENDERING_WINDOW_HPP_

namespace engine {
namespace rendering {
class Window final : public Noncopyable {
 public:
    INLINE Window(HWND hwnd, unsigned int width, unsigned int height);
    INLINE HWND get_hwnd() const;
    INLINE unsigned int get_width() const;
    INLINE unsigned int get_height() const;

 private:
    HWND hwnd_;
    unsigned int width_ = 0;
    unsigned int height_ = 0;
};
}  // namespace rendering
}  // namespace engine

#if !defined _DEBUG

#include <engine\rendering\Window.inl>

#endif

#endif  // MULTI_THREADED_RENDERER_ENGINE_RENDERING_WINDOW_HPP_
