// Copyright 2017 Tomasz Kosek. All rights reserved.

#include <cstring>
#include <string>

namespace engine {
namespace rendering {
INLINE Instances_group::Instances_group(unsigned int k0, unsigned int k1, unsigned int k2,
    unsigned int k3) : key_{ k0, k1, k2, k3 } {
}
INLINE const unsigned int* Instances_group::get_data() const {
    return &key_[0];
}
INLINE bool operator < (const engine::rendering::Instances_group& left,
    const engine::rendering::Instances_group& right) {
    return (std::memcmp(left.get_data(), right.get_data(), Instances_group::SIZE) < 0);
}
}  // namespace rendering
}  // namespace engine
