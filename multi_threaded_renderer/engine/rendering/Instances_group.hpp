// Copyright 2017 Tomasz Kosek. All rights reserved.

#ifndef MULTI_THREADED_RENDERER_ENGINE_RENDERING_INSTANCES_GROUP_HPP_
#define MULTI_THREADED_RENDERER_ENGINE_RENDERING_INSTANCES_GROUP_HPP_

namespace engine {
namespace rendering {
class Instances_group final {
 public:
    INLINE Instances_group(unsigned int k0, unsigned int k1, unsigned int k2,
        unsigned int k3);
    INLINE const unsigned int* get_data() const;

    static const unsigned int SIZE = 4;

 private:
    unsigned int key_[SIZE] = { 0, 0, 0, 0 };
};

INLINE bool operator < (const engine::rendering::Instances_group& left,
    const engine::rendering::Instances_group& right);
}  // namespace rendering
}  // namespace engine

#if !defined _DEBUG

#include <engine\rendering\Instances_group.inl>

#endif

#endif  // MULTI_THREADED_RENDERER_ENGINE_RENDERING_INSTANCES_GROUP_HPP_
