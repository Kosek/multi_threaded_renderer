// Copyright 2017 Tomasz Kosek. All rights reserved.

#include <stdafx.h>

#include <engine\rendering\Target.hpp>

#if defined _DEBUG
#include <engine\rendering\Target.inl>
#endif
