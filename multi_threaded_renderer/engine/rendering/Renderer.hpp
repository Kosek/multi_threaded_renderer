// Copyright 2017 Tomasz Kosek. All rights reserved.

#ifndef MULTI_THREADED_RENDERER_ENGINE_RENDERING_RENDERER_HPP_
#define MULTI_THREADED_RENDERER_ENGINE_RENDERING_RENDERER_HPP_

#include <d3d11.h>
#include <map>
#include <memory>
#include <tuple>
#include <utility>
#include <vector>

#include <engine\assets3\Material.hpp>
#include <engine\assets3\Model.hpp>
#include <engine\data_structures\Embedded_list.hpp>
#include <engine\data_structures\Segmented_memory.hpp>
#include <engine\debug\Debug_info.hpp>
#include <engine\debug\Engine_debug.hpp>
#include <engine\game\Context.hpp>
#include <engine\hlsl\Instances_defs.hlsl>  // NOLINT
#include <engine\logging\Time.hpp>
#include <engine\rendering\Instances_group.hpp>
#include <engine\rendering\Target_fwd.hpp>
#include <engine\scene3\Node_fwd.hpp>
#include <engine\scene3\view\Camera_fwd.hpp>
#include <engine\utilities\buffers\Matrix_buffer.hpp>
#include <engine\utilities\memory\Raw_ptr.hpp>

namespace engine {
namespace rendering {

extern const char PARAMETERS_INSTANCED_RENDERING_NULL_NOT_ALLOWED[];
extern const char PARAMETERS_INSTANCED_RENDERING_ELEMENT_ALREADY_IN_LIST[];
extern const char PARAMETERS_INSTANCED_RENDERING_ELEMENT_NOT_IN_CURRENT_LIST[];

class Renderer final : public Noncopyable {
 public:
    INLINE Renderer(const engine::rendering::Target* t);
    INLINE ~Renderer();
    INLINE void setup();
    INLINE void unsetup();
    INLINE void render();
    ENGINE_DEBUG(INLINE const engine::debug::Debug_info& get_debug_info() const;)
    INLINE ID3D11Device* get_device() const;

 private:
    class Statistics final : public Noncopyable {
     public:
        using Frame_number = unsigned int;
        using Geometry_chunks_count = unsigned int;

        class iCumulative_counters : public Noncopyable {
         public:
            INLINE void increment_frame_number();
            INLINE void increment_triangles_count(unsigned int v);
            INLINE void increment_instances_count(unsigned int v);
            INLINE const Frame_number& get_frame_number() const;
            INLINE unsigned int get_triangles_count() const;
            INLINE unsigned int get_instances_count() const;

         protected:
            Frame_number frame_number_ = Frame_number();
            unsigned int triangles_count_ = 0;
            unsigned int instances_count_ = 0;
        };

        class iPer_frame_counters : public Noncopyable {
         public:
            INLINE void set_geometry_chunks_count(const Geometry_chunks_count& v);
            INLINE const Geometry_chunks_count& get_geometry_chunks_count() const;
            INLINE void increment_draw_indexed_instanced_calls_count();
            INLINE unsigned int get_draw_indexed_instanced_calls_count() const;
            INLINE void increment_instances_count(unsigned int v);
            INLINE unsigned int get_instances_count() const;

         protected:
            Geometry_chunks_count geometry_chunks_count_ = 0;
            unsigned int draw_indexed_instanced_calls_count_ = 0;
            unsigned int instances_count_ = 0;
        };

        INLINE ~Statistics();
        INLINE void update(float frame_time);
        INLINE iCumulative_counters& get_cumulative_counters();
        INLINE const iCumulative_counters& get_cumulative_counters() const;
        INLINE iPer_frame_counters& get_per_frame_counters();
        INLINE const iPer_frame_counters& get_per_frame_counters() const;

     private:
        using Draw_indexed_instanced_calls_count = unsigned int;
        using Triangles_count_per_second = float;
        using Instances_count_per_second = float;
        using Instances_count_per_frame = unsigned int;
        using Entry = std::tuple<engine::logging::Time, Frame_number,
            Draw_indexed_instanced_calls_count, Triangles_count_per_second,
            Instances_count_per_second, Instances_count_per_frame, Geometry_chunks_count>;

        class Cumulative_counters final : public iCumulative_counters {
         public:
            INLINE void reset();
        };

        class Per_frame_counters final : public iPer_frame_counters {
         public:
            INLINE void reset();
        };

        INLINE void append_entries_to_log_file();

        float sum_of_frame_time_ = 0;
        Cumulative_counters cumulative_counters_;
        Per_frame_counters per_frame_counters_;
        std::vector<Entry> entries_;
    };

    class Geometry_to_render_selector final : public Noncopyable {
     public:
        INLINE void store_material_parameter_values_and_transformation_matrices(
            const engine::scene3::view::Camera* camera,
            CXMMATRIX projection_matrix);

        class Parameters final {
         public:
            INLINE Parameters() = default;
            INLINE Parameters(CXMMATRIX world_matrix,
                const engine::assets3::Material::Parameters& parameters);
            INLINE CXMMATRIX get_world_matrix() const;
            INLINE const engine::assets3::Material::Parameters& get_parameters() const;

         private:
            class Parameters_for_instanced_rendering_list_element;
         public:
            class Parameters_for_instanced_rendering_list final :
                 public engine::data_structures::Embedded_list<
                Parameters_for_instanced_rendering_list,
                Parameters,
                Parameters_for_instanced_rendering_list_element,
                PARAMETERS_INSTANCED_RENDERING_NULL_NOT_ALLOWED,
                PARAMETERS_INSTANCED_RENDERING_ELEMENT_ALREADY_IN_LIST,
                PARAMETERS_INSTANCED_RENDERING_ELEMENT_NOT_IN_CURRENT_LIST> {
            };

         private:
            class Parameters_for_instanced_rendering_list_element final :
                 public engine::data_structures::Embedded_list<
                Parameters_for_instanced_rendering_list,
                Parameters,
                Parameters_for_instanced_rendering_list_element,
                PARAMETERS_INSTANCED_RENDERING_NULL_NOT_ALLOWED,
                PARAMETERS_INSTANCED_RENDERING_ELEMENT_ALREADY_IN_LIST,
                PARAMETERS_INSTANCED_RENDERING_ELEMENT_NOT_IN_CURRENT_LIST>::
                Copyable_element {
             public:
                using Type = engine::data_structures::Embedded_list<
                    Parameters_for_instanced_rendering_list,
                    Parameters,
                    Parameters_for_instanced_rendering_list_element,
                    PARAMETERS_INSTANCED_RENDERING_NULL_NOT_ALLOWED,
                    PARAMETERS_INSTANCED_RENDERING_ELEMENT_ALREADY_IN_LIST,
                    PARAMETERS_INSTANCED_RENDERING_ELEMENT_NOT_IN_CURRENT_LIST>::
                    Copyable_element;

                INLINE static Type& get_embedded_list_element(Parameters* p);
                INLINE static const Type& get_embedded_list_element(const Parameters* p);
            };

            XMMATRIX world_matrix_;
            engine::assets3::Material::Parameters parameters_;
            Parameters_for_instanced_rendering_list_element
                parameters_for_instanced_rendering_list_element_;
        };

        using Mesh_material_to_render =
            std::pair<
            engine::utilities::memory::Raw_ptr<const engine::assets3::Model::Mesh_material>,
            Parameters>;
        using Mesh_materials_to_render = engine::data_structures::Segmented_memory<
            Mesh_material_to_render, 1024 * 100>;

        INLINE void clear();
        INLINE Renderer::Geometry_to_render_selector::Mesh_materials_to_render&
            Renderer::Geometry_to_render_selector::get_mesh_materials_to_render();
        INLINE const Renderer::Geometry_to_render_selector::Mesh_materials_to_render&
            Renderer::Geometry_to_render_selector::get_mesh_materials_to_render() const;

     private:
        engine::utilities::memory::Raw_ptr<const engine::scene3::view::Camera> camera_;
        XMMATRIX projection_matrix_;
        Mesh_materials_to_render mesh_materials_to_render_;
    };

    class Geometry_to_render_grouper final : public Noncopyable {
     public:
        INLINE void perform_grouping(Geometry_to_render_selector::Mesh_materials_to_render&
            geometry_to_render);

        using Mesh_material_vertices_start_index = unsigned int;
        using Key = std::tuple<
            engine::utilities::memory::Raw_ptr<const engine::vertex_stream::Vertex_buffer>,
            const Mesh_material_vertices_start_index, const Instances_group,
            engine::utilities::memory::Raw_ptr<const engine::assets3::Model::Mesh_material>>;
        using Grouped_mesh_materials_to_render =
            std::map<Key, Geometry_to_render_selector::Parameters::
            Parameters_for_instanced_rendering_list>;

        INLINE const Grouped_mesh_materials_to_render&
            get_grouped_mesh_materials_to_render() const;

     private:
        INLINE void clear();

        Grouped_mesh_materials_to_render grouped_mesh_materials_to_render_;
    };

    class Geometry_drawer final : public Noncopyable {
     public:
        INLINE Geometry_drawer(Statistics& statistics);  // NOLINT
        INLINE ~Geometry_drawer();
        INLINE void setup(ID3D11Device* device);
        INLINE void unsetup();
        INLINE void draw(ID3D11DeviceContext* device_context,
            const Geometry_to_render_grouper::Grouped_mesh_materials_to_render&
            grouped_geometry_to_render, CXMMATRIX view_matrix,
            CXMMATRIX projection_matrix);

     private:
        static_assert(
            D3D11_REQ_TEXTURE2D_ARRAY_AXIS_DIMENSION < D3D11_REQ_CONSTANT_BUFFER_ELEMENT_COUNT,
            "Select valid min value for instances count!");

        INLINE void draw(ID3D11DeviceContext* device_context,
            const Geometry_to_render_selector::Parameters::
            Parameters_for_instanced_rendering_list& instances_list,
            const engine::assets3::Model::Mesh_material* mm,
            CXMMATRIX view_projection_matrix);
        INLINE void draw(ID3D11DeviceContext* device_context,
            const engine::assets3::Model::Mesh_material* mm, unsigned int instances_count);

        engine::utilities::buffers::Matrix_buffer instances_world_matrices_;
        engine::data_structures::Array<
            engine::utilities::memory::Raw_ptr<ID3D11ShaderResourceView>, HLSL_MAX_INSTANCES_COUNT>
            textures_;
        Statistics& statistics_;
    };

    INLINE void synchronize_with_context_if_needed(engine::game::Context* main_context);
    INLINE void setup_device_and_swap_chain_and_device_context_depth_stencil(HWND hWnd,
        int screen_width, int screen_height);
    INLINE XMMATRIX calculate_projection_matrix(const engine::scene3::view::Camera* camera) const;

    engine::utilities::memory::Raw_ptr<const engine::rendering::Target> target_;
    engine::utilities::memory::Raw_ptr<IDXGISwapChain> swap_chain_;
    engine::utilities::memory::Raw_ptr<ID3D11Device> device_;
    engine::utilities::memory::Raw_ptr<ID3D11DeviceContext> device_context_;
    engine::utilities::memory::Raw_ptr<ID3D11RenderTargetView> render_target_view_;
    engine::utilities::memory::Raw_ptr<ID3D11Texture2D> depth_stencil_texture_;
    engine::utilities::memory::Raw_ptr<ID3D11DepthStencilState> depth_stencil_state_;
    engine::utilities::memory::Raw_ptr<ID3D11DepthStencilView> depth_stencil_view_;
    engine::utilities::memory::Raw_ptr<ID3D11RasterizerState> rasterizer_state_;
    Statistics statistics_;
    Geometry_to_render_selector geometry_to_render_selector_;
    Geometry_to_render_grouper geometry_to_render_grouper_;
    Geometry_drawer geometry_drawer_;
    engine::utilities::memory::Raw_ptr<engine::game::Context>
        main_context_renderer_is_registered_to_;
    std::unique_ptr<engine::game::Context::Renderer_synchronizator>
        registrator_to_main_context_;
    ENGINE_DEBUG(engine::debug::Debug_info debug_info_;)
};
}  // namespace rendering
}  // namespace engine

#if !defined _DEBUG
#include <engine\rendering\Renderer.inl>
#endif

#endif  // MULTI_THREADED_RENDERER_ENGINE_RENDERING_RENDERER_HPP_
