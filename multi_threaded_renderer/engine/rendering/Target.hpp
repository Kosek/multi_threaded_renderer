// Copyright 2017 Tomasz Kosek. All rights reserved.

#ifndef MULTI_THREADED_RENDERER_ENGINE_RENDERING_TARGET_HPP_
#define MULTI_THREADED_RENDERER_ENGINE_RENDERING_TARGET_HPP_

#include <engine\naming\Unique_name.hpp>
#include <engine\rendering\Window_fwd.hpp>
#include <engine\scene3\view\Camera_fwd.hpp>
#include <engine\utilities\memory\Raw_ptr.hpp>

namespace engine {
namespace rendering {

class Target final : public Noncopyable {
 public:
    class Name : public engine::naming::Unique_name {
     public:
        using engine::naming::Unique_name::Unique_name;
    };

    INLINE Target(const Name& name, const Window* w);
    // INLINE Target(Texture texture, const Window* display_window = nullptr) - future
    INLINE const Name& get_name() const;
    INLINE const Window* get_window() const;
    INLINE void set_camera(const engine::scene3::view::Camera* camera);
    INLINE const engine::scene3::view::Camera* get_camera() const;

 private:
    const Name name_;
    engine::utilities::memory::Raw_ptr<const Window> window_;
    engine::utilities::memory::Raw_ptr<const engine::scene3::view::Camera> camera_;
};

}  // namespace rendering
}  // namespace engine

#if !defined _DEBUG
#include <engine\rendering\Target.inl>
#endif

#endif  // MULTI_THREADED_RENDERER_ENGINE_RENDERING_TARGET_HPP_
