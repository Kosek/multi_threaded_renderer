// Copyright 2017 Tomasz Kosek. All rights reserved.

namespace engine {
namespace rendering {
INLINE Window::Window(HWND hwnd, unsigned int width, unsigned int height)
    : hwnd_(hwnd), width_(width), height_(height) {
}
INLINE HWND Window::get_hwnd() const {
    return hwnd_;
}
INLINE unsigned int Window::get_width() const {
    return width_;
}
INLINE unsigned int Window::get_height() const {
    return height_;
}
}  // namespace rendering
}  // namespace engine
