// Copyright 2017 Tomasz Kosek. All rights reserved.

#include <stdafx.h>

#include <engine\rendering\Window.hpp>

#if defined _DEBUG
#include <engine\rendering\Window.inl>
#endif
