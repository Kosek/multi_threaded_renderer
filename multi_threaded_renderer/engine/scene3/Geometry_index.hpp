// Copyright 2017 Tomasz Kosek. All rights reserved.

#ifndef MULTI_THREADED_RENDERER_ENGINE_SCENE3_GEOMETRY_INDEX_HPP_
#define MULTI_THREADED_RENDERER_ENGINE_SCENE3_GEOMETRY_INDEX_HPP_

#include <map>
#include <string>
#include <utility>
#include <vector>

#include <engine\scene3\Node.hpp>
#include <engine\utilities\memory\Raw_ptr.hpp>

namespace engine {
namespace scene3 {
class Geometry_index final : public Noncopyable {
 public:
    INLINE void add(Node* n);
    INLINE void remove(const Node* n);
    INLINE void remove(const Node::Name& name);
    INLINE Node* get(const Node::Name& name);
    INLINE const Node* get(const Node::Name& name) const;
    template <typename VISITOR>
    INLINE void visit_all(/*const engine::scene3::view::Frustum& f,*/const VISITOR& visitor) const;

 private:
    INLINE void insert_to_name_index(Node* n);
    INLINE void remove_from_name_index(const Node::Name& name);
    INLINE bool exists(const Node::Name& name) const;
    INLINE void insert_to_vector(Node* n);
    INLINE void remove_from_vector(const Node* n);

    using Nodes_collection_entry =
        std::pair<std::string, engine::utilities::memory::Raw_ptr<Node>>;
    using Nodes_collection = std::map<std::string, engine::utilities::memory::Raw_ptr<Node>>;
    Nodes_collection node_names_;
    std::vector<engine::utilities::memory::Raw_ptr<Node>> nodes_;
    // Future developmnet:
    // octree
};
}  // namespace scene3
}  // namespace engine

#if !defined _DEBUG
#include <engine\scene3\Geometry_index.inl>
#endif

#include <engine\scene3\Geometry_index_header.inl>

#endif  // MULTI_THREADED_RENDERER_ENGINE_SCENE3_GEOMETRY_INDEX_HPP_
