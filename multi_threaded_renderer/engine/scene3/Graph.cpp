// Copyright 2017 Tomasz Kosek. All rights reserved.

#include <stdafx.h>

#include <engine\scene3\Graph.hpp>

#if defined _DEBUG
#include <engine\scene3\Graph.inl>
#endif

namespace engine {
namespace scene3 {
ENGINE_REGISTER_NAME_OF_TYPE(Node::Name, ROOT);
}  // namespace scene3
}  // namespace engine
