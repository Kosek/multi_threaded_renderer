// Copyright 2017 Tomasz Kosek. All rights reserved.

#include <utility>

#include <engine\scene3\Scene.hpp>
#include <engine\scene3\Space.hpp>

namespace engine {
namespace scene3 {
INLINE Smart_entry::Smart_entry(const engine::scene3::Entry::Name& name) :
    entry_(name) {
}
INLINE Smart_entry::~Smart_entry() {
    if (entry_.get_space()) {
        if (entry_.get_space()->get_scene()) {
            entry_.get_space()->get_scene()->get_mutator().remove(get());
        } else {
            entry_.get_space()->remove(get());
        }
    }
}
INLINE const Entry* Smart_entry::get() const {
    return &entry_;
}
INLINE Entry* Smart_entry::get() {
    return &entry_;
}
INLINE const Entry* Smart_entry::operator->() const {
    return get();
}
INLINE Entry* Smart_entry::operator->() {
    return get();
}
}  // namespace scene3
}  // namespace engine
