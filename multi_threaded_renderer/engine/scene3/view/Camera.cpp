// Copyright 2017 Tomasz Kosek. All rights reserved.

#include <stdafx.h>

#include <engine\scene3\view\Camera.hpp>

#if defined _DEBUG
#include <engine\scene3\view\Camera.inl>
#endif
