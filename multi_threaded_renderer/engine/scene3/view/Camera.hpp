// Copyright 2017 Tomasz Kosek. All rights reserved.

#ifndef MULTI_THREADED_RENDERER_ENGINE_SCENE3_VIEW_CAMERA_HPP_
#define MULTI_THREADED_RENDERER_ENGINE_SCENE3_VIEW_CAMERA_HPP_

#include <vector>

#include <engine\scene3\Smart_entry.hpp>

namespace engine {
namespace scene3 {
namespace view {
class Camera final : public Noncopyable {
 public:
    class Name : public Entry::Name {
        using Entry::Name::Name;
    };

    INLINE Camera(const Name& name);
    INLINE XMMATRIX get_view_matrix() const;
    INLINE float get_field_of_view() const;
    INLINE float get_near_clipping_plane() const;
    INLINE float get_far_clipping_plane() const;
    INLINE Entry* get_entry();
    INLINE const Entry* get_entry() const;
    INLINE Node* get_camera_node() const;
    INLINE Node* get_look_at_node() const;

    class Calculator final {
     public:
        class Local final {
         public:
            INLINE Local(const Camera* camera);
            INLINE XMVECTOR calculate_normalized_look_direction() const;
            INLINE XMVECTOR calculate_normalized_left_vector() const;
            INLINE XMVECTOR calculate_normalized_up_vector() const;

         private:
            const Camera* camera_ = nullptr;
        };

        // i.e. class World { ... }
    };

    template <class CALCULATOR_TYPE>
    INLINE CALCULATOR_TYPE get_calculator() const;

 private:
    Smart_entry entry_;
    engine::utilities::memory::Raw_ptr<Node> camera_node_;
    engine::utilities::memory::Raw_ptr<Node> look_at_node_;
    XMVECTOR up_direction_ = XMVectorSet(0, 1, 0, 0);
};
}  // namespace view
}  // namespace scene3
}  // namespace engine

#include <engine\scene3\view\Camera_header.inl>

#if !defined _DEBUG
#include <engine\scene3\view\Camera.inl>
#endif

#endif  // MULTI_THREADED_RENDERER_ENGINE_SCENE3_VIEW_CAMERA_HPP_
