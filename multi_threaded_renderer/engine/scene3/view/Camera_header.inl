// Copyright 2017 Tomasz Kosek. All rights reserved.

namespace engine {
namespace scene3 {
namespace view {
template <class CALCULATOR_TYPE>
INLINE CALCULATOR_TYPE Camera::get_calculator() const {
    return CALCULATOR_TYPE(this);
}
}  // namespace view
}  // namespace scene3
}  // namespace engine
