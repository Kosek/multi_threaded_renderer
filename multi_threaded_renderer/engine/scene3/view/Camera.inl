// Copyright 2017 Tomasz Kosek. All rights reserved.

#include <D3DX10math.h>
#include <memory>
#include <vector>

#include <engine\debug\Assert.hpp>
#include <engine\scene3\Scene.hpp>
#include <engine\scene3\Space.hpp>

namespace engine {
namespace scene3 {
namespace view {
INLINE Camera::Camera(const Name& name) : entry_(name) {
    entry_->set(std::make_unique<Node>());
    camera_node_ = entry_->get_node();
    look_at_node_ = camera_node_->add(std::make_unique<Node>());
    look_at_node_->set_position(XMFLOAT3{0, 0, 1});
}
INLINE XMMATRIX Camera::get_view_matrix() const {
    XMVECTOR eye_position = camera_node_->get_world_position();
    XMVECTOR look_at = look_at_node_->get_world_position();
    return XMMatrixLookAtLH(eye_position, look_at, up_direction_);
}
INLINE float Camera::get_field_of_view() const {
    return static_cast<float>(D3DX_PI) / 2.0f;
}
INLINE float Camera::get_near_clipping_plane() const {
    return 0.1f;
}
INLINE float Camera::get_far_clipping_plane() const {
    return 1000.0f;
}
INLINE const Entry* Camera::get_entry() const {
    return entry_.get();
}
INLINE Entry* Camera::get_entry() {
    return entry_.get();
}
INLINE Node* Camera::get_camera_node() const {
    return camera_node_;
}
INLINE Node* Camera::get_look_at_node() const {
    return look_at_node_;
}
}  // namespace view
}  // namespace scene3
}  // namespace engine

namespace engine {
namespace scene3 {
namespace view {
INLINE Camera::Calculator::Local::Local(const Camera* camera) : camera_(camera) {
}
INLINE XMVECTOR Camera::Calculator::Local::calculate_normalized_look_direction() const {
    XMVECTOR ret = camera_->look_at_node_->get_position();
    ret = XMVector3Normalize(ret);
    return ret;
}
INLINE XMVECTOR Camera::Calculator::Local::calculate_normalized_left_vector() const {
    XMVECTOR ret = XMVector3Cross(camera_->look_at_node_->get_position(), camera_->up_direction_);
    ret = XMVector3Normalize(ret);
    return ret;
}
INLINE XMVECTOR Camera::Calculator::Local::calculate_normalized_up_vector() const {
    return XMVector3Normalize(camera_->up_direction_);
}
}  // namespace view
}  // namespace scene3
}  // namespace engine
