// Copyright 2017 Tomasz Kosek. All rights reserved.

#ifndef MULTI_THREADED_RENDERER_ENGINE_SCENE3_VIEW_CAMERA_FWD_HPP_
#define MULTI_THREADED_RENDERER_ENGINE_SCENE3_VIEW_CAMERA_FWD_HPP_

namespace engine {
namespace scene3 {
namespace view {

class Camera;

}  // namespace view
}  // namespace scene3
}  // namespace engine


#endif  // MULTI_THREADED_RENDERER_ENGINE_SCENE3_VIEW_CAMERA_FWD_HPP_
