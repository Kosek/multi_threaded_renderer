// Copyright 2017 Tomasz Kosek. All rights reserved.

#include <stdafx.h>

#include <engine\scene3\Scene.hpp>

#if defined _DEBUG
#include <engine\scene3\Scene.inl>
#endif

namespace engine {
namespace scene3 {
const char SCENE_CONTEXT_NULL_NOT_ALLOWED[] = { "Scene must not be null!" };
const char SCENE_CONTEXT_ELEMENT_ALREADY_IN_LIST[] = { "Scene already added to context!" };
const char SCENE_CONTEXT_ELEMENT_NOT_IN_CURRENT_LIST[] = {
    "Scene is not added to current context!" };

const char SCENE_TO_UPDATE_CONTEXT_NULL_NOT_ALLOWED[] = { "Scene must not be null!" };
const char SCENE_TO_UPDATE_CONTEXT_ELEMENT_ALREADY_IN_LIST[] = {
    "Scene already added to update list!" };
const char SCENE_TO_UPDATE_CONTEXT_ELEMENT_NOT_IN_CURRENT_LIST[] = {
    "Scene is not added to current update list!" };
}  // namespace scene3
}  // namespace engine
