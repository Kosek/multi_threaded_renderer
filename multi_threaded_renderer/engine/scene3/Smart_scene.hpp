// Copyright 2017 Tomasz Kosek. All rights reserved.

#ifndef MULTI_THREADED_RENDERER_ENGINE_SCENE3_SMART_SCENE_HPP_
#define MULTI_THREADED_RENDERER_ENGINE_SCENE3_SMART_SCENE_HPP_

#include <memory>

#include <engine\scene3\Scene.hpp>

namespace engine {
namespace scene3 {
class Smart_scene final : public Noncopyable {
 public:
    INLINE Smart_scene(const engine::scene3::Scene::Name& name);
    INLINE ~Smart_scene();
    INLINE Smart_scene(Smart_scene&&) = default;
    INLINE Smart_scene& operator=(Smart_scene&&) = default;
    INLINE const Scene* get() const;
    INLINE Scene* get();
    INLINE const Scene* operator->() const;
    INLINE Scene* operator->();

 private:
    Scene scene_;
};
}  // namespace scene3
}  // namespace engine

#if !defined _DEBUG
#include <engine\scene3\Smart_scene.inl>
#endif

#endif  // MULTI_THREADED_RENDERER_ENGINE_SCENE3_SMART_SCENE_HPP_
