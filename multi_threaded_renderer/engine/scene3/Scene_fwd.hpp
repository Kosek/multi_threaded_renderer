// Copyright 2017 Tomasz Kosek. All rights reserved.

#ifndef MULTI_THREADED_RENDERER_ENGINE_SCENE3_SCENE_FWD_HPP_
#define MULTI_THREADED_RENDERER_ENGINE_SCENE3_SCENE_FWD_HPP_

namespace engine {
namespace scene3 {

class Scene;

}  // namespace scene3
}  // namespace engine

#endif  // MULTI_THREADED_RENDERER_ENGINE_SCENE3_SCENE_FWD_HPP_
