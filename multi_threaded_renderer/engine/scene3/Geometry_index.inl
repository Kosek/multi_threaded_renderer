// Copyright 2017 Tomasz Kosek. All rights reserved.

#include <algorithm>
#include <utility>
#include <vector>

#include <engine\debug\Assert.hpp>
#include <engine\scene3\Node.hpp>

namespace engine {
namespace scene3 {
INLINE void Geometry_index::add(Node* n) {
    if (!n->get_name().is_empty()) {
        insert_to_name_index(n);
    }
    insert_to_vector(n);
}
INLINE void Geometry_index::remove(const Node* n) {
    ENGINE_ASSERT(n, "Node must not be null!");
    if (!n->get_name().is_empty()) {
        remove_from_name_index(n->get_name().get());
    }
    remove_from_vector(n);
}
INLINE void Geometry_index::remove(const Node::Name& name) {
    Node* n = get(name);
    if (n) {
        remove(n);
    }
}
INLINE void Geometry_index::insert_to_name_index(Node* n) {
    ENGINE_ASSERT(n, "Node must not be null!");
    ENGINE_ASSERT(!exists(n->get_name().get()), "Node already exists!");
    node_names_.insert(Nodes_collection_entry(n->get_name().get(), n));
    n->get_iterator().visit_all_nodes([&](Node* cn) {
        node_names_.insert(Nodes_collection_entry(cn->get_name().get(), cn));
    });
}
INLINE void Geometry_index::remove_from_name_index(const Node::Name& name) {
    auto found = node_names_.find(name.get());
    if (found == node_names_.end()) {
        return;
    } else {
        const Node* found_ptr = found->second;
        node_names_.erase(node_names_.find(found_ptr->get_name().get()));
        found_ptr->get_iterator().visit_all_nodes([&](const Node* n) {
            node_names_.erase(node_names_.find(n->get_name().get()));
        });
    }
}
INLINE bool Geometry_index::exists(const Node::Name& name) const {
    return (node_names_.find(name.get()) == node_names_.end());
}
INLINE void Geometry_index::insert_to_vector(Node* n) {
    nodes_.push_back(n);
}
INLINE void Geometry_index::remove_from_vector(const Node* n) {
    auto found = std::find_if(nodes_.begin(), nodes_.end(), [&n](const Node* node) {
        return node == n; });
    if (found != nodes_.end()) {
        nodes_.erase(found);
    }
}
INLINE const Node* Geometry_index::get(const Node::Name& name) const {
    auto found = node_names_.find(name.get());
    if (found == node_names_.end()) {
        return nullptr;
    } else {
        return static_cast<const Node*>(found->second);
    }
}
INLINE Node* Geometry_index::get(const Node::Name& name) {
    auto found = node_names_.find(name.get());
    if (found == node_names_.end()) {
        return nullptr;
    } else {
        return static_cast<Node*>(found->second);
    }
}
}  // namespace scene3
}  // namespace engine
