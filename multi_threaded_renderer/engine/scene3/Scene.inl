// Copyright 2017 Tomasz Kosek. All rights reserved.

#include <memory>
#include <utility>

#include <engine\debug\Assert.hpp>

namespace engine {
namespace scene3 {
INLINE Scene::Scene(const Name& name)
    : name_(name), content_(this) {
}
INLINE const Scene::Name& Scene::get_name() const {
    return name_;
}
INLINE const Scene::Positioning& Scene::get_positioning() const {
    return positioning_;
}
INLINE Scene::Positioning& Scene::get_positioning() {
    return positioning_;
}
INLINE const Scene::Content& Scene::get_content() const {
    return content_;
}
INLINE Scene::Content& Scene::get_content() {
    return content_;
}
INLINE const Scene::Updater& Scene::get_updater() const {
    return updater_;
}
INLINE Scene::Updater& Scene::get_updater() {
    return updater_;
}
INLINE Scene::Mutator Scene::get_mutator() {
    return Mutator(this);
}
INLINE Scene::Query Scene::create_query() {
    return Query(this);
}
INLINE game::Context* Scene::get_context() const {
    if (scenes_in_context_list_element_.get_list()) {
        return scenes_in_context_list_element_.get_list()->get_context();
    } else {
        return nullptr;
    }
}
INLINE void Scene::update(engine::game::Context* c) {
    updater_.update(c);
}
INLINE bool Scene::is_receive_update_enabled() const {
    return scenes_to_update_in_context_list_element_.is_in_list();
}
}  // namespace scene3
}  // namespace engine

namespace engine {
namespace scene3 {
INLINE const Geometry_index* Scene::Positioning::get_geometry_index() const {
    return &geometry_index_;
}
INLINE Geometry_index* Scene::Positioning::get_geometry_index() {
    return &geometry_index_;
}
INLINE const Graph* Scene::Positioning::get_graph() const {
    return &graph_;
}
INLINE Graph* Scene::Positioning::get_graph() {
    return &graph_;
}
INLINE Node* Scene::Positioning::add(Node* parent, std::unique_ptr<Node>&& n) {
    ENGINE_ASSERT(parent, "Parent must not be null!");
    ENGINE_ASSERT(parent->get_root_node() == graph_.get_root(),
        "Parent does not belong to current graph!");
    geometry_index_.add(n.get());
    Node* ret = parent->add(std::move(n));
    return ret;
}
INLINE void Scene::Positioning::remove(Node* n) {
    ENGINE_ASSERT(n, "Node must not be null!");
    ENGINE_ASSERT(n->get_entry(), "Node must be attached to entry!");
    geometry_index_.remove(n);
    n->remove_from_parent().release();  // Entry is the owner of Node
}
INLINE Node* Scene::Positioning::remove(const Node::Name& name) {
    Node* ret = get(name);
    if (ret) {
        remove(ret);
    }
    return ret;
}
INLINE Node* Scene::Positioning::get(const Node::Name& name) {
    return geometry_index_.get(name.get());
}
INLINE const Node* Scene::Positioning::get(const Node::Name& name) const {
    return geometry_index_.get(name.get());
}
}  // namespace scene3
}  // namespace engine

namespace engine {
namespace scene3 {
INLINE Scene::Content::Content(Scene* s)
    : spaces_(s) {
}
INLINE void Scene::Content::add(Space* s) {
    spaces_.add(s);
    if (!s->get_name().is_empty()) {
        space_index_.add(s);
    }

    s->get_iterator().visit_all_entries([&](Entry* e) {
        if (!e->get_name().is_empty()) {
            entry_index_.add(e);
        }
    });
    s->get_iterator().visit_all_spaces([&](Space* visited_space) {
        spaces_.add(visited_space);
        if (!visited_space->get_name().is_empty()) {
            space_index_.add(visited_space);
        }
    });
}
INLINE void Scene::Content::remove(Space* s) {
    if (s->is_receive_update_enabled()) {
        s->get_scene()->get_updater().remove(s);
    }
    spaces_.remove(s);
    if (!s->get_name().is_empty()) {
        space_index_.remove(s);
    }
    s->get_iterator().visit_all_entries([&](const Entry* e) {
        if (!e->get_name().is_empty()) {
            entry_index_.remove(e);
        }
    });
    s->get_iterator().visit_all_spaces([&](Space* visited_space) {
        spaces_.remove(visited_space);
        space_index_.remove(visited_space);
    });
    if (s->get_parent()) {
        s->get_parent()->remove(s);
    }
}
INLINE Space* Scene::Content::remove(const Space::Name& name) {
    Space* ret = get(name);
    if (ret) {
        remove(ret);
    }
    return ret;
}
INLINE Space* Scene::Content::get(const Space::Name& name) {
    return space_index_.get(name);
}
INLINE const Space* Scene::Content::get(const Space::Name& name) const {
    return space_index_.get(name);
}
INLINE void Scene::Content::add(Space* parent, Space* child) {
    ENGINE_ASSERT(parent, "Parent must not be null!");
    ENGINE_ASSERT(child, "Child must not be null!");
    ENGINE_ASSERT(contains(parent), "Parent space is not present in current scene!");
    ENGINE_ASSERT(!child->is_in_scene(), "Space already added to scene!");
    parent->add(child);
    add(child);
}
INLINE void Scene::Content::add(Space* parent, Entry* e) {
    ENGINE_ASSERT(parent, "Space must not be null!");
    ENGINE_ASSERT(e, "Entry must not be null!");
    ENGINE_ASSERT(!e->is_in_space(), "Entry must not be in space!");
    ENGINE_ASSERT(contains(parent), "Space is not present in current scene!");
    parent->add(e);
    if (!e->get_name().is_empty()) {
        entry_index_.add(e);
    }
}
INLINE void Scene::Content::remove(Entry* e) {
    ENGINE_ASSERT(e, "Entry must not be null!");
    ENGINE_ASSERT(e->is_in_space(), "Entry should be added to space!");
    e->get_space()->remove(e);
    entry_index_.remove(e);
}
INLINE Entry* Scene::Content::remove(const Entry::Name& name) {
    Entry* ret = get(name);
    if (ret) {
        remove(ret);
    }
    return ret;
}
INLINE bool Scene::Content::contains(const Space* s) const {
    return spaces_.contains(s);
}
INLINE Entry* Scene::Content::get(const Entry::Name& name) {
    return entry_index_.get(name);
}
INLINE const Entry* Scene::Content::get(const Entry::Name& name) const {
    return entry_index_.get(name);
}
INLINE Scene::Content::Iterator Scene::Content::get_iterator() const {
    return Scene::Content::Iterator(this);
}
}  // namespace scene3
}  // namespace engine

namespace engine {
namespace scene3 {
INLINE void Scene::Updater::add(engine::scene3::Space* s) {
    spaces_.add(s);
}
INLINE void Scene::Updater::remove(engine::scene3::Space* s) {
    spaces_.remove(s);
}
INLINE void Scene::Updater::update(engine::game::Context* c) {
    spaces_.get_iterator().visit_all([&c](Space* s) {
        s->update(c);
    });
}
}  // namespace scene3
}  // namespace engine

namespace engine {
namespace scene3 {
INLINE Scene::Content::Iterator::Iterator(const Content* content)
    : content_(content) {
}
}  // namespace scene3
}  // namespace engine

namespace engine {
namespace scene3 {
INLINE Scene::Mutator::Mutator(Scene* s)
    : scene_(s) {
}
INLINE Scene::Mutator::Add_space Scene::Mutator::add(Space* s) {
    return Add_space(s, *this);
}
INLINE void Scene::Mutator::remove(Space* s) {
    scene_->get_content().remove(s);
    s->get_iterator().visit_all_entries([&](Entry* e) {
        if (e->get_node()) {
            scene_->get_positioning().remove(e->get_node());
        }
    });
}
INLINE Space* Scene::Mutator::remove(const Space::Name& name) {
    return scene_->get_content().remove(name);
}
INLINE Scene::Mutator::Add_entry Scene::Mutator::add(Entry* e) {
    return Add_entry(e, *this);
}
INLINE Entry* Scene::Mutator::remove(const Entry::Name& name) {
    return scene_->get_content().remove(name);
}
INLINE Scene::Mutator::Remove_entry Scene::Mutator::remove(Entry* e) {
    return Remove_entry(e, *this);
}
INLINE Scene::Mutator::Move_entry Scene::Mutator::move(Entry* e) {
    return Move_entry(e, *this);
}
INLINE Scene::Mutator::Move_space Scene::Mutator::move(Space* s) {
    return Move_space(s, *this);
}
INLINE void Scene::Mutator::enable_update(const engine::scene3::Space::Name& name) {
    scene_->get_updater().add(scene_->get_content().get(name));
}
INLINE void Scene::Mutator::disable_update(const engine::scene3::Space::Name& name) {
    scene_->get_updater().remove(scene_->get_content().get(name));
}
INLINE void Scene::Mutator::enable_update(const engine::scene3::Entry::Name& name) {
    Entry* e = scene_->get_content().get(name);
    e->get_space()->enable_update(e);
}
INLINE void Scene::Mutator::disable_update(const engine::scene3::Entry::Name& name) {
    Entry* e = scene_->get_content().get(name);
    e->get_space()->disable_update(e);
}

INLINE Scene::Mutator::Add_space::Add_space(Space* s, const Mutator& b)
    : space_(s), mutator_(b) {
}
INLINE Scene::Mutator::Add_space::~Add_space() {
    if (space_) {
        mutator_.scene_->get_content().add(space_);
        space_ = nullptr;
    }
}

INLINE Scene::Mutator::Add_space::Under_space Scene::Mutator::Add_space::under(Space* parent) {
    return Under_space(parent, *this);
}
INLINE Scene::Mutator::Add_space::Under_space::Under_space(Space* parent, Add_space& add)
    : parent_(parent), add_space_(add) {
}
INLINE Scene::Mutator::Add_space::Under_space::~Under_space() {
    add_space_.mutator_.scene_->get_content().add(parent_, add_space_.space_);
    add_space_.space_ = nullptr;
}

INLINE void Scene::Mutator::Add_space::Under_space::under(Node* parent) {
    add_space_.space_->get_iterator().visit_all_entries([&](Entry* e) {
        add_space_.mutator_.scene_->get_positioning().add(parent,
            std::unique_ptr<Node>(e->get_node()));  // intentional std::unique_ptr creation
    });
}

INLINE void Scene::Mutator::Add_space::under(Node* parent) {
    space_->get_iterator().visit_all_entries([&](Entry* e) {
        mutator_.scene_->get_positioning().add(parent,
            std::unique_ptr<Node>(e->get_node()));  // intentional std::unique_ptr creation
    });
}

INLINE Scene::Mutator::Add_entry::Add_entry(Entry* e, const Mutator& b)
    : entry_(e), mutator_(b) {
}
INLINE Scene::Mutator::Add_entry::~Add_entry() {
    ENGINE_ASSERT(was_to_space_called_,
        "Method to(space) was not called! Add entry will have no effect");
}
INLINE Scene::Mutator::Add_entry::To_space Scene::Mutator::Add_entry::to(Space* s) {
    return To_space(s, *this);
}

INLINE Scene::Mutator::Add_entry::To_space::To_space(Space* s, Add_entry& add_entry)
    : space_(s), add_entry_(add_entry) {
    ENGINE_DEBUG(add_entry_.was_to_space_called_ = true;)
}
INLINE Scene::Mutator::Add_entry::To_space::~To_space() {
    add_entry_.mutator_.scene_->get_content().add(space_, add_entry_.entry_);
}
INLINE void Scene::Mutator::Add_entry::To_space::under(Node* parent) {
    add_entry_.mutator_.scene_->get_positioning().add(
        parent, std::unique_ptr<Node>(add_entry_.entry_->get_node()));
        // intentional std::unique_ptr creation
}

INLINE Scene::Mutator::Remove_entry::Remove_entry(Entry* e, const Mutator& b)
    : entry_(e), mutator_(b) {
}
INLINE Scene::Mutator::Remove_entry::~Remove_entry() {
    if (entry_) {
        mutator_.scene_->get_content().remove(entry_);
        if (entry_->get_node()) {
            mutator_.scene_->get_positioning().remove(entry_->get_node());
        }
    }
}
INLINE void Scene::Mutator::Remove_entry::from_position() {
    mutator_.scene_->get_positioning().remove(entry_->get_node());
    entry_ = nullptr;
}

INLINE Scene::Mutator::Move_entry::Move_entry(Entry* e, const Mutator& b)
    : entry_(e), mutator_(b)
{}
INLINE void Scene::Mutator::Move_entry::under(Node* parent) {
    if (entry_->get_node()->get_parent()) {
        mutator_.scene_->get_positioning().remove(entry_->get_node());
    }
    mutator_.scene_->get_positioning().add(parent, std::unique_ptr<Node>(entry_->get_node()));
        // intentional std::unique_ptr creation
}
INLINE void Scene::Mutator::Move_entry::to(Space* s) {
    if (entry_->is_in_space()) {
        mutator_.scene_->get_content().remove(entry_);
    }
    mutator_.scene_->get_content().add(s, entry_);
}

INLINE Scene::Mutator::Move_space::Move_space(Space* s, const Mutator& b)
    : space_(s), mutator_(b) {
}
INLINE Scene::Mutator::Move_space::~Move_space() {
    ENGINE_ASSERT(was_under_space_called_ || was_under_scene_called_,
        "None of methods under(space) or under_scene was called! Move space has no effect!");
}
INLINE Scene::Mutator::Move_space::Under_space Scene::Mutator::Move_space::under(Space* s) {
    return Under_space(s, *this);
}
INLINE Scene::Mutator::Move_space::Under_scene Scene::Mutator::Move_space::under_scene() {
    return Under_scene(*this);
}

INLINE Scene::Mutator::Move_space::Under_space::Under_space(Space* parent, Move_space& move_space)
    : parent_(parent), move_space_(move_space) {
    ENGINE_DEBUG(move_space_.was_under_space_called_ = true;)
}
INLINE Scene::Mutator::Move_space::Under_space::~Under_space() {
    move_space_.mutator_.scene_->get_content().remove(move_space_.space_);
    move_space_.mutator_.scene_->get_content().add(parent_, move_space_.space_);
}
INLINE void Scene::Mutator::Move_space::Under_space::under(Node* parent) {
    move_space_.space_->get_iterator().visit_all_entries([&](Entry* e) {
        move_space_.mutator_.scene_->get_positioning().remove(e->get_node());
        move_space_.mutator_.scene_->get_positioning().add(parent,
            std::unique_ptr<Node>(e->get_node()));  // intentional std::unique_ptr creation
    });
}

INLINE Scene::Mutator::Move_space::Under_scene::Under_scene(Move_space& move_space)
    : move_space_(move_space) {
    ENGINE_DEBUG(move_space_.was_under_scene_called_ = true;)
}
INLINE Scene::Mutator::Move_space::Under_scene::~Under_scene() {
    move_space_.mutator_.scene_->get_content().remove(move_space_.space_);
    move_space_.mutator_.scene_->get_content().add(move_space_.space_);
}
INLINE void Scene::Mutator::Move_space::Under_scene::under(Node* parent) {
    move_space_.space_->get_iterator().visit_all_entries([&](Entry* e) {
        move_space_.mutator_.scene_->get_positioning().remove(e->get_node());
        move_space_.mutator_.scene_->get_positioning().add(parent,
            std::unique_ptr<Node>(e->get_node()));  // intentional std::unique_ptr creation
    });
}

}  // namespace scene3
}  // namespace engine

namespace engine {
namespace scene3 {
INLINE Scene::Query::Query(Scene* scene)
    : scene_(scene) {
}
INLINE Space* Scene::Query::select(const Space::Name& name) {
    return scene_->get_content().get(name);
}
INLINE Entry* Scene::Query::select(const Entry::Name& name) {
    return scene_->get_content().get(name);
}
INLINE Node* Scene::Query::select(const Node::Name& name) {
    return scene_->get_positioning().get(name);
}
INLINE Node* Scene::Query::select_root() {
    return scene_->get_positioning().get_graph()->get_root();
}
INLINE Scene::Query::Nodes_iterator Scene::Query::select_nodes(
/*const engine::scene3::view::Frustum& f*/) {
    return Nodes_iterator(scene_);
}
}  // namespace scene3
}  // namespace engine

namespace engine {
namespace scene3 {
INLINE Scene::Query::Nodes_iterator::Nodes_iterator(Scene* scene) : scene_(scene) {
    ENGINE_ASSERT(scene_, "Scene must not be null!");
}
}  // namespace scene3
}  // namespace engine

namespace engine {
namespace scene3 {
INLINE Scene::Scenes_in_context_list::Scenes_in_context_list(game::Context* c)
    : context_(c) {
    ENGINE_ASSERT(context_, "Context must not be null!");
}
INLINE game::Context* Scene::Scenes_in_context_list::get_context() const {
    return context_;
}
}  // namespace scene3
}  // namespace engine

namespace engine {
namespace scene3 {
INLINE Scene::Scenes_in_context_list_element::Type&
Scene::Scenes_in_context_list_element::get_embedded_list_element(Scene* s) {
    return s->scenes_in_context_list_element_;
}
INLINE const Scene::Scenes_in_context_list_element::Type&
Scene::Scenes_in_context_list_element::get_embedded_list_element(const Scene* s) {
    return s->scenes_in_context_list_element_;
}
}  // namespace scene3
}  // namespace engine

namespace engine {
namespace scene3 {
INLINE Scene::Scenes_to_update_in_context_list_element::Type&
Scene::Scenes_to_update_in_context_list_element::get_embedded_list_element(
    Scene* s) {
    return s->scenes_to_update_in_context_list_element_;
}
INLINE const Scene::Scenes_to_update_in_context_list_element::Type&
Scene::Scenes_to_update_in_context_list_element::get_embedded_list_element(
    const Scene* s) {
    return s->scenes_to_update_in_context_list_element_;
}
}  // namespace scene3
}  // namespace engine
