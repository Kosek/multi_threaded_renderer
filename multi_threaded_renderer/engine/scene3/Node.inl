// Copyright 2017 Tomasz Kosek. All rights reserved.

#include <memory>
#include <utility>

#include <engine\data_structures\Embedded_list.hpp>
#include <engine\debug\Assert.hpp>

namespace engine {
namespace scene3 {
INLINE Node::Node()
    : child_nodes_(this) {
}
INLINE Node::Node(const Name& name)
    : name_(name), child_nodes_(this) {
}
INLINE Node::~Node() {
    ENGINE_ASSERT(!get_parent(), "Node is not allowed to be child node during destruction!");
    child_nodes_.remove_all([&](const Node* n) {
        if (n->get_entry() == nullptr) {
            delete n;
        }
    });
}
INLINE const FXMVECTOR Node::get_position() const {
    return position_;
}
INLINE void Node::set_position(const XMFLOAT3& position) {
    position_ = XMLoadFloat3(&position);
}
INLINE void Node::set_position(const FXMVECTOR position) {
    position_ = position;
}
INLINE Node* Node::get_parent() const {
    if (child_nodes_element_.is_in_list()) {
        return child_nodes_element_.get_list()->get_parent();
    } else {
        return nullptr;
    }
}
INLINE const Node* Node::get_root_node() const {
    if (get_parent()) {
        return get_parent()->get_root_node();
    } else {
        return this;
    }
}
INLINE Node* Node::get_root_node() {
    if (get_parent()) {
        return get_parent()->get_root_node();
    } else {
        return this;
    }
}
INLINE XMMATRIX Node::get_world_matrix() const {
    if (get_parent()) {
        XMMATRIX transformation_matrix =
            XMMatrixTransformation(
                XMVectorSet(0.0f, 0.0f, 0.0f, 0.0f),
                XMVectorSet(0.0f, 0.0f, 0.0f, 0.0f),
                XMVectorSet(1.0f, 1.0f, 1.0f, 1.0f),
                XMVectorSet(0.0f, 0.0f, 0.0f, 0.0f),
                rotation_quaternion_,
                position_);
        return XMMatrixMultiply(transformation_matrix, get_parent()->get_world_matrix());
    } else {
        return XMMatrixTransformation(
            XMVectorSet(0.0f, 0.0f, 0.0f, 0.0f),
            XMVectorSet(0.0f, 0.0f, 0.0f, 0.0f),
            XMVectorSet(1.0f, 1.0f, 1.0f, 1.0f),
            XMVectorSet(0.0f, 0.0f, 0.0f, 0.0f),
            rotation_quaternion_,
            position_);
    }
}
INLINE XMVECTOR Node::get_world_position() const {
    return XMVector3Transform(XMVectorSet(0.0f, 0.0f, 0.0f, 1.0f), get_world_matrix());
}
INLINE const Node::Name& Node::get_name() const {
    return name_;
}
INLINE Node* Node::add(std::unique_ptr<Node>&& child) {
    std::unique_ptr<Node> tmp = std::move(child);
    Node* ret = tmp.get();
    tmp.release();
    child_nodes_.add(ret);
    return ret;
}
INLINE std::unique_ptr<Node> Node::remove(Node* child) {
    child_nodes_.remove(child);
    return std::unique_ptr<Node>(child);
}
INLINE std::unique_ptr<Node> Node::remove_from_parent() {
    if (get_parent()) {
        return get_parent()->remove(this);
    } else {
        return std::unique_ptr<Node>();
    }
}
INLINE Node::Iterator Node::get_iterator() const {
    return Iterator(this);
}
INLINE Entry* Node::get_entry() const {
    return move_executor_.entry_;
}
INLINE void Node::set_entry(Entry* e) {
    ENGINE_ASSERT(!move_executor_.entry_, "Entry is already set!");
    move_executor_.entry_ = e;
}
INLINE void Node::rotate_yaw(float angle) {
    rotation_quaternion_ = XMQuaternionMultiply(
        rotation_quaternion_, XMQuaternionRotationRollPitchYaw(0, angle, 0));
}
INLINE void Node::rotate_pitch(float angle) {
    rotation_quaternion_ = XMQuaternionMultiply(
        rotation_quaternion_, XMQuaternionRotationRollPitchYaw(angle, 0, 0));
}
INLINE void Node::rotate_roll(float angle) {
    rotation_quaternion_ = XMQuaternionMultiply(
        rotation_quaternion_, XMQuaternionRotationRollPitchYaw(0, 0, angle));
}
INLINE void Node::rotate(const FXMVECTOR rotation) {
    rotation_quaternion_ = XMQuaternionMultiply(
        rotation_quaternion_, XMQuaternionRotationRollPitchYawFromVector(rotation));
}
INLINE const FXMVECTOR Node::get_rotation_quaternion() const {
    return rotation_quaternion_;
}
INLINE void Node::translate(const FXMVECTOR v) {
    position_ += v;
}
}  // namespace scene3
}  // namespace engine

namespace engine {
namespace scene3 {
INLINE Node::Iterator::Iterator(const Node* n)
    : root_(n) {
}
}  // namespace scene3
}  // namespace engine

namespace engine {
namespace scene3 {
INLINE Node::Child_nodes_list::Child_nodes_list(Node* parent)
    : parent_(parent) {
}
INLINE Node* Node::Child_nodes_list::get_parent() const {
    return parent_;
}
}  // namespace scene3
}  // namespace engine

namespace engine {
namespace scene3 {
INLINE Node::Child_nodes_list_element::Type&
Node::Child_nodes_list_element::get_embedded_list_element(Node* n) {
    return n->child_nodes_element_;
}
INLINE const Node::Child_nodes_list_element::Type&
Node::Child_nodes_list_element::get_embedded_list_element(const Node* n) {
    return n->child_nodes_element_;
}
}  // namespace scene3
}  // namespace engine

namespace engine {
namespace scene3 {
INLINE Node::Move_executor::Move_executor(Node::Move_executor&& from) {
    *this = std::move(from);
}
INLINE Node::Move_executor& Node::Move_executor::operator=(Node::Move_executor&& from) {
    entry_ = from.entry_;
    from.entry_ = nullptr;
    return *this;
}
}  // namespace scene3
}  // namespace engine
