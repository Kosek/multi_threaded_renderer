// Copyright 2017 Tomasz Kosek. All rights reserved.

namespace engine {
namespace scene3 {

INLINE Graph::Graph()
    : root_(ROOT) {
}

INLINE Node* Graph::get_root() {
    return &root_;
}

INLINE const Node* Graph::get_root() const {
    return &root_;
}
}  // namespace scene3
}  // namespace engine
