// Copyright 2017 Tomasz Kosek. All rights reserved.

#include <stdafx.h>

#include <engine\scene3\Entry.hpp>

#if defined _DEBUG
#include <engine\scene3\Entry.inl>
#endif

namespace engine {
namespace scene3 {
const char ENTRY_SPACE_NULL_NOT_ALLOWED[] = { "Entry must not be null!" };
const char ENTRY_SPACE_ELEMENT_ALREADY_IN_LIST[] = { "Entry already added to space!" };
const char ENTRY_SPACE_ELEMENT_NOT_IN_CURRENT_LIST[] = { "Entry is not added to current space!" };

const char ENTRY_TO_UPDATE_NULL_NOT_ALLOWED[] = { "Entry must not be null!" };
const char ENTRY_TO_UPDATE_ELEMENT_ALREADY_IN_LIST[] = { "Entry already added to update list!" };
const char ENTRY_TO_UPDATE_ELEMENT_NOT_IN_CURRENT_LIST[] = {
    "Entry is not added to current update list!" };
}  // namespace scene3
}  // namespace engine
