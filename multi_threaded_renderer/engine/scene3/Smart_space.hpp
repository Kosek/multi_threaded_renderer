// Copyright 2017 Tomasz Kosek. All rights reserved.

#ifndef MULTI_THREADED_RENDERER_ENGINE_SCENE3_SMART_SPACE_HPP_
#define MULTI_THREADED_RENDERER_ENGINE_SCENE3_SMART_SPACE_HPP_

#include <memory>

#include <engine\scene3\Space.hpp>

namespace engine {
namespace scene3 {
class Smart_space final : public Noncopyable {
 public:
    INLINE Smart_space() = default;
    INLINE Smart_space(const engine::scene3::Space::Name& name);
    INLINE ~Smart_space();
    INLINE Smart_space(Smart_space&&) = default;
    INLINE Smart_space& operator=(Smart_space&&) = default;
    INLINE const Space* get() const;
    INLINE Space* get();
    INLINE const Space* operator->() const;
    INLINE Space* operator->();

 private:
    Space space_;
};
}  // namespace scene3
}  // namespace engine

#if !defined _DEBUG
#include <engine\scene3\Smart_space.inl>
#endif

#endif  // MULTI_THREADED_RENDERER_ENGINE_SCENE3_SMART_SPACE_HPP_
