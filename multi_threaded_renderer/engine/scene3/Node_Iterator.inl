// Copyright 2017 Tomasz Kosek. All rights reserved.

namespace engine {
namespace scene3 {
template <typename VISITOR>
INLINE void Node::Iterator::visit_all_nodes(const VISITOR& visitor) const {
    root_->child_nodes_.get_iterator().visit_all([&visitor](Node* n) {
        visitor(n);
        n->get_iterator().visit_all_nodes(visitor);
    });
}

template <typename VISITOR>
INLINE void Node::Iterator::visit_child_nodes(const VISITOR& visitor) const {
    root_->child_nodes_.get_iterator().visit_all(visitor);
}
}  // namespace scene3
}  // namespace engine
