// Copyright 2017 Tomasz Kosek. All rights reserved.

#ifndef MULTI_THREADED_RENDERER_ENGINE_SCENE3_SCENE_HPP_
#define MULTI_THREADED_RENDERER_ENGINE_SCENE3_SCENE_HPP_

#include <memory>
#include <utility>

#include <engine\data_structures\Name_index.hpp>
#include <engine\game\Context_fwd.hpp>
#include <engine\naming\Unique_name.hpp>
#include <engine\scene3\Geometry_index.hpp>
#include <engine\scene3\Graph.hpp>
#include <engine\scene3\Space.hpp>
#include <engine\utilities\memory\Raw_ptr.hpp>

namespace engine {
namespace scene3 {

extern const char SCENE_CONTEXT_NULL_NOT_ALLOWED[];
extern const char SCENE_CONTEXT_ELEMENT_ALREADY_IN_LIST[];
extern const char SCENE_CONTEXT_ELEMENT_NOT_IN_CURRENT_LIST[];

extern const char SCENE_TO_UPDATE_CONTEXT_NULL_NOT_ALLOWED[];
extern const char SCENE_TO_UPDATE_CONTEXT_ELEMENT_ALREADY_IN_LIST[];
extern const char SCENE_TO_UPDATE_CONTEXT_ELEMENT_NOT_IN_CURRENT_LIST[];

class Scene final : public Noncopyable {
 public:
    class Name : public engine::naming::Unique_name {
     public:
        using engine::naming::Unique_name::Unique_name;
    };

    INLINE Scene(const Name& name);
    INLINE const Name& get_name() const;
    INLINE game::Context* get_context() const;
    INLINE void update(engine::game::Context* c);
    INLINE bool is_receive_update_enabled() const;

    class Mutator final {
        friend Scene;

     public:
        class Add_space final {
         public:
            class Under_space final {
             public:
                INLINE Under_space(Space* parent, Add_space& add);  // NOLINT
                INLINE ~Under_space();
                INLINE void under(Node* parent);

             private:
                engine::utilities::memory::Raw_ptr<Space> parent_;
                Add_space& add_space_;
            };

            INLINE Add_space(Space* s, const Mutator& b);
            INLINE ~Add_space();
            INLINE Under_space under(Space* parent);
            INLINE void under(Node* parent);

         private:
            engine::utilities::memory::Raw_ptr<Space> space_;
            const Mutator& mutator_;
        };

        class Add_entry final {
         public:
            class To_space final {
             public:
                INLINE To_space(Space* s, Add_entry& add_entry);  // NOLINT
                INLINE ~To_space();
                INLINE void under(Node* parent);

             private:
                engine::utilities::memory::Raw_ptr<Space> space_;
                Add_entry& add_entry_;
            };

            INLINE Add_entry(Entry* e, const Mutator& b);
            INLINE ~Add_entry();
            INLINE To_space to(Space* s);

         private:
            engine::utilities::memory::Raw_ptr<Entry> entry_;
            const Mutator& mutator_;
            ENGINE_DEBUG(bool was_to_space_called_ = false;)
        };

        class Remove_entry final {
         public:
            INLINE Remove_entry(Entry* e, const Mutator& b);
            INLINE ~Remove_entry();
            INLINE void from_position();

         private:
            engine::utilities::memory::Raw_ptr<Entry> entry_;
            const Mutator& mutator_;
        };

        class Move_entry final {
         public:
            INLINE Move_entry(Entry* e, const Mutator& b);
            INLINE void under(Node* parent);
            INLINE void to(Space* s);

         private:
            engine::utilities::memory::Raw_ptr<Entry> entry_;
            const Mutator& mutator_;
        };

        class Move_space final {
         public:
            class Under_space final {
             public:
                INLINE Under_space(Space* parent, Move_space& move_space);  // NOLINT
                INLINE ~Under_space();
                INLINE void under(Node* parent);

             private:
                engine::utilities::memory::Raw_ptr<Space> parent_;
                Move_space& move_space_;
            };

            class Under_scene final {
             public:
                INLINE Under_scene(Move_space& move_space);  // NOLINT
                INLINE ~Under_scene();
                INLINE void under(Node* parent);

             private:
                Move_space& move_space_;
            };

            INLINE Move_space(Space* s, const Mutator& b);
            INLINE ~Move_space();
            INLINE Under_space under(Space* s);
            INLINE Under_scene under_scene();

         private:
            engine::utilities::memory::Raw_ptr<Space> space_;
            const Mutator& mutator_;
            ENGINE_DEBUG(bool was_under_space_called_ = false;)
            ENGINE_DEBUG(bool was_under_scene_called_ = false;)
        };

        INLINE Add_space add(Space* s);
        INLINE void remove(Space* s);
        INLINE Space* remove(const Space::Name& name);
        INLINE Add_entry add(Entry* e);
        INLINE Entry* remove(const Entry::Name& name);
        INLINE Remove_entry remove(Entry* e);
        INLINE Move_entry move(Entry* e);
        INLINE Move_space move(Space* s);
        INLINE void enable_update(const engine::scene3::Space::Name& name);
        INLINE void disable_update(const engine::scene3::Space::Name& name);
        INLINE void enable_update(const engine::scene3::Entry::Name& name);
        INLINE void disable_update(const engine::scene3::Entry::Name& name);

     private:
        INLINE Mutator(Scene* s);
        engine::utilities::memory::Raw_ptr<Scene> scene_;
    };

    INLINE Mutator get_mutator();

    class Query final {
        friend Scene;

     public:
        INLINE Query(Scene* scene);
        INLINE Space* select(const Space::Name& name);
        INLINE Entry* select(const Entry::Name& name);
        INLINE Node* select(const Node::Name& name);
        INLINE Node* select_root();

        class Nodes_iterator final {
         public:
            INLINE Nodes_iterator(Scene* scene);
            template <typename VISITOR>
            INLINE void visit_all(const VISITOR& visitor) const;

         private:
            engine::utilities::memory::Raw_ptr<Scene> scene_;
        };

        INLINE Nodes_iterator select_nodes(/*const engine::scene3::view::Frustum& f*/);

     private:
        engine::utilities::memory::Raw_ptr<Scene> scene_;
    };

    INLINE Query create_query();

 private:
    class Scenes_in_context_list_element;
    class Scenes_to_update_in_context_list_element;
 public:
    class Scenes_in_context_list final : public engine::data_structures::Embedded_list<
        Scenes_in_context_list,
        Scene,
        Scenes_in_context_list_element,
        SCENE_CONTEXT_NULL_NOT_ALLOWED,
        SCENE_CONTEXT_ELEMENT_ALREADY_IN_LIST,
        SCENE_CONTEXT_ELEMENT_NOT_IN_CURRENT_LIST> {
     public:
        INLINE Scenes_in_context_list(game::Context* c);
        INLINE game::Context* get_context() const;

     private:
        engine::utilities::memory::Raw_ptr<game::Context> context_;
    };
    class Scenes_to_update_in_context_list final : public engine::data_structures::Embedded_list<
        Scenes_to_update_in_context_list,
        Scene,
        Scenes_to_update_in_context_list_element,
        SCENE_TO_UPDATE_CONTEXT_NULL_NOT_ALLOWED,
        SCENE_TO_UPDATE_CONTEXT_ELEMENT_ALREADY_IN_LIST,
        SCENE_TO_UPDATE_CONTEXT_ELEMENT_NOT_IN_CURRENT_LIST> {
    };

 private:
    class Positioning final : public Noncopyable {
     public:
        INLINE const Geometry_index* get_geometry_index() const;
        INLINE Geometry_index* get_geometry_index();
        INLINE const Graph* get_graph() const;
        INLINE Graph* get_graph();
        INLINE Node* add(Node* parent, std::unique_ptr<Node>&& n);
        INLINE void remove(Node* n);
        INLINE Node* remove(const Node::Name& name);
        INLINE const Node* get(const Node::Name& name) const;
        INLINE Node* get(const Node::Name& name);

     private:
        Geometry_index geometry_index_;
        Graph graph_;
    };

    class Content final : public Noncopyable {
     public:
        INLINE Content(Scene* s);
        INLINE void add(Space* s);
        INLINE void add(Space* parent, Space* child);
        INLINE void remove(Space* s);
        INLINE Space* remove(const Space::Name& name);
        INLINE Space* get(const Space::Name& name);
        INLINE const Space* get(const Space::Name& name) const;
        INLINE void add(Space* parent, Entry* e);
        INLINE void remove(Entry* e);
        INLINE Entry* remove(const Entry::Name& name);
        INLINE Entry* get(const Entry::Name& name);
        INLINE const Entry* get(const Entry::Name& name) const;

        class Iterator final {
            friend Content;

         public:
            template <class VISITOR>
            INLINE void visit_all_spaces(const VISITOR& visitor) const;

         private:
            INLINE Iterator(const Content* content);
            engine::utilities::memory::Raw_ptr<const Content> content_;
        };

        INLINE Iterator get_iterator() const;

     private:
        INLINE bool contains(const Space* s) const;

        Space::Spaces_in_scene_list spaces_;
        engine::data_structures::Name_index<Space> space_index_;
        engine::data_structures::Name_index<Entry> entry_index_;
    };

    class Scenes_in_context_list_element final : public engine::data_structures::Embedded_list<
        Scenes_in_context_list,
        Scene,
        Scenes_in_context_list_element,
        SCENE_CONTEXT_NULL_NOT_ALLOWED,
        SCENE_CONTEXT_ELEMENT_ALREADY_IN_LIST,
        SCENE_CONTEXT_ELEMENT_NOT_IN_CURRENT_LIST>::Element {
     public:
        using Type = engine::data_structures::Embedded_list<
            Scenes_in_context_list,
            Scene,
            Scenes_in_context_list_element,
            SCENE_CONTEXT_NULL_NOT_ALLOWED,
            SCENE_CONTEXT_ELEMENT_ALREADY_IN_LIST,
            SCENE_CONTEXT_ELEMENT_NOT_IN_CURRENT_LIST>::Element;

        INLINE static Type& get_embedded_list_element(Scene* s);
        INLINE static const Type& get_embedded_list_element(const Scene* s);
    };
    class Scenes_to_update_in_context_list_element final :
        public engine::data_structures::Embedded_list<
        Scenes_to_update_in_context_list,
        Scene,
        Scenes_to_update_in_context_list_element,
        SCENE_TO_UPDATE_CONTEXT_NULL_NOT_ALLOWED,
        SCENE_TO_UPDATE_CONTEXT_ELEMENT_ALREADY_IN_LIST,
        SCENE_TO_UPDATE_CONTEXT_ELEMENT_NOT_IN_CURRENT_LIST>::Element {
     public:
        using Type = engine::data_structures::Embedded_list<
            Scenes_to_update_in_context_list,
            Scene,
            Scenes_to_update_in_context_list_element,
            SCENE_TO_UPDATE_CONTEXT_NULL_NOT_ALLOWED,
            SCENE_TO_UPDATE_CONTEXT_ELEMENT_ALREADY_IN_LIST,
            SCENE_TO_UPDATE_CONTEXT_ELEMENT_NOT_IN_CURRENT_LIST>::Element;

        INLINE static Type& get_embedded_list_element(Scene* s);
        INLINE static const Type& get_embedded_list_element(const Scene* s);
    };

    class Updater final : public Noncopyable {
     public:
        INLINE void add(engine::scene3::Space* s);
        INLINE void remove(engine::scene3::Space* s);
        INLINE void update(engine::game::Context* c);

     private:
        Space::Spaces_to_update_in_scene_list spaces_;
    };

    INLINE const Positioning& get_positioning() const;
    INLINE Positioning& get_positioning();
    INLINE const Content& get_content() const;
    INLINE Content& get_content();
    INLINE const Updater& get_updater() const;
    INLINE Updater& get_updater();

    const Name name_;
    Positioning positioning_;
    Content content_;
    Scenes_in_context_list_element scenes_in_context_list_element_;
    Scenes_to_update_in_context_list_element scenes_to_update_in_context_list_element_;
    Updater updater_;
};
}  // namespace scene3
}  // namespace engine

#include <engine\scene3\Scene_header.inl>

#if !defined _DEBUG
#include <engine\scene3\Scene.inl>
#endif

#endif  // MULTI_THREADED_RENDERER_ENGINE_SCENE3_SCENE_HPP_
