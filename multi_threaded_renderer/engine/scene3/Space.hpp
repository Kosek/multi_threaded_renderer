// Copyright 2017 Tomasz Kosek. All rights reserved.

#ifndef MULTI_THREADED_RENDERER_ENGINE_SCENE3_SPACE_HPP_
#define MULTI_THREADED_RENDERER_ENGINE_SCENE3_SPACE_HPP_

#include <utility>

#include <engine\data_structures\Embedded_list.hpp>
#include <engine\frame\update\Receiver.hpp>
#include <engine\game\Context_fwd.hpp>
#include <engine\naming\Name.hpp>
#include <engine\scene3\Entry.hpp>
#include <engine\scene3\Scene_fwd.hpp>

namespace engine {
namespace scene3 {

extern const char SPACE_SCENE_NULL_NOT_ALLOWED[];
extern const char SPACE_SCENE_ELEMENT_ALREADY_IN_LIST[];
extern const char SPACE_SCENE_ELEMENT_NOT_IN_CURRENT_LIST[];

extern const char SPACE_SPACE_NULL_NOT_ALLOWED[];
extern const char SPACE_SPACE_ELEMENT_ALREADY_IN_LIST[];
extern const char SPACE_SPACE_ELEMENT_NOT_IN_CURRENT_LIST[];

extern const char SPACE_TO_UPDATE_SCENE_NULL_NOT_ALLOWED[];
extern const char SPACE_TO_UPDATE_SCENE_ELEMENT_ALREADY_IN_LIST[];
extern const char SPACE_TO_UPDATE_SCENE_ELEMENT_NOT_IN_CURRENT_LIST[];

extern const char SPACE_TO_UPDATE_SPACE_NULL_NOT_ALLOWED[];
extern const char SPACE_TO_UPDATE_SPACE_ELEMENT_ALREADY_IN_LIST[];
extern const char SPACE_TO_UPDATE_SPACE_ELEMENT_NOT_IN_CURRENT_LIST[];

class Space final : public Noncopyable {
 public:
    class Name : public engine::naming::Name {
     public:
        using engine::naming::Name::Name;
    };

    INLINE Space();
    INLINE Space(const Name& name);
    INLINE ~Space();
    INLINE Space(Space&&) = default;
    INLINE Space& operator=(Space&&) = default;
    INLINE const Name& get_name() const;
    INLINE void add(Space* child);
    INLINE void remove(Space* child);
    INLINE void add(Entry* entry);
    INLINE void remove(Entry* entry);
    INLINE bool is_in_scene() const;
    INLINE Space* get_parent() const;
    INLINE Scene* get_scene() const;
    INLINE void enable_update(Entry* e);
    INLINE void disable_update(Entry* e);
    INLINE void enable_update(Space* e);
    INLINE void disable_update(Space* e);
    INLINE void update(engine::game::Context* c);
    INLINE bool is_receive_update_enabled() const;

 private:
    class Spaces_in_scene_list_element;
    class Spaces_to_update_in_scene_list_element;
 public:
    class Spaces_in_scene_list final : public engine::data_structures::Embedded_list<
        Spaces_in_scene_list,
        Space,
        Spaces_in_scene_list_element,
        SPACE_SCENE_NULL_NOT_ALLOWED,
        SPACE_SCENE_ELEMENT_ALREADY_IN_LIST,
        SPACE_SCENE_ELEMENT_NOT_IN_CURRENT_LIST> {
     public:
        INLINE Spaces_in_scene_list(Scene* s);
        INLINE Scene* get_scene() const;

     private:
        engine::utilities::memory::Raw_ptr<Scene> scene_;
    };

    class Spaces_to_update_in_scene_list final : public engine::data_structures::Embedded_list<
        Spaces_to_update_in_scene_list,
        Space,
        Spaces_to_update_in_scene_list_element,
        SPACE_TO_UPDATE_SCENE_NULL_NOT_ALLOWED,
        SPACE_TO_UPDATE_SCENE_ELEMENT_ALREADY_IN_LIST,
        SPACE_TO_UPDATE_SCENE_ELEMENT_NOT_IN_CURRENT_LIST> {
    };

    class Iterator final {
        friend Space;

     public:
        template <typename VISITOR>
        INLINE void visit_all_entries(const VISITOR& visitor) const;
        template <typename VISITOR>
        INLINE void visit_all_spaces(const VISITOR& visitor) const;

     private:
        INLINE Iterator(const Space* s);
        engine::utilities::memory::Raw_ptr<const Space> space_;
    };

    Iterator get_iterator() const;

 private:
    class Spaces_in_scene_list_element final : public engine::data_structures::Embedded_list<
        Spaces_in_scene_list,
        Space,
        Spaces_in_scene_list_element,
        SPACE_SCENE_NULL_NOT_ALLOWED,
        SPACE_SCENE_ELEMENT_ALREADY_IN_LIST,
        SPACE_SCENE_ELEMENT_NOT_IN_CURRENT_LIST>::Element {
     public:
        using Type = engine::data_structures::Embedded_list<
            Spaces_in_scene_list,
            Space,
            Spaces_in_scene_list_element,
            SPACE_SCENE_NULL_NOT_ALLOWED,
            SPACE_SCENE_ELEMENT_ALREADY_IN_LIST,
            SPACE_SCENE_ELEMENT_NOT_IN_CURRENT_LIST>::Element;

        INLINE static Type& get_embedded_list_element(Space* s);
        INLINE static const Type& get_embedded_list_element(const Space* s);
    };

    class Child_spaces_list;
    class Child_spaces_list_element final : public engine::data_structures::Embedded_list<
        Child_spaces_list,
        Space,
        Child_spaces_list_element,
        SPACE_SPACE_NULL_NOT_ALLOWED,
        SPACE_SPACE_ELEMENT_ALREADY_IN_LIST,
        SPACE_SPACE_ELEMENT_NOT_IN_CURRENT_LIST>::Element {
     public:
        using Type = engine::data_structures::Embedded_list<
            Child_spaces_list,
            Space,
            Child_spaces_list_element,
            SPACE_SPACE_NULL_NOT_ALLOWED,
            SPACE_SPACE_ELEMENT_ALREADY_IN_LIST,
            SPACE_SPACE_ELEMENT_NOT_IN_CURRENT_LIST>::Element;

        INLINE static Type& get_embedded_list_element(Space* s);
        INLINE static const Type& get_embedded_list_element(const Space* s);
    };
    class Child_spaces_list final : public engine::data_structures::Embedded_list<
        Child_spaces_list,
        Space,
        Child_spaces_list_element,
        SPACE_SPACE_NULL_NOT_ALLOWED,
        SPACE_SPACE_ELEMENT_ALREADY_IN_LIST,
        SPACE_SPACE_ELEMENT_NOT_IN_CURRENT_LIST> {
     public:
        INLINE Child_spaces_list(Space* parent_space);
        INLINE Space* get_parent_space() const;

     private:
        engine::utilities::memory::Raw_ptr<Space> parent_space_;
    };

    class Spaces_to_update_in_scene_list_element final :
        public engine::data_structures::Embedded_list<
        Spaces_to_update_in_scene_list,
        Space,
        Spaces_to_update_in_scene_list_element,
        SPACE_TO_UPDATE_SCENE_NULL_NOT_ALLOWED,
        SPACE_TO_UPDATE_SCENE_ELEMENT_ALREADY_IN_LIST,
        SPACE_TO_UPDATE_SCENE_ELEMENT_NOT_IN_CURRENT_LIST>::Element {
     public:
        using Type = engine::data_structures::Embedded_list<
            Spaces_to_update_in_scene_list,
            Space,
            Spaces_to_update_in_scene_list_element,
            SPACE_TO_UPDATE_SCENE_NULL_NOT_ALLOWED,
            SPACE_TO_UPDATE_SCENE_ELEMENT_ALREADY_IN_LIST,
            SPACE_TO_UPDATE_SCENE_ELEMENT_NOT_IN_CURRENT_LIST>::Element;

        INLINE static Type& get_embedded_list_element(Space* s);
        INLINE static const Type& get_embedded_list_element(const Space* s);
    };

    class Spaces_to_update_in_space_list;
    class Spaces_to_update_in_space_list_element final :
        public engine::data_structures::Embedded_list<
        Spaces_to_update_in_space_list,
        Space,
        Spaces_to_update_in_space_list_element,
        SPACE_TO_UPDATE_SPACE_NULL_NOT_ALLOWED,
        SPACE_TO_UPDATE_SPACE_ELEMENT_ALREADY_IN_LIST,
        SPACE_TO_UPDATE_SPACE_ELEMENT_NOT_IN_CURRENT_LIST>::Element {
     public:
        using Type = engine::data_structures::Embedded_list<
            Spaces_to_update_in_space_list,
            Space,
            Spaces_to_update_in_space_list_element,
            SPACE_TO_UPDATE_SPACE_NULL_NOT_ALLOWED,
            SPACE_TO_UPDATE_SPACE_ELEMENT_ALREADY_IN_LIST,
            SPACE_TO_UPDATE_SPACE_ELEMENT_NOT_IN_CURRENT_LIST>::Element;

        INLINE static Type& get_embedded_list_element(Space* s);
        INLINE static const Type& get_embedded_list_element(const Space* s);
    };
    class Spaces_to_update_in_space_list final : public engine::data_structures::Embedded_list<
        Spaces_to_update_in_space_list,
        Space,
        Spaces_to_update_in_space_list_element,
        SPACE_TO_UPDATE_SPACE_NULL_NOT_ALLOWED,
        SPACE_TO_UPDATE_SPACE_ELEMENT_ALREADY_IN_LIST,
        SPACE_TO_UPDATE_SPACE_ELEMENT_NOT_IN_CURRENT_LIST> {
    };

    class Updater final : public Noncopyable {
     public:
        INLINE void add(Entry* e);
        INLINE void remove(Entry* e);
        INLINE void add(Space* s);
        INLINE void remove(Space* s);
        INLINE void update(engine::game::Context* c);

     private:
        engine::frame::update::Receiver::Receivers_to_update_list update_receivers_;
        Spaces_to_update_in_space_list spaces_;
    };

    const Name name_;
    Spaces_in_scene_list_element spaces_in_scene_list_element_;
    Child_spaces_list_element child_spaces_list_element_;
    Entry::Entries_in_space_list entries_;
    Child_spaces_list child_spaces_;
    Spaces_to_update_in_scene_list_element spaces_to_update_in_scene_list_element_;
    Spaces_to_update_in_space_list_element spaces_to_update_in_space_list_element_;
    Updater updater_;
    // Future development:
    // physics
    // ai
};
}  // namespace scene3
}  // namespace engine

#include <engine\scene3\Space_header.inl>

#if !defined _DEBUG
#include <engine\scene3\Space.inl>
#endif

#endif  // MULTI_THREADED_RENDERER_ENGINE_SCENE3_SPACE_HPP_
