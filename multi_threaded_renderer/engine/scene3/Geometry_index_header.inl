// Copyright 2017 Tomasz Kosek. All rights reserved.

namespace engine {
namespace scene3 {
template <typename VISITOR>
INLINE void Geometry_index::visit_all(/*const engine::scene3::view::Frustum& f,*/
    const VISITOR& visitor) const {
    for (const Node* n : nodes_) {
        visitor(n);
    }
}
}  // namespace scene3
}  // namespace engine
