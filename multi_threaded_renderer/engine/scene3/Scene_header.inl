// Copyright 2017 Tomasz Kosek. All rights reserved.

namespace engine {
namespace scene3 {
template <class VISITOR>
INLINE void Scene::Content::Iterator::visit_all_spaces(const VISITOR& visitor) const {
    content_->child_spaces_.get_iterator().visit_all(visitor);
}
}  // namespace scene3
}  // namespace engine
namespace engine {
namespace scene3 {
template <typename VISITOR>
INLINE void Scene::Query::Nodes_iterator::visit_all(const VISITOR& visitor) const {
    scene_->get_positioning().get_geometry_index()->visit_all(visitor);
}
}  // namespace scene3
}  // namespace engine
