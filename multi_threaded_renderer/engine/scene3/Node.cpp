// Copyright 2017 Tomasz Kosek. All rights reserved.

#include <stdafx.h>

#include <engine\scene3\Node.hpp>

#if defined _DEBUG
#include <engine\scene3\Node.inl>
#endif

namespace engine {
namespace scene3 {
const char NODE_NODE_NULL_NOT_ALLOWED[] = { "Node must not be null!" };
const char NODE_NODE_ELEMENT_ALREADY_IN_LIST[] = { "Node already added as child node!" };
const char NODE_NODE_ELEMENT_NOT_IN_CURRENT_LIST[] = { "Node is not child of current parent!" };
}  // namespace scene3
}  // namespace engine
