// Copyright 2017 Tomasz Kosek. All rights reserved.

#include <memory>
#include <utility>

#include <engine\data_structures\Embedded_list.hpp>
#include <engine\debug\Assert.hpp>
#include <engine\frame\update\Receiver.hpp>

namespace engine {
namespace scene3 {
INLINE Entry::Entry(const Name& name)
    : name_(name) {
}
INLINE Entry::~Entry() {
    ENGINE_ASSERT(!is_in_space(), "Entry is not allowed to be part of space during destruction!");
}
INLINE std::unique_ptr<Node> Entry::set(std::unique_ptr<Node>&& node) {
    std::unique_ptr<Node> ret(std::move(node_));
    node_ = std::move(node);
    if (node_.get()) {
        node_.get()->set_entry(this);
    }
    return ret;
}
INLINE const Node* Entry::get_node() const {
    return node_.get();
}
INLINE Node* Entry::get_node() {
    return node_.get();
}
INLINE const engine::frame::update::Receiver* Entry::get_update_receiver() const {
    return script_.get();
}
INLINE engine::frame::update::Receiver* Entry::get_update_receiver() {
    return script_.get();
}
INLINE const Entry::Name& Entry::get_name() const {
    return name_;
}
INLINE bool Entry::is_in_space() const {
    return entries_in_space_list_element_.is_in_list();
}
INLINE const Space* Entry::get_space() const {
    if (entries_in_space_list_element_.get_list()) {
        return entries_in_space_list_element_.get_list()->get_space();
    } else {
        return nullptr;
    }
}
INLINE Space* Entry::get_space() {
    if (entries_in_space_list_element_.get_list()) {
        return entries_in_space_list_element_.get_list()->get_space();
    } else {
        return nullptr;
    }
}
INLINE bool Entry::is_receive_update_enabled() const {
    return get_update_receiver() && get_update_receiver()->is_receive_update_enabled();
}
INLINE std::unique_ptr<engine::assets3::Model::Instance> Entry::set(
    std::unique_ptr<engine::assets3::Model::Instance>&& model_instance) {
    std::unique_ptr<engine::assets3::Model::Instance> ret = std::move(model_instance_);
    model_instance_ = std::move(model_instance);
    return ret;
}
INLINE engine::assets3::Model::Instance* Entry::get_model_instance() const {
    return model_instance_.get();
}
INLINE void Entry::set(const engine::assets::Asset::Ptr<engine::assets::Script>& script) {
    script_ = script;
}
INLINE engine::assets::Script* Entry::get_script() {
    return script_.get();
}
INLINE const engine::assets::Script* Entry::get_script() const {
    return script_.get();
}
}  // namespace scene3
}  // namespace engine

namespace engine {
namespace scene3 {
INLINE Entry::Entries_in_space_list::Entries_in_space_list(Space* s)
    : space_(s) {
    ENGINE_ASSERT(s, "Space must not be null!")
}
INLINE Space* Entry::Entries_in_space_list::get_space() const {
    return space_;
}
}  // namespace scene3
}  // namespace engine

namespace engine {
namespace scene3 {
INLINE Entry::Entries_in_space_list_element::Type&
Entry::Entries_in_space_list_element::get_embedded_list_element(Entry* e) {
    return e->entries_in_space_list_element_;
}
INLINE const Entry::Entries_in_space_list_element::Type&
Entry::Entries_in_space_list_element::get_embedded_list_element(const Entry* e) {
    return e->entries_in_space_list_element_;
}
}  // namespace scene3
}  // namespace engine
