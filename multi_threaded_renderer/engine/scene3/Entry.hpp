// Copyright 2017 Tomasz Kosek. All rights reserved.

#ifndef MULTI_THREADED_RENDERER_ENGINE_SCENE3_ENTRY_HPP_
#define MULTI_THREADED_RENDERER_ENGINE_SCENE3_ENTRY_HPP_

#include <memory>

#include <engine\assets\Script.hpp>
#include <engine\assets3\Model.hpp>
#include <engine\data_structures\Embedded_list.hpp>
#include <engine\frame\update\Receiver_fwd.hpp>
#include <engine\game\Context_fwd.hpp>
#include <engine\scene3\Node.hpp>
#include <engine\scene3\Space_fwd.hpp>

namespace engine {
namespace scene3 {

extern const char ENTRY_SPACE_NULL_NOT_ALLOWED[];
extern const char ENTRY_SPACE_ELEMENT_ALREADY_IN_LIST[];
extern const char ENTRY_SPACE_ELEMENT_NOT_IN_CURRENT_LIST[];

extern const char ENTRY_TO_UPDATE_NULL_NOT_ALLOWED[];
extern const char ENTRY_TO_UPDATE_ELEMENT_ALREADY_IN_LIST[];
extern const char ENTRY_TO_UPDATE_ELEMENT_NOT_IN_CURRENT_LIST[];

class Entry final : public Noncopyable {
 public:
    class Name : public engine::naming::Name {
     public:
        using engine::naming::Name::Name;
    };

    INLINE Entry() = default;
    INLINE Entry(const Name& name);
    INLINE ~Entry();
    INLINE Entry(Entry&&) = default;
    INLINE Entry& operator=(Entry&&) = default;
    INLINE std::unique_ptr<Node> set(std::unique_ptr<Node>&& node);
    INLINE const Node* get_node() const;
    INLINE Node* get_node();
    INLINE const engine::frame::update::Receiver* get_update_receiver() const;
    INLINE engine::frame::update::Receiver* get_update_receiver();
    INLINE const Name& get_name() const;
    INLINE bool is_in_space() const;
    INLINE const Space* get_space() const;
    INLINE Space* get_space();
    INLINE bool is_receive_update_enabled() const;
    INLINE std::unique_ptr<engine::assets3::Model::Instance> set(
        std::unique_ptr<engine::assets3::Model::Instance>&& model_instance);
    INLINE engine::assets3::Model::Instance* get_model_instance() const;
    INLINE void set(const engine::assets::Asset::Ptr<engine::assets::Script>& script);
    INLINE engine::assets::Script* get_script();
    INLINE const engine::assets::Script* get_script() const;

 private:
    class Entries_in_space_list_element;
    class Entries_to_update_in_space_list_element;
 public:
    class Entries_in_space_list final : public engine::data_structures::Embedded_list<
        Entries_in_space_list,
        Entry,
        Entries_in_space_list_element,
        ENTRY_SPACE_NULL_NOT_ALLOWED,
        ENTRY_SPACE_ELEMENT_ALREADY_IN_LIST,
        ENTRY_SPACE_ELEMENT_NOT_IN_CURRENT_LIST> {
     public:
        INLINE Entries_in_space_list(Space* s);
        INLINE Space* get_space() const;

     private:
        engine::utilities::memory::Raw_ptr<Space> space_;
    };

 private:
    class Entries_in_space_list_element final : public engine::data_structures::Embedded_list<
        Entries_in_space_list,
        Entry,
        Entries_in_space_list_element,
        ENTRY_SPACE_NULL_NOT_ALLOWED,
        ENTRY_SPACE_ELEMENT_ALREADY_IN_LIST,
        ENTRY_SPACE_ELEMENT_NOT_IN_CURRENT_LIST>::Element {
     public:
        using Type = engine::data_structures::Embedded_list<
            Entries_in_space_list,
            Entry,
            Entries_in_space_list_element,
            ENTRY_SPACE_NULL_NOT_ALLOWED,
            ENTRY_SPACE_ELEMENT_ALREADY_IN_LIST,
            ENTRY_SPACE_ELEMENT_NOT_IN_CURRENT_LIST>::Element;

        INLINE static Type& get_embedded_list_element(Entry* e);
        INLINE static const Type& get_embedded_list_element(const Entry* e);
    };
    Name name_;
    Entries_in_space_list_element entries_in_space_list_element_;
    std::unique_ptr<Node> node_;
    std::unique_ptr<engine::assets3::Model::Instance> model_instance_;
    engine::assets::Asset::Ptr<engine::assets::Script> script_;
};
}  // namespace scene3
}  // namespace engine

#if !defined _DEBUG
#include <engine\scene3\Entry.inl>
#endif

#endif  // MULTI_THREADED_RENDERER_ENGINE_SCENE3_ENTRY_HPP_
