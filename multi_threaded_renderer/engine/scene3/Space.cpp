// Copyright 2017 Tomasz Kosek. All rights reserved.

#include <stdafx.h>

#include <engine\scene3\Space.hpp>

#if defined _DEBUG
#include <engine\scene3\Space.inl>
#endif

namespace engine {
namespace scene3 {
const char SPACE_SCENE_NULL_NOT_ALLOWED[] = { "Space must not be null!" };
const char SPACE_SCENE_ELEMENT_ALREADY_IN_LIST[] = { "Space already added to scene!" };
const char SPACE_SCENE_ELEMENT_NOT_IN_CURRENT_LIST[] = { "Space is not added to current scene!" };

const char SPACE_SPACE_NULL_NOT_ALLOWED[] = { "Space must not be null!" };
const char SPACE_SPACE_ELEMENT_ALREADY_IN_LIST[] = { "Space already added as child space!" };
const char SPACE_SPACE_ELEMENT_NOT_IN_CURRENT_LIST[] = {
    "Space is not a child of current space!" };

const char SPACE_TO_UPDATE_SCENE_NULL_NOT_ALLOWED[] = { "Space must not be null!" };
const char SPACE_TO_UPDATE_SCENE_ELEMENT_ALREADY_IN_LIST[] = {
    "Space already added to scene update list!" };
const char SPACE_TO_UPDATE_SCENE_ELEMENT_NOT_IN_CURRENT_LIST[] = {
    "Space is not added to current scene update list!" };

const char SPACE_TO_UPDATE_SPACE_NULL_NOT_ALLOWED[] = { "Space must not be null!" };
const char SPACE_TO_UPDATE_SPACE_ELEMENT_ALREADY_IN_LIST[] = {
    "Space already added to space update list!" };
const char SPACE_TO_UPDATE_SPACE_ELEMENT_NOT_IN_CURRENT_LIST[] = {
    "Space is not added to current space update list!" };
}  // namespace scene3
}  // namespace engine
