// Copyright 2017 Tomasz Kosek. All rights reserved.

#ifndef MULTI_THREADED_RENDERER_ENGINE_SCENE3_GRAPH_HPP_
#define MULTI_THREADED_RENDERER_ENGINE_SCENE3_GRAPH_HPP_

#include <memory>

#include <engine\naming\Name.hpp>
#include <engine\scene3\Node.hpp>

namespace engine {
namespace scene3 {
class Graph final : public Noncopyable {
 public:
    INLINE Graph();
    INLINE Node* get_root();
    INLINE const Node* get_root() const;

 private:
    Node root_;
    // Future development:
    // memory pool for Nodes
};

ENGINE_DECLARE_NAME_OF_TYPE(Node::Name, ROOT);
}  // namespace scene3
}  // namespace engine

#if !defined _DEBUG
#include <engine\scene3\Graph.inl>
#endif

#endif  // MULTI_THREADED_RENDERER_ENGINE_SCENE3_GRAPH_HPP_
