// Copyright 2017 Tomasz Kosek. All rights reserved.

#include <utility>

#include <engine\game\Context.hpp>

namespace engine {
namespace scene3 {
INLINE Smart_scene::Smart_scene(const engine::scene3::Scene::Name& name) :
    scene_(name) {
}
INLINE Smart_scene::~Smart_scene() {
    if (scene_.get_context()) {
        scene_.get_context()->remove(get());
    }
}
INLINE const Scene* Smart_scene::get() const {
    return &scene_;
}
INLINE Scene* Smart_scene::get() {
    return &scene_;
}
INLINE const Scene* Smart_scene::operator->() const {
    return get();
}
INLINE Scene* Smart_scene::operator->() {
    return get();
}
}  // namespace scene3
}  // namespace engine
