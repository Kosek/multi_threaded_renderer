// Copyright 2017 Tomasz Kosek. All rights reserved.

#include <algorithm>
#include <utility>

#include <engine\data_structures\Embedded_list.hpp>
#include <engine\debug\Assert.hpp>

namespace engine {
namespace scene3 {

INLINE Space::Space()
    : Space(Name()) {
}
INLINE Space::Space(const Name& name)
    : name_(name), entries_(this), child_spaces_(this) {
}
INLINE Space::~Space() {
    ENGINE_ASSERT(!is_in_scene(), "Space is not allowed to be part of scene during destruction!")
        ENGINE_ASSERT(!get_parent(), "Space is not allowed to be child space during destruction!");
}
INLINE const Space::Name& Space::get_name() const {
    return name_;
}
INLINE void Space::add(Space* child) {
    ENGINE_ASSERT(!child->get_parent(), "Space already added as a child space!");
    child_spaces_.add(child);
}
INLINE void Space::remove(Space* child) {
    ENGINE_ASSERT(child, "Child space cannot be null!");
    ENGINE_ASSERT(child->get_parent() != nullptr, "Child space has no parent!");
    ENGINE_ASSERT(child->get_parent() == this, "Child space has different parent!");
    child_spaces_.remove(child);
    if (child->is_receive_update_enabled()) {
        updater_.remove(child);
    }
}
INLINE void Space::add(Entry* entry) {
    entries_.add(entry);
}
INLINE void Space::remove(Entry* entry) {
    entries_.remove(entry);
    if (entry->is_receive_update_enabled()) {
        updater_.remove(entry);
    }
}
INLINE bool Space::is_in_scene() const {
    return spaces_in_scene_list_element_.is_in_list();
}
INLINE Space* Space::get_parent() const {
    if (child_spaces_list_element_.is_in_list()) {
        return child_spaces_list_element_.get_list()->get_parent_space();
    } else {
        return nullptr;
    }
}
INLINE Scene* Space::get_scene() const {
    return spaces_in_scene_list_element_.get_list()->get_scene();
}
INLINE Space::Iterator Space::get_iterator() const {
    return Space::Iterator(this);
}
INLINE void Space::enable_update(Entry* e) {
    ENGINE_ASSERT(e->get_space() == this, "Entry is not added to current space!");
    updater_.add(e);
}
INLINE void Space::disable_update(Entry* e) {
    ENGINE_ASSERT(e->get_space() == this, "Entry is not added to current space!");
    updater_.remove(e);
}
INLINE void Space::enable_update(Space* s) {
    ENGINE_ASSERT(s->get_parent() == this, "Space is not added to current space!");
    updater_.add(s);
}
INLINE void Space::disable_update(Space* s) {
    ENGINE_ASSERT(s->get_parent() == this, "Space is not added to current space!");
    updater_.remove(s);
}
INLINE void Space::update(engine::game::Context* c) {
    updater_.update(c);
}
INLINE bool Space::is_receive_update_enabled() const {
    return spaces_to_update_in_scene_list_element_.is_in_list() ||
        spaces_to_update_in_space_list_element_.is_in_list();
}
}  // namespace scene3
}  // namespace engine

namespace engine {
namespace scene3 {
INLINE Space::Spaces_in_scene_list::Spaces_in_scene_list(Scene* s)
    : scene_(s) {
}
INLINE Scene* Space::Spaces_in_scene_list::get_scene() const {
    return scene_;
}
}  // namespace scene3
}  // namespace engine

namespace engine {
namespace scene3 {
INLINE Space::Spaces_in_scene_list_element::Type&
Space::Spaces_in_scene_list_element::get_embedded_list_element(Space* s) {
    return s->spaces_in_scene_list_element_;
}
INLINE const Space::Spaces_in_scene_list_element::Type&
Space::Spaces_in_scene_list_element::get_embedded_list_element(const Space* s) {
    return s->spaces_in_scene_list_element_;
}
}  // namespace scene3
}  // namespace engine

namespace engine {
namespace scene3 {
INLINE Space::Child_spaces_list_element::Type&
Space::Child_spaces_list_element::get_embedded_list_element(Space* s) {
    return s->child_spaces_list_element_;
}
INLINE const Space::Child_spaces_list_element::Type&
Space::Child_spaces_list_element::get_embedded_list_element(const Space* s) {
    return s->child_spaces_list_element_;
}

}  // namespace scene3
}  // namespace engine

namespace engine {
namespace scene3 {
INLINE Space::Child_spaces_list::Child_spaces_list(Space* parent_space)
    : parent_space_(parent_space) {
    ENGINE_ASSERT(parent_space_, "Parent space must not be null!");
}
INLINE Space* Space::Child_spaces_list::get_parent_space() const {
    return parent_space_;
}
}  // namespace scene3
}  // namespace engine

namespace engine {
namespace scene3 {
INLINE Space::Iterator::Iterator(const Space* s)
    : space_(s) {
}
}  // namespace scene3
}  // namespace engine

namespace engine {
namespace scene3 {
INLINE Space::Spaces_to_update_in_scene_list_element::Type&
Space::Spaces_to_update_in_scene_list_element::get_embedded_list_element(Space* s) {
    return s->spaces_to_update_in_scene_list_element_;
}
INLINE const Space::Spaces_to_update_in_scene_list_element::Type&
Space::Spaces_to_update_in_scene_list_element::get_embedded_list_element(const Space* s) {
    return s->spaces_to_update_in_scene_list_element_;
}
}  // namespace scene3
}  // namespace engine

namespace engine {
namespace scene3 {
INLINE Space::Spaces_to_update_in_space_list_element::Type&
Space::Spaces_to_update_in_space_list_element::get_embedded_list_element(Space* s) {
    return s->spaces_to_update_in_space_list_element_;
}
INLINE const Space::Spaces_to_update_in_space_list_element::Type&
Space::Spaces_to_update_in_space_list_element::get_embedded_list_element(const Space* s) {
    return s->spaces_to_update_in_space_list_element_;
}
}  // namespace scene3
}  // namespace engine

namespace engine {
namespace scene3 {
INLINE void Space::Updater::add(Entry* e) {
    update_receivers_.add(e->get_update_receiver());
}
INLINE void Space::Updater::remove(Entry* e) {
    update_receivers_.remove(e->get_update_receiver());
}
INLINE void Space::Updater::add(Space* s) {
    spaces_.add(s);
}
INLINE void Space::Updater::remove(Space* s) {
    spaces_.remove(s);
}
INLINE void Space::Updater::update(engine::game::Context* c) {
    update_receivers_.get_iterator().visit_all([&c](engine::frame::update::Receiver* r) {
        r->update(c);
    });
    spaces_.get_iterator().visit_all([&c](Space* s) {
        s->update(c);
    });
}
}  // namespace scene3
}  // namespace engine
