// Copyright 2017 Tomasz Kosek. All rights reserved.

#ifndef MULTI_THREADED_RENDERER_ENGINE_SCENE3_NODE_HPP_
#define MULTI_THREADED_RENDERER_ENGINE_SCENE3_NODE_HPP_

#include <memory>
#include <string>
#include <utility>

#include <engine\data_structures\Embedded_list.hpp>
#include <engine\naming\Name.hpp>
#include <engine\scene3\Entry_fwd.hpp>

namespace engine {
namespace scene3 {
extern const char NODE_NODE_NULL_NOT_ALLOWED[];
extern const char NODE_NODE_ELEMENT_ALREADY_IN_LIST[];
extern const char NODE_NODE_ELEMENT_NOT_IN_CURRENT_LIST[];

class Node final : public Noncopyable {
 public:
    class Name : public engine::naming::Name {
     public:
        using engine::naming::Name::Name;
    };

    INLINE Node();
    INLINE Node(const Name& name);
    INLINE ~Node();
    INLINE Node(Node&&) = default;
    INLINE Node& operator=(Node&&) = default;
    INLINE const Name& get_name() const;
    INLINE const FXMVECTOR get_position() const;
    INLINE void set_position(const XMFLOAT3& position);
    INLINE void set_position(const FXMVECTOR position);
    INLINE Node* get_parent() const;
    INLINE Node* get_root_node();
    INLINE const Node* get_root_node() const;
    INLINE Node* add(std::unique_ptr<Node>&& child);
    INLINE std::unique_ptr<Node> remove(Node* child);
    INLINE std::unique_ptr<Node> remove_from_parent();
    INLINE XMMATRIX get_world_matrix() const;
    INLINE XMVECTOR get_world_position() const;
    INLINE Entry* get_entry() const;
    INLINE void set_entry(Entry* e);
    INLINE void rotate_yaw(float angle);
    INLINE void rotate_pitch(float angle);
    INLINE void rotate_roll(float angle);
    INLINE void rotate(const FXMVECTOR rotation);
    INLINE const FXMVECTOR get_rotation_quaternion() const;
    INLINE void translate(const FXMVECTOR v);

    class Iterator final {
        friend Node;

     public:
        template <typename VISITOR>
        INLINE void visit_all_nodes(const VISITOR& visitor) const;
        template <typename VISITOR>
        INLINE void visit_child_nodes(const VISITOR& visitor) const;

     private:
        INLINE Iterator(const Node* n);

        engine::utilities::memory::Raw_ptr<const Node> root_;
    };

    INLINE Iterator get_iterator() const;

 private:
    class Child_nodes_list;

    class Child_nodes_list_element final : public engine::data_structures::Embedded_list<
        Child_nodes_list,
        Node,
        Child_nodes_list_element,
        NODE_NODE_NULL_NOT_ALLOWED,
        NODE_NODE_ELEMENT_ALREADY_IN_LIST,
        NODE_NODE_ELEMENT_NOT_IN_CURRENT_LIST>::Element {
     public:
        using Type = engine::data_structures::Embedded_list<
            Child_nodes_list,
            Node,
            Child_nodes_list_element,
            NODE_NODE_NULL_NOT_ALLOWED,
            NODE_NODE_ELEMENT_ALREADY_IN_LIST,
            NODE_NODE_ELEMENT_NOT_IN_CURRENT_LIST>::Element;

        INLINE static Type& get_embedded_list_element(Node* n);
        INLINE static const Type& get_embedded_list_element(const Node* n);
    };

    class Child_nodes_list final : public engine::data_structures::Embedded_list<
        Child_nodes_list,
        Node,
        Child_nodes_list_element,
        NODE_NODE_NULL_NOT_ALLOWED,
        NODE_NODE_ELEMENT_ALREADY_IN_LIST,
        NODE_NODE_ELEMENT_NOT_IN_CURRENT_LIST> {
     public:
        INLINE Child_nodes_list(Node* parent);
        INLINE Node* get_parent() const;

     private:
        engine::utilities::memory::Raw_ptr<Node> parent_;
    };

    class Move_executor final : public Noncopyable {
     public:
        INLINE Move_executor() = default;
        INLINE Move_executor(Move_executor&& from);
        INLINE Move_executor& operator=(Move_executor&& from);

        engine::utilities::memory::Raw_ptr<Entry> entry_;
    };

    const Name name_;
    Child_nodes_list child_nodes_;
    Child_nodes_list_element child_nodes_element_;
    XMVECTOR position_ = XMVectorSet(0, 0, 0, 0);
    XMVECTOR rotation_quaternion_ = XMQuaternionIdentity();
    // XMVECTOR scale_ = XMVectorSet(1, 1, 1, 1);
    Move_executor move_executor_;
};
}  // namespace scene3
}  // namespace engine

#include <engine\scene3\Node_Iterator.inl>

#if !defined _DEBUG
#include <engine\scene3\Node.inl>
#endif

#endif  // MULTI_THREADED_RENDERER_ENGINE_SCENE3_NODE_HPP_
