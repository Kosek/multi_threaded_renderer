// Copyright 2017 Tomasz Kosek. All rights reserved.

#include <utility>

#include <engine\scene3\Scene.hpp>

namespace engine {
namespace scene3 {
INLINE Smart_space::Smart_space(const engine::scene3::Space::Name& name) :
space_(name) {
}
INLINE Smart_space::~Smart_space() {
    if (space_.get_scene()) {
        space_.get_scene()->get_mutator().remove(get());
    } else if (space_.get_parent()) {
        space_.get_parent()->remove(get());
    }
}
INLINE Space* Smart_space::get() {
    return &space_;
}
INLINE const Space* Smart_space::get() const {
    return &space_;
}
INLINE const Space* Smart_space::operator->() const {
    return get();
}
INLINE Space* Smart_space::operator->() {
    return get();
}
}  // namespace scene3
}  // namespace engine
