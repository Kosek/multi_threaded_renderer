// Copyright 2017 Tomasz Kosek. All rights reserved.

#ifndef MULTI_THREADED_RENDERER_ENGINE_SCENE3_SMART_ENTRY_HPP_
#define MULTI_THREADED_RENDERER_ENGINE_SCENE3_SMART_ENTRY_HPP_

#include <engine\scene3\Entry.hpp>

namespace engine {
namespace scene3 {
class Smart_entry final : public Noncopyable {
 public:
    INLINE Smart_entry() = default;
    INLINE Smart_entry(const engine::scene3::Entry::Name& name);
    INLINE Smart_entry(Smart_entry&&) = default;
    INLINE Smart_entry& operator=(Smart_entry&&) = default;
    INLINE ~Smart_entry();
    INLINE Entry* get();
    INLINE const Entry* get() const;
    INLINE Entry* operator->();
    INLINE const Entry* operator->() const;

 private:
    Entry entry_;
};
}  // namespace scene3
}  // namespace engine

#if !defined _DEBUG
#include <engine\scene3\Smart_entry.inl>
#endif

#endif  // MULTI_THREADED_RENDERER_ENGINE_SCENE3_SMART_ENTRY_HPP_
