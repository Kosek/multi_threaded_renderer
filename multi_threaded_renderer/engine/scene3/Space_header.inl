// Copyright 2017 Tomasz Kosek. All rights reserved.

namespace engine {
namespace scene3 {
template <typename VISITOR>
INLINE void Space::Iterator::visit_all_entries(const VISITOR& visitor) const {
    auto entries = space_->entries_.get_iterator();
    entries.visit_all(visitor);

    auto spaces = space_->child_spaces_.get_iterator();
    spaces.visit_all([&visitor](const Space* space) {
        space->get_iterator().visit_all_entries(visitor);
    });
}

template <typename VISITOR>
INLINE void Space::Iterator::visit_all_spaces(const VISITOR& visitor) const {
    auto spaces = space_->child_spaces_.get_iterator();
    spaces.visit_all([&visitor](Space* space) {
        visitor(space);
        space->get_iterator().visit_all_spaces(visitor);
    });
}
}  // namespace scene3
}  // namespace engine
