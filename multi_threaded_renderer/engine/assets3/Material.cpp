// Copyright 2017 Tomasz Kosek. All rights reserved.

#include <stdafx.h>

#include <engine\assets3\Material.hpp>

#if defined _DEBUG
#include <engine\assets3\Material.inl>
#endif

namespace engine {
namespace assets3 {
Material::~Material() {
}
}  // namespace assets3
}  // namespace engine
