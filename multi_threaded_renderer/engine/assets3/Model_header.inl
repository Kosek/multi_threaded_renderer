// Copyright 2017 Tomasz Kosek. All rights reserved.

#include <memory>
#include <utility>
#include <vector>

namespace engine {
namespace assets3 {

template <typename STRIDE_SCHEMA>
INLINE Model::Mutator::Add_vertices Model::Mutator::add(
    const std::vector<typename STRIDE_SCHEMA::Stride>& vertices) {
    const unsigned int size_of_vertices =
        static_cast<unsigned int>(vertices.size() * sizeof(STRIDE_SCHEMA::Stride));
    std::vector<uint8_t> vertices_blob(size_of_vertices);
    std::memcpy(vertices_blob.data(), vertices.data(), size_of_vertices);
    return Add_vertices(std::move(vertices_blob), static_cast<unsigned int>(vertices.size()),
        STRIDE_SCHEMA::get_instance(), *this);
}

template <typename STRIDE_SCHEMA>
INLINE Model::Mutator::Set_material::As_name::To_vertices<STRIDE_SCHEMA>
Model::Mutator::Set_material::As_name::to(const Vertices_part* vp) {
    ENGINE_ASSERT(STRIDE_SCHEMA::is_compatible_with(
        this->set_material_.material_->get_parameters().get_material_type()),
        "Vertex stride schema must be compatible with material!");
    return To_vertices<STRIDE_SCHEMA>(vp, *this);
}

template <typename STRIDE_SCHEMA>
INLINE Model::Mutator::Set_material::As_name::To_vertices<STRIDE_SCHEMA>::To_vertices(
    const Vertices_part* vp, const As_name& an) : vertices_part_(vp), as_name_(an) {
    ENGINE_DEBUG(as_name_.set_material_.was_to_vertices_called_ = true;)
}

template <typename STRIDE_SCHEMA>
INLINE Model::Mutator::Set_material::As_name::To_vertices<STRIDE_SCHEMA>::~To_vertices() {
    std::unique_ptr<const Mesh_material_in_preparation> mmip =
        std::make_unique<Mesh_material_in_preparation>(as_name_.mesh_material_name_,
            vertices_part_, as_name_.set_material_.material_,
            as_name_.set_material_.material_->get_parameters());
    ENGINE_ASSERT(as_name_.set_material_.mutator_.vertex_buffers_in_preparation_.get(
        mmip->get_vertices_part_ref()->get_vertex_buffer_in_preparation_ref()->
        get_vertex_buffer_name()).get() ==
        mmip->get_vertices_part_ref()->get_vertex_buffer_in_preparation_ref(),
        "Vertices part are not included in currect builder!");
    as_name_.set_material_.mutator_.get_mesh_materials_in_preparation().add(std::move(mmip));
}

template <typename STRIDE_SCHEMA>
template <typename CONFIGURATOR>
INLINE void Model::Mutator::Set_material::As_name::To_vertices<STRIDE_SCHEMA>::and_configure_as(
    const CONFIGURATOR& configurator) {
    STRIDE_SCHEMA* s = static_cast<STRIDE_SCHEMA*>(vertices_part_->part_vertices.data());
    int vertices_count = vertices_part_->part_vertices.size();
    for (int i = 0; i < vertices_count; ++i, ++s) {
        const index = i;
        configurator(s, index);
    }
}
}  // namespace assets3
}  // namespace engine

namespace engine {
namespace assets3 {
template <typename MATERIAL_TYPE>
INLINE typename MATERIAL_TYPE::Configurator Model::Instance::configure(
    const Model::Mesh_material::Name& name) {
    return MATERIAL_TYPE::Configurator(get_parameters(name));
}
}  // namespace assets3
}  // namespace engine

namespace engine {
namespace assets3 {
template <class VISITOR>
INLINE void Model::Instance::Iterator::visit_all_mesh_materials(const VISITOR& visitor) const {
    instance_->mesh_material_parameters_.get_iterator().visit_all(visitor);
}
}  // namespace assets3
}  // namespace engine

namespace engine {
namespace assets3 {
template <typename VISITOR>
INLINE void
    Model::Instance::Mesh_material_parameters::Iterator::visit_all(const VISITOR& visitor) const {
    const Model::Mesh_materials::Mesh_materials_by_name& model_mesh_materials =
        instance_->get_model()->get_mesh_materials().get_mesh_materials_by_name();

    auto mmm = model_mesh_materials.cbegin();
    auto imm = mesh_material_parameters_->mesh_material_parameters_by_name_.cbegin();
    while (mmm != model_mesh_materials.cend() &&
        imm != mesh_material_parameters_->mesh_material_parameters_by_name_.cend()) {
        int cmp = std::get<0>(*imm).compare(mmm->get()->get_name());
        if (cmp == 0) {
            visitor(mmm->get(), std::get<1>(*imm).get());
            ++mmm;
            ++imm;
        } else if (cmp > 0) {
            visitor(mmm->get(), mmm->get()->get_parameters());
            ++mmm;
        } else {
            ENGINE_ASSERT(false, "Count of mesh materials in model instance should be equal to count of model mesh materials at most!");  // NOLINT
        }
    }
    while (mmm != model_mesh_materials.cend()) {
        visitor(mmm->get(), mmm->get()->get_parameters());
        ++mmm;
    }
}
}  // namespace assets3
}  // namespace engine
