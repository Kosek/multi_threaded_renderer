// Copyright 2017 Tomasz Kosek. All rights reserved.

#include <algorithm>
#include <memory>
#include <utility>
#include <vector>

namespace engine {
namespace assets3 {
INLINE Model::Model(const Name& name)
    : Asset(name) {
}
INLINE void Model::set(std::unique_ptr<const Mesh>&& mesh) {
    mesh_ = std::move(mesh);
}
INLINE const Mesh* Model::get_mesh() const {
    return mesh_.get();
}
INLINE void Model::add(std::unique_ptr<const Mesh_material>&& mesh_material) {
    mesh_materials_.add(std::move(mesh_material));
}
INLINE std::unique_ptr<Model::Instance> Model::create_instance() {
    return Model::Instance::create_instance(engine::assets::Asset::Ptr<Model>::make_from_pointer(
        this));
}
INLINE Model::Mutator Model::get_mutator(ID3D11Device* device) {
    return Mutator(device, engine::assets::Asset::Ptr<Model>::make_from_pointer(this));
}
INLINE const Model::Mesh_materials& Model::get_mesh_materials() const {
    return mesh_materials_;
}
}  // namespace assets3
}  // namespace engine

namespace engine {
namespace assets3 {
INLINE Model::Mesh_material::Mesh_material(const Name& name,
    const engine::assets3::Mesh::Chunk* mesh_chunk,
    const engine::assets::Asset::Ptr<Material>& material,
    const Material::Parameters& material_parameters,
    unsigned int vertices_start_index,
    unsigned int indices_start_index,
    unsigned int indices_count)
    : name_(name), mesh_chunk_(mesh_chunk), material_(material),
    material_parameters_(material_parameters), vertices_start_index_(vertices_start_index),
    indices_start_index_(indices_start_index), indices_count_(indices_count) {
    ENGINE_ASSERT(mesh_chunk_, "Sub mesh must not be null!");
    ENGINE_ASSERT(material_, "Material must not be null!");
}
INLINE const Model::Mesh_material::Name& Model::Mesh_material::get_name() const {
    return name_;
}
INLINE int Model::Mesh_material::Comparator::cmp(
    const Model::Mesh_material::Name& key,
    const std::unique_ptr<const Mesh_material>& mm) const {
    return key.compare(get_key(mm));
}
INLINE const Model::Mesh_material::Name& Model::Mesh_material::Comparator::get_key(
    const std::unique_ptr<const Mesh_material>& mm) const {
    return mm->get_name();
}
INLINE const engine::assets::Asset::Ptr<Material>& Model::Mesh_material::get_material() const {
    return material_;
}
INLINE unsigned int Model::Mesh_material::get_vertices_start_index() const {
    return vertices_start_index_;
}
INLINE unsigned int Model::Mesh_material::get_indices_start_index() const {
    return indices_start_index_;
}
INLINE unsigned int Model::Mesh_material::get_indices_count() const {
    return indices_count_;
}
INLINE const engine::assets3::Mesh::Chunk* Model::Mesh_material::get_mesh_chunk() const {
    return mesh_chunk_;
}
INLINE const Material::Parameters& Model::Mesh_material::get_parameters() const {
    return material_parameters_;
}
}  // namespace assets3
}  // namespace engine

namespace engine {
namespace assets3 {
INLINE Model::Mutator::Vertices_part::Vertices_part(const Model::Mutator::Vertices_part::
    Construction_key& key) {
}
INLINE void Model::Mutator::Vertices_part::setup(
    const Model::Mutator::Vertex_buffer_in_preparation* vbip_ref,
    unsigned int vertices_start_index,
    unsigned int vertices_count,
    std::vector<uint8_t>&& part_vertices,
    unsigned int indices_start_index,
    const std::vector<unsigned int>&& part_indices) {
    ENGINE_ASSERT(!vbip_ref_, "Vertices part is already setup!");
    vbip_ref_ = vbip_ref;
    part_vertices_blob_ = std::move(part_vertices);
    part_indices_ = std::move(part_indices);
    vertices_start_index_ = vertices_start_index;
    vertices_count_ = vertices_count;
    indices_start_index_ = indices_start_index;
    indices_count_ = static_cast<unsigned int>(part_indices_.size());
}
INLINE const std::vector<uint8_t>& Model::Mutator::Vertices_part::
get_part_vertices_blob() const {
    return part_vertices_blob_;
}
INLINE const std::vector<unsigned int>& Model::Mutator::Vertices_part::get_part_indices() const {
    return part_indices_;
}
INLINE const Model::Mutator::Vertex_buffer_in_preparation* Model::Mutator::Vertices_part::
get_vertex_buffer_in_preparation_ref() const {
    return vbip_ref_;
}
INLINE int Model::Mutator::Vertex_buffer_in_preparation::Comparator::cmp(
    const engine::vertex_stream::Vertex_buffer::Name& key,
    const std::unique_ptr<Vertex_buffer_in_preparation>& vbip) const {
    return key.compare(get_key(vbip));
}
INLINE const engine::vertex_stream::Vertex_buffer::Name& Model::Mutator::
Vertex_buffer_in_preparation::Comparator::get_key(
    const std::unique_ptr<Vertex_buffer_in_preparation>& vbip) const {
    return vbip->get_vertex_buffer_name();
}
}  // namespace assets3
}  // namespace engine

namespace engine {
namespace assets3 {
INLINE Model::Mutator::Mutator(ID3D11Device* device, const engine::assets::Asset::Ptr<Model>& m)
    : device_(device), model_(m) {
    ENGINE_ASSERT(model_, "Model must not be null!");
}
INLINE Model::Mutator::~Mutator() {
    commit();
}
INLINE const bool Model::Mutator::is_empty() const {
    return !model_ || vertex_buffers_in_preparation_.is_empty();
}
INLINE Model::Mutator::Mesh_materials_in_preparation&
Model::Mutator::get_mesh_materials_in_preparation() {
    return mesh_materials_in_preparation_;
}
INLINE Model::Mutator::Set_material Model::Mutator::set(
    const engine::assets::Asset::Ptr<Material>& m) {
    return Set_material(m, *this);
}
INLINE void Model::Mutator::commit() {
    if (!committed_ && !is_empty()) {
        std::unique_ptr<Mesh> mesh = std::make_unique<Mesh>(Mesh::Name(
            model_->get_name().get()));
        for (auto vbip_it = vertex_buffers_in_preparation_.cbegin();
            vbip_it != vertex_buffers_in_preparation_.cend(); ++vbip_it) {
            const std::unique_ptr<Vertex_buffer_in_preparation>& vbip = *vbip_it;
            mesh->add_chunk(build_mesh_chunk(vbip));
        }
        model_->set(std::move(mesh));
        for (auto mmip_it = mesh_materials_in_preparation_.cbegin();
            mmip_it != mesh_materials_in_preparation_.cend(); ++mmip_it) {
            const std::unique_ptr<const Mesh_material_in_preparation>& mmip = *mmip_it;
            model_->add(build_mesh_material(mmip));
        }
    }
    committed_ = true;
}
INLINE std::unique_ptr<const Mesh::Chunk> Model::Mutator::build_mesh_chunk(
    const std::unique_ptr<Vertex_buffer_in_preparation>& vbip) {
    std::vector<uint8_t> vb_blob;
    std::vector<unsigned int> indices;
    for (auto vp_it = vbip->get_vertices().cbegin();
        vp_it != vbip->get_vertices().cend(); ++vp_it) {
        vb_blob.insert(
            vb_blob.end(),
            vp_it->get()->get_part_vertices_blob().cbegin(),
            vp_it->get()->get_part_vertices_blob().cend());
        unsigned int prev_indices_count = static_cast<unsigned int>(indices.size());
        indices.insert(
            indices.end(),
            vp_it->get()->get_part_indices().cbegin(),
            vp_it->get()->get_part_indices().cend());
        std::transform(indices.begin() + prev_indices_count, indices.end(),
            indices.begin() + prev_indices_count, [&vp_it](unsigned int index) {
                return index + vp_it->get()->vertices_start_index_;
            });
    }
    std::unique_ptr<engine::vertex_stream::Vertex_buffer> vb =
        std::make_unique<engine::vertex_stream::Vertex_buffer>(
            vbip->get_vertex_buffer_name());
    vb->setup(device_, vbip->get_stride_schema_ref(), vb_blob);
    std::unique_ptr<engine::vertex_stream::Index_buffer> ib =
        std::make_unique<engine::vertex_stream::Index_buffer>(
            vbip->get_vertex_buffer_name().get());
    ib->setup(device_, indices);

    std::unique_ptr<Mesh::Chunk> sm = std::make_unique<Mesh::Chunk>(
        vbip->get_vertex_buffer_name().get());
    sm->setup(std::move(vb), std::move(ib));
    return std::move(sm);
}
INLINE std::unique_ptr<const Model::Mesh_material> Model::Mutator::build_mesh_material(
    const std::unique_ptr<const Mesh_material_in_preparation>& mmip) {
    engine::assets3::Mesh::Chunk::Name mesh_chunk_name(
        mmip->get_vertices_part_ref()->get_vertex_buffer_in_preparation_ref()
        ->get_vertex_buffer_name().get());
    ENGINE_ASSERT(model_->get_mesh(), "Mesh should be already set to model!");
    const Mesh::Chunk* mesh_chunk = model_->get_mesh()->get_chunk(mesh_chunk_name);
    ENGINE_ASSERT(mesh_chunk, "Given sub mesh should be present in model!");
    std::unique_ptr<Model::Mesh_material> mm = std::make_unique<Model::Mesh_material>(
        mmip->get_mesh_material_name(),
        mesh_chunk,
        mmip->get_material(),
        mmip->get_material_parameters(),
        mmip->get_vertices_part_ref()->vertices_start_index_,
        mmip->get_vertices_part_ref()->indices_start_index_,
        mmip->get_vertices_part_ref()->indices_count_);
    return std::move(mm);
}

INLINE Model::Mutator::Vertex_buffer_in_preparation::Vertex_buffer_in_preparation(
    const engine::vertex_stream::Vertex_buffer::Name& vb_name,
    const engine::vertex_stream::Stride_schema* stride_schema_ref)
    : vb_name_(vb_name), stride_schema_ref_(stride_schema_ref) {
}
INLINE void Model::Mutator::Vertex_buffer_in_preparation::add(
    std::unique_ptr<const Vertices_part>&& vertices_part) {
    vertices_count_ += vertices_part->vertices_count_;
    indices_count_ += static_cast<unsigned int>(vertices_part->part_indices_.size());
    vertices_.push_back(std::move(vertices_part));
}
INLINE const engine::vertex_stream::Vertex_buffer::Name&
Model::Mutator::Vertex_buffer_in_preparation::get_vertex_buffer_name() const {
    return vb_name_;
}
INLINE const engine::vertex_stream::Stride_schema*
Model::Mutator::Vertex_buffer_in_preparation::get_stride_schema_ref() const {
    return stride_schema_ref_;
}
INLINE const std::vector<std::unique_ptr<const Model::Mutator::Vertices_part>>&
Model::Mutator::Vertex_buffer_in_preparation::get_vertices() const {
    return vertices_;
}
INLINE unsigned int
Model::Mutator::Vertex_buffer_in_preparation::get_vertices_count() const {
    return vertices_count_;
}
INLINE unsigned int
Model::Mutator::Vertex_buffer_in_preparation::get_indices_count() const {
    return indices_count_;
}

INLINE Model::Mutator::Set_material::Set_material(
    const engine::assets::Asset::Ptr<Material>& m, Model::Mutator& b)
    : material_(m), mutator_(b) {
    ENGINE_ASSERT(material_, "Material must not be null!");
}
INLINE Model::Mutator::Set_material::~Set_material() {
    ENGINE_ASSERT(was_to_vertices_called_,
        "Material was not applied to vertices! It has no effect!");
}
INLINE Model::Mutator::Set_material::As_name Model::Mutator::Set_material::as(
    const Model::Mesh_material::Name& mesh_material_name) {
    return As_name(mesh_material_name, *this);
}

INLINE Model::Mutator::Set_material::As_name::As_name(
    const Model::Mesh_material::Name& mesh_material_name, Set_material& sm)
    : mesh_material_name_(mesh_material_name), set_material_(sm) {
}
INLINE Model::Mutator::Set_material::As_name::~As_name() {
    ENGINE_ASSERT(set_material_.was_to_vertices_called_,
        "Material was not applied to vertices! It has no effect!");
}

INLINE Model::Mutator::Add_vertices::Add_vertices(
    const std::vector<uint8_t>&& vertices_blob, const unsigned int vertices_count,
    const engine::vertex_stream::Stride_schema* stride_schema_ref,
    Mutator& b)
    : vertices_blob_(std::move(vertices_blob)), vertices_count_(vertices_count),
    stride_schema_ref_(stride_schema_ref), mutator_(b) {
}
INLINE Model::Mutator::Add_vertices::~Add_vertices() {
    ENGINE_ASSERT(were_vertices_and_indices_added_to_vertex_buffer_,
        "Vertices and indices were not added to vertex buffer! It has no effect!");
}
INLINE Model::Mutator::Add_vertices::With_indices Model::Mutator::Add_vertices::with
(const std::vector<unsigned int>&& indices) {
    return With_indices(std::move(indices), *this);
}

INLINE Model::Mutator::Add_vertices::With_indices::With_indices(
    const std::vector<unsigned int>&& indices,
    Add_vertices& add_vertices)
    : indices_(std::move(indices)), add_vertices_(add_vertices) {
}
INLINE Model::Mutator::Add_vertices::With_indices::~With_indices() {
    ENGINE_ASSERT(add_vertices_.were_vertices_and_indices_added_to_vertex_buffer_,
        "Vertices and indices were not added to vertex buffer! It has no effect!");
}
INLINE const Model::Mutator::Vertices_part* Model::Mutator::Add_vertices::With_indices::to(
    const engine::vertex_stream::Vertex_buffer::Name& vb_name) {
    ENGINE_DEBUG(add_vertices_.were_vertices_and_indices_added_to_vertex_buffer_ = true;)
    if (!add_vertices_.mutator_.vertex_buffers_in_preparation_.contains(vb_name)) {
        add_vertices_.mutator_.vertex_buffers_in_preparation_.add(
            std::make_unique<Vertex_buffer_in_preparation>(vb_name,
                add_vertices_.stride_schema_ref_));
    }
    std::unique_ptr<Vertex_buffer_in_preparation>& vbip =
        add_vertices_.mutator_.vertex_buffers_in_preparation_.get(vb_name);
    ENGINE_ASSERT(vbip->get_stride_schema_ref() == add_vertices_.stride_schema_ref_,
        "Given vertices have different vertex stride schema!");
    std::unique_ptr<Vertices_part> vertices_part = std::make_unique<Vertices_part>(
        Vertices_part::Construction_key());
    Vertices_part* ret = vertices_part.get();
    unsigned int vertices_start_index = vbip->get_vertices_count();
    vertices_part->setup(
        vbip.get(),
        vertices_start_index,
        add_vertices_.vertices_count_,
        std::move(add_vertices_.vertices_blob_),
        vbip->get_indices_count(),
        std::move(indices_));
    vbip->add(std::move(vertices_part));
    return ret;
}

INLINE Model::Mutator::Mesh_material_in_preparation::Mesh_material_in_preparation(
    const Model::Mesh_material::Name& mesh_material_name, const Vertices_part* vertices_part_ref,
    const engine::assets::Asset::Ptr<Material>& material,
    const Material::Parameters& material_parameters)
    : mesh_material_name_(mesh_material_name), vertices_part_ref_(vertices_part_ref),
    material_(material), material_parameters_(material_parameters) {
    ENGINE_ASSERT(vertices_part_ref_, "Vertices part must not be null!");
    ENGINE_ASSERT(material_, "Material must not be null!");
}
INLINE const Model::Mesh_material::Name&
Model::Mutator::Mesh_material_in_preparation::get_mesh_material_name() const {
    return mesh_material_name_;
}
INLINE const Model::Mutator::Vertices_part*
Model::Mutator::Mesh_material_in_preparation::get_vertices_part_ref() const {
    return vertices_part_ref_;
}
INLINE engine::assets::Asset::Ptr<Material>&
Model::Mutator::Mesh_material_in_preparation::get_material() {
    return material_;
}
INLINE const engine::assets::Asset::Ptr<Material>&
Model::Mutator::Mesh_material_in_preparation::get_material() const {
    return material_;
}
INLINE const Material::Parameters&
Model::Mutator::Mesh_material_in_preparation::get_material_parameters() const {
    return material_parameters_;
}
INLINE int Model::Mutator::Mesh_material_in_preparation::Comparator::cmp(
    const Model::Mesh_material::Name& key,
    const std::unique_ptr<const Mesh_material_in_preparation>& mmip) const {
    return key.compare(get_key(mmip));
}
INLINE const Model::Mesh_material::Name&
Model::Mutator::Mesh_material_in_preparation::Comparator::get_key(
    const std::unique_ptr<const Mesh_material_in_preparation>& mmip) const {
    return mmip->get_mesh_material_name();
}
}  // namespace assets3
}  // namespace engine

namespace engine {
namespace assets3 {
INLINE Model::Instance::Instance(const Construction_key key,
    const engine::assets::Asset::Ptr<Model>& model)
    : model_(model), mesh_material_parameters_(this) {
}
INLINE Material::Standalone_parameters& Model::Instance::get_parameters(
    const Model::Mesh_material::Name& name) {
    ENGINE_ASSERT(model_->get_mesh_materials().get(name),
        "Mesh material of given name does not exist!");
    if (!mesh_material_parameters_.contains(name)) {
        mesh_material_parameters_.add(*model_->mesh_materials_.get(name));
    }
    return mesh_material_parameters_.get(name);
}
INLINE std::unique_ptr<Model::Instance> Model::Instance::create_instance(
    const engine::assets::Asset::Ptr<Model>& model) {
    return std::make_unique<Model::Instance>(Construction_key(), model);
}
INLINE Model::Instance::Iterator Model::Instance::get_iterator() const {
    return Iterator(this);
}
INLINE const Model* Model::Instance::get_model() const {
    return model_.get();
}
}  // namespace assets3
}  // namespace engine

namespace engine {
namespace assets3 {
INLINE Model::Instance::Iterator::Iterator(const Instance* instance) : instance_(instance) {
    ENGINE_ASSERT(instance_, "Model instance must not be NULL!");
}
}  // namespace assets3
}  // namespace engine

namespace engine {
namespace assets3 {
INLINE void Model::Mesh_materials::add(
    std::unique_ptr<const Mesh_material>&& mesh_material) {
    const Mesh_material* mm = mesh_material.get();
    mesh_materials_by_name_.add(std::move(mesh_material));
}
INLINE const Model::Mesh_material* Model::Mesh_materials::get(
    const Model::Mesh_material::Name& name) const {
    return mesh_materials_by_name_.get(name).get();
}
INLINE const Model::Mesh_materials::Mesh_materials_by_name&
Model::Mesh_materials::get_mesh_materials_by_name() const {
    return mesh_materials_by_name_;
}
}  // namespace assets3
}  // namespace engine

namespace engine {
namespace assets3 {
INLINE Model::Instance::Mesh_material_parameters::Mesh_material_parameters(
    const Instance* instance) : instance_(instance) {
    ENGINE_ASSERT(instance_, "Instance must not be null!");
}
INLINE void Model::Instance::Mesh_material_parameters::add(
    const Model::Mesh_material& mm) {
    mesh_material_parameters_by_name_.add(std::make_pair(mm.get_name(),
        Material::Standalone_parameters(mm.get_material()->get_parameters())));
}
INLINE const Material::Standalone_parameters& Model::Instance::Mesh_material_parameters::get(
    const Model::Mesh_material::Name& name) const {
    return mesh_material_parameters_by_name_.get(name).second;
}
INLINE Material::Standalone_parameters& Model::Instance::Mesh_material_parameters::get(
    const Model::Mesh_material::Name& name) {
    return mesh_material_parameters_by_name_.get(name).second;
}
INLINE bool Model::Instance::Mesh_material_parameters::contains(
    const Model::Mesh_material::Name& name) const {
    return mesh_material_parameters_by_name_.contains(name);
}
INLINE Model::Instance::Mesh_material_parameters::Iterator
Model::Instance::Mesh_material_parameters::get_iterator() const {
    return Iterator(instance_, this);
}
}  // namespace assets3
}  // namespace engine

namespace engine {
namespace assets3 {
INLINE Model::Instance::Mesh_material_parameters::Iterator::Iterator(const Instance* instance,
    const Mesh_material_parameters* mesh_material_parameters) : instance_(instance),
    mesh_material_parameters_(mesh_material_parameters) {
    ENGINE_ASSERT(instance_, "Instance must not be null!");
    ENGINE_ASSERT(mesh_material_parameters_, "Mesh material parameters must not be null!");
}
}  // namespace assets3
}  // namespace engine

namespace engine {
namespace assets3 {
INLINE int Model::Instance::Mesh_material_parameters::
Material_parameters_comparator::cmp(
    const Model::Mesh_material::Name& key,
    const std::pair<Model::Mesh_material::Name, Material::Standalone_parameters>& params) const {
    return key.compare(get_key(params));
}
INLINE const Model::Mesh_material::Name&
Model::Instance::Mesh_material_parameters::Material_parameters_comparator::get_key(
    const std::pair<Model::Mesh_material::Name, Material::Standalone_parameters>& params) const {
    return params.first;
}
}  // namespace assets3
}  // namespace engine
