// Copyright 2017 Tomasz Kosek. All rights reserved.

#ifndef MULTI_THREADED_RENDERER_ENGINE_ASSETS3_MODEL_HPP_
#define MULTI_THREADED_RENDERER_ENGINE_ASSETS3_MODEL_HPP_

#include <memory>
#include <utility>
#include <vector>

#include <engine\assets\Asset.hpp>
#include <engine\assets3\Material.hpp>
#include <engine\assets3\Mesh.hpp>
#include <engine\utilities\memory\Raw_ptr.hpp>

namespace engine {
namespace assets3 {
class Model final : public assets::Asset {
 public:
    class Name final : public Asset::Name {
        using Asset::Name::Name;
    };

    class Mesh_material final : public Noncopyable {
     public:
        class Name final : public Asset::Name {
            using Asset::Name::Name;
        };

        INLINE Mesh_material(const Name& name,
            const Mesh::Chunk* mesh_chunk,
            const engine::assets::Asset::Ptr<Material>& material,
            const Material::Parameters& material_parameters,
            unsigned int vertices_start_index,
            unsigned int indices_start_index,
            unsigned int indices_count);
        INLINE const Name& get_name() const;
        INLINE const engine::assets::Asset::Ptr<Material>& get_material() const;
        INLINE unsigned int get_vertices_start_index() const;
        INLINE unsigned int get_indices_start_index() const;
        INLINE unsigned int get_indices_count() const;
        INLINE const Mesh::Chunk* get_mesh_chunk() const;
        INLINE const Material::Parameters& get_parameters() const;

        class Comparator final : public Noncopyable {
         public:
            INLINE int cmp(
                const Model::Mesh_material::Name& key,
                const std::unique_ptr<const Mesh_material>& mm) const;
            INLINE const Model::Mesh_material::Name& get_key(
                const std::unique_ptr<const Mesh_material>& mm) const;
        };

     private:
        const Name name_;
        engine::utilities::memory::Raw_ptr<const Mesh::Chunk> mesh_chunk_;
        engine::assets::Asset::Ptr<Material> material_;
        Material::Parameters material_parameters_;
        unsigned int vertices_start_index_ = 0;
        unsigned int indices_start_index_ = 0;
        unsigned int indices_count_ = 0;
    };

    class Mutator final {
     private:
        class Vertex_buffer_in_preparation;
     public:
        class Vertices_part final : public Noncopyable {
         private:
            class Construction_key final : public Noncopyable {};
         public:
            friend Mutator;
            friend Vertex_buffer_in_preparation;

            explicit Vertices_part(const Construction_key& key);

         private:
            INLINE void setup(
                const Vertex_buffer_in_preparation* vbip_ref,
                unsigned int vertices_start_index,
                unsigned int vertices_count,
                std::vector<uint8_t>&& part_vertices,
                unsigned int indices_start_index,
                const std::vector<unsigned int>&& part_indices);

            INLINE const std::vector<uint8_t>& get_part_vertices_blob() const;
            INLINE const std::vector<unsigned int>& get_part_indices() const;
            INLINE const Vertex_buffer_in_preparation*
                get_vertex_buffer_in_preparation_ref() const;

            engine::utilities::memory::Raw_ptr<const Vertex_buffer_in_preparation> vbip_ref_;
            std::vector<uint8_t> part_vertices_blob_;
            std::vector<unsigned int> part_indices_;
            unsigned int vertices_start_index_ = 0;
            unsigned int vertices_count_ = 0;
            unsigned int indices_start_index_ = 0;
            unsigned int indices_count_ = 0;
        };

        class Set_material final {
         public:
            class As_name final {
             public:
                template <typename STRIDE_SCHEMA>
                class To_vertices final {
                 public:
                    INLINE To_vertices(const Vertices_part* vp, const As_name& an);
                    INLINE ~To_vertices();
                    template <typename CONFIGURATOR>
                    INLINE void and_configure_as(const CONFIGURATOR& configurator);

                 private:
                    engine::utilities::memory::Raw_ptr<const Vertices_part> vertices_part_;
                    const As_name& as_name_;
                };

                INLINE As_name(const Model::Mesh_material::Name& material_name,
                    Set_material& sm);  // NOLINT
                INLINE ~As_name();
                template <typename STRIDE_SCHEMA>
                INLINE To_vertices<STRIDE_SCHEMA> to(const Vertices_part* vp);

             private:
                const Model::Mesh_material::Name mesh_material_name_;
                Set_material& set_material_;
            };

            INLINE Set_material(const engine::assets::Asset::Ptr<Material>& m,
                Mutator& b);  // NOLINT
            INLINE ~Set_material();
            INLINE As_name as(const Model::Mesh_material::Name& name);

         private:
            engine::assets::Asset::Ptr<Material> material_;
            Mutator& mutator_;
            ENGINE_DEBUG(bool was_to_vertices_called_ = false;)
        };

        class Add_vertices final {
         public:
            class With_indices final {
             public:
                INLINE With_indices(const std::vector<unsigned int>&& indices,
                    Add_vertices& add_vertices);  // NOLINT
                INLINE ~With_indices();
                INLINE const Vertices_part* to(
                    const engine::vertex_stream::Vertex_buffer::Name& vb_name);

             private:
                const std::vector<unsigned int> indices_;
                Add_vertices& add_vertices_;
            };

            INLINE Add_vertices(
                const std::vector<uint8_t>&& vertices_blob,
                const unsigned int vertices_count,
                const engine::vertex_stream::Stride_schema* stride_schema_ref,
                Mutator& b);  // NOLINT
            INLINE ~Add_vertices();
            INLINE With_indices with(const std::vector<unsigned int>&& indices);

         private:
            std::vector<uint8_t> vertices_blob_;
            const unsigned int vertices_count_ = 0;
            engine::utilities::memory::Raw_ptr<const engine::vertex_stream::Stride_schema>
                stride_schema_ref_;
            Mutator& mutator_;
            ENGINE_DEBUG(bool were_vertices_and_indices_added_to_vertex_buffer_ = false;)
        };

        INLINE Mutator(ID3D11Device* device, const engine::assets::Asset::Ptr<Model>& m);
        INLINE ~Mutator();
        INLINE Mutator(Mutator&&) = default;
        INLINE Mutator& operator=(Mutator&&) = default;
        template <typename STRIDE_SCHEMA>
        INLINE Add_vertices add(const std::vector<typename STRIDE_SCHEMA::Stride>& vertices);
        INLINE Set_material set(const engine::assets::Asset::Ptr<Material>& m);
        INLINE void commit();

     private:
        class Vertex_buffer_in_preparation final : public Noncopyable {
         public:
            class Comparator final : public Noncopyable {
             public:
                INLINE int cmp(
                    const engine::vertex_stream::Vertex_buffer::Name& key,
                    const std::unique_ptr<Vertex_buffer_in_preparation>& vbip) const;
                INLINE const engine::vertex_stream::Vertex_buffer::Name& get_key(
                    const std::unique_ptr<Vertex_buffer_in_preparation>& vbip) const;
            };
            INLINE Vertex_buffer_in_preparation(
                const engine::vertex_stream::Vertex_buffer::Name& vb_name,
                const engine::vertex_stream::Stride_schema* stride_schema_ref);
            INLINE void add(std::unique_ptr<const Vertices_part>&& vertices_part);
            INLINE const engine::vertex_stream::Vertex_buffer::Name&
                get_vertex_buffer_name() const;
            INLINE const engine::vertex_stream::Stride_schema*
                get_stride_schema_ref() const;
            INLINE const std::vector<std::unique_ptr<const Vertices_part>>&
                get_vertices() const;
            INLINE unsigned int get_vertices_count() const;
            INLINE unsigned int get_indices_count() const;

         private:
            const engine::vertex_stream::Vertex_buffer::Name vb_name_;
            engine::utilities::memory::Raw_ptr<const engine::vertex_stream::Stride_schema>
                stride_schema_ref_;
            std::vector<std::unique_ptr<const Vertices_part>> vertices_;
            unsigned int vertices_count_ = 0;
            unsigned int indices_count_ = 0;
        };
        class Mesh_material_in_preparation final : public Noncopyable {
         public:
            class Comparator final {
             public:
                INLINE int cmp(
                    const Model::Mesh_material::Name& key,
                    const std::unique_ptr<const Mesh_material_in_preparation>& mmip) const;
                INLINE const Model::Mesh_material::Name& get_key(
                    const std::unique_ptr<const Mesh_material_in_preparation>& mmip) const;
            };

            INLINE Mesh_material_in_preparation(
                const Model::Mesh_material::Name& mesh_material_name,
                const Vertices_part* vertices_part_ref,
                const engine::assets::Asset::Ptr<Material>& material,
                const Material::Parameters& material_parameters);
            INLINE const Model::Mesh_material::Name& get_mesh_material_name() const;
            INLINE const Vertices_part* get_vertices_part_ref() const;
            INLINE engine::assets::Asset::Ptr<Material>& get_material();
            INLINE const engine::assets::Asset::Ptr<Material>& get_material() const;
            INLINE const Material::Parameters& get_material_parameters() const;

         private:
            const Model::Mesh_material::Name mesh_material_name_;
            engine::utilities::memory::Raw_ptr<const Vertices_part> vertices_part_ref_;
            engine::assets::Asset::Ptr<Material> material_;
            const Material::Parameters material_parameters_;
        };

        using Mesh_materials_in_preparation = engine::data_structures::Sorted_vector<
            const Model::Mesh_material::Name,
            std::unique_ptr<const Mesh_material_in_preparation>,
            Mesh_material_in_preparation::Comparator>;

        INLINE std::unique_ptr<const Mesh::Chunk> build_mesh_chunk(
            const std::unique_ptr<Vertex_buffer_in_preparation>& vbip);
        INLINE std::unique_ptr<const Model::Mesh_material> build_mesh_material(
            const std::unique_ptr<const Mesh_material_in_preparation>& mmip);
        INLINE const bool is_empty() const;
        INLINE Mesh_materials_in_preparation& get_mesh_materials_in_preparation();

        engine::utilities::memory::Raw_ptr<ID3D11Device> device_;
        engine::assets::Asset::Ptr<Model> model_;
        engine::data_structures::Sorted_vector<
            const engine::vertex_stream::Vertex_buffer::Name,
            std::unique_ptr<Vertex_buffer_in_preparation>,
            Vertex_buffer_in_preparation::Comparator> vertex_buffers_in_preparation_;
        Mesh_materials_in_preparation mesh_materials_in_preparation_;
        bool committed_ = false;
    };

    class Instance final : public Noncopyable {
        friend Model;

     private:
        class Construction_key final : public Noncopyable {};
     public:
        class Iterator final {
            friend Instance;

         public:
            template <class VISITOR>
            INLINE void visit_all_mesh_materials(const VISITOR& visitor) const;

         private:
            INLINE Iterator(const Instance* instance);
            engine::utilities::memory::Raw_ptr<const Instance> instance_;
        };

        INLINE Instance(const Construction_key key,
            const engine::assets::Asset::Ptr<Model>& model);
        template <typename MATERIAL_TYPE>
        INLINE typename MATERIAL_TYPE::Configurator configure(
            const Model::Mesh_material::Name& name);
        INLINE Iterator get_iterator() const;
        INLINE const Model* get_model() const;

     private:
        INLINE static std::unique_ptr<Instance> create_instance(
            const engine::assets::Asset::Ptr<Model>& model);

        INLINE Material::Standalone_parameters& get_parameters(
            const Model::Mesh_material::Name& name);

        class Mesh_material_parameters final : public Noncopyable {
         public:
            INLINE Mesh_material_parameters(const Instance* instance);
            INLINE void add(const Model::Mesh_material& mm);
            INLINE const Material::Standalone_parameters& get(
                const Model::Mesh_material::Name& name) const;
            INLINE Material::Standalone_parameters& get(const Model::Mesh_material::Name& name);
            INLINE bool contains(const Model::Mesh_material::Name& name) const;

            class Iterator final {
                friend Mesh_material_parameters;

             public:
                INLINE Iterator(const Instance *instance,
                    const Mesh_material_parameters* mesh_material_parameters);
                template <typename VISITOR>
                INLINE void visit_all(const VISITOR& visitor) const;

             private:
                engine::utilities::memory::Raw_ptr<const Instance> instance_;
                engine::utilities::memory::Raw_ptr<const Mesh_material_parameters>
                    mesh_material_parameters_;
            };

            INLINE Iterator get_iterator() const;

         private:
            class Material_parameters_comparator final : public Noncopyable {
             public:
                INLINE int cmp(const Model::Mesh_material::Name& key,
                    const std::pair<Model::Mesh_material::Name,
                    Material::Standalone_parameters>& params) const;
                INLINE const Model::Mesh_material::Name& get_key(
                    const std::pair<Model::Mesh_material::Name, Material::Standalone_parameters>&
                    params) const;
            };

            engine::utilities::memory::Raw_ptr<const Instance> instance_;
            engine::data_structures::Sorted_vector<
                const Model::Mesh_material::Name,
                std::pair<Model::Mesh_material::Name, Material::Standalone_parameters>,
                Material_parameters_comparator>
                mesh_material_parameters_by_name_;
        };

        const engine::assets::Asset::Ptr<Model> model_;
        Mesh_material_parameters mesh_material_parameters_;
    };

    INLINE Model(const Name& name);
    INLINE std::unique_ptr<Instance> create_instance();
    INLINE Mutator get_mutator(ID3D11Device* device);

 private:
    class Mesh_materials final : public Noncopyable {
     public:
        INLINE void add(std::unique_ptr<const Mesh_material>&& mesh_material);
        INLINE const Mesh_material* get(const Model::Mesh_material::Name& name) const;
        using Mesh_materials_by_name = engine::data_structures::Sorted_vector<
            const Model::Mesh_material::Name,
            std::unique_ptr<const Model::Mesh_material>,
            Model::Mesh_material::Comparator>;
        INLINE const Mesh_materials_by_name& get_mesh_materials_by_name() const;

     private:
        engine::data_structures::Sorted_vector<
            const Model::Mesh_material::Name,
            std::unique_ptr<const Model::Mesh_material>,
            Model::Mesh_material::Comparator>
            mesh_materials_by_name_;
    };

    INLINE void set(std::unique_ptr<const Mesh>&& mesh);
    INLINE const Mesh* get_mesh() const;
    INLINE void add(std::unique_ptr<const Mesh_material>&& mesh_material);
    INLINE const Mesh_materials& get_mesh_materials() const;

    std::unique_ptr<const Mesh> mesh_;
    Mesh_materials mesh_materials_;
};
}  // namespace assets3
}  // namespace engine

#include <engine\assets3\Model_header.inl>

#if !defined _DEBUG
#include <engine\assets3\Model.inl>
#endif

#endif  // MULTI_THREADED_RENDERER_ENGINE_ASSETS3_MODEL_HPP_
