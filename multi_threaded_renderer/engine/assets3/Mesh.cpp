// Copyright 2017 Tomasz Kosek. All rights reserved.

#include <stdafx.h>

#include <engine\assets3\Mesh.hpp>

#if defined _DEBUG
#include <engine\assets3\Mesh.inl>
#endif
