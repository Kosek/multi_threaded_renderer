// Copyright 2017 Tomasz Kosek. All rights reserved.

#include <d3dx11.h>
#include <string>

#include <engine\debug\Assert.hpp>

namespace engine {
namespace assets3 {
INLINE Texture::Texture(const Name& name)
    : Asset(name) {
}
INLINE Texture::~Texture() {
    if (is_loaded()) {
        unload();
    }
}
INLINE void Texture::load(ID3D11Device* device) {
    ENGINE_ASSERT(device, "Device must not be null!");
    ENGINE_ASSERT(!is_loaded(), "Texture is already loaded!");

    std::wstring texture_file_path =
        std::wstring(get_name().get().begin(), get_name().get().end());
    HRESULT res = D3DX11CreateShaderResourceViewFromFile(
        device, texture_file_path.c_str(), NULL, NULL, &shader_resource_view_, NULL);
    ENGINE_ASSERT(!FAILED(res), "Failed to load texture from file!");
}
INLINE void Texture::unload() {
    ENGINE_ASSERT(is_loaded(), "Texture must be loaded to perform unload!");

    if (shader_resource_view_) {
        shader_resource_view_->Release();
    }
    shader_resource_view_ = nullptr;
}
INLINE bool Texture::is_loaded() const {
    return shader_resource_view_ != nullptr;
}
INLINE ID3D11ShaderResourceView* Texture::get() const {
    return shader_resource_view_;
}
}  // namespace assets3
}  // namespace engine
