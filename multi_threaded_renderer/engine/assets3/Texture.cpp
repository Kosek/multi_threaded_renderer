// Copyright 2017 Tomasz Kosek. All rights reserved.

#include <stdafx.h>

#include <engine\assets3\Texture.hpp>

#if defined _DEBUG
#include <engine\assets3\Texture.inl>
#endif
