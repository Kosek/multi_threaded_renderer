// Copyright 2017 Tomasz Kosek. All rights reserved.

#ifndef MULTI_THREADED_RENDERER_ENGINE_ASSETS3_MESH_HPP_
#define MULTI_THREADED_RENDERER_ENGINE_ASSETS3_MESH_HPP_

#include <memory>
#include <utility>
#include <vector>

#include <engine\assets\Asset.hpp>
#include <engine\assets3\Material.hpp>
#include <engine\data_structures\Sorted_vector.hpp>
#include <engine\utilities\memory\Raw_ptr.hpp>
#include <engine\vertex_stream\Index_buffer.hpp>
#include <engine\vertex_stream\Vertex_buffer.hpp>

namespace engine {
namespace assets3 {
class Mesh final : public assets::Asset {
 public:
    class Name final : public Asset::Name {
        using Asset::Name::Name;
    };

    class Chunk final : public Noncopyable {
     public:
        class Name final : public engine::naming::Unique_name {
            using naming::Unique_name::Unique_name;
        };
        INLINE Chunk(const Name& name);
        INLINE const engine::vertex_stream::Vertex_buffer* get_vertex_buffer() const;
        INLINE const engine::vertex_stream::Index_buffer* get_index_buffer() const;
        INLINE const Name& get_name() const;
        INLINE void setup(
            std::unique_ptr<engine::vertex_stream::Vertex_buffer>&& vb,
            std::unique_ptr<engine::vertex_stream::Index_buffer>&& ib);

        class Comparator final : public Noncopyable {
         public:
            INLINE int cmp(const Name& key,
                const std::unique_ptr<const Chunk>& chunk) const;
            INLINE const Name& get_key(
                const std::unique_ptr<const Chunk>& chunk) const;
        };

     private:
        const Name name_;
        std::unique_ptr<const engine::vertex_stream::Vertex_buffer> vb_;
        std::unique_ptr<const engine::vertex_stream::Index_buffer> ib_;
    };

    INLINE Mesh(const Name& name);
    INLINE void add_chunk(std::unique_ptr<const Chunk>&& chunk);
    INLINE std::unique_ptr<const Chunk> remove_chunk(const Chunk::Name& chunk);
    INLINE void remove_chunk(std::unique_ptr<const Chunk>&& chunk);
    INLINE const Mesh::Chunk* get_chunk(const Chunk::Name& chunk_name) const;
    INLINE int get_chunks_count() const;

    class Iterator final {
     public:
        INLINE Iterator(const Mesh* mesh);
        template <typename VISITOR>
        INLINE void visit_all_chunks(const VISITOR& visitor) const;

     private:
        engine::utilities::memory::Raw_ptr<const Mesh> mesh_;
    };

    INLINE Iterator get_iterator() const;

 private:
    engine::data_structures::Sorted_vector<const Chunk::Name, std::unique_ptr<const Chunk>,
        Chunk::Comparator> chunks_;
};
}  // namespace assets3
}  // namespace engine

#include <engine\assets3\Mesh_header.inl>

#if !defined _DEBUG
#include <engine\assets3\Mesh.inl>
#endif

#endif  // MULTI_THREADED_RENDERER_ENGINE_ASSETS3_MESH_HPP_
