// Copyright 2017 Tomasz Kosek. All rights reserved.

#include <memory>
#include <utility>

#include <engine\debug\Assert.hpp>

namespace engine {
namespace assets3 {
INLINE Mesh::Mesh(const Mesh::Name& name)
    : Asset(name) {
}
INLINE void Mesh::add_chunk(std::unique_ptr<const Chunk>&& chunk) {
    chunks_.add(std::move(chunk));
}
INLINE std::unique_ptr<const Mesh::Chunk> Mesh::remove_chunk(
    const Chunk::Name& chunk_name) {
    return std::move(chunks_.remove(chunk_name));
}
INLINE void Mesh::remove_chunk(std::unique_ptr<const Chunk>&& chunk) {
    chunks_.remove(chunk->get_name());
}
INLINE const Mesh::Chunk* Mesh::get_chunk(const Chunk::Name& chunk_name) const {
    return chunks_.get(chunk_name).get();
}
INLINE int Mesh::get_chunks_count() const {
    return chunks_.get_count();
}
}  // namespace assets3
}  // namespace engine

namespace engine {
namespace assets3 {
INLINE Mesh::Chunk::Chunk(const Mesh::Chunk::Name& name)
    : name_(name) {
}
INLINE const engine::vertex_stream::Vertex_buffer* Mesh::Chunk::get_vertex_buffer() const {
    return vb_.get();
}
INLINE const engine::vertex_stream::Index_buffer* Mesh::Chunk::get_index_buffer() const {
    return ib_.get();
}
INLINE const Mesh::Chunk::Name& Mesh::Chunk::get_name() const {
    return name_;
}
INLINE Mesh::Iterator Mesh::get_iterator() const {
    return Mesh::Iterator(this);
}
INLINE void Mesh::Chunk::setup(
    std::unique_ptr<engine::vertex_stream::Vertex_buffer>&& vb,
    std::unique_ptr<engine::vertex_stream::Index_buffer>&& ib) {
    ENGINE_ASSERT(vb.get(), "Vertex buffer must not be null!");
    ENGINE_ASSERT(ib.get(), "Index buffer must not be null!");
    vb_ = std::move(vb);
    ib_ = std::move(ib);
}
}  // namespace assets3
}  // namespace engine

namespace engine {
namespace assets3 {
INLINE int Mesh::Chunk::Comparator::cmp(const Name& key,
    const std::unique_ptr<const Chunk>& chunk) const {
    return key.compare(get_key(chunk));
}
INLINE const Mesh::Chunk::Name& Mesh::Chunk::Comparator::get_key(
    const std::unique_ptr<const Chunk>& chunk) const {
    return chunk->get_name();
}
}  // namespace assets3
}  // namespace engine

namespace engine {
namespace assets3 {
INLINE Mesh::Iterator::Iterator(const Mesh* mesh)
    : mesh_(mesh) {
}
}  // namespace assets3
}  // namespace engine
