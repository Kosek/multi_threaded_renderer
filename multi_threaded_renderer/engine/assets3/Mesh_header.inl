// Copyright 2017 Tomasz Kosek. All rights reserved.

namespace engine {
namespace assets3 {
template <typename VISITOR>
INLINE void Mesh::Iterator::visit_all_chunks(const VISITOR& visitor) const {
    for (auto it = chunks_.cbegin(); it != chunks_.cend(); ++it) {
        visitor(it->get());
    }
}
}  // namespace assets3
}  // namespace engine
