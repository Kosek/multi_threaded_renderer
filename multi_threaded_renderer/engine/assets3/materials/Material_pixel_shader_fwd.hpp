// Copyright 2017 Tomasz Kosek. All rights reserved.

#ifndef MULTI_THREADED_RENDERER_ENGINE_ASSETS3_MATERIALS_MATERIAL_PIXEL_SHADER_FWD_HPP_
#define MULTI_THREADED_RENDERER_ENGINE_ASSETS3_MATERIALS_MATERIAL_PIXEL_SHADER_FWD_HPP_

namespace engine {
namespace assets3 {
namespace materials {

class Material_pixel_shader;

}  // namespace materials
}  // namespace assets3
}  // namespace engine


#endif  // MULTI_THREADED_RENDERER_ENGINE_ASSETS3_MATERIALS_MATERIAL_PIXEL_SHADER_FWD_HPP_
