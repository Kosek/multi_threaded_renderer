// Copyright 2017 Tomasz Kosek. All rights reserved.

#include <stdafx.h>

#include <engine\assets3\materials\Material_pixel_shader.hpp>

#if defined _DEBUG
#include <engine\assets3\materials\Material_pixel_shader.inl>
#endif
