// Copyright 2017 Tomasz Kosek. All rights reserved.

#ifndef MULTI_THREADED_RENDERER_ENGINE_ASSETS3_MATERIALS_MATERIAL_PIXEL_SHADER_HPP_
#define MULTI_THREADED_RENDERER_ENGINE_ASSETS3_MATERIALS_MATERIAL_PIXEL_SHADER_HPP_

#include <d3d11.h>

#include <engine\naming\Unique_name.hpp>
#include <engine\utilities\memory\Raw_ptr.hpp>

namespace engine {
namespace assets3 {
namespace materials {
class Material_pixel_shader final : public Noncopyable {
 public:
    class Id : public engine::naming::Unique_name {
        using engine::naming::Unique_name::Unique_name;
    };

    INLINE Material_pixel_shader(const Id& id);
    INLINE ~Material_pixel_shader();
    INLINE void load(ID3D11Device* device);
    INLINE void unload();
    INLINE bool is_loaded() const;
    INLINE const Id& get_name() const;
    INLINE ID3D11PixelShader* get_pixel_shader() const;

 private:
    const Id name_;
    engine::utilities::memory::Raw_ptr<ID3D10Blob> pixel_shader_blob_;
    engine::utilities::memory::Raw_ptr<ID3D11PixelShader> pixel_shader_;
};
}  // namespace materials
}  // namespace assets3
}  // namespace engine

#if !defined _DEBUG
#include <engine\assets3\materials\Material_pixel_shader.inl>
#endif

#endif  // MULTI_THREADED_RENDERER_ENGINE_ASSETS3_MATERIALS_MATERIAL_PIXEL_SHADER_HPP_
