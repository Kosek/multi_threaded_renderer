// Copyright 2017 Tomasz Kosek. All rights reserved.

#include <engine/assets3/materials/textured/Pixel_shader_input.hlsl>
#include <engine/hlsl/Instances_defs.hlsl>

Texture2D<float4> textures_0[HLSL_MAX_INSTANCES_COUNT] : register(t0);

SamplerState linear_sample {
    Filter = MIN_MAG_MIP_LINEAR;
    AddressU = Wrap;
    AddressV = Wrap;
};

float4 main(Pixel_shader_input input) : SV_TARGET {
    [forcecase] switch (input.instance_id) {
        case 0:
            return textures_0[0].Sample(linear_sample, input.tex_coord);
        case 1:
            return textures_0[1].Sample(linear_sample, input.tex_coord);
        case 2:
            return textures_0[2].Sample(linear_sample, input.tex_coord);
        case 3:
            return textures_0[3].Sample(linear_sample, input.tex_coord);
        case 4:
            return textures_0[4].Sample(linear_sample, input.tex_coord);
        case 5:
            return textures_0[5].Sample(linear_sample, input.tex_coord);
        case 6:
            return textures_0[6].Sample(linear_sample, input.tex_coord);
        case 7:
            return textures_0[7].Sample(linear_sample, input.tex_coord);
        case 8:
            return textures_0[8].Sample(linear_sample, input.tex_coord);
        case 9:
            return textures_0[9].Sample(linear_sample, input.tex_coord);
        case 10:
            return textures_0[10].Sample(linear_sample, input.tex_coord);
        case 11:
            return textures_0[11].Sample(linear_sample, input.tex_coord);
        case 12:
            return textures_0[12].Sample(linear_sample, input.tex_coord);
        case 13:
            return textures_0[13].Sample(linear_sample, input.tex_coord);
        case 14:
            return textures_0[14].Sample(linear_sample, input.tex_coord);
        case 15:
            return textures_0[15].Sample(linear_sample, input.tex_coord);
        case 16:
            return textures_0[16].Sample(linear_sample, input.tex_coord);
        case 17:
            return textures_0[17].Sample(linear_sample, input.tex_coord);
        case 18:
            return textures_0[18].Sample(linear_sample, input.tex_coord);
        case 19:
            return textures_0[19].Sample(linear_sample, input.tex_coord);
        case 20:
            return textures_0[20].Sample(linear_sample, input.tex_coord);
        case 21:
            return textures_0[21].Sample(linear_sample, input.tex_coord);
        case 22:
            return textures_0[22].Sample(linear_sample, input.tex_coord);
        case 23:
            return textures_0[23].Sample(linear_sample, input.tex_coord);
        case 24:
            return textures_0[24].Sample(linear_sample, input.tex_coord);
        case 25:
            return textures_0[25].Sample(linear_sample, input.tex_coord);
        case 26:
            return textures_0[26].Sample(linear_sample, input.tex_coord);
        case 27:
            return textures_0[27].Sample(linear_sample, input.tex_coord);
        case 28:
            return textures_0[28].Sample(linear_sample, input.tex_coord);
        case 29:
            return textures_0[29].Sample(linear_sample, input.tex_coord);
        case 30:
            return textures_0[30].Sample(linear_sample, input.tex_coord);
        case 31:
            return textures_0[31].Sample(linear_sample, input.tex_coord);
        case 32:
            return textures_0[32].Sample(linear_sample, input.tex_coord);
        case 33:
            return textures_0[33].Sample(linear_sample, input.tex_coord);
        case 34:
            return textures_0[34].Sample(linear_sample, input.tex_coord);
        case 35:
            return textures_0[35].Sample(linear_sample, input.tex_coord);
        case 36:
            return textures_0[36].Sample(linear_sample, input.tex_coord);
        case 37:
            return textures_0[37].Sample(linear_sample, input.tex_coord);
        case 38:
            return textures_0[38].Sample(linear_sample, input.tex_coord);
        case 39:
            return textures_0[39].Sample(linear_sample, input.tex_coord);
        case 40:
            return textures_0[40].Sample(linear_sample, input.tex_coord);
        case 41:
            return textures_0[41].Sample(linear_sample, input.tex_coord);
        case 42:
            return textures_0[42].Sample(linear_sample, input.tex_coord);
        case 43:
            return textures_0[43].Sample(linear_sample, input.tex_coord);
        case 44:
            return textures_0[44].Sample(linear_sample, input.tex_coord);
        case 45:
            return textures_0[45].Sample(linear_sample, input.tex_coord);
        case 46:
            return textures_0[46].Sample(linear_sample, input.tex_coord);
        case 47:
            return textures_0[47].Sample(linear_sample, input.tex_coord);
        case 48:
            return textures_0[48].Sample(linear_sample, input.tex_coord);
        case 49:
            return textures_0[49].Sample(linear_sample, input.tex_coord);
        case 50:
            return textures_0[50].Sample(linear_sample, input.tex_coord);
        case 51:
            return textures_0[51].Sample(linear_sample, input.tex_coord);
        case 52:
            return textures_0[52].Sample(linear_sample, input.tex_coord);
        case 53:
            return textures_0[53].Sample(linear_sample, input.tex_coord);
        case 54:
            return textures_0[54].Sample(linear_sample, input.tex_coord);
        case 55:
            return textures_0[55].Sample(linear_sample, input.tex_coord);
        case 56:
            return textures_0[56].Sample(linear_sample, input.tex_coord);
        case 57:
            return textures_0[57].Sample(linear_sample, input.tex_coord);
        case 58:
            return textures_0[58].Sample(linear_sample, input.tex_coord);
        case 59:
            return textures_0[59].Sample(linear_sample, input.tex_coord);
        case 60:
            return textures_0[60].Sample(linear_sample, input.tex_coord);
        case 61:
            return textures_0[61].Sample(linear_sample, input.tex_coord);
        case 62:
            return textures_0[62].Sample(linear_sample, input.tex_coord);
        case 63:
            return textures_0[63].Sample(linear_sample, input.tex_coord);
        case 64:
            return textures_0[64].Sample(linear_sample, input.tex_coord);
        case 65:
            return textures_0[65].Sample(linear_sample, input.tex_coord);
        case 66:
            return textures_0[66].Sample(linear_sample, input.tex_coord);
        case 67:
            return textures_0[67].Sample(linear_sample, input.tex_coord);
        case 68:
            return textures_0[68].Sample(linear_sample, input.tex_coord);
        case 69:
            return textures_0[69].Sample(linear_sample, input.tex_coord);
        case 70:
            return textures_0[70].Sample(linear_sample, input.tex_coord);
        case 71:
            return textures_0[71].Sample(linear_sample, input.tex_coord);
        case 72:
            return textures_0[72].Sample(linear_sample, input.tex_coord);
        case 73:
            return textures_0[73].Sample(linear_sample, input.tex_coord);
        case 74:
            return textures_0[74].Sample(linear_sample, input.tex_coord);
        case 75:
            return textures_0[75].Sample(linear_sample, input.tex_coord);
        case 76:
            return textures_0[76].Sample(linear_sample, input.tex_coord);
        case 77:
            return textures_0[77].Sample(linear_sample, input.tex_coord);
        case 78:
            return textures_0[78].Sample(linear_sample, input.tex_coord);
        case 79:
            return textures_0[79].Sample(linear_sample, input.tex_coord);
        case 80:
            return textures_0[80].Sample(linear_sample, input.tex_coord);
        case 81:
            return textures_0[81].Sample(linear_sample, input.tex_coord);
        case 82:
            return textures_0[82].Sample(linear_sample, input.tex_coord);
        case 83:
            return textures_0[83].Sample(linear_sample, input.tex_coord);
        case 84:
            return textures_0[84].Sample(linear_sample, input.tex_coord);
        case 85:
            return textures_0[85].Sample(linear_sample, input.tex_coord);
        case 86:
            return textures_0[86].Sample(linear_sample, input.tex_coord);
        case 87:
            return textures_0[87].Sample(linear_sample, input.tex_coord);
        case 88:
            return textures_0[88].Sample(linear_sample, input.tex_coord);
        case 89:
            return textures_0[89].Sample(linear_sample, input.tex_coord);
        case 90:
            return textures_0[90].Sample(linear_sample, input.tex_coord);
        case 91:
            return textures_0[91].Sample(linear_sample, input.tex_coord);
        case 92:
            return textures_0[92].Sample(linear_sample, input.tex_coord);
        case 93:
            return textures_0[93].Sample(linear_sample, input.tex_coord);
        case 94:
            return textures_0[94].Sample(linear_sample, input.tex_coord);
        case 95:
            return textures_0[95].Sample(linear_sample, input.tex_coord);
        case 96:
            return textures_0[96].Sample(linear_sample, input.tex_coord);
        case 97:
            return textures_0[97].Sample(linear_sample, input.tex_coord);
        case 98:
            return textures_0[98].Sample(linear_sample, input.tex_coord);
        case 99:
            return textures_0[99].Sample(linear_sample, input.tex_coord);
        case 100:
            return textures_0[100].Sample(linear_sample, input.tex_coord);
        case 101:
            return textures_0[101].Sample(linear_sample, input.tex_coord);
        case 102:
            return textures_0[102].Sample(linear_sample, input.tex_coord);
        case 103:
            return textures_0[103].Sample(linear_sample, input.tex_coord);
        case 104:
            return textures_0[104].Sample(linear_sample, input.tex_coord);
        case 105:
            return textures_0[105].Sample(linear_sample, input.tex_coord);
        case 106:
            return textures_0[106].Sample(linear_sample, input.tex_coord);
        case 107:
            return textures_0[107].Sample(linear_sample, input.tex_coord);
        case 108:
            return textures_0[108].Sample(linear_sample, input.tex_coord);
        case 109:
            return textures_0[109].Sample(linear_sample, input.tex_coord);
        case 110:
            return textures_0[110].Sample(linear_sample, input.tex_coord);
        case 111:
            return textures_0[111].Sample(linear_sample, input.tex_coord);
        case 112:
            return textures_0[112].Sample(linear_sample, input.tex_coord);
        case 113:
            return textures_0[113].Sample(linear_sample, input.tex_coord);
        case 114:
            return textures_0[114].Sample(linear_sample, input.tex_coord);
        case 115:
            return textures_0[115].Sample(linear_sample, input.tex_coord);
        case 116:
            return textures_0[116].Sample(linear_sample, input.tex_coord);
        case 117:
            return textures_0[117].Sample(linear_sample, input.tex_coord);
        case 118:
            return textures_0[118].Sample(linear_sample, input.tex_coord);
        case 119:
            return textures_0[119].Sample(linear_sample, input.tex_coord);
        case 120:
            return textures_0[120].Sample(linear_sample, input.tex_coord);
        case 121:
            return textures_0[121].Sample(linear_sample, input.tex_coord);
        case 122:
            return textures_0[122].Sample(linear_sample, input.tex_coord);
        case 123:
            return textures_0[123].Sample(linear_sample, input.tex_coord);
        case 124:
            return textures_0[124].Sample(linear_sample, input.tex_coord);
        case 125:
            return textures_0[125].Sample(linear_sample, input.tex_coord);
        case 126:
            return textures_0[126].Sample(linear_sample, input.tex_coord);
        case 127:
            return textures_0[127].Sample(linear_sample, input.tex_coord);
        default:
            return 0;
    }
}