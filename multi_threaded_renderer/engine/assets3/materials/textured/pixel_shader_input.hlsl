struct Pixel_shader_input {
    float4 position : SV_POSITION;
    float2 tex_coord : TEXCOORD0;
    uint instance_id : SV_InstanceId;
};