// Copyright 2017 Tomasz Kosek. All rights reserved.

#include <memory>
#include <utility>

#include <engine\hlsl\Instances_defs.hlsl>  // NOLINT
#include <engine\naming\Unique_Id.hpp>

namespace engine {
namespace assets3 {
namespace materials {
INLINE Textured::Builder::Builder(const Material::Name& name)
    : material_(std::make_unique<Textured_material>(name, Textured::ID)) {

    setup(&material_->get_parameters_for_setup());

    material_->get_parameters_for_setup().set_material_vertex_shader(&Textured::vs_);
    material_->get_parameters_for_setup().set_material_pixel_shader(&Textured::ps_);
}
INLINE engine::assets::Asset::Ptr<Material> Textured::Builder::build(ID3D11Device* device) {
    Textured::vs_.load(device);
    Textured::ps_.load(device);
    return engine::assets::Asset::Ptr<Material>::make_from_pointer(std::move(material_));
}
INLINE Textured::Builder& Textured::Builder::set_texture(
    const engine::assets::Asset::Ptr<Texture>& texture) {
    material_->set_texture(texture);
    return Configurator_base<Builder, Material::Parameters>::set_texture(texture);
}
}  // namespace materials
}  // namespace assets3
}  // namespace engine

namespace engine {
namespace assets3 {
namespace materials {
INLINE Textured::Builder::Textured_material::Textured_material(const Name& name,
    const engine::naming::Unique_id& material_type)
    : Material(name, material_type, Textured::MAX_TEXTURES_COUNT) {
}
INLINE void Textured::Builder::Textured_material::set_texture(
    const engine::assets::Asset::Ptr<Texture>& texture) {
    textures_refs_.set(static_cast<unsigned int>(Textured::TEXTURE_SLOT), texture);
}
}  // namespace materials
}  // namespace assets3
}  // namespace engine

namespace engine {
namespace assets3 {
namespace materials {
INLINE Textured::Configurator::Configurator(Material::Standalone_parameters& parameters) {
    setup(&parameters);
}
}  // namespace materials
}  // namespace assets3
}  // namespace engine
