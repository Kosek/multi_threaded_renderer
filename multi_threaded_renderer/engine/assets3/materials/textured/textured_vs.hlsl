// Copyright 2017 Tomasz Kosek. All rights reserved.

#include <engine/assets3/materials/textured/Pixel_shader_input.hlsl>
#include <engine/hlsl/Instances_defs.hlsl>

cbuffer world_view_projection_matrices_for_instances_buffer {
    float4x4 world_view_projection_matrices_for_instances[HLSL_MAX_INSTANCES_COUNT];
};

struct Vertex_shader_input {
    float3 position : POSITION;
    float2 tex_coord : TEXCOORD0;
    uint instance_id : SV_InstanceId;
};

Pixel_shader_input main(Vertex_shader_input input) {
    Pixel_shader_input output;

    output.position = mul(float4(input.position, 1.0f),
        world_view_projection_matrices_for_instances[input.instance_id]);
    output.tex_coord = input.tex_coord;
    output.instance_id = input.instance_id;

    return output;
}