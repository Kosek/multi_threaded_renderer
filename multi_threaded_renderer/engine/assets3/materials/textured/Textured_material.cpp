// Copyright 2017 Tomasz Kosek. All rights reserved.

#include <stdafx.h>

#include <engine\assets3\materials\textured\Textured_material.hpp>
#include <engine\vertex_stream\stride_schemas\Textured.hpp>

#if defined _DEBUG
#include <engine\assets3\materials\textured\Textured_material.inl>
#endif

namespace engine {
namespace assets3 {
namespace materials {

Material_vertex_shader Textured::vs_(Material_vertex_shader::Id(
    "engine/assets3/materials/textured/Textured_vs.hlsl"),
    engine::vertex_stream::stride_schemas::Textured::get_instance());
Material_pixel_shader Textured::ps_(Material_pixel_shader::Id(
    "engine/assets3/materials/textured/Textured_ps.hlsl"));
const engine::naming::Unique_id Textured::ID(1);
const unsigned int Textured::MAX_TEXTURES_COUNT = 0;
const Material::Parameters::Texture_slot Textured::TEXTURE_SLOT =
    Material::Parameters::Texture_slot::SLOT_0;
}  // namespace materials
}  // namespace assets3
}  // namespace engine
