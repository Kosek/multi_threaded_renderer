// Copyright 2017 Tomasz Kosek. All rights reserved.

#include <engine\debug\Assert.hpp>

namespace engine {
namespace assets3 {
namespace materials {
template<class DERIVED, class PARAMETERS>
INLINE void Textured::Configurator_base<DERIVED, PARAMETERS>::setup(PARAMETERS* parameters) {
    ENGINE_ASSERT(parameters->get_material_type().compare(Textured::ID) == 0,
        "Current configurator does not match material type!");
    Material::Parameters::Configurator::setup(&parameters->get());
    parameters_to_configure_ = parameters;
}
template<class DERIVED, class PARAMETERS>
INLINE DERIVED& Textured::Configurator_base<DERIVED, PARAMETERS>::set_texture(
    const engine::assets::Asset::Ptr<Texture>& texture) {
    ENGINE_ASSERT(texture, "Texture must not be null!");
    parameters_to_configure_->set_texture(Textured::TEXTURE_SLOT, texture);
    return static_cast<DERIVED&>(*this);
}
}  // namespace materials
}  // namespace assets3
}  // namespace engine
