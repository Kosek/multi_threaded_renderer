// Copyright 2017 Tomasz Kosek. All rights reserved.

#ifndef MULTI_THREADED_RENDERER_ENGINE_ASSETS3_MATERIALS_TEXTURED_TEXTURED_MATERIAL_HPP_
#define MULTI_THREADED_RENDERER_ENGINE_ASSETS3_MATERIALS_TEXTURED_TEXTURED_MATERIAL_HPP_

#include <memory>

#include <engine\assets3\Material.hpp>
#include <engine\assets3\materials\Material_pixel_shader.hpp>
#include <engine\assets3\materials\Material_vertex_shader.hpp>
#include <engine\assets3\Texture.hpp>
#include <engine\debug\Assert.hpp>
#include <engine\naming\Unique_Id.hpp>

namespace engine {
namespace assets3 {
namespace materials {
class Textured final : public Noncopyable {
 private:
    static const unsigned int MAX_TEXTURES_COUNT;
    static const Material::Parameters::Texture_slot TEXTURE_SLOT;

    template<class DERIVED, class PARAMETERS>
    class Configurator_base : public Material::Parameters::Configurator {
     public:
        INLINE DERIVED& set_texture(const engine::assets::Asset::Ptr<Texture>& texture);

     protected:
        using Material::Parameters::Configurator::Configurator;
        INLINE void setup(PARAMETERS* parameters);

        PARAMETERS* parameters_to_configure_ = nullptr;
    };

 public:
    static const engine::naming::Unique_id ID;

    class Configurator final : public Configurator_base<Configurator,
        Material::Standalone_parameters> {
     public:
        INLINE Configurator(Material::Standalone_parameters& parameters);  // NOLINT
    };

    class Builder final : public Configurator_base<Builder, Material::Parameters> {
     public:
        INLINE Builder(const Material::Name& name);
        INLINE Builder& set_texture(const engine::assets::Asset::Ptr<Texture>& texture);
        INLINE engine::assets::Asset::Ptr<Material> build(ID3D11Device* device);

     private:
        class Textured_material final : public Material {
            friend Builder;

         public:
            INLINE Textured_material(const Name& name,
                const engine::naming::Unique_id& material_type);
            INLINE void set_texture(const engine::assets::Asset::Ptr<Texture>& texture);

         private:
            engine::data_structures::Array<engine::assets::Asset::Ptr<Texture>,
                static_cast<unsigned int>(Material::Parameters::Texture_slot::SLOTS_COUNT)>
                textures_refs_;
        };
        std::unique_ptr<Textured_material> material_;
    };

 private:
    static Material_vertex_shader vs_;
    static Material_pixel_shader ps_;
};
}  // namespace materials
}  // namespace assets3
}  // namespace engine

#include <engine\assets3\materials\textured\Textured_material_header.inl>

#if !defined _DEBUG
#include <engine\assets3\materials\textured\Textured_material.inl>
#endif

#endif  // MULTI_THREADED_RENDERER_ENGINE_ASSETS3_MATERIALS_TEXTURED_TEXTURED_MATERIAL_HPP_
