// Copyright 2017 Tomasz Kosek. All rights reserved.

#include <string>

#include <engine\debug\Assert.hpp>
#include <engine\hlsl\Include_reader.hpp>

namespace engine {
namespace assets3 {
namespace materials {
INLINE Material_vertex_shader::Material_vertex_shader(
    const Id& id,
    const engine::vertex_stream::Stride_schema* stride_schema)
    : name_(id), stride_schema_(stride_schema) {
    ENGINE_ASSERT(stride_schema, "Stride schema cannot be null!");
}
INLINE Material_vertex_shader::~Material_vertex_shader() {
    if (is_loaded()) {
        unload();
    }
}
INLINE void Material_vertex_shader::load(ID3D11Device* device) {
    ENGINE_ASSERT(device, "Device must not be null!");
    ENGINE_ASSERT(!is_loaded(), "Material pixel shader is already loaded!");

    engine::hlsl::Include_reader include_reader;
    ID3D10Blob *error = nullptr;
    const std::wstring shader_file_path =
        std::wstring(get_name().get().cbegin(), get_name().get().cend());

    HRESULT r = D3DX11CompileFromFile(shader_file_path.c_str(), 0, &include_reader, "main",
        "vs_4_0", 0, 0, 0, &vertex_shader_blob_, &error, 0);
    ENGINE_ASSERT(r == NOERROR, "Error while compiling shader!");

    r = device->CreateVertexShader(vertex_shader_blob_->GetBufferPointer(),
        vertex_shader_blob_->GetBufferSize(), NULL, &vertex_shader_);
    ENGINE_ASSERT(r == NOERROR, "Error while creating vertex shader!");

    r = device->CreateInputLayout(
        stride_schema_->get_input_element_descritpion(),
        stride_schema_->get_input_element_description_elements_count(),
        vertex_shader_blob_->GetBufferPointer(),
        vertex_shader_blob_->GetBufferSize(),
        &input_layout_);
    ENGINE_ASSERT(r == NOERROR, "Error while creating input layout!");
}
INLINE void Material_vertex_shader::unload() {
    ENGINE_ASSERT(is_loaded(), "Texture must be loaded to perform unload!");
    if (input_layout_) {
        input_layout_->Release();
    }
    input_layout_ = nullptr;
    if (vertex_shader_) {
        vertex_shader_->Release();
    }
    vertex_shader_ = nullptr;
    if (vertex_shader_blob_) {
        vertex_shader_blob_->Release();
    }
    vertex_shader_blob_ = nullptr;
}
INLINE bool Material_vertex_shader::is_loaded() const {
    return vertex_shader_blob_ != nullptr;
}
INLINE const Material_vertex_shader::Id& Material_vertex_shader::get_name() const {
    return name_;
}
INLINE ID3D11VertexShader* Material_vertex_shader::get_vertex_shader() const {
    return vertex_shader_;
}
INLINE ID3D11InputLayout* Material_vertex_shader::get_input_layout() const {
    return input_layout_;
}
}  // namespace materials
}  // namespace assets3
}  // namespace engine
