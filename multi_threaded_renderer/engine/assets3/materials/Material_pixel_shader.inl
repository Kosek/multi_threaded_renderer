// Copyright 2017 Tomasz Kosek. All rights reserved.

#include <d3dx11.h>
#include <string>

#include <engine\debug\Assert.hpp>
#include <engine\hlsl\Include_reader.hpp>

namespace engine {
namespace assets3 {
namespace materials {
INLINE Material_pixel_shader::Material_pixel_shader(const Id& id)
    : name_(id) {
}
INLINE Material_pixel_shader::~Material_pixel_shader() {
    if (is_loaded()) {
        unload();
    }
}
INLINE void Material_pixel_shader::load(ID3D11Device* device) {
    ENGINE_ASSERT(device, "Device must not be null!");
    ENGINE_ASSERT(!is_loaded(), "Material pixel shader is already loaded!");

    engine::hlsl::Include_reader include_reader;
    ID3D10Blob *error = nullptr;
    const std::wstring shader_file_path =
        std::wstring(get_name().get().cbegin(), get_name().get().cend());
    HRESULT r = D3DX11CompileFromFile(shader_file_path.c_str(), 0, &include_reader, "main",
        "ps_4_0", 0, 0, 0, &pixel_shader_blob_, &error, 0);
    ENGINE_ASSERT(r == NOERROR, "Error while compiling shader!");

    r = device->CreatePixelShader(pixel_shader_blob_->GetBufferPointer(),
        pixel_shader_blob_->GetBufferSize(), NULL, &pixel_shader_);
    ENGINE_ASSERT(r == NOERROR, "Error while creating pixel shader!");
}
INLINE void Material_pixel_shader::unload() {
    ENGINE_ASSERT(is_loaded(), "Texture must be loaded to perform unload!");
    if (pixel_shader_) {
        pixel_shader_->Release();
    }
    pixel_shader_ = nullptr;
    if (pixel_shader_blob_) {
        pixel_shader_blob_->Release();
    }
    pixel_shader_blob_ = nullptr;
}
INLINE bool Material_pixel_shader::is_loaded() const {
    return pixel_shader_blob_ != nullptr;
}
INLINE const Material_pixel_shader::Id& Material_pixel_shader::get_name() const {
    return name_;
}
INLINE ID3D11PixelShader* Material_pixel_shader::get_pixel_shader() const {
    return pixel_shader_;
}
}  // namespace materials
}  // namespace assets3
}  // namespace engine
