// Copyright 2017 Tomasz Kosek. All rights reserved.

#ifndef MULTI_THREADED_RENDERER_ENGINE_ASSETS3_TEXTURE_HPP_
#define MULTI_THREADED_RENDERER_ENGINE_ASSETS3_TEXTURE_HPP_

#include <d3d11.h>

#include <engine\assets\Asset.hpp>

namespace engine {
namespace assets3 {
class Texture final : public assets::Asset {
 public:
    class Name : public Asset::Name {
        using Asset::Name::Name;
    };

    INLINE Texture(const Name& name);
    INLINE ~Texture();
    INLINE void load(ID3D11Device* device);
    INLINE void unload();
    INLINE bool is_loaded() const;
    INLINE ID3D11ShaderResourceView* get() const;

 private:
    engine::utilities::memory::Raw_ptr<ID3D11ShaderResourceView> shader_resource_view_;
};
}  // namespace assets3
}  // namespace engine

#if !defined _DEBUG
#include <engine\assets3\Texture.inl>
#endif

#endif  // MULTI_THREADED_RENDERER_ENGINE_ASSETS3_TEXTURE_HPP_
