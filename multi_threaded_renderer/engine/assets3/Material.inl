// Copyright 2017 Tomasz Kosek. All rights reserved.

namespace engine {
namespace assets3 {
INLINE Material::Material(const Name& name, const engine::naming::Unique_id& material_type,
    unsigned int max_textures_count)
    : Asset(name), parameters_(material_type, max_textures_count) {
}
INLINE const Material::Parameters& Material::get_parameters() const {
    return parameters_;
}
INLINE Material::Parameters& Material::get_parameters_for_setup() {
    return parameters_;
}
}  // namespace assets3
}  // namespace engine

namespace engine {
namespace assets3 {
INLINE Material::Parameters::Parameters(const engine::naming::Unique_id& material_type,
    unsigned int max_textures_count)
    : material_type_(material_type), max_textures_count_(max_textures_count) {
}
INLINE void Material::Parameters::set_texture(Texture_slot slot,
    const engine::assets::Asset::Ptr<Texture>& texture) {
    ID3D11ShaderResourceView* srv = texture->get();
    textures_.set(static_cast<unsigned int>(slot),
        engine::utilities::memory::Raw_ptr<ID3D11ShaderResourceView>(srv));
}
INLINE const engine::data_structures::Array<
    engine::utilities::memory::Raw_ptr<ID3D11ShaderResourceView>,
    static_cast<unsigned int>(Material::Parameters::Texture_slot::SLOTS_COUNT)>&
    Material::Parameters::get_textures() const {
    return textures_;
}
INLINE void Material::Parameters::set_material_vertex_shader(
    const materials::Material_vertex_shader* mvs) {
    material_vertex_shader_ = mvs;
}
INLINE void Material::Parameters::set_material_pixel_shader(
    const materials::Material_pixel_shader* mps) {
    material_pixel_shader_ = mps;
}
INLINE const engine::naming::Unique_id& Material::Parameters::get_material_type() const {
    return material_type_;
}
INLINE const materials::Material_vertex_shader*
Material::Parameters::get_material_vertex_shader() const {
    return material_vertex_shader_;
}
INLINE const materials::Material_pixel_shader*
Material::Parameters::get_material_pixel_shader() const {
    return material_pixel_shader_;
}
INLINE engine::rendering::Instances_group Material::Parameters::get_instances_group() const {
    return engine::rendering::Instances_group(0, 0, 0, 0);
}
INLINE unsigned int Material::Parameters::get_max_textures_count() const {
    return max_textures_count_;
}
INLINE const Material::Parameters& Material::Parameters::get() const {
    return *this;
}
INLINE Material::Parameters& Material::Parameters::get() {
    return *this;
}
}  // namespace assets3
}  // namespace engine

namespace engine {
namespace assets3 {
INLINE void Material::Parameters::Configurator::setup(Material::Parameters* parameters) {
    ENGINE_ASSERT(parameters, "Parameters must not be null!");
    ENGINE_ASSERT(!parameters_, "Parameters are already set!");
    parameters_ = parameters;
}
}  // namespace assets3
}  // namespace engine

namespace engine {
namespace assets3 {
INLINE Material::Standalone_parameters::Standalone_parameters(
    const Parameters& parameters) : parameters_(parameters) {
}
INLINE const Material::Parameters& Material::Standalone_parameters::get() const {
    return parameters_;
}
INLINE Material::Parameters& Material::Standalone_parameters::get() {
    return parameters_;
}
INLINE void Material::Standalone_parameters::set_texture(Parameters::Texture_slot slot,
    const engine::assets::Asset::Ptr<Texture>& texture) {
    parameters_.set_texture(slot, texture);
    textures_refs_.set(static_cast<unsigned int>(slot), texture);
}
INLINE const engine::naming::Unique_id&
Material::Standalone_parameters::get_material_type() const {
    return parameters_.get_material_type();
}
}  // namespace assets3
}  // namespace engine
