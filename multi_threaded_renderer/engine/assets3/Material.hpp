// Copyright 2017 Tomasz Kosek. All rights reserved.

#ifndef MULTI_THREADED_RENDERER_ENGINE_ASSETS3_MATERIAL_HPP_
#define MULTI_THREADED_RENDERER_ENGINE_ASSETS3_MATERIAL_HPP_

#include <memory>

#include <engine\assets\Asset.hpp>
#include <engine\assets3\materials\Material_pixel_shader_fwd.hpp>
#include <engine\assets3\materials\Material_vertex_shader_fwd.hpp>
#include <engine\assets3\Texture.hpp>
#include <engine\data_structures\Array.hpp>
#include <engine\naming\Unique_Id.hpp>
#include <engine\rendering\Instances_group.hpp>
#include <engine\utilities\memory\Raw_ptr.hpp>

namespace engine {
namespace assets3 {

class Material : public assets::Asset {
 public:
    class Name : public Asset::Name {
        using Asset::Name::Name;
    };

    class Parameters final {
     public:
        class Configurator : public Noncopyable {
         public:
            INLINE Configurator() = default;

         protected:
            INLINE void setup(Parameters* parameters);
            engine::utilities::memory::Raw_ptr<Parameters> parameters_;
        };

        enum class Texture_slot {
            SLOT_0 = 0,
            SLOT_1 = 1,
            SLOT_2 = 2,
            SLOT_3 = 3,
            SLOTS_COUNT = 4
        };

        INLINE Parameters(const engine::naming::Unique_id& material_type,
            unsigned int max_textures_count);
        INLINE void set_texture(Texture_slot slot,
            const engine::assets::Asset::Ptr<Texture>& texture);
        INLINE const engine::data_structures::Array<
            engine::utilities::memory::Raw_ptr<ID3D11ShaderResourceView>,
            static_cast<unsigned int>(Texture_slot::SLOTS_COUNT)>& get_textures() const;
        INLINE void set_material_vertex_shader(
            const materials::Material_vertex_shader* mvs);
        INLINE void set_material_pixel_shader(const materials::Material_pixel_shader* mps);
        INLINE const engine::naming::Unique_id& get_material_type() const;
        INLINE engine::rendering::Instances_group get_instances_group() const;
        INLINE const materials::Material_vertex_shader* get_material_vertex_shader() const;
        INLINE const materials::Material_pixel_shader* get_material_pixel_shader() const;
        INLINE unsigned int get_max_textures_count() const;
        INLINE const Parameters& get() const;
        INLINE Parameters& get();

     private:
        engine::naming::Unique_id material_type_;
        engine::data_structures::Array<
            engine::utilities::memory::Raw_ptr<ID3D11ShaderResourceView>,
            static_cast<unsigned int>(Texture_slot::SLOTS_COUNT)> textures_;
        engine::utilities::memory::Raw_ptr<const materials::Material_vertex_shader>
            material_vertex_shader_;
        engine::utilities::memory::Raw_ptr<const materials::Material_pixel_shader>
            material_pixel_shader_;
        unsigned int max_textures_count_ = 0;
    };

    class Standalone_parameters final {
     public:
        INLINE Standalone_parameters(const Parameters& parameters);
        INLINE const Parameters& get() const;
        INLINE Parameters& get();
        INLINE void set_texture(Parameters::Texture_slot slot,
            const engine::assets::Asset::Ptr<Texture>& texture);
        INLINE const engine::naming::Unique_id& get_material_type() const;

     private:
        Parameters parameters_;
        engine::data_structures::Array<engine::assets::Asset::Ptr<Texture>,
            static_cast<unsigned int>(Parameters::Texture_slot::SLOTS_COUNT)>
            textures_refs_;
    };

    virtual ~Material();
    INLINE const Parameters& get_parameters() const;

 protected:
    INLINE Material(const Name& name, const engine::naming::Unique_id& material_type,
        unsigned int max_textures_count);
    INLINE Parameters& get_parameters_for_setup();

 private:
    Parameters parameters_;
};

}  // namespace assets3
}  // namespace engine

#if !defined _DEBUG
#include <engine\assets3\Material.inl>
#endif

#endif  // MULTI_THREADED_RENDERER_ENGINE_ASSETS3_MATERIAL_HPP_
