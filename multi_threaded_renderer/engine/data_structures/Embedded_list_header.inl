// Copyright 2017 Tomasz Kosek. All rights reserved.

#include <utility>

#include <engine\debug\Assert.hpp>

namespace engine {
namespace data_structures {
template <
    typename LIST,
    typename T,
    typename ELEMENT_INTERFACE,
    const char* NULL_NOT_ALLOWED,
    const char* ELEMENT_ALREADY_IN_LIST,
    const char* ELEMENT_NOT_IN_CURRENT_LIST>
    INLINE Embedded_list<
    LIST,
    T,
    ELEMENT_INTERFACE,
    NULL_NOT_ALLOWED,
    ELEMENT_ALREADY_IN_LIST,
    ELEMENT_NOT_IN_CURRENT_LIST>::~Embedded_list() {
    remove_all();
}
template <
    typename LIST,
    typename T,
    typename ELEMENT_INTERFACE,
    const char* NULL_NOT_ALLOWED,
    const char* ELEMENT_ALREADY_IN_LIST,
    const char* ELEMENT_NOT_IN_CURRENT_LIST>
    INLINE void Embedded_list<
    LIST,
    T,
    ELEMENT_INTERFACE,
    NULL_NOT_ALLOWED,
    ELEMENT_ALREADY_IN_LIST,
    ELEMENT_NOT_IN_CURRENT_LIST>::add(T* e) {
    ENGINE_ASSERT(e, NULL_NOT_ALLOWED);
    ENGINE_ASSERT(!ELEMENT_INTERFACE::get_embedded_list_element(e).is_in_list(),
        ELEMENT_ALREADY_IN_LIST);

    if (first_) {
        ELEMENT_INTERFACE::get_embedded_list_element(first_).prev_ = e;
    }
    ELEMENT_INTERFACE::get_embedded_list_element(e).next_ = first_;
    first_ = e;
    ELEMENT_INTERFACE::get_embedded_list_element(e).list_ = static_cast<LIST*>(this);
}
template <
    typename LIST,
    typename T,
    typename ELEMENT_INTERFACE,
    const char* NULL_NOT_ALLOWED,
    const char* ELEMENT_ALREADY_IN_LIST,
    const char* ELEMENT_NOT_IN_CURRENT_LIST>
    INLINE T* Embedded_list<
    LIST,
    T,
    ELEMENT_INTERFACE,
    NULL_NOT_ALLOWED,
    ELEMENT_ALREADY_IN_LIST,
    ELEMENT_NOT_IN_CURRENT_LIST>::pop() {
    T* ret = first_;
    if (ret) {
        first_ = ELEMENT_INTERFACE::get_embedded_list_element(ret).next_;
        if (first_) {
            ELEMENT_INTERFACE::get_embedded_list_element(first_).prev_ = nullptr;
        }
        ELEMENT_INTERFACE::get_embedded_list_element(ret).prev_ = nullptr;
        ELEMENT_INTERFACE::get_embedded_list_element(ret).next_ = nullptr;
        ELEMENT_INTERFACE::get_embedded_list_element(ret).list_ = nullptr;
    }
    return ret;
}

template <
    typename LIST,
    typename T,
    typename ELEMENT_INTERFACE,
    const char* NULL_NOT_ALLOWED,
    const char* ELEMENT_ALREADY_IN_LIST,
    const char* ELEMENT_NOT_IN_CURRENT_LIST>
    INLINE void Embedded_list<
    LIST,
    T,
    ELEMENT_INTERFACE,
    NULL_NOT_ALLOWED,
    ELEMENT_ALREADY_IN_LIST,
    ELEMENT_NOT_IN_CURRENT_LIST>::remove(T* e) {
    ENGINE_ASSERT(e, NULL_NOT_ALLOWED);
    ENGINE_ASSERT(contains(e), ELEMENT_NOT_IN_CURRENT_LIST);

    if (first_ == e) {
        first_ = ELEMENT_INTERFACE::get_embedded_list_element(e).next_;
    }
    if (ELEMENT_INTERFACE::get_embedded_list_element(e).next_) {
        ELEMENT_INTERFACE::get_embedded_list_element(
            ELEMENT_INTERFACE::get_embedded_list_element(e).next_).prev_ =
            ELEMENT_INTERFACE::get_embedded_list_element(e).prev_;
    }
    if (ELEMENT_INTERFACE::get_embedded_list_element(e).prev_) {
        ELEMENT_INTERFACE::get_embedded_list_element(
            ELEMENT_INTERFACE::get_embedded_list_element(e).prev_).next_ =
            ELEMENT_INTERFACE::get_embedded_list_element(e).next_;
    }
    ELEMENT_INTERFACE::get_embedded_list_element(e).next_ = nullptr;
    ELEMENT_INTERFACE::get_embedded_list_element(e).prev_ = nullptr;
    ELEMENT_INTERFACE::get_embedded_list_element(e).list_ = nullptr;
}

template <
    typename LIST,
    typename T,
    typename ELEMENT_INTERFACE,
    const char* NULL_NOT_ALLOWED,
    const char* ELEMENT_ALREADY_IN_LIST,
    const char* ELEMENT_NOT_IN_CURRENT_LIST>
    INLINE bool Embedded_list<
    LIST,
    T,
    ELEMENT_INTERFACE,
    NULL_NOT_ALLOWED,
    ELEMENT_ALREADY_IN_LIST,
    ELEMENT_NOT_IN_CURRENT_LIST>::contains(const T* e) const {
    return this == ELEMENT_INTERFACE::get_embedded_list_element(e).list_;
}

template <
    typename LIST,
    typename T,
    typename ELEMENT_INTERFACE,
    const char* NULL_NOT_ALLOWED,
    const char* ELEMENT_ALREADY_IN_LIST,
    const char* ELEMENT_NOT_IN_CURRENT_LIST>
    INLINE typename Embedded_list<
    LIST,
    T,
    ELEMENT_INTERFACE,
    NULL_NOT_ALLOWED,
    ELEMENT_ALREADY_IN_LIST,
    ELEMENT_NOT_IN_CURRENT_LIST>::Iterator Embedded_list<
    LIST,
    T,
    ELEMENT_INTERFACE,
    NULL_NOT_ALLOWED,
    ELEMENT_ALREADY_IN_LIST,
    ELEMENT_NOT_IN_CURRENT_LIST>::get_iterator() const {
    return Embedded_list<LIST, T, ELEMENT_INTERFACE, NULL_NOT_ALLOWED, ELEMENT_ALREADY_IN_LIST,
        ELEMENT_NOT_IN_CURRENT_LIST>::Iterator(this);
}

template <
    typename LIST,
    typename T,
    typename ELEMENT_INTERFACE,
    const char* NULL_NOT_ALLOWED,
    const char* ELEMENT_ALREADY_IN_LIST,
    const char* ELEMENT_NOT_IN_CURRENT_LIST>
    INLINE bool Embedded_list<
    LIST,
    T,
    ELEMENT_INTERFACE,
    NULL_NOT_ALLOWED,
    ELEMENT_ALREADY_IN_LIST,
    ELEMENT_NOT_IN_CURRENT_LIST>::is_empty() const {
    return first_ == nullptr;
}
}  // namespace data_structures
}  // namespace engine

namespace engine {
namespace data_structures {
template <
    typename LIST,
    typename T,
    typename ELEMENT_INTERFACE,
    const char* NULL_NOT_ALLOWED,
    const char* ELEMENT_ALREADY_IN_LIST,
    const char* ELEMENT_NOT_IN_CURRENT_LIST>
    INLINE bool Embedded_list<
    LIST,
    T,
    ELEMENT_INTERFACE,
    NULL_NOT_ALLOWED,
    ELEMENT_ALREADY_IN_LIST,
    ELEMENT_NOT_IN_CURRENT_LIST>::Copyable_element::is_in_list() const {
    return list_ != nullptr;
}

template <
    typename LIST,
    typename T,
    typename ELEMENT_INTERFACE,
    const char* NULL_NOT_ALLOWED,
    const char* ELEMENT_ALREADY_IN_LIST,
    const char* ELEMENT_NOT_IN_CURRENT_LIST>
    INLINE LIST* Embedded_list<
    LIST,
    T,
    ELEMENT_INTERFACE,
    NULL_NOT_ALLOWED,
    ELEMENT_ALREADY_IN_LIST,
    ELEMENT_NOT_IN_CURRENT_LIST>::Copyable_element::get_list() const {
    return list_;
}
}  // namespace data_structures
}  // namespace engine

namespace engine {
namespace data_structures {
template <
    typename LIST,
    typename T,
    typename ELEMENT_INTERFACE,
    const char* NULL_NOT_ALLOWED,
    const char* ELEMENT_ALREADY_IN_LIST,
    const char* ELEMENT_NOT_IN_CURRENT_LIST>
    INLINE Embedded_list<
    LIST,
    T,
    ELEMENT_INTERFACE,
    NULL_NOT_ALLOWED,
    ELEMENT_ALREADY_IN_LIST,
    ELEMENT_NOT_IN_CURRENT_LIST>::Iterator::Iterator(const Embedded_list* l)
    : list_(l) {
    ENGINE_ASSERT(list_, "List must not be null!");
}

template <
    typename LIST,
    typename T,
    typename ELEMENT_INTERFACE,
    const char* NULL_NOT_ALLOWED,
    const char* ELEMENT_ALREADY_IN_LIST,
    const char* ELEMENT_NOT_IN_CURRENT_LIST>
    template<typename VISITOR>
INLINE void Embedded_list<
    LIST,
    T,
    ELEMENT_INTERFACE,
    NULL_NOT_ALLOWED,
    ELEMENT_ALREADY_IN_LIST,
    ELEMENT_NOT_IN_CURRENT_LIST>::Iterator::visit_all(const VISITOR& visitor) const {
    T* e = list_->first_;
    while (e) {
        visitor(e);
        e = ELEMENT_INTERFACE::get_embedded_list_element(e).next_;
    }
}

template <
    typename LIST,
    typename T,
    typename ELEMENT_INTERFACE,
    const char* NULL_NOT_ALLOWED,
    const char* ELEMENT_ALREADY_IN_LIST,
    const char* ELEMENT_NOT_IN_CURRENT_LIST>
    template <typename VISITOR>
INLINE void Embedded_list<
    LIST,
    T,
    ELEMENT_INTERFACE,
    NULL_NOT_ALLOWED,
    ELEMENT_ALREADY_IN_LIST,
    ELEMENT_NOT_IN_CURRENT_LIST>::remove_all(const VISITOR& visitor) {
    while (T* e = pop()) {
        visitor(e);
    }
}

template <
    typename LIST,
    typename T,
    typename ELEMENT_INTERFACE,
    const char* NULL_NOT_ALLOWED,
    const char* ELEMENT_ALREADY_IN_LIST,
    const char* ELEMENT_NOT_IN_CURRENT_LIST>
    INLINE void Embedded_list<
    LIST,
    T,
    ELEMENT_INTERFACE,
    NULL_NOT_ALLOWED,
    ELEMENT_ALREADY_IN_LIST,
    ELEMENT_NOT_IN_CURRENT_LIST>::remove_all() {
    while (pop()) {
    }
}
}  // namespace data_structures
}  // namespace engine
