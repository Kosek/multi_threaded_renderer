// Copyright 2017 Tomasz Kosek. All rights reserved.

#include <utility>

#include <engine\debug\Assert.hpp>

namespace engine {
namespace data_structures {
template <typename T, unsigned int MAX_SIZE>
INLINE void Array<T, MAX_SIZE>::push_back(const T& e) {
    ENGINE_ASSERT(get_count() < get_max_size(), "Cannot push back as array is full!");
    array_.at(elements_count_++) = e;
}
template <typename T, unsigned int MAX_SIZE>
INLINE void Array<T, MAX_SIZE>::push_back(T&& e) {
    ENGINE_ASSERT(get_count() < get_max_size(), "Cannot push back as array is full!");
    array_.at(elements_count_++) = std::move(e);
}
template <typename T, unsigned int MAX_SIZE>
INLINE void Array<T, MAX_SIZE>::set(const unsigned int index, const T& value) {
    if (index >= get_count()) {
        unsigned int elements_to_add = index - get_count() + 1;
        do {
            T v = T();
            push_back(v);
        } while (--elements_to_add);
    }
    array_.at(index) = value;
}
template <typename T, unsigned int MAX_SIZE>
INLINE T& Array<T, MAX_SIZE>::get(const unsigned int index) {
    ENGINE_ASSERT(index < get_count(), "Index out of range!");
    return array_[index];
}
template <typename T, unsigned int MAX_SIZE>
INLINE const T& Array<T, MAX_SIZE>::get(const unsigned int index) const {
    ENGINE_ASSERT(index < get_count(), "Index out of range!");
    return array_[index];
}
template <typename T, unsigned int MAX_SIZE>
INLINE bool Array<T, MAX_SIZE>::is_empty() const {
    return get_count() == 0;
}
template <typename T, unsigned int MAX_SIZE>
INLINE unsigned int Array<T, MAX_SIZE>::get_count() const {
    return elements_count_;
}
template <typename T, unsigned int MAX_SIZE>
INLINE unsigned int Array<T, MAX_SIZE>::get_max_size() const {
    return MAX_SIZE;
}
template <typename T, unsigned int MAX_SIZE>
INLINE void Array<T, MAX_SIZE>::remove_all() {
    elements_count_ = 0;
    std::for_each(array_.begin(), array_.end(), [](T& e) {
        e = T();
    });
}
template <typename T, unsigned int MAX_SIZE>
INLINE typename Array<T, MAX_SIZE>::Iterator Array<T, MAX_SIZE>::get_iterator() const {
    return Iterator(*this);
}
template <typename T, unsigned int MAX_SIZE>
INLINE const T* Array<T, MAX_SIZE>::get_data() const {
    return array_.data();
}
}  // namespace data_structures
}  // namespace engine

namespace engine {
namespace data_structures {
template <typename T, unsigned int MAX_SIZE>
INLINE Array<T, MAX_SIZE>::Iterator::Iterator(const Array<T, MAX_SIZE>& array)
    : array_(array) {
}

template <typename T, unsigned int MAX_SIZE>
template <typename VISITOR>
INLINE void Array<T, MAX_SIZE>::Iterator::visit_all(const VISITOR& visitor) const {
    const T* end = array_.array_.data() + array_.elements_count_;
    T const* e = array_.array_.data();
    while (e < end) {
        visitor(*e);
        ++e;
    }
}
}  // namespace data_structures
}  // namespace engine
