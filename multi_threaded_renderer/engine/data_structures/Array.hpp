// Copyright 2017 Tomasz Kosek. All rights reserved.

#ifndef MULTI_THREADED_RENDERER_ENGINE_DATA_STRUCTURES_ARRAY_HPP_
#define MULTI_THREADED_RENDERER_ENGINE_DATA_STRUCTURES_ARRAY_HPP_

#include <array>
#include <initializer_list>
#include <utility>

namespace engine {
namespace data_structures {
template <typename T, unsigned int MAX_SIZE>
class Array final {
 public:
    INLINE void push_back(const T& e);
    INLINE void push_back(T&& e);
    INLINE void set(const unsigned int index, const T& value);
    INLINE T& get(const unsigned int index);
    INLINE const T& get(const unsigned int index) const;
    INLINE bool is_empty() const;
    INLINE unsigned int get_count() const;
    INLINE unsigned int get_max_size() const;
    INLINE const T* get_data() const;
    INLINE void remove_all();

    class Iterator final {
        friend Array;

     public:
        template <typename VISITOR>
        INLINE void visit_all(const VISITOR& visitor) const;

     private:
        INLINE Iterator(const Array<T, MAX_SIZE>& array);

        const Array<T, MAX_SIZE>& array_;
    };

    INLINE Iterator get_iterator() const;

 private:
    std::array<T, MAX_SIZE> array_;
    unsigned int elements_count_ = 0;
};
}  // namespace data_structures
}  // namespace engine

#include <engine\data_structures\Array_header.inl>


#endif  // MULTI_THREADED_RENDERER_ENGINE_DATA_STRUCTURES_ARRAY_HPP_
