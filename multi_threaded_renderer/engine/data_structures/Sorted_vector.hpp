// Copyright 2017 Tomasz Kosek. All rights reserved.

#ifndef MULTI_THREADED_RENDERER_ENGINE_DATA_STRUCTURES_SORTED_VECTOR_HPP_
#define MULTI_THREADED_RENDERER_ENGINE_DATA_STRUCTURES_SORTED_VECTOR_HPP_

#include <utility>
#include <vector>

namespace engine {
namespace data_structures {
template <typename KEY, typename VALUE, typename COMPARATOR>
class Sorted_vector final : public Noncopyable {
 public:
    INLINE void add(const VALUE& v);
    INLINE void add(VALUE&& v);
    INLINE VALUE remove(const KEY& k);
    INLINE bool contains(const KEY& k) const;
    INLINE const VALUE& get(const KEY& k) const;
    INLINE VALUE& get(const KEY& k);
    INLINE unsigned int get_count() const;
    INLINE typename std::vector<VALUE>::const_iterator cbegin() const;
    INLINE typename std::vector<VALUE>::const_iterator cend() const;
    INLINE bool is_empty() const;
    INLINE void remove_all();
    INLINE void reserve(int size);

 private:
    INLINE typename std::vector<VALUE>::iterator find(const KEY& k);
    INLINE typename std::vector<VALUE>::const_iterator find(const KEY& k) const;
    std::vector<VALUE> v_;
};
}  // namespace data_structures
}  // namespace engine

#include <engine\data_structures\Sorted_vector_header.inl>


#endif  // MULTI_THREADED_RENDERER_ENGINE_DATA_STRUCTURES_SORTED_VECTOR_HPP_
