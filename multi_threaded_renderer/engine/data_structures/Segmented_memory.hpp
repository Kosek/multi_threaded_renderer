// Copyright 2017 Tomasz Kosek. All rights reserved.

#ifndef MULTI_THREADED_RENDERER_ENGINE_DATA_STRUCTURES_SEGMENTED_MEMORY_HPP_
#define MULTI_THREADED_RENDERER_ENGINE_DATA_STRUCTURES_SEGMENTED_MEMORY_HPP_

#include <vector>

namespace engine {
namespace data_structures {
template <typename T, unsigned int ELEMENTS_COUNT_IN_SEGMENT>
class Segmented_memory final : public Noncopyable {
    static_assert(ELEMENTS_COUNT_IN_SEGMENT > 0, "ELEMENTS_COUNT_IN_SEGMENT must be at least 1");

 public:
    INLINE Segmented_memory();
    INLINE void push_back(const T& e);
    INLINE void reset_elements_count();
    class Iterator final {
        friend Segmented_memory;

     public:
        template <typename VISITOR>
        INLINE void visit_all(const VISITOR& visitor) const;

     private:
        INLINE Iterator(Segmented_memory& segmented_memory);  // NOLINT

        Segmented_memory& segmented_memory_;
    };
    INLINE Iterator get_iterator();

 private:
    INLINE void alloc_element(const T& e);
    INLINE void alloc_segment();
    INLINE std::vector<T>& get_last_segment();

    std::vector<std::vector<T>> segments_;
    int last_segment_ = -1;
    int last_element_ = -1;
};
}  // namespace data_structures
}  // namespace engine

#include <engine\data_structures\Segmented_memory_header.inl>


#endif  // MULTI_THREADED_RENDERER_ENGINE_DATA_STRUCTURES_SEGMENTED_MEMORY_HPP_
