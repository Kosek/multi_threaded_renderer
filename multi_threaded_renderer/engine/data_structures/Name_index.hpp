// Copyright 2017 Tomasz Kosek. All rights reserved.

#ifndef MULTI_THREADED_RENDERER_ENGINE_DATA_STRUCTURES_NAME_INDEX_HPP_
#define MULTI_THREADED_RENDERER_ENGINE_DATA_STRUCTURES_NAME_INDEX_HPP_

#include <map>
#include <string>
#include <utility>

#include <engine\naming\Name.hpp>
#include <engine\utilities\memory\Raw_ptr.hpp>

namespace engine {
namespace data_structures {
template <typename T> class Name_index final : public Noncopyable {
 public:
    INLINE void add(T* e);
    INLINE void remove(const T* e);
    INLINE void remove(const engine::naming::Name& name);
    INLINE T* get(const engine::naming::Name& name);
    INLINE const T* get(const engine::naming::Name& name) const;
    INLINE bool contains(const engine::naming::Name& name) const;

 private:
    using Collection_entry = std::pair<std::string, engine::utilities::memory::Raw_ptr<T>>;
    using Collection = std::map<std::string, engine::utilities::memory::Raw_ptr<T>>;
    Collection map_;
};
}  // namespace data_structures
}  // namespace engine

#include <engine\data_structures\Name_index_header.inl>


#endif  // MULTI_THREADED_RENDERER_ENGINE_DATA_STRUCTURES_NAME_INDEX_HPP_
