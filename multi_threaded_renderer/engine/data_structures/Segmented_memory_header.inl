// Copyright 2017 Tomasz Kosek. All rights reserved.

#include <utility>
#include <vector>

namespace engine {
namespace data_structures {
template <typename T, unsigned int ELEMENTS_COUNT_IN_SEGMENT>
INLINE Segmented_memory<T, ELEMENTS_COUNT_IN_SEGMENT>::Segmented_memory() {
    alloc_segment();
}
template <typename T, unsigned int ELEMENTS_COUNT_IN_SEGMENT>
INLINE void Segmented_memory<T, ELEMENTS_COUNT_IN_SEGMENT>::push_back(const T& e) {
    alloc_element(e);
}
template <typename T, unsigned int ELEMENTS_COUNT_IN_SEGMENT>
INLINE void Segmented_memory<T, ELEMENTS_COUNT_IN_SEGMENT>::alloc_element(const T& e) {
    ++last_element_;
    if (last_element_ >= ELEMENTS_COUNT_IN_SEGMENT) {
        alloc_segment();
        last_element_ = 0;
    }
    if (last_element_ >= static_cast<int>(get_last_segment().size())) {
        get_last_segment().push_back(std::move(e));
    } else {
        get_last_segment()[last_element_] = std::move(e);
    }
}
template <typename T, unsigned int ELEMENTS_COUNT_IN_SEGMENT>
INLINE std::vector<T>& Segmented_memory<T, ELEMENTS_COUNT_IN_SEGMENT>::get_last_segment() {
    return segments_[last_segment_];
}
template <typename T, unsigned int ELEMENTS_COUNT_IN_SEGMENT>
INLINE void Segmented_memory<T, ELEMENTS_COUNT_IN_SEGMENT>::alloc_segment() {
    ++last_segment_;
    if (last_segment_ >= static_cast<int>(segments_.size())) {
        segments_.push_back({});
        segments_.back().reserve(ELEMENTS_COUNT_IN_SEGMENT);
    }
}
template <typename T, unsigned int ELEMENTS_COUNT_IN_SEGMENT>
INLINE void Segmented_memory<T, ELEMENTS_COUNT_IN_SEGMENT>::reset_elements_count() {
    last_segment_ = 0;
    last_element_ = -1;
}
template <typename T, unsigned int ELEMENTS_COUNT_IN_SEGMENT>
INLINE typename Segmented_memory<T, ELEMENTS_COUNT_IN_SEGMENT>::Iterator
Segmented_memory<T, ELEMENTS_COUNT_IN_SEGMENT>::get_iterator() {
    return Iterator(*this);
}
}  // namespace data_structures
}  // namespace engine

namespace engine {
namespace data_structures {
template <typename T, unsigned int ELEMENTS_COUNT_IN_SEGMENT>
INLINE Segmented_memory<T, ELEMENTS_COUNT_IN_SEGMENT>::Iterator::Iterator(
    Segmented_memory& segmented_memory) :
    segmented_memory_(segmented_memory) {
}
template <typename T, unsigned int ELEMENTS_COUNT_IN_SEGMENT>
template <typename VISITOR>
INLINE void Segmented_memory<T, ELEMENTS_COUNT_IN_SEGMENT>::Iterator::visit_all(
    const VISITOR& visitor) const {
    for (int si = 0; si <= segmented_memory_.last_segment_ - 1; ++si) {
        for (int ei = 0; ei < ELEMENTS_COUNT_IN_SEGMENT; ++ei) {
            visitor(segmented_memory_.segments_[si][ei]);
        }
    }
    std::vector<T>& last_segment =
        segmented_memory_.segments_[segmented_memory_.last_segment_];
    for (int ei = 0; ei <= segmented_memory_.last_element_; ++ei) {
        visitor(last_segment[ei]);
    }
}
}  // namespace data_structures
}  // namespace engine
