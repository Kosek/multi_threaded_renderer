// Copyright 2017 Tomasz Kosek. All rights reserved.

#include <utility>

#include <engine\debug\Assert.hpp>
#include <engine\scene3\Entry.hpp>

namespace engine {
namespace data_structures {
template <typename T>
INLINE void Name_index<T>::add(T* e) {
    ENGINE_ASSERT(e, "Element must not be null!");
    ENGINE_ASSERT(!static_cast<const engine::naming::Name&>(e->get_name()).is_empty(),
        "Element name must not be empty!");
    ENGINE_ASSERT(!contains(e->get_name()), "Element already exists!");
    map_.insert(Collection_entry(e->get_name().get(), e));
}
template <typename T>
INLINE void Name_index<T>::remove(const T* e) {
    ENGINE_ASSERT(e, "Element must not be null!");
    remove(e->get_name());
}
template <typename T>
INLINE void Name_index<T>::remove(const engine::naming::Name& name) {
    ENGINE_ASSERT(!name.is_empty(), "Element must not be null!");
    ENGINE_ASSERT(contains(name), "Element does not exist in index!");
    map_.erase(map_.find(name.get()));
}
template <typename T>
INLINE T* Name_index<T>::get(const engine::naming::Name& name) {
    auto found = map_.find(name.get());
    if (found == map_.end()) {
        return nullptr;
    } else {
        return static_cast<T*>(found->second);
    }
}
template <typename T>
INLINE const T* Name_index<T>::get(const engine::naming::Name& name) const {
    auto found = map_.find(name.get());
    if (found == map_.end()) {
        return nullptr;
    } else {
        return static_cast<const T*>(found->second);
    }
}
template <typename T>
INLINE bool Name_index<T>::contains(const engine::naming::Name& name) const {
    return (map_.find(name.get()) != map_.end());
}
}  // namespace data_structures
}  // namespace engine
