// Copyright 2017 Tomasz Kosek. All rights reserved.

#ifndef MULTI_THREADED_RENDERER_ENGINE_DATA_STRUCTURES_EMBEDDED_LIST_HPP_
#define MULTI_THREADED_RENDERER_ENGINE_DATA_STRUCTURES_EMBEDDED_LIST_HPP_

#include <utility>

#include <engine\utilities\memory\Raw_ptr.hpp>

namespace engine {
namespace data_structures {
template <
    typename LIST,
    typename T,
    typename ELEMENT_INTERFACE,
    const char* NULL_NOT_ALLOWED,
    const char* ELEMENT_ALREADY_IN_LIST,
    const char* ELEMENT_NOT_IN_CURRENT_LIST>
class Embedded_list : public Noncopyable {
 public:
    INLINE Embedded_list() = default;
    INLINE ~Embedded_list();
    INLINE void add(T* e);
    INLINE void remove(T* e);
    template <typename VISITOR>
    INLINE void remove_all(const VISITOR& visitor);
    INLINE void remove_all();

    class Iterator final {
        friend Embedded_list;

     public:
        template <typename VISITOR>
        INLINE void visit_all(const VISITOR& visitor) const;

     private:
        INLINE Iterator(const Embedded_list* l);

        engine::utilities::memory::Raw_ptr<const Embedded_list> list_;
    };

    class Copyable_element {
        friend Embedded_list;

     public:
        INLINE bool is_in_list() const;
        INLINE LIST* get_list() const;

     private:
        engine::utilities::memory::Raw_ptr<LIST> list_;
        engine::utilities::memory::Raw_ptr<T> next_;
        engine::utilities::memory::Raw_ptr<T> prev_;
    };

    class Element : public Copyable_element,  public Noncopyable {
    };

    INLINE Iterator get_iterator() const;
    INLINE bool contains(const T* e) const;
    INLINE bool is_empty() const;

 private:
    INLINE T* pop();

    engine::utilities::memory::Raw_ptr<T> first_;
};
}  // namespace data_structures
}  // namespace engine

#include <engine\data_structures\Embedded_list_header.inl>


#endif  // MULTI_THREADED_RENDERER_ENGINE_DATA_STRUCTURES_EMBEDDED_LIST_HPP_
