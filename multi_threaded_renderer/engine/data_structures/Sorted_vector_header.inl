// Copyright 2017 Tomasz Kosek. All rights reserved.

#include <algorithm>
#include <utility>
#include <vector>

#include <engine\debug\Assert.hpp>

namespace engine {
namespace data_structures {
template <typename KEY, typename VALUE, typename COMPARATOR>
INLINE void Sorted_vector<KEY, VALUE, COMPARATOR>::add(const VALUE& v) {
    const KEY& k = COMPARATOR().get_key(v);
    ENGINE_ASSERT(!contains(k), "Key already exists in sorted vector!");
    auto insert_at = find(k);
    v_.insert(insert_at, v);
}
template <typename KEY, typename VALUE, typename COMPARATOR>
INLINE void Sorted_vector<KEY, VALUE, COMPARATOR>::add(VALUE&& v) {
    const KEY& k = COMPARATOR().get_key(v);
    ENGINE_ASSERT(!contains(k), "Key already exists in sorted vector!");
    auto insert_at = find(k);
    v_.insert(insert_at, std::move(v));
}
template <typename KEY, typename VALUE, typename COMPARATOR>
INLINE VALUE Sorted_vector<KEY, VALUE, COMPARATOR>::remove(const KEY& k) {
    auto remove_at = find(k);
    ENGINE_ASSERT(remove_at != v_.cend(), "Element to remove is not present in vector!");
    VALUE ret = std::move(*remove_at);
    v_.erase(remove_at);
    return std::move(ret);
}
template <typename KEY, typename VALUE, typename COMPARATOR>
INLINE bool Sorted_vector<KEY, VALUE, COMPARATOR>::contains(const KEY& k) const {
    std::vector<VALUE>::const_iterator found = find(k);
    return (found != v_.end()) && (COMPARATOR().cmp(k, *found) == 0);
}
template <typename KEY, typename VALUE, typename COMPARATOR>
INLINE typename std::vector<VALUE>::iterator
Sorted_vector<KEY, VALUE, COMPARATOR>::find(const KEY& k) {
    if (v_.size()) {
        std::vector<VALUE>::iterator greater = std::upper_bound(v_.begin(), v_.end(), k,
            [](const KEY& key, const VALUE& value) {
            return (COMPARATOR().cmp(key, value) < 0); });
        if (greater != v_.begin() && COMPARATOR().cmp(k, *(greater - 1)) == 0) {
            return greater - 1;
        } else {
            return greater;
        }
    } else {
        return v_.end();
    }
}
template <typename KEY, typename VALUE, typename COMPARATOR>
INLINE typename std::vector<VALUE>::const_iterator
Sorted_vector<KEY, VALUE, COMPARATOR>::find(const KEY& k) const {
    if (v_.size()) {
        std::vector<VALUE>::const_iterator greater = std::upper_bound(v_.cbegin(), v_.cend(), k,
            [](const KEY& key, const VALUE& value) {
            return (COMPARATOR().cmp(key, value) < 0); });
        if (greater != v_.cbegin() && COMPARATOR().cmp(k, *(greater - 1)) == 0) {
            return greater - 1;
        } else {
            return greater;
        }
    } else {
        return v_.cend();
    }
}
template <typename KEY, typename VALUE, typename COMPARATOR>
INLINE VALUE& Sorted_vector<KEY, VALUE, COMPARATOR>::get(const KEY& k) {
    ENGINE_ASSERT(contains(k), "Key does not exist in sorted vector!");
    return *find(k);
}
template <typename KEY, typename VALUE, typename COMPARATOR>
INLINE const VALUE& Sorted_vector<KEY, VALUE, COMPARATOR>::get(const KEY& k) const {
    ENGINE_ASSERT(contains(k), "Key does not exist in sorted vector!");
    return *find(k);
}
template <typename KEY, typename VALUE, typename COMPARATOR>
INLINE unsigned int Sorted_vector<KEY, VALUE, COMPARATOR>::get_count() const {
    return static_cast<unsigned int>(v_.size());
}
template <typename KEY, typename VALUE, typename COMPARATOR>
INLINE typename std::vector<VALUE>::const_iterator
Sorted_vector<KEY, VALUE, COMPARATOR>::cbegin() const {
    return v_.cbegin();
}
template <typename KEY, typename VALUE, typename COMPARATOR>
INLINE typename std::vector<VALUE>::const_iterator
Sorted_vector<KEY, VALUE, COMPARATOR>::cend() const {
    return v_.cend();
}
template <typename KEY, typename VALUE, typename COMPARATOR>
INLINE bool Sorted_vector<KEY, VALUE, COMPARATOR>::is_empty() const {
    return v_.empty();
}
template <typename KEY, typename VALUE, typename COMPARATOR>
INLINE void Sorted_vector<KEY, VALUE, COMPARATOR>::remove_all() {
    return v_.clear();
}
template <typename KEY, typename VALUE, typename COMPARATOR>
INLINE void Sorted_vector<KEY, VALUE, COMPARATOR>::reserve(int size) {
    return v_.reserve(size);
}
}  // namespace data_structures
}  // namespace engine
