// Copyright 2017 Tomasz Kosek. All rights reserved.

#ifndef MULTI_THREADED_RENDERER_ENGINE_FRAME_SIMULATOR_HPP_
#define MULTI_THREADED_RENDERER_ENGINE_FRAME_SIMULATOR_HPP_

#include <memory>
#include <thread>  // NOLINT

#include <engine\game\Context.hpp>
#include <engine\game\Main_script_fwd.hpp>

namespace engine {
namespace frame {
class Simulator final : public Noncopyable {
 public:
    INLINE Simulator();
    INLINE ~Simulator();
    INLINE void setup(engine::game::Main_script* main_script);
    INLINE void unsetup();
    INLINE void start();
    INLINE void stop();
#if defined SINGLE_THREADED_RENDERING
    INLINE void simulate_frame_thread_main();
#endif

 private:
#if !defined SINGLE_THREADED_RENDERING
    INLINE void simulate_frame_thread_main();
#endif

    engine::game::Context main_context_;
    std::unique_ptr<std::thread> frame_simulation_thread_;
    engine::utilities::memory::Raw_ptr<engine::game::Main_script> main_script_;
    bool stop_frame_simulation_thread_ = false;
    bool frame_thread_is_running_ = false;
};

ENGINE_DECLARE_UNIQUE_NAME_OF_TYPE(game::Context::Name, MAIN_CONTEXT);
}  // namespace frame
}  // namespace engine

#if !defined _DEBUG
#include <engine\frame\Simulator.inl>
#endif

#endif  // MULTI_THREADED_RENDERER_ENGINE_FRAME_SIMULATOR_HPP_
