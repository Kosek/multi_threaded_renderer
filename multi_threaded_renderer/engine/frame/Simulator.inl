// Copyright 2017 Tomasz Kosek. All rights reserved.

#include <memory>

#include <engine\debug\Assert.hpp>
#include <engine\game\Main_script.hpp>
#include <engine\threading\Local_storage.hpp>

namespace engine {
namespace frame {
INLINE Simulator::Simulator() : main_context_(MAIN_CONTEXT) {
}
INLINE Simulator::~Simulator() {
    unsetup();
}
INLINE void Simulator::setup(engine::game::Main_script* main_script) {
    ENGINE_ASSERT(!frame_simulation_thread_.get(),
        "Frame simulatoin thread is already started!");
    main_script_ = main_script;
}
INLINE void Simulator::unsetup() {
    stop();
    main_script_ = nullptr;
}
#if !defined SINGLE_THREADED_RENDERING
INLINE void Simulator::start() {
    _ReadBarrier();
    ENGINE_ASSERT(!frame_thread_is_running_,
        "Frame simulatoin thread is already started!");
    stop_frame_simulation_thread_ = false;
    frame_thread_is_running_ = true;
    _WriteBarrier();
    frame_simulation_thread_ = std::make_unique<std::thread>(
        &Simulator::simulate_frame_thread_main, this);
}
INLINE void Simulator::stop() {
    stop_frame_simulation_thread_ = true;
    _WriteBarrier();
    if (frame_simulation_thread_ &&
        frame_simulation_thread_->get_id() != std::this_thread::get_id()) {
        frame_simulation_thread_->join();
    }
    frame_thread_is_running_ = false;
    _WriteBarrier();
}
INLINE void Simulator::simulate_frame_thread_main() {
    engine::threading::Local_storage::Main_context_setter mcs(main_context_);
    main_context_.start();
    main_script_->start(&main_context_);
    _ReadBarrier();
    while (!stop_frame_simulation_thread_) {
        _ReadBarrier();
        main_context_.update();
    }
    main_script_->stop(&main_context_);
    main_context_.stop();
}
#endif
#if defined SINGLE_THREADED_RENDERING
INLINE void Simulator::simulate_frame_thread_main() {
    engine::threading::Local_storage::Main_context_setter mcs(main_context_);
    main_context_.update();
}
INLINE void Simulator::start() {
    engine::threading::Local_storage::Main_context_setter mcs(main_context_);
    main_context_.start();
    main_script_->start(&main_context_);
}
INLINE void Simulator::stop() {
    engine::threading::Local_storage::Main_context_setter mcs(main_context_);
    main_script_->stop(&main_context_);
    main_context_.stop();
}
#endif

}  // namespace frame
}  // namespace engine
