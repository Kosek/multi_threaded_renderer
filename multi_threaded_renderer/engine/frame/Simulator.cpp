// Copyright 2017 Tomasz Kosek. All rights reserved.

#include <stdafx.h>

#include <engine\frame\Simulator.hpp>

#if defined _DEBUG
#include <engine\frame\Simulator.inl>
#endif

namespace engine {
namespace frame {
ENGINE_REGISTER_UNIQUE_NAME_OF_TYPE(game::Context::Name, MAIN_CONTEXT);
}  // namespace frame
}  // namespace engine
