// Copyright 2017 Tomasz Kosek. All rights reserved.

#ifndef MULTI_THREADED_RENDERER_ENGINE_FRAME_UPDATE_RECEIVER_HPP_
#define MULTI_THREADED_RENDERER_ENGINE_FRAME_UPDATE_RECEIVER_HPP_

#include <engine\data_structures\Embedded_list.hpp>
#include <engine\game\Context_fwd.hpp>

namespace engine {
namespace frame {
namespace update {

extern const char RECEIVER_TO_UPDATE_NULL_NOT_ALLOWED[];
extern const char RECEIVER_TO_UPDATE_ELEMENT_ALREADY_IN_LIST[];
extern const char RECEIVER_TO_UPDATE_ELEMENT_NOT_IN_CURRENT_LIST[];

class Receiver : public Noncopyable {
 public:
    virtual ~Receiver();
    virtual void update(engine::game::Context* context) = 0;
    INLINE bool is_receive_update_enabled() const;

 private:
    class Receivers_to_update_list_element;
 public:
    class Receivers_to_update_list final : public engine::data_structures::Embedded_list<
        Receivers_to_update_list,
        Receiver,
        Receivers_to_update_list_element,
        RECEIVER_TO_UPDATE_NULL_NOT_ALLOWED,
        RECEIVER_TO_UPDATE_ELEMENT_ALREADY_IN_LIST,
        RECEIVER_TO_UPDATE_ELEMENT_NOT_IN_CURRENT_LIST> {
    };

 private:
    class Receivers_to_update_list_element final : public engine::data_structures::Embedded_list<
        Receivers_to_update_list,
        Receiver,
        Receivers_to_update_list_element,
        RECEIVER_TO_UPDATE_NULL_NOT_ALLOWED,
        RECEIVER_TO_UPDATE_ELEMENT_ALREADY_IN_LIST,
        RECEIVER_TO_UPDATE_ELEMENT_NOT_IN_CURRENT_LIST>::Element {
     public:
        using Type = engine::data_structures::Embedded_list<
            Receivers_to_update_list,
            Receiver,
            Receivers_to_update_list_element,
            RECEIVER_TO_UPDATE_NULL_NOT_ALLOWED,
            RECEIVER_TO_UPDATE_ELEMENT_ALREADY_IN_LIST,
            RECEIVER_TO_UPDATE_ELEMENT_NOT_IN_CURRENT_LIST>::Element;

        INLINE static Type& get_embedded_list_element(Receiver* e);
        INLINE static const Type& get_embedded_list_element(const Receiver* e);
    };

    Receivers_to_update_list_element receivers_to_update_list_element_;
};
}  // namespace update
}  // namespace frame
}  // namespace engine

#if !defined _DEBUG
#include <engine\frame\update\Receiver.inl>
#endif

#endif  // MULTI_THREADED_RENDERER_ENGINE_FRAME_UPDATE_RECEIVER_HPP_
