// Copyright 2017 Tomasz Kosek. All rights reserved.

#include <stdafx.h>

#include <engine\frame\update\Receiver.hpp>

#if defined _DEBUG
#include <engine\frame\update\Receiver.inl>
#endif

namespace engine {
namespace frame {
namespace update {
const char RECEIVER_TO_UPDATE_NULL_NOT_ALLOWED[] = { "Script must not be null!" };
const char RECEIVER_TO_UPDATE_ELEMENT_ALREADY_IN_LIST[] = {
    "Script already added to update list!" };
const char RECEIVER_TO_UPDATE_ELEMENT_NOT_IN_CURRENT_LIST[] = {
    "Script is not added to current update list!" };

Receiver::~Receiver() {
}
}  // namespace update
}  // namespace frame
}  // namespace engine
