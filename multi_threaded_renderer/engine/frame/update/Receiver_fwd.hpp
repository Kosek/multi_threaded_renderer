// Copyright 2017 Tomasz Kosek. All rights reserved.

#ifndef MULTI_THREADED_RENDERER_ENGINE_FRAME_UPDATE_RECEIVER_FWD_HPP_
#define MULTI_THREADED_RENDERER_ENGINE_FRAME_UPDATE_RECEIVER_FWD_HPP_

namespace engine {
namespace frame {
namespace update {

class Receiver;

}  // namespace update
}  // namespace frame
}  // namespace engine

#endif  // MULTI_THREADED_RENDERER_ENGINE_FRAME_UPDATE_RECEIVER_FWD_HPP_
