// Copyright 2017 Tomasz Kosek. All rights reserved.

namespace engine {
namespace frame {
namespace update {
INLINE bool Receiver::is_receive_update_enabled() const {
    return receivers_to_update_list_element_.is_in_list();
}
}  // namespace update
}  // namespace frame
}  // namespace engine

namespace engine {
namespace frame {
namespace update {
INLINE Receiver::Receivers_to_update_list_element::Type&
Receiver::Receivers_to_update_list_element::get_embedded_list_element(Receiver* e) {
    return e->receivers_to_update_list_element_;
}
INLINE const Receiver::Receivers_to_update_list_element::Type&
Receiver::Receivers_to_update_list_element::get_embedded_list_element(const Receiver* e) {
    return e->receivers_to_update_list_element_;
}
}  // namespace update
}  // namespace frame
}  // namespace engine
