// Copyright 2017 Tomasz Kosek. All rights reserved.

#include <stdafx.h>

#include <fstream>
#include <sstream>
#include <streambuf>
#include <string>

#include <engine\debug\Assert.hpp>
#include <engine\hlsl\Include_reader.hpp>

namespace engine {
namespace hlsl {

HRESULT STDMETHODCALLTYPE Include_reader::Open(D3D_INCLUDE_TYPE IncludeType, LPCSTR pFileName,
    LPCVOID pParentData, LPCVOID *ppData, UINT *pBytes) {
    std::ifstream include_file(pFileName);
    if (!include_file.is_open()) {
        ENGINE_ASSERT(false, "Failed to open include file!");
        return E_FAIL;
    }
    std::stringstream buffer;
    buffer << include_file.rdbuf();
    file_content_ = buffer.str();
    *ppData = static_cast<const void*>(file_content_.c_str());
    *pBytes = static_cast<unsigned int>(file_content_.size());
    return S_OK;
}
HRESULT STDMETHODCALLTYPE Include_reader::Close(LPCVOID pData) {
    return S_OK;
}

}  // namespace hlsl
}  // namespace engine
