#if !defined INSTANCES_DEFS_HLSL
#define INSTANCES_DEFS_HLSL

#define HLSL_MAX_INSTANCES_COUNT 128  // If one changes this value, one should also re-generate the
                                      // switch-case statement in engine::assets3::materials::textured::Texutred_ps.hlsl NOLINT
                                      // in main!

#endif