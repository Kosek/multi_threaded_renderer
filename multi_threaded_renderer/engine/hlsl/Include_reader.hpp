// Copyright 2017 Tomasz Kosek. All rights reserved.

#ifndef MULTI_THREADED_RENDERER_ENGINE_HLSL_INCLUDE_READER_HPP_
#define MULTI_THREADED_RENDERER_ENGINE_HLSL_INCLUDE_READER_HPP_

#include <string>

namespace engine {
namespace hlsl {

struct Include_reader final : public ID3DInclude {
    HRESULT STDMETHODCALLTYPE Open(D3D_INCLUDE_TYPE IncludeType, LPCSTR pFileName,
        LPCVOID pParentData, LPCVOID *ppData, UINT *pBytes) override;
    HRESULT STDMETHODCALLTYPE Close(LPCVOID pData) override;

 private:
    std::string file_content_;
};

}  // namespace hlsl
}  // namespace engine

#endif  // MULTI_THREADED_RENDERER_ENGINE_HLSL_INCLUDE_READER_HPP_
