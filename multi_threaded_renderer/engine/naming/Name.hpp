// Copyright 2017 Tomasz Kosek. All rights reserved.

#ifndef MULTI_THREADED_RENDERER_ENGINE_NAMING_NAME_HPP_
#define MULTI_THREADED_RENDERER_ENGINE_NAMING_NAME_HPP_

#include <string>

namespace engine {
namespace naming {
class Name {
 public:
    INLINE Name(const std::string& name);
    INLINE const std::string& get() const;
    INLINE bool is_empty() const;
    INLINE int compare(const Name& right) const;
    INLINE static std::string make_path(const std::string& path);

 protected:
    INLINE Name() = default;

 private:
    std::string name_;
};
}  // namespace naming
}  // namespace engine

#define ENGINE_DECLARE_NAME(___name___)\
extern engine::naming::Name ___name___;

#define ENGINE_REGISTER_NAME(___name___)\
engine::naming::Name ___name___(#___name___);

#define ENGINE_DECLARE_NAME_OF_TYPE(___type___, ___name___)\
extern ___type___ ___name___;

#define ENGINE_REGISTER_NAME_OF_TYPE(___type___, ___name___)\
___type___ ___name___(#___name___);

#define ENGINE_REGISTER_NAME_OF_TYPE_AS_PATH(___type___, ___name___)\
___type___ ___name___(___type___::make_path(#___name___));

#if !defined _DEBUG
#include <engine\naming\Name.inl>
#endif

#endif  // MULTI_THREADED_RENDERER_ENGINE_NAMING_NAME_HPP_
