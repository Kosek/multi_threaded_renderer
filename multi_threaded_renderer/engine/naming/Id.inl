// Copyright 2017 Tomasz Kosek. All rights reserved.

namespace engine {
namespace naming {
INLINE Id::Id(const unsigned int& id)
    : id_(id) {
}
INLINE const unsigned int Id::get() const {
    return id_;
}
INLINE bool Id::is_empty() const {
    return id_ == 0;
}
INLINE int Id::compare(const Id& right) const {
    return id_ - right.id_;
}
}  // namespace naming
}  // namespace engine
