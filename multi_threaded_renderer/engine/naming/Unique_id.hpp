// Copyright 2017 Tomasz Kosek. All rights reserved.

#ifndef MULTI_THREADED_RENDERER_ENGINE_NAMING_UNIQUE_ID_HPP_
#define MULTI_THREADED_RENDERER_ENGINE_NAMING_UNIQUE_ID_HPP_

#include <engine\naming\Id.hpp>

namespace engine {
namespace naming {
class Unique_id : public Id {
 public:
    INLINE Unique_id(const unsigned int& id);  // NOLINT

 private:
    using Id::is_empty;
};
}  // namespace naming
}  // namespace engine

#define ENGINE_DECLARE_UNIQUE_ID(___id___)\
extern engine::naming::Unique_id ___id___;

#define ENGINE_REGISTER_UNIQUE_ID(___id___)\
engine::naming::Unique_id ___id___(#___id___);

#define ENGINE_DECLARE_UNIQUE_ID_OF_TYPE(___type___, ___id___)\
ENGINE_DECLARE_NAME_OF_TYPE(___type___, ___id___);

#define ENGINE_REGISTER_UNIQUE_ID_OF_TYPE(___type___, ___id___)\
ENGINE_REGISTER_NAME_OF_TYPE(___type___, ___id___);

#if !defined _DEBUG
#include <engine\naming\Unique_id.inl>
#endif

#endif  // MULTI_THREADED_RENDERER_ENGINE_NAMING_UNIQUE_ID_HPP_
