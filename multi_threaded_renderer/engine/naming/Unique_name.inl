// Copyright 2017 Tomasz Kosek. All rights reserved.

#include <string>

#include <engine\debug\Assert.hpp>

namespace engine {
namespace naming {
INLINE Unique_name::Unique_name(const std::string& name)
    : Name(name) {
    ENGINE_ASSERT(!name.empty(), "Name must not be empty!");
}
}  // namespace naming
}  // namespace engine
