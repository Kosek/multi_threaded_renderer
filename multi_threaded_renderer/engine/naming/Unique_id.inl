// Copyright 2017 Tomasz Kosek. All rights reserved.

#include <engine\debug\Assert.hpp>

namespace engine {
namespace naming {
INLINE Unique_id::Unique_id(const unsigned int& id)
    : Id(id) {
    ENGINE_ASSERT(id != 0, "Id must not be 0!");
}
}  // namespace naming
}  // namespace engine
