// Copyright 2017 Tomasz Kosek. All rights reserved.

#include <stdafx.h>

#include <engine\naming\Unique_id.hpp>

#if defined _DEBUG
#include <engine\naming\Unique_id.inl>
#endif
