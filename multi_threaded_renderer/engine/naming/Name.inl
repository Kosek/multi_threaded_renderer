// Copyright 2017 Tomasz Kosek. All rights reserved.

#include <sstream>
#include <string>

#include <engine\debug\Assert.hpp>

namespace engine {
namespace naming {
INLINE Name::Name(const std::string& name)
    : name_(name) {
}
INLINE const std::string& Name::get() const {
    return name_;
}
INLINE bool Name::is_empty() const {
    return name_.empty();
}
INLINE int Name::compare(const Name& right) const {
    return name_.compare(right.get());
}
INLINE std::string Name::make_path(const std::string& name) {
    char ret[512];
    ENGINE_ASSERT(name.size() < sizeof(ret), "Given string is too long!");
    int ci = static_cast<int>(name.size());
    ret[ci--] = '\0';
    while (ci >= 0 && (name[ci] != '_')) {
        ret[ci] = name[ci];
        --ci;
    }
    ret[ci--] = '.';
    while (ci >= 0) {
        if (name[ci] == '_') {
            ret[ci] = '\\';
        } else {
            ret[ci] = name[ci];
        }
        --ci;
    }
    return std::string(ret);
}
}  // namespace naming
}  // namespace engine
