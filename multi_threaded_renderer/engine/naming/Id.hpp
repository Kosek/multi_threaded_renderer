// Copyright 2017 Tomasz Kosek. All rights reserved.

#ifndef MULTI_THREADED_RENDERER_ENGINE_NAMING_ID_HPP_
#define MULTI_THREADED_RENDERER_ENGINE_NAMING_ID_HPP_

namespace engine {
namespace naming {
class Id {
 public:
    INLINE Id(const unsigned int& id);  // NOLINT
    INLINE const unsigned int get() const;
    INLINE bool is_empty() const;
    INLINE int compare(const Id& right) const;

 protected:
    INLINE Id() = default;

 private:
    unsigned int id_ = 0;
};
}  // namespace naming
}  // namespace engine

#define ENGINE_DECLARE_ID(___id___)\
extern engine::naming::Id ___id___;

#define ENGINE_REGISTER_ID(___id___)\
engine::naming::Id ___id___(#___id___);

#define ENGINE_DECLARE_ID_OF_TYPE(___type___, ___id___)\
extern ___type___ ___id___;

#define ENGINE_REGISTER_ID_OF_TYPE(___type___, ___id___)\
___type___ ___id___(#___id___);

#if !defined _DEBUG

#include <engine\naming\Id.inl>

#endif

#endif  // MULTI_THREADED_RENDERER_ENGINE_NAMING_ID_HPP_
