// Copyright 2017 Tomasz Kosek. All rights reserved.

#ifndef MULTI_THREADED_RENDERER_ENGINE_NAMING_UNIQUE_NAME_HPP_
#define MULTI_THREADED_RENDERER_ENGINE_NAMING_UNIQUE_NAME_HPP_

#include <string>

#include <engine\naming\Name.hpp>

namespace engine {
namespace naming {
class Unique_name : public Name {
 public:
    INLINE Unique_name(const std::string& name);

 private:
    using Name::is_empty;
};
}  // namespace naming
}  // namespace engine

#define ENGINE_DECLARE_UNIQUE_NAME(___name___)\
extern engine::naming::Unique_name ___name___;

#define ENGINE_REGISTER_UNIQUE_NAME(___name___)\
engine::naming::Unique_name ___name___(#___name___);

#define ENGINE_DECLARE_UNIQUE_NAME_OF_TYPE(___type___, ___name___)\
ENGINE_DECLARE_NAME_OF_TYPE(___type___, ___name___);

#define ENGINE_REGISTER_UNIQUE_NAME_OF_TYPE(___type___, ___name___)\
ENGINE_REGISTER_NAME_OF_TYPE(___type___, ___name___);

#define ENGINE_REGISTER_UNIQUE_NAME_OF_TYPE_AS_PATH(___type___, ___name___)\
ENGINE_REGISTER_NAME_OF_TYPE_AS_PATH(___type___, ___name___);

#if !defined _DEBUG
#include <engine\naming\Unique_name.inl>
#endif

#endif  // MULTI_THREADED_RENDERER_ENGINE_NAMING_UNIQUE_NAME_HPP_
