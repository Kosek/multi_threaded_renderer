// Copyright 2017 Tomasz Kosek. All rights reserved.

#include <iomanip>

namespace engine {
namespace logging {
INLINE Time::Time(const tm& date, const std::chrono::hours& hours,
    const std::chrono::minutes& minutes, const std::chrono::seconds& seconds,
    const std::chrono::system_clock::duration& milliseconds) : date_(date),
    hours_(hours), minutes_(minutes), seconds_(seconds), milliseconds_(milliseconds) {
}

INLINE Time Time::now() {
    using days = std::chrono::duration<int, std::ratio_multiply<std::chrono::hours::period,
        std::ratio<24>>::type>;
    std::chrono::system_clock::time_point now = std::chrono::system_clock::now();
    std::chrono::system_clock::duration tp = now.time_since_epoch();
    days d = std::chrono::duration_cast<days>(tp);
    tp -= d;
    std::chrono::hours h = std::chrono::duration_cast<std::chrono::hours>(tp);
    tp -= h;
    std::chrono::minutes m = std::chrono::duration_cast<std::chrono::minutes>(tp);
    tp -= m;
    std::chrono::seconds s = std::chrono::duration_cast<std::chrono::seconds>(tp);
    tp -= s;

    auto in_time_t = std::chrono::system_clock::to_time_t(now);
    struct tm date;
    localtime_s(&date, &in_time_t);

    return { date, h, m, s, tp };
}

INLINE const tm& Time::get_date() const {
    return date_;
}

INLINE int Time::get_hour() const {
    return hours_.count();
}

INLINE int Time::get_minute() const {
    return minutes_.count();
}

INLINE __int64 Time::get_second() const {
    return seconds_.count();
}

INLINE __int64 Time::get_millisecond() const {
    return milliseconds_.count();
}
INLINE Time::Writer Time::get_writer() {
    return Writer(*this);
}
}  // namespace logging
}  // namespace engine

namespace engine {
namespace logging {
INLINE Time::Writer::Writer(const Time& time) : time_(time) {
}
INLINE void Time::Writer::write(std::ostream& s) {
    s << std::put_time(&time_.get_date(), "%F");
    s << " " << time_.get_hour() << ":" << time_.get_minute() << ":" << time_.get_second()
        << "." << time_.get_millisecond();
}
}  // namespace logging
}  // namespace engine
