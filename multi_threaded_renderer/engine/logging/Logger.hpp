// Copyright 2017 Tomasz Kosek. All rights reserved.

#ifndef MULTI_THREADED_RENDERER_ENGINE_LOGGING_LOGGER_HPP_
#define MULTI_THREADED_RENDERER_ENGINE_LOGGING_LOGGER_HPP_

#include <fstream>
#include <string>

namespace engine {
namespace logging {
class Logger final : public Noncopyable {
 private:
    enum class Severity {
        INFO,
        ERR
    };

 public:
    INLINE static void error(const char* filename, int line, const char* function,
        const char* msg);

 private:
    INLINE static const char* severity_to_string(Severity s);

    static const char* log_file_path_;
};
}  // namespace logging
}  // namespace engine

#if !defined _DEBUG
#include <engine\logging\Logger.inl>
#endif

#endif  // MULTI_THREADED_RENDERER_ENGINE_LOGGING_LOGGER_HPP_
