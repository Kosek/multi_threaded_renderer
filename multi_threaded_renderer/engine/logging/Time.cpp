// Copyright 2017 Tomasz Kosek. All rights reserved.

#include <stdafx.h>

#include <engine\logging\Time.hpp>

#if defined _DEBUG
#include <engine\logging\Time.inl>
#endif
