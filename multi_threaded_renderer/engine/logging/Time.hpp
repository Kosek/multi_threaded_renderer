// Copyright 2017 Tomasz Kosek. All rights reserved.

#ifndef MULTI_THREADED_RENDERER_ENGINE_LOGGING_TIME_HPP_
#define MULTI_THREADED_RENDERER_ENGINE_LOGGING_TIME_HPP_

#include <chrono>  //NOLINT

namespace engine {
namespace logging {
class Time final {
 public:
    INLINE static Time now();

    INLINE const tm& get_date() const;
    INLINE int get_hour() const;
    INLINE int get_minute() const;
    INLINE __int64 get_second() const;
    INLINE __int64 get_millisecond() const;

    class Writer {
     public:
        INLINE Writer(const Time& time);
        INLINE void write(std::ostream& stream);

     private:
        const Time& time_;
    };

    INLINE Writer get_writer();

 private:
    INLINE Time(const tm& date, const std::chrono::hours& hours,
        const std::chrono::minutes& minutes, const std::chrono::seconds& seconds,
        const std::chrono::system_clock::duration& milliseconds);

    tm date_ = { 0, 0, 0, 0, 0, 0, 0, 0, 0 };
    std::chrono::hours hours_ = std::chrono::hours::zero();
    std::chrono::minutes minutes_ = std::chrono::minutes::zero();
    std::chrono::seconds seconds_ = std::chrono::seconds::zero();
    std::chrono::system_clock::duration milliseconds_ =
        std::chrono::system_clock::duration::zero();
};
}  // namespace logging
}  // namespace engine

#if !defined _DEBUG
#include <engine\logging\Time.inl>
#endif

#endif  // MULTI_THREADED_RENDERER_ENGINE_LOGGING_TIME_HPP_
