// Copyright 2017 Tomasz Kosek. All rights reserved.

#include <fstream>
#include <string>

#include <engine\logging\Time.hpp>

namespace engine {
namespace logging {
INLINE void Logger::error(const char* filename, int line, const char* function, const char* msg) {
    std::ofstream file;
    file.open(log_file_path_, std::ofstream::out | std::ofstream::app);
    if (file.is_open()) {
        Time::now().get_writer().write(file);
        file << " " << Logger::severity_to_string(Severity::ERR) << " " << msg <<
            " (" << filename << ":" << line << "->" << function << ")" << std::endl;
    }
}
INLINE const char* Logger::severity_to_string(Severity s) {
    switch (s) {
    case Severity::INFO:
        return "INFO";
    case Severity::ERR:
        return "ERR";
    default:
        return "Unknow";
    }
}
}  // namespace logging
}  // namespace engine
