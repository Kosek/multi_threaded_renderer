// Copyright 2017 Tomasz Kosek. All rights reserved.

#include <stdafx.h>

#include <engine\logging\Logger.hpp>

#if defined _DEBUG
#include <engine\logging\Logger.inl>
#endif

namespace engine {
namespace logging {
const char* Logger::log_file_path_ = "engine.log";
}  // namespace logging
}  // namespace engine
