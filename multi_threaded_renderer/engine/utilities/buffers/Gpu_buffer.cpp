// Copyright 2017 Tomasz Kosek. All rights reserved.

#include <stdafx.h>

#include <engine\utilities\buffers\Gpu_buffer.hpp>

#if defined _DEBUG
#include <engine\utilities\buffers\Gpu_buffer.inl>
#endif
