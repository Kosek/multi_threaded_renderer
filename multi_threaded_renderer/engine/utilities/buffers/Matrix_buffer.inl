// Copyright 2017 Tomasz Kosek. All rights reserved.

namespace engine {
namespace utilities {
namespace buffers {
INLINE void Matrix_buffer::setup(ID3D11Device* device, unsigned int matrices_count) {
    Gpu_buffer::setup(device, D3D11_BUFFER_DESC{
        matrices_count * sizeof(XMFLOAT4X4),  // UINT ByteWidth;
        D3D11_USAGE_DYNAMIC,  // D3D11_USAGE Usage;
        D3D11_BIND_CONSTANT_BUFFER,  // UINT BindFlags;
        D3D11_CPU_ACCESS_WRITE,  // UINT CPUAccessFlags;
        0,  // UINT MiscFlags;
        0  // UINT StructureByteStride;
    });
}
}  // namespace buffers
}  // namespace utilities
}  // namespace engine
