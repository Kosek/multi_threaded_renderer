// Copyright 2017 Tomasz Kosek. All rights reserved.

namespace engine {
namespace utilities {
namespace buffers {
template <class T>
INLINE void Gpu_buffer::Writer::write(const T& e) {
    ENGINE_ASSERT(sizeof(e) <= get_bytes_left(),
        "Cannot write any more data to buffer as it is full!");
    *(static_cast<T*>(static_cast<void*>(current_ptr_))) = e;
    current_ptr_ = static_cast<T*>(static_cast<void*>(current_ptr_)) + 1;
}
}  // namespace buffers
}  // namespace utilities
}  // namespace engine
