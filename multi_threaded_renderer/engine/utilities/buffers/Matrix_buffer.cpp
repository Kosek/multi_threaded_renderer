// Copyright 2017 Tomasz Kosek. All rights reserved.

#include <stdafx.h>

#include <engine\utilities\buffers\Matrix_buffer.hpp>

#if defined _DEBUG
#include <engine\utilities\buffers\Matrix_buffer.inl>
#endif
