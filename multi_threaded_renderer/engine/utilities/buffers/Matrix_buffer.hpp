// Copyright 2017 Tomasz Kosek. All rights reserved.

#ifndef MULTI_THREADED_RENDERER_ENGINE_UTILITIES_BUFFERS_MATRIX_BUFFER_HPP_
#define MULTI_THREADED_RENDERER_ENGINE_UTILITIES_BUFFERS_MATRIX_BUFFER_HPP_

#include <engine\utilities\buffers\Gpu_buffer.hpp>

namespace engine {
namespace utilities {
namespace buffers {
class Matrix_buffer final : public Gpu_buffer {
 public:
    using Gpu_buffer::Gpu_buffer;
    INLINE void setup(ID3D11Device* device, unsigned int matrices_count);
};
}  // namespace buffers
}  // namespace utilities
}  // namespace engine

#if !defined _DEBUG
#include <engine\utilities\buffers\Matrix_buffer.inl>
#endif

#endif  // MULTI_THREADED_RENDERER_ENGINE_UTILITIES_BUFFERS_MATRIX_BUFFER_HPP_
