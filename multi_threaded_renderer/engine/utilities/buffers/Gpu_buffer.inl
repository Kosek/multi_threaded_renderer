// Copyright 2017 Tomasz Kosek. All rights reserved.

#include <engine\debug\Assert.hpp>

namespace engine {
namespace utilities {
namespace buffers {
INLINE Gpu_buffer::~Gpu_buffer() {
    unsetup();
}
INLINE void Gpu_buffer::setup(ID3D11Device* device,
    const D3D11_BUFFER_DESC& buffer_description) {
    description_ = buffer_description;
    HRESULT r = device->CreateBuffer(&buffer_description, nullptr, &buffer_);
    ENGINE_ASSERT(r == NOERROR, "Error while creating buffer!");
}
INLINE void Gpu_buffer::unsetup() {
    if (buffer_) {
        buffer_->Release();
    }
    buffer_ = nullptr;
}
INLINE unsigned int Gpu_buffer::get_byte_width() const {
    return description_.ByteWidth;
}
INLINE ID3D11Buffer*const* Gpu_buffer::get_buffer_ptr_ptr() const {
    return &buffer_;
}
}  // namespace buffers
}  // namespace utilities
}  // namespace engine

namespace engine {
namespace utilities {
namespace buffers {
INLINE Gpu_buffer::Writer::Writer(ID3D11DeviceContext* device_context,
    const Gpu_buffer& gpu_buffer) : device_context_(device_context), gpu_buffer_(gpu_buffer) {
    lock();
}
INLINE Gpu_buffer::Writer::~Writer() {
    unlock();
}
INLINE void Gpu_buffer::Writer::lock() {
    HRESULT r = device_context_->Map(gpu_buffer_.buffer_, 0, D3D11_MAP_WRITE_DISCARD, 0,
        &mapped_resource_);
    ENGINE_ASSERT(r == NOERROR, "Failed to lock buffer!");

    current_ptr_ = mapped_resource_.pData;
}
INLINE void Gpu_buffer::Writer::unlock() {
    device_context_->Unmap(gpu_buffer_.buffer_, 0);
    current_ptr_ = nullptr;
}
INLINE unsigned int Gpu_buffer::Writer::get_bytes_written() const {
    return static_cast<unsigned int>(static_cast<uint8_t*>(static_cast<void*>(current_ptr_)) -
        static_cast<uint8_t*>(mapped_resource_.pData));
}
INLINE unsigned int Gpu_buffer::Writer::get_bytes_left() const {
    return gpu_buffer_.get_byte_width() - get_bytes_written();
}
INLINE void Gpu_buffer::Writer::reset() {
    unlock();
    lock();
}
}  // namespace buffers
}  // namespace utilities
}  // namespace engine
