// Copyright 2017 Tomasz Kosek. All rights reserved.

#ifndef MULTI_THREADED_RENDERER_ENGINE_UTILITIES_BUFFERS_GPU_BUFFER_HPP_
#define MULTI_THREADED_RENDERER_ENGINE_UTILITIES_BUFFERS_GPU_BUFFER_HPP_

#include <d3d11.h>

#include <engine\utilities\memory\Raw_ptr.hpp>

namespace engine {
namespace utilities {
namespace buffers {
class Gpu_buffer : public Noncopyable {
 public:
    INLINE Gpu_buffer() = default;
    INLINE ~Gpu_buffer();
    INLINE void setup(ID3D11Device* device,
        const D3D11_BUFFER_DESC& buffer_description);
    INLINE void unsetup();
    INLINE unsigned int get_byte_width() const;
    INLINE ID3D11Buffer*const* get_buffer_ptr_ptr() const;

    class Writer final : public Noncopyable {
     public:
        INLINE Writer(ID3D11DeviceContext* device_context, const Gpu_buffer& gpu_buffer);
        INLINE ~Writer();
        template <class T>
        INLINE void write(const T& e);
        INLINE void reset();

     private:
        INLINE void lock();
        INLINE void unlock();
        INLINE unsigned int get_bytes_written() const;
        INLINE unsigned int get_bytes_left() const;

        const Gpu_buffer& gpu_buffer_;
        D3D11_MAPPED_SUBRESOURCE mapped_resource_;
        engine::utilities::memory::Raw_ptr<void> current_ptr_;
        engine::utilities::memory::Raw_ptr<ID3D11DeviceContext> device_context_;
    };

 private:
    D3D11_BUFFER_DESC description_;
    engine::utilities::memory::Raw_ptr<ID3D11Buffer> buffer_;
};
}  // namespace buffers
}  // namespace utilities
}  // namespace engine

#if !defined _DEBUG
#include <engine\utilities\buffers\Gpu_buffer.inl>
#endif

#include <engine\utilities\buffers\Gpu_buffer_header.inl>


#endif  // MULTI_THREADED_RENDERER_ENGINE_UTILITIES_BUFFERS_GPU_BUFFER_HPP_
