// Copyright 2017 Tomasz Kosek. All rights reserved.

namespace engine {
namespace utilities {
namespace memory {
template <class T>
INLINE Raw_ptr<T>::Raw_ptr(T* ptr) : ptr_(ptr) {
}
template <class T>
INLINE Raw_ptr<T>::Raw_ptr(Raw_ptr<T>&& from) {
    *this = from;
}
template <class T>
INLINE Raw_ptr<T>& Raw_ptr<T>::operator=(Raw_ptr<T>&& from) {
    ptr_ = from.ptr_;
    from.ptr_ = nullptr;
    return *this;
}
template <class T>
INLINE Raw_ptr<T>& Raw_ptr<T>::operator=(const Raw_ptr<T>& source) {
    ptr_ = source.ptr_;
    return *this;
}
template <class T>
INLINE Raw_ptr<T>& Raw_ptr<T>::operator=(T* ptr) {
    ptr_ = ptr;
    return *this;
}
template <class T>
template<class U>
INLINE Raw_ptr<T>& Raw_ptr<T>::operator=(U* ptr) {
    ptr_ = ptr;
    return *this;
}
template <class T>
INLINE T* Raw_ptr<T>::operator->() const {
    return get();
}
template <class T>
INLINE T* Raw_ptr<T>::get() const {
    return ptr_;
}
template <class T>
INLINE Raw_ptr<T>::operator T*() const {
    return ptr_;
}
template <class T>
INLINE T** Raw_ptr<T>::operator&() {  // NOLINT
    return &ptr_;
}
template <class T>
INLINE T*const* Raw_ptr<T>::operator&() const {  // NOLINT
    return &ptr_;
}
}  // namespace memory
}  // namespace utilities
}  // namespace engine
