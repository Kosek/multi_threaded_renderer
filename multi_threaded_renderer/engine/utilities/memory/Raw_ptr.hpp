// Copyright 2017 Tomasz Kosek. All rights reserved.

#ifndef MULTI_THREADED_RENDERER_ENGINE_UTILITIES_MEMORY_RAW_PTR_HPP_
#define MULTI_THREADED_RENDERER_ENGINE_UTILITIES_MEMORY_RAW_PTR_HPP_

namespace engine {
namespace utilities {
namespace memory {

template <class T>
class Raw_ptr final {
 public:
    INLINE Raw_ptr() = default;
    INLINE Raw_ptr(T* ptr);
    INLINE Raw_ptr(Raw_ptr<T>&& from);
    INLINE Raw_ptr(const Raw_ptr<T>&) = default;
    INLINE Raw_ptr<T>& operator=(Raw_ptr<T>&& from);
    INLINE Raw_ptr<T>& operator=(const Raw_ptr<T>& source);
    INLINE Raw_ptr<T>& operator=(T* ptr);
    template<class U>
    INLINE Raw_ptr<T>& operator=(U* ptr);
    INLINE T* operator->() const;
    INLINE operator T*() const;
    INLINE T** operator&();  // NOLINT
    INLINE T*const* operator&() const;  // NOLINT

 private:
    INLINE T* get() const;

    T* ptr_ = nullptr;
};

}  // namespace memory
}  // namespace utilities
}  // namespace engine

#include <engine\utilities\memory\Raw_ptr_header.inl>

#endif  // MULTI_THREADED_RENDERER_ENGINE_UTILITIES_MEMORY_RAW_PTR_HPP_
