// Copyright 2017 Tomasz Kosek. All rights reserved.

#include <stdafx.h>

#include <engine\game\Context.hpp>
#include <game\Cubes_position_updater_script.hpp>

#if defined _DEBUG
#include <game\Cubes_position_updater_script.inl>
#endif

namespace game {

void Cubes_position_updater_script::update(engine::game::Context* context) {
    const float rotation_speed_per_milli = (1.0f / 360.0f);
    for (int i = 0; i < cubes_.size(); ++i) {
        const Cubes_position_updater_script::Rotation& r = cubes_rotation_[i];
        cubes_[i]->get_node()->rotate(
            r.get_rotation() * r.get_rotation_speed() * rotation_speed_per_milli *
            context->get_current_frame().get_frame_time_millis());
    }
}

}  // namespace game
