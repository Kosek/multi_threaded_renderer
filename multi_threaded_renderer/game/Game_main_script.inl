// Copyright 2017 Tomasz Kosek. All rights reserved.

#include <array>
#include <memory>
#include <tuple>
#include <utility>
#include <vector>

#include <engine\rendering\Target.hpp>
#include <game\Cube_face_material_names.hpp>
#include <game\Cubes_position_updater_script.hpp>
#include <game\Textures_names.hpp>
#include <game\Textures_randomization_script.hpp>

namespace game {
INLINE Game_main_script::Game_main_script(ID3D11Device* device,
    engine::rendering::Target* target) : body_(std::make_unique<Body>(device, target)) {
}

INLINE Game_main_script::Body::Body(ID3D11Device* device, engine::rendering::Target* target)
    : device_(device), target_(target), scene_(MAIN_SCENE), space_(MAIN_SPACE), camera_(CAMERA),
    free_look_camera_script_entry_(FREE_LOOK_CAMERA_SCRIPT_ENTRY),
    textures_randomization_script_entry_(TEXTURES_RANDOMIZATION_SCRIPT_ENTRY),
    cubes_position_update_script_entry_(CUBES_POSITION_UPDATE_SCRIPT_ENTRY),
    load_overhead_simulation_script_entry_(LOAD_OVERHEAD_SIMULATION_SCRIPT_ENTRY) {
    ENGINE_ASSERT(device_, "Device must not be null!");
}

INLINE void Game_main_script::Body::start(engine::game::Context* context) {
    create_assets();
    create_and_setup_cubes();
    for (engine::scene3::Smart_entry& cube_entry : cubes_) {
        space_->add(cube_entry.get());
    }
    static_cast<Cubes_position_updater_script*>(cubes_position_update_script_.get())->setup();

    free_look_camera_script_entry_->set(free_look_camera_script_);
    textures_randomization_script_entry_->set(textures_randomization_script_);
    cubes_position_update_script_entry_->set(cubes_position_update_script_);
    load_overhead_simulation_script_entry_->set(load_overhead_simulation_script_);

    scene_->get_mutator().add(space_.get()).under(scene_->create_query().select_root());
    scene_->get_mutator().add(camera_.get_entry()).to(space_.get()).under(
        scene_->create_query().select_root());
    scene_->get_mutator().add(free_look_camera_script_entry_.get()).to(space_.get());
    scene_->get_mutator().add(textures_randomization_script_entry_.get()).to(space_.get());
    scene_->get_mutator().add(cubes_position_update_script_entry_.get()).to(space_.get());
    scene_->get_mutator().add(load_overhead_simulation_script_entry_.get()).to(space_.get());
    scene_->get_mutator().enable_update(MAIN_SPACE);
    scene_->get_mutator().enable_update(FREE_LOOK_CAMERA_SCRIPT_ENTRY);
    scene_->get_mutator().enable_update(TEXTURES_RANDOMIZATION_SCRIPT_ENTRY);
    scene_->get_mutator().enable_update(CUBES_POSITION_UPDATE_SCRIPT_ENTRY);
    scene_->get_mutator().enable_update(LOAD_OVERHEAD_SIMULATION_SCRIPT_ENTRY);
    context->add(scene_.get());
    context->enable_update(MAIN_SCENE);

    target_->set_camera(&camera_);
    camera_.get_camera_node()->set_position(XMFLOAT3{0, 0, -5});

    context->get_player()->set_window_that_mouse_coordinates_will_be_calculated_relative_to(
        target_->get_window());
}

INLINE void Game_main_script::Body::create_and_setup_cubes() {
    const unsigned int CUBES_COUNT_ON_THE_EDGE_OF_CUBE = 14;
    const unsigned int CUBES_COUNT = CUBES_COUNT_ON_THE_EDGE_OF_CUBE
        * CUBES_COUNT_ON_THE_EDGE_OF_CUBE * CUBES_COUNT_ON_THE_EDGE_OF_CUBE;
    cubes_ = create_cube_instances(cube_model_, CUBES_COUNT, textures_);
    static_cast<Textures_randomization_script*>(
        textures_randomization_script_.get())->assign_random_textures_to_cubes(cubes_, textures_);

    unsigned int i = 0;
    const float distance_between_centers_of_cubes = 13.0f;
    for (engine::scene3::Smart_entry& cube_entry : cubes_) {
        unsigned int column = i % CUBES_COUNT_ON_THE_EDGE_OF_CUBE;
        unsigned int row = (i / CUBES_COUNT_ON_THE_EDGE_OF_CUBE) % CUBES_COUNT_ON_THE_EDGE_OF_CUBE;
        unsigned int slice =
            (i / (CUBES_COUNT_ON_THE_EDGE_OF_CUBE * CUBES_COUNT_ON_THE_EDGE_OF_CUBE));

        cube_entry->set(std::make_unique<engine::scene3::Node>());
        cube_entry->get_node()->set_position(XMFLOAT3{ column * distance_between_centers_of_cubes,
            row * distance_between_centers_of_cubes, slice * distance_between_centers_of_cubes });

        ++i;
    }
}

INLINE std::vector<engine::scene3::Smart_entry> Game_main_script::Body::create_cube_instances(
    const engine::assets::Asset::Ptr<engine::assets3::Model>& cube, const int instances_count,
    const std::vector<engine::assets::Asset::Ptr<engine::assets3::Texture>>& textures) {
    std::vector<engine::scene3::Smart_entry> ret(instances_count);
    for (engine::scene3::Smart_entry& cube_entry : ret) {
        cube_entry->set(cube->create_instance());
    }
    return ret;
}

INLINE engine::assets::Asset::Ptr<engine::assets3::Model>
Game_main_script::Body::create_cube_model(ID3D11Device* device,
    const engine::assets::Asset::Ptr<engine::assets3::Material>& textured_material) {
    auto model = engine::assets::Asset::Ptr<engine::assets3::Model>::make_asset(CUBE);

    engine::assets3::Model::Mutator model_mutator = model->get_mutator(device);

    const XMVECTOR front_tl = XMVectorSet(-1, 1, -1, 0);
    const XMVECTOR front_tr = XMVectorSet(1, 1, -1, 0);
    const XMVECTOR front_br = XMVectorSet(1, -1, -1, 0);
    const XMVECTOR front_bl = XMVectorSet(-1, -1, -1, 0);

    const XMVECTOR back_tl = XMVectorSet(-1, 1, 1, 0);
    const XMVECTOR back_tr = XMVectorSet(1, 1, 1, 0);
    const XMVECTOR back_br = XMVectorSet(1, -1, 1, 0);
    const XMVECTOR back_bl = XMVectorSet(-1, -1, 1, 0);

    add_cube_face(model_mutator, textured_material, CUBE_FRONT_MATERIAL,
        front_tl, front_tr, front_br, front_bl);
    add_cube_face(model_mutator, textured_material, CUBE_BACK_MATERIAL,
        back_tr, back_tl, back_bl, back_br);
    add_cube_face(model_mutator, textured_material, CUBE_TOP_MATERIAL,
        front_tl, back_tl, back_tr, front_tr);
    add_cube_face(model_mutator, textured_material, CUBE_BOTTOM_MATERIAL,
        back_bl, front_bl, front_br, back_br);
    add_cube_face(model_mutator, textured_material, CUBE_LEFT_MATERIAL,
        back_tl, front_tl, front_bl, back_bl);
    add_cube_face(model_mutator, textured_material, CUBE_RIGHT_MATERIAL,
        front_tr, back_tr, back_br, front_br);

    model_mutator.commit();

    return model;
}

INLINE void Game_main_script::Body::add_cube_face(engine::assets3::Model::Mutator& model_mutator,
    const engine::assets::Asset::Ptr<engine::assets3::Material>& material,
    const engine::assets3::Model::Mesh_material::Name& mesh_material_name, const FXMVECTOR tl,
    const FXMVECTOR tr, const FXMVECTOR br, const FXMVECTOR bl) {
    const unsigned int HORIZONTAL_AND_VERTICAL_RECTANGLES_COUNT_INSIDE_THE_FACE = 14;

    std::tuple<Vertices, Indices> vertices_and_indices = create_cube_face_vertices_and_indices(
        tl, tr, br, bl, HORIZONTAL_AND_VERTICAL_RECTANGLES_COUNT_INSIDE_THE_FACE);

    const engine::assets3::Model::Mutator::Vertices_part* face =
        model_mutator.add<engine::vertex_stream::stride_schemas::Textured>(
            std::get<0>(vertices_and_indices))
        .with(std::move(std::get<1>(vertices_and_indices)))
        .to(CUBE_VB);
    model_mutator
        .set(material)
        .as(mesh_material_name)
        .to<engine::vertex_stream::stride_schemas::Textured>(face);
}

INLINE std::tuple<Game_main_script::Body::Vertices, Game_main_script::Body::Indices>
Game_main_script::Body::create_cube_face_vertices_and_indices(
    const FXMVECTOR tl, const FXMVECTOR tr, const FXMVECTOR br, const FXMVECTOR bl,
    const unsigned int horizontal_and_vertical_rectangles_count_inside_the_face) {
    const XMVECTOR vertex_vert_direction = XMVector3Normalize(bl - tl);
    const XMVECTOR vertex_horz_direction = XMVector3Normalize(tr - tl);
    const float vertex_step = XMVectorGetX(XMVector3Length(tr - tl)) /
        horizontal_and_vertical_rectangles_count_inside_the_face;
    const float texture_step = 1.0f / horizontal_and_vertical_rectangles_count_inside_the_face;
    const XMVECTOR texture_vert_direction = XMVectorSet(0, 1, 0, 0);

    auto create_row_of_vertices = [&horizontal_and_vertical_rectangles_count_inside_the_face,
        &vertex_step, &vertex_horz_direction, &texture_step](
            const FXMVECTOR vertex_start, const FXMVECTOR vertex_end,
            const FXMVECTOR texture_start, const FXMVECTOR texture_end) {
        auto v3 = [](const FXMVECTOR v) {
            XMFLOAT3 ret;
            XMStoreFloat3(&ret, v);
            return ret;
        };
        auto v2 = [](const FXMVECTOR v) {
            XMFLOAT2 ret;
            XMStoreFloat2(&ret, v);
            return ret;
        };
        const XMVECTOR texture_horz_direction = XMVectorSet(1, 0, 0, 0);

        Vertices ret;
        for (unsigned int hi = 0; hi < horizontal_and_vertical_rectangles_count_inside_the_face;
            ++hi) {
            ret.push_back({ v3(vertex_start + hi * vertex_step * vertex_horz_direction),
                v2(texture_start + hi * texture_step * texture_horz_direction) });
        }
        ret.push_back({ v3(vertex_end), v2(texture_end) });
        return ret;
    };

    auto create_indices_for_row = [&horizontal_and_vertical_rectangles_count_inside_the_face](
        const unsigned int row_number) {
        Indices ret;
        for (unsigned int rect_i = 0;
            rect_i < horizontal_and_vertical_rectangles_count_inside_the_face; ++rect_i) {
            const unsigned int number_of_vertices_in_row =
                horizontal_and_vertical_rectangles_count_inside_the_face + 1;
            const unsigned int tl = row_number * number_of_vertices_in_row + rect_i;
            const unsigned int tr = tl + 1;
            const unsigned int br = (row_number + 1) * number_of_vertices_in_row + rect_i + 1;
            const unsigned int bl = br - 1;
            Indices rectangle_indices{ tl, tr, br, tl, br, bl };
            ret.insert(ret.end(), rectangle_indices.begin(), rectangle_indices.end());
        }
        return ret;
    };

    std::tuple<Vertices, Indices> ret;

    for (unsigned int vi = 0; vi < horizontal_and_vertical_rectangles_count_inside_the_face;
        ++vi) {
        Vertices row = create_row_of_vertices(tl + vi * vertex_step * vertex_vert_direction,
            tr + vi * vertex_step * vertex_vert_direction,
            XMVectorSet(0, 0, 0, 0) + vi * texture_step * texture_vert_direction,
            XMVectorSet(1, 0, 0, 0) + vi * texture_step * texture_vert_direction);
        std::get<0>(ret).insert(std::get<0>(ret).end(), row.begin(), row.end());
    }
    Vertices row = create_row_of_vertices(bl, br, XMVectorSet(0, 1, 0, 0),
        XMVectorSet(1, 1, 0, 0));
    std::get<0>(ret).insert(std::get<0>(ret).end(), row.begin(), row.end());

    for (unsigned int vi = 0; vi < horizontal_and_vertical_rectangles_count_inside_the_face;
        ++vi) {
        Indices row_indices = create_indices_for_row(vi);
        std::get<1>(ret).insert(std::get<1>(ret).end(), row_indices.begin(), row_indices.end());
    }

    return ret;
}

INLINE void Game_main_script::Body::create_assets() {
    texture_default_ = engine::assets::Asset::Ptr<engine::assets3::Texture>::make_asset(
        GAME_ASSETS_TEXTURES_GREEN_JPG);
    texture_default_->load(device_);

    textures_ = load_textures(device_);
    std::random_device random_device;
    std::mt19937 mersenne_twister_engine(random_device());
    std::shuffle(textures_.begin(), textures_.end(), mersenne_twister_engine);

    material_textured_ = engine::assets3::materials::Textured::Builder(MATERIAL_TEXTURED)
        .set_texture(texture_default_)
        .build(device_);

    cube_model_ = create_cube_model(device_, material_textured_);

    free_look_camera_script_ =
        engine::assets::Asset::Ptr<game::Free_look_camera_script>::make_asset(
            FREE_LOOK_CAMERA_SCRIPT, &camera_);

    textures_randomization_script_ =
        engine::assets::Asset::Ptr<game::Textures_randomization_script>::make_asset(
            TEXTURES_RANDOMIZATION_SCRIPT, cubes_, textures_);

    cubes_position_update_script_ =
        engine::assets::Asset::Ptr<game::Cubes_position_updater_script>::make_asset(
            CUBES_POSITION_UPDATE_SCRIPT, cubes_);

    load_overhead_simulation_script_ =
        engine::assets::Asset::Ptr<game::Load_overhead_simulation_script>::make_asset(
            LOAD_OVERHEAD_SIMULATION_SCRIPT);
}

INLINE std::vector<engine::assets::Asset::Ptr<engine::assets3::Texture>>
Game_main_script::Body::load_textures(ID3D11Device* device) {
    std::vector<engine::assets::Asset::Ptr<engine::assets3::Texture>> ret(100);
    ret[0] = engine::assets::Asset::Ptr<engine::assets3::Texture>::make_asset(
        GAME_ASSETS_TEXTURES_TEXTURE0_JPG);
    ret[0]->load(device);
    ret[1] = engine::assets::Asset::Ptr<engine::assets3::Texture>::make_asset(
        GAME_ASSETS_TEXTURES_TEXTURE1_JPG);
    ret[1]->load(device);
    ret[2] = engine::assets::Asset::Ptr<engine::assets3::Texture>::make_asset(
        GAME_ASSETS_TEXTURES_TEXTURE2_JPG);
    ret[2]->load(device);
    ret[3] = engine::assets::Asset::Ptr<engine::assets3::Texture>::make_asset(
        GAME_ASSETS_TEXTURES_TEXTURE3_JPG);
    ret[3]->load(device);
    ret[4] = engine::assets::Asset::Ptr<engine::assets3::Texture>::make_asset(
        GAME_ASSETS_TEXTURES_TEXTURE4_JPG);
    ret[4]->load(device);
    ret[5] = engine::assets::Asset::Ptr<engine::assets3::Texture>::make_asset(
        GAME_ASSETS_TEXTURES_TEXTURE5_JPG);
    ret[5]->load(device);
    ret[6] = engine::assets::Asset::Ptr<engine::assets3::Texture>::make_asset(
        GAME_ASSETS_TEXTURES_TEXTURE6_JPG);
    ret[6]->load(device);
    ret[7] = engine::assets::Asset::Ptr<engine::assets3::Texture>::make_asset(
        GAME_ASSETS_TEXTURES_TEXTURE7_JPG);
    ret[7]->load(device);
    ret[8] = engine::assets::Asset::Ptr<engine::assets3::Texture>::make_asset(
        GAME_ASSETS_TEXTURES_TEXTURE8_JPG);
    ret[8]->load(device);
    ret[9] = engine::assets::Asset::Ptr<engine::assets3::Texture>::make_asset(
        GAME_ASSETS_TEXTURES_TEXTURE9_JPG);
    ret[9]->load(device);
    ret[10] = engine::assets::Asset::Ptr<engine::assets3::Texture>::make_asset(
        GAME_ASSETS_TEXTURES_TEXTURE10_JPG);
    ret[10]->load(device);
    ret[11] = engine::assets::Asset::Ptr<engine::assets3::Texture>::make_asset(
        GAME_ASSETS_TEXTURES_TEXTURE11_JPG);
    ret[11]->load(device);
    ret[12] = engine::assets::Asset::Ptr<engine::assets3::Texture>::make_asset(
        GAME_ASSETS_TEXTURES_TEXTURE12_JPG);
    ret[12]->load(device);
    ret[13] = engine::assets::Asset::Ptr<engine::assets3::Texture>::make_asset(
        GAME_ASSETS_TEXTURES_TEXTURE13_JPG);
    ret[13]->load(device);
    ret[14] = engine::assets::Asset::Ptr<engine::assets3::Texture>::make_asset(
        GAME_ASSETS_TEXTURES_TEXTURE14_JPG);
    ret[14]->load(device);
    ret[15] = engine::assets::Asset::Ptr<engine::assets3::Texture>::make_asset(
        GAME_ASSETS_TEXTURES_TEXTURE15_JPG);
    ret[15]->load(device);
    ret[16] = engine::assets::Asset::Ptr<engine::assets3::Texture>::make_asset(
        GAME_ASSETS_TEXTURES_TEXTURE16_JPG);
    ret[16]->load(device);
    ret[17] = engine::assets::Asset::Ptr<engine::assets3::Texture>::make_asset(
        GAME_ASSETS_TEXTURES_TEXTURE17_JPG);
    ret[17]->load(device);
    ret[18] = engine::assets::Asset::Ptr<engine::assets3::Texture>::make_asset(
        GAME_ASSETS_TEXTURES_TEXTURE18_JPG);
    ret[18]->load(device);
    ret[19] = engine::assets::Asset::Ptr<engine::assets3::Texture>::make_asset(
        GAME_ASSETS_TEXTURES_TEXTURE19_JPG);
    ret[19]->load(device);
    ret[20] = engine::assets::Asset::Ptr<engine::assets3::Texture>::make_asset(
        GAME_ASSETS_TEXTURES_TEXTURE20_JPG);
    ret[20]->load(device);
    ret[21] = engine::assets::Asset::Ptr<engine::assets3::Texture>::make_asset(
        GAME_ASSETS_TEXTURES_TEXTURE21_JPG);
    ret[21]->load(device);
    ret[22] = engine::assets::Asset::Ptr<engine::assets3::Texture>::make_asset(
        GAME_ASSETS_TEXTURES_TEXTURE22_JPG);
    ret[22]->load(device);
    ret[23] = engine::assets::Asset::Ptr<engine::assets3::Texture>::make_asset(
        GAME_ASSETS_TEXTURES_TEXTURE23_JPG);
    ret[23]->load(device);
    ret[24] = engine::assets::Asset::Ptr<engine::assets3::Texture>::make_asset(
        GAME_ASSETS_TEXTURES_TEXTURE24_JPG);
    ret[24]->load(device);
    ret[25] = engine::assets::Asset::Ptr<engine::assets3::Texture>::make_asset(
        GAME_ASSETS_TEXTURES_TEXTURE25_JPG);
    ret[25]->load(device);
    ret[26] = engine::assets::Asset::Ptr<engine::assets3::Texture>::make_asset(
        GAME_ASSETS_TEXTURES_TEXTURE26_JPG);
    ret[26]->load(device);
    ret[27] = engine::assets::Asset::Ptr<engine::assets3::Texture>::make_asset(
        GAME_ASSETS_TEXTURES_TEXTURE27_JPG);
    ret[27]->load(device);
    ret[28] = engine::assets::Asset::Ptr<engine::assets3::Texture>::make_asset(
        GAME_ASSETS_TEXTURES_TEXTURE28_JPG);
    ret[28]->load(device);
    ret[29] = engine::assets::Asset::Ptr<engine::assets3::Texture>::make_asset(
        GAME_ASSETS_TEXTURES_TEXTURE29_JPG);
    ret[29]->load(device);
    ret[30] = engine::assets::Asset::Ptr<engine::assets3::Texture>::make_asset(
        GAME_ASSETS_TEXTURES_TEXTURE30_JPG);
    ret[30]->load(device);
    ret[31] = engine::assets::Asset::Ptr<engine::assets3::Texture>::make_asset(
        GAME_ASSETS_TEXTURES_TEXTURE31_JPG);
    ret[31]->load(device);
    ret[32] = engine::assets::Asset::Ptr<engine::assets3::Texture>::make_asset(
        GAME_ASSETS_TEXTURES_TEXTURE32_JPG);
    ret[32]->load(device);
    ret[33] = engine::assets::Asset::Ptr<engine::assets3::Texture>::make_asset(
        GAME_ASSETS_TEXTURES_TEXTURE33_JPG);
    ret[33]->load(device);
    ret[34] = engine::assets::Asset::Ptr<engine::assets3::Texture>::make_asset(
        GAME_ASSETS_TEXTURES_TEXTURE34_JPG);
    ret[34]->load(device);
    ret[35] = engine::assets::Asset::Ptr<engine::assets3::Texture>::make_asset(
        GAME_ASSETS_TEXTURES_TEXTURE35_JPG);
    ret[35]->load(device);
    ret[36] = engine::assets::Asset::Ptr<engine::assets3::Texture>::make_asset(
        GAME_ASSETS_TEXTURES_TEXTURE36_JPG);
    ret[36]->load(device);
    ret[37] = engine::assets::Asset::Ptr<engine::assets3::Texture>::make_asset(
        GAME_ASSETS_TEXTURES_TEXTURE37_JPG);
    ret[37]->load(device);
    ret[38] = engine::assets::Asset::Ptr<engine::assets3::Texture>::make_asset(
        GAME_ASSETS_TEXTURES_TEXTURE38_JPG);
    ret[38]->load(device);
    ret[39] = engine::assets::Asset::Ptr<engine::assets3::Texture>::make_asset(
        GAME_ASSETS_TEXTURES_TEXTURE39_JPG);
    ret[39]->load(device);
    ret[40] = engine::assets::Asset::Ptr<engine::assets3::Texture>::make_asset(
        GAME_ASSETS_TEXTURES_TEXTURE40_JPG);
    ret[40]->load(device);
    ret[41] = engine::assets::Asset::Ptr<engine::assets3::Texture>::make_asset(
        GAME_ASSETS_TEXTURES_TEXTURE41_JPG);
    ret[41]->load(device);
    ret[42] = engine::assets::Asset::Ptr<engine::assets3::Texture>::make_asset(
        GAME_ASSETS_TEXTURES_TEXTURE42_JPG);
    ret[42]->load(device);
    ret[43] = engine::assets::Asset::Ptr<engine::assets3::Texture>::make_asset(
        GAME_ASSETS_TEXTURES_TEXTURE43_JPG);
    ret[43]->load(device);
    ret[44] = engine::assets::Asset::Ptr<engine::assets3::Texture>::make_asset(
        GAME_ASSETS_TEXTURES_TEXTURE44_JPG);
    ret[44]->load(device);
    ret[45] = engine::assets::Asset::Ptr<engine::assets3::Texture>::make_asset(
        GAME_ASSETS_TEXTURES_TEXTURE45_JPG);
    ret[45]->load(device);
    ret[46] = engine::assets::Asset::Ptr<engine::assets3::Texture>::make_asset(
        GAME_ASSETS_TEXTURES_TEXTURE46_JPG);
    ret[46]->load(device);
    ret[47] = engine::assets::Asset::Ptr<engine::assets3::Texture>::make_asset(
        GAME_ASSETS_TEXTURES_TEXTURE47_JPG);
    ret[47]->load(device);
    ret[48] = engine::assets::Asset::Ptr<engine::assets3::Texture>::make_asset(
        GAME_ASSETS_TEXTURES_TEXTURE48_JPG);
    ret[48]->load(device);
    ret[49] = engine::assets::Asset::Ptr<engine::assets3::Texture>::make_asset(
        GAME_ASSETS_TEXTURES_TEXTURE49_JPG);
    ret[49]->load(device);
    ret[50] = engine::assets::Asset::Ptr<engine::assets3::Texture>::make_asset(
        GAME_ASSETS_TEXTURES_TEXTURE50_JPG);
    ret[50]->load(device);
    ret[51] = engine::assets::Asset::Ptr<engine::assets3::Texture>::make_asset(
        GAME_ASSETS_TEXTURES_TEXTURE51_JPG);
    ret[51]->load(device);
    ret[52] = engine::assets::Asset::Ptr<engine::assets3::Texture>::make_asset(
        GAME_ASSETS_TEXTURES_TEXTURE52_JPG);
    ret[52]->load(device);
    ret[53] = engine::assets::Asset::Ptr<engine::assets3::Texture>::make_asset(
        GAME_ASSETS_TEXTURES_TEXTURE53_JPG);
    ret[53]->load(device);
    ret[54] = engine::assets::Asset::Ptr<engine::assets3::Texture>::make_asset(
        GAME_ASSETS_TEXTURES_TEXTURE54_JPG);
    ret[54]->load(device);
    ret[55] = engine::assets::Asset::Ptr<engine::assets3::Texture>::make_asset(
        GAME_ASSETS_TEXTURES_TEXTURE55_JPG);
    ret[55]->load(device);
    ret[56] = engine::assets::Asset::Ptr<engine::assets3::Texture>::make_asset(
        GAME_ASSETS_TEXTURES_TEXTURE56_JPG);
    ret[56]->load(device);
    ret[57] = engine::assets::Asset::Ptr<engine::assets3::Texture>::make_asset(
        GAME_ASSETS_TEXTURES_TEXTURE57_JPG);
    ret[57]->load(device);
    ret[58] = engine::assets::Asset::Ptr<engine::assets3::Texture>::make_asset(
        GAME_ASSETS_TEXTURES_TEXTURE58_JPG);
    ret[58]->load(device);
    ret[59] = engine::assets::Asset::Ptr<engine::assets3::Texture>::make_asset(
        GAME_ASSETS_TEXTURES_TEXTURE59_JPG);
    ret[59]->load(device);
    ret[60] = engine::assets::Asset::Ptr<engine::assets3::Texture>::make_asset(
        GAME_ASSETS_TEXTURES_TEXTURE60_JPG);
    ret[60]->load(device);
    ret[61] = engine::assets::Asset::Ptr<engine::assets3::Texture>::make_asset(
        GAME_ASSETS_TEXTURES_TEXTURE61_JPG);
    ret[61]->load(device);
    ret[62] = engine::assets::Asset::Ptr<engine::assets3::Texture>::make_asset(
        GAME_ASSETS_TEXTURES_TEXTURE62_JPG);
    ret[62]->load(device);
    ret[63] = engine::assets::Asset::Ptr<engine::assets3::Texture>::make_asset(
        GAME_ASSETS_TEXTURES_TEXTURE63_JPG);
    ret[63]->load(device);
    ret[64] = engine::assets::Asset::Ptr<engine::assets3::Texture>::make_asset(
        GAME_ASSETS_TEXTURES_TEXTURE64_JPG);
    ret[64]->load(device);
    ret[65] = engine::assets::Asset::Ptr<engine::assets3::Texture>::make_asset(
        GAME_ASSETS_TEXTURES_TEXTURE65_JPG);
    ret[65]->load(device);
    ret[66] = engine::assets::Asset::Ptr<engine::assets3::Texture>::make_asset(
        GAME_ASSETS_TEXTURES_TEXTURE66_JPG);
    ret[66]->load(device);
    ret[67] = engine::assets::Asset::Ptr<engine::assets3::Texture>::make_asset(
        GAME_ASSETS_TEXTURES_TEXTURE67_JPG);
    ret[67]->load(device);
    ret[68] = engine::assets::Asset::Ptr<engine::assets3::Texture>::make_asset(
        GAME_ASSETS_TEXTURES_TEXTURE68_JPG);
    ret[68]->load(device);
    ret[69] = engine::assets::Asset::Ptr<engine::assets3::Texture>::make_asset(
        GAME_ASSETS_TEXTURES_TEXTURE69_JPG);
    ret[69]->load(device);
    ret[70] = engine::assets::Asset::Ptr<engine::assets3::Texture>::make_asset(
        GAME_ASSETS_TEXTURES_TEXTURE70_JPG);
    ret[70]->load(device);
    ret[71] = engine::assets::Asset::Ptr<engine::assets3::Texture>::make_asset(
        GAME_ASSETS_TEXTURES_TEXTURE71_JPG);
    ret[71]->load(device);
    ret[72] = engine::assets::Asset::Ptr<engine::assets3::Texture>::make_asset(
        GAME_ASSETS_TEXTURES_TEXTURE72_JPG);
    ret[72]->load(device);
    ret[73] = engine::assets::Asset::Ptr<engine::assets3::Texture>::make_asset(
        GAME_ASSETS_TEXTURES_TEXTURE73_JPG);
    ret[73]->load(device);
    ret[74] = engine::assets::Asset::Ptr<engine::assets3::Texture>::make_asset(
        GAME_ASSETS_TEXTURES_TEXTURE74_JPG);
    ret[74]->load(device);
    ret[75] = engine::assets::Asset::Ptr<engine::assets3::Texture>::make_asset(
        GAME_ASSETS_TEXTURES_TEXTURE75_JPG);
    ret[75]->load(device);
    ret[76] = engine::assets::Asset::Ptr<engine::assets3::Texture>::make_asset(
        GAME_ASSETS_TEXTURES_TEXTURE76_JPG);
    ret[76]->load(device);
    ret[77] = engine::assets::Asset::Ptr<engine::assets3::Texture>::make_asset(
        GAME_ASSETS_TEXTURES_TEXTURE77_JPG);
    ret[77]->load(device);
    ret[78] = engine::assets::Asset::Ptr<engine::assets3::Texture>::make_asset(
        GAME_ASSETS_TEXTURES_TEXTURE78_JPG);
    ret[78]->load(device);
    ret[79] = engine::assets::Asset::Ptr<engine::assets3::Texture>::make_asset(
        GAME_ASSETS_TEXTURES_TEXTURE79_JPG);
    ret[79]->load(device);
    ret[80] = engine::assets::Asset::Ptr<engine::assets3::Texture>::make_asset(
        GAME_ASSETS_TEXTURES_TEXTURE80_JPG);
    ret[80]->load(device);
    ret[81] = engine::assets::Asset::Ptr<engine::assets3::Texture>::make_asset(
        GAME_ASSETS_TEXTURES_TEXTURE81_JPG);
    ret[81]->load(device);
    ret[82] = engine::assets::Asset::Ptr<engine::assets3::Texture>::make_asset(
        GAME_ASSETS_TEXTURES_TEXTURE82_JPG);
    ret[82]->load(device);
    ret[83] = engine::assets::Asset::Ptr<engine::assets3::Texture>::make_asset(
        GAME_ASSETS_TEXTURES_TEXTURE83_JPG);
    ret[83]->load(device);
    ret[84] = engine::assets::Asset::Ptr<engine::assets3::Texture>::make_asset(
        GAME_ASSETS_TEXTURES_TEXTURE84_JPG);
    ret[84]->load(device);
    ret[85] = engine::assets::Asset::Ptr<engine::assets3::Texture>::make_asset(
        GAME_ASSETS_TEXTURES_TEXTURE85_JPG);
    ret[85]->load(device);
    ret[86] = engine::assets::Asset::Ptr<engine::assets3::Texture>::make_asset(
        GAME_ASSETS_TEXTURES_TEXTURE86_JPG);
    ret[86]->load(device);
    ret[87] = engine::assets::Asset::Ptr<engine::assets3::Texture>::make_asset(
        GAME_ASSETS_TEXTURES_TEXTURE87_JPG);
    ret[87]->load(device);
    ret[88] = engine::assets::Asset::Ptr<engine::assets3::Texture>::make_asset(
        GAME_ASSETS_TEXTURES_TEXTURE88_JPG);
    ret[88]->load(device);
    ret[89] = engine::assets::Asset::Ptr<engine::assets3::Texture>::make_asset(
        GAME_ASSETS_TEXTURES_TEXTURE89_JPG);
    ret[89]->load(device);
    ret[90] = engine::assets::Asset::Ptr<engine::assets3::Texture>::make_asset(
        GAME_ASSETS_TEXTURES_TEXTURE90_JPG);
    ret[90]->load(device);
    ret[91] = engine::assets::Asset::Ptr<engine::assets3::Texture>::make_asset(
        GAME_ASSETS_TEXTURES_TEXTURE91_JPG);
    ret[91]->load(device);
    ret[92] = engine::assets::Asset::Ptr<engine::assets3::Texture>::make_asset(
        GAME_ASSETS_TEXTURES_TEXTURE92_JPG);
    ret[92]->load(device);
    ret[93] = engine::assets::Asset::Ptr<engine::assets3::Texture>::make_asset(
        GAME_ASSETS_TEXTURES_TEXTURE93_JPG);
    ret[93]->load(device);
    ret[94] = engine::assets::Asset::Ptr<engine::assets3::Texture>::make_asset(
        GAME_ASSETS_TEXTURES_TEXTURE94_JPG);
    ret[94]->load(device);
    ret[95] = engine::assets::Asset::Ptr<engine::assets3::Texture>::make_asset(
        GAME_ASSETS_TEXTURES_TEXTURE95_JPG);
    ret[95]->load(device);
    ret[96] = engine::assets::Asset::Ptr<engine::assets3::Texture>::make_asset(
        GAME_ASSETS_TEXTURES_TEXTURE96_JPG);
    ret[96]->load(device);
    ret[97] = engine::assets::Asset::Ptr<engine::assets3::Texture>::make_asset(
        GAME_ASSETS_TEXTURES_TEXTURE97_JPG);
    ret[97]->load(device);
    ret[98] = engine::assets::Asset::Ptr<engine::assets3::Texture>::make_asset(
        GAME_ASSETS_TEXTURES_TEXTURE98_JPG);
    ret[98]->load(device);
    ret[99] = engine::assets::Asset::Ptr<engine::assets3::Texture>::make_asset(
        GAME_ASSETS_TEXTURES_TEXTURE99_JPG);
    ret[99]->load(device);
    return ret;
}

}  // namespace game
