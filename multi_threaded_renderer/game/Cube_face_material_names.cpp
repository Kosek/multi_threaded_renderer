// Copyright 2017 Tomasz Kosek. All rights reserved.

#include <stdafx.h>

#include <game\Cube_face_material_names.hpp>

namespace game {

ENGINE_REGISTER_UNIQUE_NAME_OF_TYPE(
    engine::assets3::Model::Mesh_material::Name, CUBE_FRONT_MATERIAL);
ENGINE_REGISTER_UNIQUE_NAME_OF_TYPE(
    engine::assets3::Model::Mesh_material::Name, CUBE_BACK_MATERIAL);
ENGINE_REGISTER_UNIQUE_NAME_OF_TYPE(
    engine::assets3::Model::Mesh_material::Name, CUBE_TOP_MATERIAL);
ENGINE_REGISTER_UNIQUE_NAME_OF_TYPE(
    engine::assets3::Model::Mesh_material::Name, CUBE_BOTTOM_MATERIAL);
ENGINE_REGISTER_UNIQUE_NAME_OF_TYPE(
    engine::assets3::Model::Mesh_material::Name, CUBE_LEFT_MATERIAL);
ENGINE_REGISTER_UNIQUE_NAME_OF_TYPE(
    engine::assets3::Model::Mesh_material::Name, CUBE_RIGHT_MATERIAL);

}  // namespace game
