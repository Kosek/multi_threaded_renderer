// Copyright 2017 Tomasz Kosek. All rights reserved.

namespace game {

INLINE Load_overhead_simulation_script::Load_overhead_simulation_script(
    const engine::assets::Script::Name& name) : Script(name) {
}

}  // namespace game
