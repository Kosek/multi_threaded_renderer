// Copyright 2017 Tomasz Kosek. All rights reserved.

#include <vector>

namespace game {

INLINE Cubes_position_updater_script::Cubes_position_updater_script(
    const engine::assets::Script::Name& name, std::vector<engine::scene3::Smart_entry>& cubes)
    : Script(name), cubes_(cubes) {
}
INLINE void Cubes_position_updater_script::setup() {
    cubes_rotation_ = Cubes_position_updater_script::generate_random_rotations(
        static_cast<unsigned int>(cubes_.size()));
}
INLINE std::vector<Cubes_position_updater_script::Rotation>
Cubes_position_updater_script::generate_random_rotations(unsigned int count) {
    Cubes_position_updater_script::Rotation_randomizer rand;
    std::vector<Cubes_position_updater_script::Rotation> ret(count);
    for (Cubes_position_updater_script::Rotation& r : ret) {
        using Rotation = XMVECTOR;
        r = Cubes_position_updater_script::Rotation(Rotation{ rand.generate_angle(),
            rand.generate_angle(), rand.generate_angle(), 0.0f }, rand.generate_rotation_speed());
    }
    return ret;
}

}  // namespace game

namespace game {

INLINE Cubes_position_updater_script::Rotation::Rotation(const FXMVECTOR rotation,
    float rotation_speed) : rotation_(rotation), rotation_speed_(rotation_speed) {
}
INLINE const FXMVECTOR Cubes_position_updater_script::Rotation::get_rotation() const {
    return rotation_;
}
INLINE float Cubes_position_updater_script::Rotation::get_rotation_speed() const {
    return rotation_speed_;
}

}  // namespace game

namespace game {

INLINE Cubes_position_updater_script::Rotation_randomizer::Rotation_randomizer()
    : generator_(random_device_()), angle_range_(1, 360), rotation_speed_range_(1, 100) {
}
INLINE float Cubes_position_updater_script::Rotation_randomizer::generate_angle() {
    unsigned int rand = angle_range_(generator_);
    return static_cast<float>(rand) * (XM_PI / 180.0f);
}
INLINE float Cubes_position_updater_script::Rotation_randomizer::generate_rotation_speed() {
    return 0.005f * static_cast<float>(rotation_speed_range_(generator_));
}

}  // namespace game
