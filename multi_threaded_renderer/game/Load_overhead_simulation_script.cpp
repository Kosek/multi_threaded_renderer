// Copyright 2017 Tomasz Kosek. All rights reserved.

#include <stdafx.h>

#include <thread>  // NOLINT

#include <engine\global_defs\Load_simulation.hpp>
#include <game\Load_overhead_simulation_script.hpp>

#if defined _DEBUG
#include <game\Load_overhead_simulation_script.inl>
#endif

namespace game {

void Load_overhead_simulation_script::update(engine::game::Context* context) {
#if defined SIMULATE_LOAD_OVERHEAD_FOR_SIMULATION_THREAD
    std::this_thread::sleep_for(
        std::chrono::duration<float, std::milli>((1000.0f / 60.0f) * 0.7f));
#endif
}

}  // namespace game
