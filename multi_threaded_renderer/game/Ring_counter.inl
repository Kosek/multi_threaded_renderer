// Copyright 2017 Tomasz Kosek. All rights reserved.

namespace game {

INLINE Ring_counter::Ring_counter(unsigned int count) : count_(count) {
}
INLINE Ring_counter::operator unsigned int() const {
    return get();
}
INLINE Ring_counter Ring_counter::operator++(int) {
    Ring_counter ret = *this;
    counter_ = (counter_ + 1) % count_;
    return ret;
}
INLINE unsigned int Ring_counter::get() const {
    return counter_;
}

}  // namespace game
