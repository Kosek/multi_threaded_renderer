// Copyright 2017 Tomasz Kosek. All rights reserved.

#include <stdafx.h>

#include <vector>

#include <engine\assets3\materials\textured\Textured_material.hpp>
#include <engine\game\Context.hpp>
#include <engine\vertex_stream\stride_schemas\Textured.hpp>
#include <game\Game_main_script.hpp>

#if defined _DEBUG
#include <game\Game_main_script.inl>
#endif

namespace game {

ENGINE_REGISTER_UNIQUE_NAME_OF_TYPE(engine::assets::Script::Name, START_SCRIPT);
ENGINE_REGISTER_UNIQUE_NAME_OF_TYPE(engine::scene3::Space::Name, MAIN_SPACE);
ENGINE_REGISTER_UNIQUE_NAME_OF_TYPE(engine::scene3::Scene::Name, MAIN_SCENE);
ENGINE_REGISTER_UNIQUE_NAME_OF_TYPE(engine::assets3::Model::Name, CUBE);
ENGINE_REGISTER_UNIQUE_NAME_OF_TYPE(engine::vertex_stream::Vertex_buffer::Name, CUBE_VB);
ENGINE_REGISTER_UNIQUE_NAME_OF_TYPE_AS_PATH(engine::assets3::Texture::Name, GAME_ASSETS_TEXTURES_GREEN_JPG);  // NOLINT
ENGINE_REGISTER_UNIQUE_NAME_OF_TYPE(engine::assets3::Material::Name, MATERIAL_TEXTURED);
ENGINE_REGISTER_UNIQUE_NAME_OF_TYPE(engine::scene3::view::Camera::Name, CAMERA);
ENGINE_REGISTER_UNIQUE_NAME_OF_TYPE(engine::assets::Script::Name, FREE_LOOK_CAMERA_SCRIPT);
ENGINE_REGISTER_UNIQUE_NAME_OF_TYPE(engine::scene3::Entry::Name, FREE_LOOK_CAMERA_SCRIPT_ENTRY);
ENGINE_REGISTER_UNIQUE_NAME_OF_TYPE(engine::assets::Script::Name, TEXTURES_RANDOMIZATION_SCRIPT);
ENGINE_REGISTER_UNIQUE_NAME_OF_TYPE(engine::scene3::Entry::Name, TEXTURES_RANDOMIZATION_SCRIPT_ENTRY);  // NOLINT
ENGINE_REGISTER_UNIQUE_NAME_OF_TYPE(engine::assets::Script::Name, CUBES_POSITION_UPDATE_SCRIPT);
ENGINE_REGISTER_UNIQUE_NAME_OF_TYPE(engine::scene3::Entry::Name, CUBES_POSITION_UPDATE_SCRIPT_ENTRY);  // NOLINT
ENGINE_REGISTER_UNIQUE_NAME_OF_TYPE(engine::assets::Script::Name, LOAD_OVERHEAD_SIMULATION_SCRIPT);
ENGINE_REGISTER_UNIQUE_NAME_OF_TYPE(engine::scene3::Entry::Name, LOAD_OVERHEAD_SIMULATION_SCRIPT_ENTRY);  // NOLINT

void Game_main_script::start(engine::game::Context* context) {
    body_->start(context);
}

void Game_main_script::stop(engine::game::Context* context) {
    body_.reset();
}

}  // namespace game
