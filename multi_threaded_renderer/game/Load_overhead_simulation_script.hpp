// Copyright 2017 Tomasz Kosek. All rights reserved.

#ifndef MULTI_THREADED_RENDERER_GAME_LOAD_OVERHEAD_SIMULATION_SCRIPT_HPP_
#define MULTI_THREADED_RENDERER_GAME_LOAD_OVERHEAD_SIMULATION_SCRIPT_HPP_

#include <engine\assets\Script.hpp>

namespace game {

class Load_overhead_simulation_script final : public engine::assets::Script {
 public:
    INLINE Load_overhead_simulation_script(const engine::assets::Script::Name& name);
    void update(engine::game::Context* context) override;
};

}  // namespace game

#if !defined _DEBUG
#include <game\Load_overhead_simulation_script.inl>
#endif

#endif  // MULTI_THREADED_RENDERER_GAME_LOAD_OVERHEAD_SIMULATION_SCRIPT_HPP_
