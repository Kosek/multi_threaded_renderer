// Copyright 2017 Tomasz Kosek. All rights reserved.

#ifndef MULTI_THREADED_RENDERER_GAME_CUBES_POSITION_UPDATER_SCRIPT_HPP_
#define MULTI_THREADED_RENDERER_GAME_CUBES_POSITION_UPDATER_SCRIPT_HPP_

#include <random>
#include <vector>

#include <engine\assets\Script.hpp>
#include <engine\scene3\Smart_entry.hpp>

namespace game {

class Cubes_position_updater_script final : public engine::assets::Script {
 public:
    INLINE Cubes_position_updater_script(const engine::assets::Script::Name& name,
        std::vector<engine::scene3::Smart_entry>& cubes);  // NOLINT
    INLINE void setup();
    void update(engine::game::Context* context) override;

 private:
    class Rotation final {
     public:
        INLINE Rotation() = default;
        INLINE Rotation(const FXMVECTOR rotation, float rotation_speed);
        INLINE const FXMVECTOR get_rotation() const;
        INLINE float get_rotation_speed() const;

     private:
        XMVECTOR rotation_ = XMVectorSet(0, 0, 0, 0);
        float rotation_speed_ = 0.0f;
    };

    INLINE static std::vector<Rotation> generate_random_rotations(unsigned int count);

    class Rotation_randomizer final {
     public:
        INLINE Rotation_randomizer();
        INLINE float generate_angle();
        INLINE float generate_rotation_speed();

     private:
        std::random_device random_device_;
        std::mt19937 generator_;
        std::uniform_int_distribution<> angle_range_;
        std::uniform_int_distribution<> rotation_speed_range_;
    };

    std::vector<engine::scene3::Smart_entry>& cubes_;
    std::vector<Rotation> cubes_rotation_;
};

}  // namespace game

#if !defined _DEBUG
#include <game\Cubes_position_updater_script.inl>
#endif

#endif  // MULTI_THREADED_RENDERER_GAME_CUBES_POSITION_UPDATER_SCRIPT_HPP_
