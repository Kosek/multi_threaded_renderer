// Copyright 2017 Tomasz Kosek. All rights reserved.

#ifndef MULTI_THREADED_RENDERER_GAME_GAME_MAIN_SCRIPT_HPP_
#define MULTI_THREADED_RENDERER_GAME_GAME_MAIN_SCRIPT_HPP_

#include <memory>
#include <tuple>
#include <vector>

#include <engine\assets3\Model.hpp>
#include <engine\game\Main_script.hpp>
#include <engine\rendering\Target_fwd.hpp>
#include <engine\scene3\Smart_entry.hpp>
#include <engine\scene3\Smart_scene.hpp>
#include <engine\scene3\Smart_space.hpp>
#include <engine\scene3\view\Camera.hpp>
#include <engine\utilities\memory\Raw_ptr.hpp>
#include <engine\vertex_stream\stride_schemas\Textured.hpp>
#include <game\Free_look_camera_script.hpp>
#include <game\Load_overhead_simulation_script.hpp>

namespace game {

class Game_main_script final : public engine::game::Main_script {
 public:
    INLINE Game_main_script(ID3D11Device* device, engine::rendering::Target* target);
    void start(engine::game::Context* context) override;
    void stop(engine::game::Context* context) override;

 private:
    class Body final : public engine::Noncopyable {
     public:
        INLINE Body(ID3D11Device* device, engine::rendering::Target* target);
        INLINE void start(engine::game::Context* context);

     private:
        using Vertices = std::vector<engine::vertex_stream::stride_schemas::Textured::Stride>;
        using Indices = std::vector<unsigned int>;

        INLINE static engine::assets::Asset::Ptr<engine::assets3::Model> create_cube_model(
            ID3D11Device* device,
            const engine::assets::Asset::Ptr<engine::assets3::Material>& textured_material);
        INLINE static void add_cube_face(engine::assets3::Model::Mutator& model_mutator,  // NOLINT
            const engine::assets::Asset::Ptr<engine::assets3::Material>& material,
            const engine::assets3::Model::Mesh_material::Name& mesh_material_name,
            const FXMVECTOR& tl, const FXMVECTOR& tr, const FXMVECTOR& br,
            const FXMVECTOR& bl);
        INLINE static std::vector<engine::scene3::Smart_entry> create_cube_instances(
            const engine::assets::Asset::Ptr<engine::assets3::Model>& cube,
            const int instances_count,
            const std::vector<engine::assets::Asset::Ptr<engine::assets3::Texture>>& textures);
        INLINE static std::vector<engine::assets::Asset::Ptr<engine::assets3::Texture>>
            load_textures(ID3D11Device* device);
        INLINE void create_assets();
        INLINE void create_and_setup_cubes();
        INLINE std::tuple<Vertices, Indices> static create_cube_face_vertices_and_indices(
            const FXMVECTOR tl, const FXMVECTOR tr, const FXMVECTOR br, const FXMVECTOR bl,
            const unsigned int horizontal_and_vertical_rectangles_count_inside_the_face);

        engine::utilities::memory::Raw_ptr<ID3D11Device> device_;
        engine::utilities::memory::Raw_ptr<engine::rendering::Target> target_;
        engine::scene3::Smart_scene scene_;
        std::vector<engine::scene3::Smart_entry> cubes_;
        engine::scene3::Smart_space space_;
        engine::scene3::view::Camera camera_;
        std::vector<engine::assets::Asset::Ptr<engine::assets3::Texture>> textures_;
        engine::assets::Asset::Ptr<engine::assets3::Texture> texture_default_;
        engine::assets::Asset::Ptr<engine::assets3::Material> material_textured_;
        engine::assets::Asset::Ptr<engine::assets3::Model> cube_model_;
        engine::assets::Asset::Ptr<engine::assets::Script> free_look_camera_script_;
        engine::scene3::Smart_entry free_look_camera_script_entry_;
        engine::assets::Asset::Ptr<engine::assets::Script> textures_randomization_script_;
        engine::scene3::Smart_entry textures_randomization_script_entry_;
        engine::assets::Asset::Ptr<engine::assets::Script> cubes_position_update_script_;
        engine::scene3::Smart_entry cubes_position_update_script_entry_;
        engine::assets::Asset::Ptr<engine::assets::Script> load_overhead_simulation_script_;
        engine::scene3::Smart_entry load_overhead_simulation_script_entry_;
    };

    std::unique_ptr<Body> body_;
};

ENGINE_DECLARE_UNIQUE_NAME_OF_TYPE(engine::scene3::Scene::Name, MAIN_SCENE);
ENGINE_DECLARE_UNIQUE_NAME_OF_TYPE(engine::scene3::Space::Name, MAIN_SPACE);
ENGINE_DECLARE_UNIQUE_NAME_OF_TYPE(engine::assets3::Model::Name, CUBE);
ENGINE_DECLARE_UNIQUE_NAME_OF_TYPE(engine::vertex_stream::Vertex_buffer::Name, CUBE_VB);
ENGINE_DECLARE_UNIQUE_NAME_OF_TYPE(engine::assets3::Texture::Name, GAME_ASSETS_TEXTURES_GREEN_JPG);
ENGINE_DECLARE_UNIQUE_NAME_OF_TYPE(engine::assets3::Material::Name, MATERIAL_TEXTURED);
ENGINE_DECLARE_UNIQUE_NAME_OF_TYPE(engine::scene3::view::Camera::Name, CAMERA);
ENGINE_DECLARE_UNIQUE_NAME_OF_TYPE(engine::assets::Script::Name, FREE_LOOK_CAMERA_SCRIPT);
ENGINE_DECLARE_UNIQUE_NAME_OF_TYPE(engine::scene3::Entry::Name, FREE_LOOK_CAMERA_SCRIPT_ENTRY);
ENGINE_DECLARE_UNIQUE_NAME_OF_TYPE(engine::assets::Script::Name, TEXTURES_RANDOMIZATION_SCRIPT);
ENGINE_DECLARE_UNIQUE_NAME_OF_TYPE(engine::scene3::Entry::Name, TEXTURES_RANDOMIZATION_SCRIPT_ENTRY);  // NOLINT
ENGINE_DECLARE_UNIQUE_NAME_OF_TYPE(engine::assets::Script::Name, CUBES_POSITION_UPDATE_SCRIPT);
ENGINE_DECLARE_UNIQUE_NAME_OF_TYPE(engine::scene3::Entry::Name, CUBES_POSITION_UPDATE_SCRIPT_ENTRY);  // NOLINT
ENGINE_DECLARE_UNIQUE_NAME_OF_TYPE(engine::assets::Script::Name, LOAD_OVERHEAD_SIMULATION_SCRIPT);
ENGINE_DECLARE_UNIQUE_NAME_OF_TYPE(engine::scene3::Entry::Name, LOAD_OVERHEAD_SIMULATION_SCRIPT_ENTRY);  // NOLINT
}  // namespace game

#if !defined _DEBUG
#include <game\Game_main_script.inl>
#endif

#endif  // MULTI_THREADED_RENDERER_GAME_GAME_MAIN_SCRIPT_HPP_
