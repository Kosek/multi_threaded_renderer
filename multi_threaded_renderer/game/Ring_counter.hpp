// Copyright 2017 Tomasz Kosek. All rights reserved.

#ifndef MULTI_THREADED_RENDERER_GAME_RING_COUNTER_HPP_
#define MULTI_THREADED_RENDERER_GAME_RING_COUNTER_HPP_

namespace game {

class Ring_counter final {
 public:
     INLINE Ring_counter(unsigned int count);
     INLINE operator unsigned int() const;
     INLINE Ring_counter operator++(int);

 private:
     INLINE unsigned int get() const;

     unsigned int count_ = 0;
     unsigned int counter_ = 0;
};

}  // namespace game

#if !defined _DEBUG

#include <game\Ring_counter.inl>

#endif

#endif  // MULTI_THREADED_RENDERER_GAME_RING_COUNTER_HPP_
