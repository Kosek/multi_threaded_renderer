// Copyright 2017 Tomasz Kosek. All rights reserved.

#include <vector>

#include <engine\assets3\materials\textured\Textured_material.hpp>
#include <game\Cube_face_material_names.hpp>

namespace game {

INLINE Textures_randomization_script::Textures_randomization_script(
    const engine::assets::Script::Name& name,
    std::vector<engine::scene3::Smart_entry>& cubes,
    const std::vector<engine::assets::Asset::Ptr<engine::assets3::Texture>>& textures) :
    Script(name), cubes_(cubes), textures_(textures),
    current_texture_index_(static_cast<unsigned int>(textures.size())) {
}

INLINE void Textures_randomization_script::assign_random_textures_to_cubes(
    std::vector<engine::scene3::Smart_entry>& cubes,
    const std::vector<engine::assets::Asset::Ptr<engine::assets3::Texture>>& textures) {
    current_texture_index_ = Textures_randomization_script::assign_random_textures_to_cubes(cubes,
        textures, current_texture_index_, random_device_);
}

INLINE Ring_counter Textures_randomization_script::assign_random_textures_to_cubes(
    std::vector<engine::scene3::Smart_entry>& cubes,
    const std::vector<engine::assets::Asset::Ptr<engine::assets3::Texture>>& textures,
    const Ring_counter& start_texture_index, std::random_device& random_device) {
    std::mt19937 mersenne_twister_engine(random_device());
    Ring_counter ret = start_texture_index;
    for (engine::scene3::Smart_entry& cube_entry : cubes) {
        ret = assign_random_textures_to_cube(cube_entry->get_model_instance(),
            textures, ret, mersenne_twister_engine);
    }
    return ret;
}

INLINE Ring_counter Textures_randomization_script::assign_random_textures_to_cube(
    engine::assets3::Model::Instance* instance,
    const std::vector<engine::assets::Asset::Ptr<engine::assets3::Texture>>& textures,
    const Ring_counter& start_texture_index, std::mt19937& mersenne_twister_engine) {
    std::array<engine::assets3::Model::Mesh_material::Name, 6> faces_names{ CUBE_FRONT_MATERIAL,
        CUBE_BACK_MATERIAL, CUBE_TOP_MATERIAL, CUBE_BOTTOM_MATERIAL, CUBE_LEFT_MATERIAL,
        CUBE_RIGHT_MATERIAL};
    std::shuffle(faces_names.begin(), faces_names.end(), mersenne_twister_engine);

    Ring_counter ret = start_texture_index;
    for (const engine::assets3::Model::Mesh_material::Name& n : faces_names) {
        instance->configure<engine::assets3::materials::Textured>(n)
            .set_texture(textures[ret++]);
    }

    return ret;
}

}  // namespace game
