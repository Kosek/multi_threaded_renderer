// Copyright 2017 Tomasz Kosek. All rights reserved.

#ifndef MULTI_THREADED_RENDERER_GAME_FREE_LOOK_CAMERA_SCRIPT_HPP_
#define MULTI_THREADED_RENDERER_GAME_FREE_LOOK_CAMERA_SCRIPT_HPP_

#include <engine\assets\Script.hpp>
#include <engine\scene3\view\Camera.hpp>

namespace game {

class Free_look_camera_script final : public engine::assets::Script {
 public:
    INLINE Free_look_camera_script(const engine::assets::Script::Name& name,
        engine::scene3::view::Camera* camera);
    void update(engine::game::Context* context) override;

 private:
    engine::scene3::view::Camera* camera_ = nullptr;
};

}  // namespace game

#if !defined _DEBUG
#include <game\Free_look_camera_script.inl>
#endif

#endif  // MULTI_THREADED_RENDERER_GAME_FREE_LOOK_CAMERA_SCRIPT_HPP_
