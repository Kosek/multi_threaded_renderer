// Copyright 2017 Tomasz Kosek. All rights reserved.

#include <stdafx.h>

#include <engine\game\Context.hpp>
#include <game\Textures_randomization_script.hpp>

#if defined _DEBUG
#include <game\Textures_randomization_script.inl>
#endif

namespace game {

void Textures_randomization_script::update(engine::game::Context* context) {
    sum_of_frame_time_ += context->get_current_frame().get_frame_time_millis();
    unsigned int seconds_count = static_cast<unsigned int>(sum_of_frame_time_ / 1000);
    if (seconds_count >= 1) {
        sum_of_frame_time_ -= seconds_count * 1000;
        assign_random_textures_to_cubes(cubes_, textures_);
    }
}

}  // namespace game
