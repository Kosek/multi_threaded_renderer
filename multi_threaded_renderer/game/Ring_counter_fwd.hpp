// Copyright 2017 Tomasz Kosek. All rights reserved.

#ifndef MULTI_THREADED_RENDERER_GAME_RING_COUNTER_FWD_HPP_
#define MULTI_THREADED_RENDERER_GAME_RING_COUNTER_FWD_HPP_

namespace game {

class Ring_counter;

}  // namespace game

#endif  // MULTI_THREADED_RENDERER_GAME_RING_COUNTER_FWD_HPP_
