// Copyright 2017 Tomasz Kosek. All rights reserved.

namespace game {

INLINE Free_look_camera_script::Free_look_camera_script(const engine::assets::Script::Name& name,
    engine::scene3::view::Camera* camera) : Script(name), camera_(camera) {
}

}  // namespace game
