// Copyright 2017 Tomasz Kosek. All rights reserved.

#include <stdafx.h>

#include <vector>

#include <engine\game\Context.hpp>
#include <game\Free_look_camera_script.hpp>

#if defined _DEBUG
#include <game\Free_look_camera_script.inl>
#endif

namespace game {

void Free_look_camera_script::update(engine::game::Context* context) {
    const float camera_speed_per_milli = 0.02f;
    using Key_type = engine::game::player::input::source::keyboard::key::Type;
    using Key_state = engine::game::player::input::source::keyboard::key::State;
    if (context->get_player()->get_input_controllers_state().get_state<Key_type::W>() ==
        Key_state::PRESSED) {
        auto c = camera_->get_calculator<engine::scene3::view::Camera::Calculator::Local>();
        camera_->get_camera_node()->translate(c.calculate_normalized_look_direction()
            * camera_speed_per_milli * context->get_current_frame().get_frame_time_millis());
    }
    if (context->get_player()->get_input_controllers_state().get_state<Key_type::S>() ==
        Key_state::PRESSED) {
        auto c = camera_->get_calculator<engine::scene3::view::Camera::Calculator::Local>();
        camera_->get_camera_node()->translate(-c.calculate_normalized_look_direction()
            * camera_speed_per_milli * context->get_current_frame().get_frame_time_millis());
    }
    if (context->get_player()->get_input_controllers_state().get_state<Key_type::A>() ==
        Key_state::PRESSED) {
        auto c = camera_->get_calculator<engine::scene3::view::Camera::Calculator::Local>();
        camera_->get_camera_node()->translate(c.calculate_normalized_left_vector()
            * camera_speed_per_milli * context->get_current_frame().get_frame_time_millis());
    }
    if (context->get_player()->get_input_controllers_state().get_state<Key_type::D>() ==
        Key_state::PRESSED) {
        auto c = camera_->get_calculator<engine::scene3::view::Camera::Calculator::Local>();
        camera_->get_camera_node()->translate(-c.calculate_normalized_left_vector()
            * camera_speed_per_milli * context->get_current_frame().get_frame_time_millis());
    }
    if (context->get_player()->get_input_controllers_state().get_state<Key_type::SPACE>() ==
        Key_state::PRESSED) {
        auto c = camera_->get_calculator<engine::scene3::view::Camera::Calculator::Local>();
        camera_->get_camera_node()->translate(c.calculate_normalized_up_vector()
            * camera_speed_per_milli * context->get_current_frame().get_frame_time_millis());
    } else if (context->get_player()->get_input_controllers_state().get_state<Key_type::LCTRL>() ==
        Key_state::PRESSED) {
        auto c = camera_->get_calculator<engine::scene3::view::Camera::Calculator::Local>();
        camera_->get_camera_node()->translate(-c.calculate_normalized_up_vector()
            * camera_speed_per_milli * context->get_current_frame().get_frame_time_millis());
    }
}

}  // namespace game
