// Copyright 2017 Tomasz Kosek. All rights reserved.

#ifndef MULTI_THREADED_RENDERER_GAME_CUBE_FACE_MATERIAL_NAMES_HPP_
#define MULTI_THREADED_RENDERER_GAME_CUBE_FACE_MATERIAL_NAMES_HPP_

#include <engine\assets3\Model.hpp>
#include <engine\naming\Unique_name.hpp>

namespace game {

ENGINE_DECLARE_UNIQUE_NAME_OF_TYPE(
    engine::assets3::Model::Mesh_material::Name, CUBE_FRONT_MATERIAL);
ENGINE_DECLARE_UNIQUE_NAME_OF_TYPE(
    engine::assets3::Model::Mesh_material::Name, CUBE_BACK_MATERIAL);
ENGINE_DECLARE_UNIQUE_NAME_OF_TYPE(
    engine::assets3::Model::Mesh_material::Name, CUBE_TOP_MATERIAL);
ENGINE_DECLARE_UNIQUE_NAME_OF_TYPE(
    engine::assets3::Model::Mesh_material::Name, CUBE_BOTTOM_MATERIAL);
ENGINE_DECLARE_UNIQUE_NAME_OF_TYPE(
    engine::assets3::Model::Mesh_material::Name, CUBE_LEFT_MATERIAL);
ENGINE_DECLARE_UNIQUE_NAME_OF_TYPE(
    engine::assets3::Model::Mesh_material::Name, CUBE_RIGHT_MATERIAL);

}  // namespace game

#endif  // MULTI_THREADED_RENDERER_GAME_CUBE_FACE_MATERIAL_NAMES_HPP_
