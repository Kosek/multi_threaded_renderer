// Copyright 2017 Tomasz Kosek. All rights reserved.

#ifndef MULTI_THREADED_RENDERER_GAME_TEXTURES_RANDOMIZATION_SCRIPT_HPP_
#define MULTI_THREADED_RENDERER_GAME_TEXTURES_RANDOMIZATION_SCRIPT_HPP_

#include <random>
#include <vector>

#include <engine\assets\Script.hpp>
#include <engine\scene3\Smart_entry.hpp>
#include <game\Ring_counter.hpp>

namespace game {

class Textures_randomization_script final : public engine::assets::Script {
 public:
    INLINE Textures_randomization_script(const engine::assets::Script::Name& name,
        std::vector<engine::scene3::Smart_entry>& cubes,  // NOLINT
        const std::vector<engine::assets::Asset::Ptr<engine::assets3::Texture>>& textures);
    void update(engine::game::Context* context) override;
    INLINE void assign_random_textures_to_cubes(std::vector<engine::scene3::Smart_entry>& cubes,  // NOLINT
        const std::vector<engine::assets::Asset::Ptr<engine::assets3::Texture>>& textures);

 private:
    INLINE static Ring_counter assign_random_textures_to_cubes(
        std::vector<engine::scene3::Smart_entry>& cubes,  // NOLINT
        const std::vector<engine::assets::Asset::Ptr<engine::assets3::Texture>>& textures,
        const Ring_counter& start_texture_index, std::random_device& random_device);  // NOLINT
    INLINE static Ring_counter assign_random_textures_to_cube(
        engine::assets3::Model::Instance* instance,
        const std::vector<engine::assets::Asset::Ptr<engine::assets3::Texture>>& textures,
        const Ring_counter& start_texture_index, std::mt19937& mersenne_twister_engine);  // NOLINT

    std::vector<engine::scene3::Smart_entry>& cubes_;
    const std::vector<engine::assets::Asset::Ptr<engine::assets3::Texture>>& textures_;
    float sum_of_frame_time_ = 0;
    Ring_counter current_texture_index_;
    std::random_device random_device_;
};

}  // namespace game

#if !defined _DEBUG
#include <game\Textures_randomization_script.inl>
#endif

#endif  // MULTI_THREADED_RENDERER_GAME_TEXTURES_RANDOMIZATION_SCRIPT_HPP_
