﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace cpplint_formatter
{
    class Program
    {
        static void Main(string[] args)
        {
            Cpplint_formatter f = new Cpplint_formatter(@"..\..\..\multi_threaded_renderer");
            f.format();
            Cpplint_formatter tests_formatter = new Cpplint_formatter(
                @"..\..\..\multi_threaded_renderer_tests\engine");
            tests_formatter.format();
        }
    }
}
