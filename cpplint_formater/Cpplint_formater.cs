﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Text.RegularExpressions;

namespace cpplint_formatter
{
    public class Cpplint_formatter
    {
        public Cpplint_formatter(string dir_path)
        {
            dir_path_ = dir_path;
        }

        public void format()
        {
            List<string> file_paths = generate_files_list(dir_path_);
            List<string> tko_bad_files = new List<string>();
            foreach(string fp in file_paths)
            {
                insert_copyright_info_at_the_beginning(fp);
                remove_spaces_at_the_end_of_lines(fp);
                add_namespace_comments(fp);
                replace_pragma_once_with_if_def(dir_path_, fp);
                remove_and_add_blank_lines_for_access_key_word(fp, Access_key_word.PRIVATE);
                remove_and_add_blank_lines_for_access_key_word(fp, Access_key_word.PROTECTED);
                remove_and_add_blank_lines_for_access_key_word(fp, Access_key_word.PUBLIC);
                make_reorder_includes_to_valid_order(fp);
                move_open_braces_to_end_of_previous_line(fp);
                move_else_to_the_same_line_as_preceding_close_bracket(fp);
                move_comments_at_2_spaces_from_code(fp);
                move_inl_includes_in_cpp_files_just_after_include_section(fp);
                add_new_line_at_the_end(fp);
            }
        }

        private static void insert_copyright_info_at_the_beginning(string file_path)
        {
            string file_content = File.ReadAllText(file_path);
            char[] spaceAndNewLine = { ' ', '\n', '\r' };
            file_content = file_content.TrimStart(spaceAndNewLine);

            StringBuilder result_content = new StringBuilder();
            StringReader sr = new StringReader(file_content);
            string line = sr.ReadLine();
            if (line != null && !line.Contains(COPYRIGHT_INFO_TEXT))
            {
                result_content.AppendLine(COPYRIGHT_INFO_TEXT);
                result_content.AppendLine();
                result_content.AppendLine(file_content);
                File.WriteAllText(file_path, result_content.ToString());
            }
        }

        private static void remove_spaces_at_the_end_of_lines(string file_path)
        {
            string file_content = File.ReadAllText(file_path);
            char[] space = { ' ' };
            bool was_file_changed = false;
            StringBuilder result_content = new StringBuilder();
            StringReader sr = new StringReader(file_content);
            string line = sr.ReadLine();
            while (line != null)
            {
                string tmp_line = String.Copy(line);
                line = line.TrimEnd(space);
                result_content.AppendLine(line);
                was_file_changed |= tmp_line.Length != line.Length;
                line = sr.ReadLine();
            }
            if (was_file_changed)
            {
                File.WriteAllText(file_path, result_content.ToString());
            }
        }

        private class Namespace_info
        {
            public Namespace_info(int level, string name)
            {
                level_ = level;
                name_ = name;
            }

            public int get_level()
            {
                return level_;
            }

            public string get_name()
            {
                return name_;
            }

            private int level_ = -1;
            private string name_ = null;
        }

        private class File_content_reader
        {
            public File_content_reader(string file_path)
            {
                file_content_ = File.ReadAllText(file_path);
                sr_ = new StringReader(file_content_);
            }

            public Token next_token()
            {
                if (was_eof_reached_)
                {
                    return Token.TOKEN_EOF;
                }
                if (char_no_ == -2)
                {
                    current_line_ = sr_.ReadLine();
                    if (current_line_ == null)
                    {
                        was_eof_reached_ = true;
                        return Token.TOKEN_EOF;
                    }
                    else
                    {
                        char_no_ = -1;
                        return Token.TOKEN_JUST_BEFORE_FIRST_CHAR_OF_NEW_LINE;
                    }
                }
                else
                {
                    ++char_no_;
                }

                if (char_no_ >= current_line_.Length)
                {
                    char_no_ = -2;
                    in_line_comment_ = false;
                    return next_token();
                }
                else
                {
                    if (in_range_comment_)
                    {
                        if (is_range_comment_end(current_line_, char_no_))
                        {
                            in_range_comment_ = false;
                            return Token.TOKEN_IN_COMMENT_SECTION;
                        }
                        else
                        {
                            return Token.TOKEN_IN_COMMENT_SECTION;
                        }
                    }
                    else if (in_line_comment_)
                    {
                        return Token.TOKEN_IN_COMMENT_SECTION;
                    }
                    else if (is_range_comment_begin(current_line_, char_no_))
                    {
                        in_range_comment_ = true;
                        return Token.TOKEN_IN_COMMENT_SECTION;
                    }
                    else if (is_line_comment(current_line_, char_no_))
                    {
                        in_line_comment_ = true;
                        return Token.TOKEN_IN_COMMENT_SECTION;
                    }
                    else if (current_line_[char_no_] == '{')
                    {
                        return Token.TOKEN_OPEN_BRACKET;
                    }
                    else if (current_line_[char_no_] == '}')
                    {
                        return Token.TOKEN_CLOSE_BRACKET;
                    }
                    else
                    {
                        return Token.TOKEN_UNKNOWN;
                    }
                }
            }

            public string get_current_line()
            {
                return current_line_;
            }

            public enum Token
            {
                TOKEN_IN_COMMENT_SECTION,
                TOKEN_OPEN_BRACKET,
                TOKEN_CLOSE_BRACKET,
                TOKEN_EOF,
                TOKEN_UNKNOWN,
                TOKEN_JUST_BEFORE_FIRST_CHAR_OF_NEW_LINE
            }

            private static bool is_range_comment_begin(string line, int char_index)
            {
                return (line[char_index] == '/' && char_index + 1 < line.Length && line[char_index + 1] == '*');
            }

            private static bool is_range_comment_end(string line, int char_index)
            {
                return (line[char_index] == '*' && char_index + 1 < line.Length && line[char_index + 1] == '/');
            }

            private static bool is_line_comment(string line, int char_index)
            {
                return (line[char_index] == '/' && char_index + 1 < line.Length && line[char_index + 1] == '/');
            }

            private static bool is_new_line(string line, int char_index)
            {
                return (line[char_index] == '\r' && char_index + 1 < line.Length && line[char_index + 1] == '\n');
            }

            private string file_content_ = null;
            private StringReader sr_ = null;
            private int char_no_ = -2;
            private string current_line_ = null;
            private bool was_eof_reached_ = false;
            private bool in_range_comment_ = false;
            private bool in_line_comment_ = false;
        }

        private static void add_namespace_comments(string file_path)
        {
            string file_content = File.ReadAllText(file_path);
            bool was_file_changed = false;
            Stack<Namespace_info> namespaces = new Stack<Namespace_info>();
            List<string> lines = new List<string>();
            int parentises_level = 0;

            File_content_reader fcr = new File_content_reader(file_path);
            File_content_reader.Token token = File_content_reader.Token.TOKEN_UNKNOWN;
            while ((token = fcr.next_token()) != File_content_reader.Token.TOKEN_EOF)
            {
                switch (token)
                {
                    case File_content_reader.Token.TOKEN_OPEN_BRACKET:
                        {
                            ++parentises_level;

                            MatchCollection matches = Regex.Matches(fcr.get_current_line(), @"namespace \w+");
                            if (matches.Count > 0)
                            {
                                if (matches.Count != 1)
                                {
                                    throw new Exception("Invalid namespace!");
                                }
                                namespaces.Push(
                                    new Namespace_info(parentises_level,
                                        matches[0].ToString().Replace("namespace ", "")));
                            }
                        }
                        break;
                    case File_content_reader.Token.TOKEN_CLOSE_BRACKET:
                        {
                            --parentises_level;

                            if (namespaces.Count > 0 && (parentises_level == (namespaces.Peek().get_level() - 1)))
                            {
                                int last_line_index = lines.Count - 1;
                                if (!lines[last_line_index].Contains("namespace"))
                                {
                                    lines[last_line_index] += "  // namespace " + namespaces.Peek().get_name();
                                    was_file_changed = true;
                                }

                                namespaces.Pop();
                            }
                        }
                        break;
                    case File_content_reader.Token.TOKEN_JUST_BEFORE_FIRST_CHAR_OF_NEW_LINE:
                        {
                            lines.Add(fcr.get_current_line());
                        }
                        break;
                }
            }

            if (namespaces.Count > 0)
            {
                throw new Exception("Namespaces do not match namespace } closures!");
            }

            if (was_file_changed)
            {
                save_file(file_path, lines);
            }
        }

        private static void replace_pragma_once_with_if_def(string dir_path, string file_path)
        {
            string file_content = File.ReadAllText(file_path);
            const string PRAGMA_ONCE = "#pragma once";
            if (!file_content.Contains(PRAGMA_ONCE))
            {
                return;
            }

            if (Path.GetExtension(file_path) != ".hpp")
            {
                throw new Exception("\"" + PRAGMA_ONCE + "\" was placed in non-header file, file_path=" + file_path + "!");
            }

            string[] splitted_dir_path = dir_path.Split('\\');
            string[] splitted_file_path = file_path.Split('\\');
            string file_name = splitted_file_path[splitted_file_path.Length - 1];
            string if_def_guard_name = file_name.Replace(".", "_").ToUpper() + "_";
            for (int i = splitted_file_path.Length - 2; i >= splitted_dir_path.Length - 1; --i)
            {
                if_def_guard_name = splitted_file_path[i].ToUpper() + "_" + if_def_guard_name;
            }

            List<string> lines = new List<string>();
            int pragma_once_line_index = -1;

            StringReader sr = new StringReader(file_content);
            string line = sr.ReadLine();
            while (line != null)
            {
                if (line == PRAGMA_ONCE)
                {
                    if (pragma_once_line_index != -1)
                    {
                        throw new Exception("File contains more than one \"#pragma once\" statement!");
                    }

                    pragma_once_line_index = lines.Count;
                }
                lines.Add(line);
                line = sr.ReadLine();
            }

            lines.RemoveAt(pragma_once_line_index);
            lines.Insert(pragma_once_line_index, "#ifndef " + if_def_guard_name);
            lines.Insert(pragma_once_line_index + 1, "#define " + if_def_guard_name);
            int li = pragma_once_line_index + 2;
            while (li < lines.Count && string.Copy(lines[li]).Trim() == "")
            {
                lines.RemoveAt(li);
            }
            li = pragma_once_line_index - 1;
            while (li >= 0 && string.Copy(lines[li]).Trim() == "")
            {
                lines.RemoveAt(li);
                --li;
                --pragma_once_line_index;
            }
            if (pragma_once_line_index + 2 < lines.Count)
            {
                lines.Insert(pragma_once_line_index + 2, "");
            }
            if (pragma_once_line_index - 1 < lines.Count)
            {
                lines.Insert(pragma_once_line_index, "");
            }
            lines.Add("");
            lines.Add("#endif  // " + if_def_guard_name);

            save_file(file_path, lines);
        }

        public enum Access_key_word
        {
            PRIVATE,
            PROTECTED,
            PUBLIC
        }

        private static void remove_and_add_blank_lines_for_access_key_word(string file_path,
            Access_key_word access_key_word)
        {
            if (!file_path.Contains(".hpp"))
            {
                return;
            }

            string key_word = null;
            switch (access_key_word)
            {
                case Access_key_word.PRIVATE:
                    key_word = "private:";
                    break;
                case Access_key_word.PROTECTED:
                    key_word = "protected:";
                    break;
                case Access_key_word.PUBLIC:
                    key_word = "public:";
                    break;
            }

            List<string> lines = read_file(file_path);

            string prev_line = null;
            for (int li = 0; li < lines.Count; ++li)
            {
                string line = lines[li];
                if (string.Copy(line).Trim() == key_word)
                {
                    if (prev_line == null)
                    {
                        throw new Exception("Access key word '" + key_word +
                            "' was detected at the begining of the file!");
                    }
                    if (string.Copy(prev_line).Trim() != "" && !prev_line.Contains("class")
                        && !prev_line.Contains("{"))
                    {
                        lines.Insert(li, "");
                        prev_line = "";
                        ++li;
                    }
                    string next_line = null;
                    int next_line_index = li + 1;
                    if (next_line_index < lines.Count)
                    {
                        next_line = lines[next_line_index];
                    }
                    if (next_line == null)
                    {
                        throw new Exception("Access key word '" + key_word +
                            "' was detected at the end of the file!");
                    }
                    while (string.Copy(next_line).Trim() == "")
                    {
                        lines.RemoveAt(next_line_index);
                        if (next_line_index < lines.Count)
                        {
                            next_line = lines[next_line_index];
                        }
                        else
                        {
                            next_line = null;
                        }
                    }
                }
                prev_line = line;
            }

            save_file(file_path, lines);
        }

        private class Include_info
        {
            public enum Type
            {
                STDAFX = 0,
                SYSTEM = 1,
                APP_H = 2,
                APP_HPP = 3,
                UNDEFINED = 4
            }

            public Include_info(Type include_type, string line)
            {
                type_ = include_type;
                line_ = line;
            }

            public Type get_type()
            {
                return type_;
            }

            public string get_line()
            {
                return line_;
            }

            private Type type_ = Type.UNDEFINED;
            private string line_ = null;
        }

        private static bool contains(List<Include_info> includes, string line)
        {
            int i = 0;
            while (i < includes.Count && includes[i].get_line().CompareTo(line) != 0)
            {
                ++i;
            }
            return i < includes.Count;
        }

        private static void make_reorder_includes_to_valid_order(string file_path)
        {
            string file_content = File.ReadAllText(file_path);
            List<string> lines = read_file(file_path);

            int li = 0;
            while (li < lines.Count && !lines[li].Contains("#include"))
            {
                ++li;
            }

            int include_section_start_index = li;
            int include_section_after_end_index = -1;
            List<Include_info> includes = new List<Include_info>();
            if (include_section_start_index < lines.Count)
            {
                string line = lines[include_section_start_index];
                while (li < lines.Count && (line.Contains("#include") || string.Copy(line).Trim() == ""))
                {
                    if (string.Copy(line).Trim() != "")
                    {
                        if (line.Contains("stdafx"))
                        {
                            includes.Add(new Include_info(Include_info.Type.STDAFX, string.Copy(line)));
                        }
                        else if (line.Contains(".h\""))
                        {
                            includes.Add(new Include_info(Include_info.Type.APP_H, string.Copy(line)));
                        }
                        else if (line.Contains("hpp") || line.Contains("hlsl"))
                        {
                            includes.Add(new Include_info(Include_info.Type.APP_HPP, string.Copy(line)));
                        }
                        else
                        {
                            includes.Add(new Include_info(Include_info.Type.SYSTEM, string.Copy(line)));
                        }
                    }
                    ++li;
                    if (li < lines.Count)
                    {
                        line = lines[li];
                    }
                }
                include_section_after_end_index = li;
            }
            else
            {
                include_section_start_index = 0;
                StringReader sr2 = new StringReader(file_content);
                string line = sr2.ReadLine();
                while (line != null && line.StartsWith("//"))
                {
                    ++include_section_start_index;
                    line = sr2.ReadLine();
                }
                include_section_after_end_index = include_section_start_index + 1;
            }

            if (!contains(includes, "#include <memory>") && (file_content.Contains("unique_ptr")
                || file_content.Contains("make_unique")))
            {
                includes.Add(new Include_info(Include_info.Type.SYSTEM, "#include <memory>"));
            }
            if (!contains(includes, "#include <string>") && file_content.Contains("string"))
            {
                includes.Add(new Include_info(Include_info.Type.SYSTEM, "#include <string>"));
            }
            if (!contains(includes, "#include <utility>") && (file_content.Contains("pair")
                || file_content.Contains("move") || file_content.Contains("forward")))
            {
                includes.Add(new Include_info(Include_info.Type.SYSTEM, "#include <utility>"));
            }
            if (!contains(includes, "#include <vector>") && file_content.Contains("vector"))
            {
                includes.Add(new Include_info(Include_info.Type.SYSTEM, "#include <vector>"));
            }
            if (!contains(includes, "#include <tuple>") && file_content.Contains("tuple"))
            {
                includes.Add(new Include_info(Include_info.Type.SYSTEM, "#include <tuple>"));
            }

            --include_section_start_index;
            while (include_section_start_index >= 0
                && string.Copy(lines[include_section_start_index]).Trim() == "")
            {
                --include_section_start_index;
            }
            ++include_section_start_index;

            lines.RemoveRange(include_section_start_index,
                include_section_after_end_index - include_section_start_index);

            includes.Sort(delegate (Include_info i1, Include_info i2)
            {
                int type_1 = (int)i1.get_type();
                int type_2 = (int)i2.get_type();
                int diff = type_1 - type_2;
                if (diff == 0)
                {
                    return i1.get_line().CompareTo(i2.get_line());
                }
                else
                {
                    return diff;
                }
            });

            Include_info.Type prev_type = Include_info.Type.UNDEFINED;
            for (int ii_index = includes.Count - 1; ii_index >= 0; --ii_index)
            {
                Include_info ii = includes[ii_index];

                if (prev_type != ii.get_type())
                {
                    lines.Insert(include_section_start_index, "");
                }

                lines.Insert(include_section_start_index, ii.get_line());

                prev_type = ii.get_type();
            }
            lines.Insert(include_section_start_index, "");

            save_file(file_path, lines);
        }

        private static void move_open_braces_to_end_of_previous_line(string file_path)
        {
            bool was_file_changed = false;
            string file_content = File.ReadAllText(file_path);
            List<string> lines = new List<string>();
            StringReader sr = new StringReader(file_content);
            string line = sr.ReadLine();
            while (line != null)
            {
                if (string.Copy(line).Trim() == "{")
                {
                    if (lines.Count > 0 && !lines[lines.Count - 1].Contains("//"))
                    {
                        lines[lines.Count - 1] += " {";
                        was_file_changed = true;
                    }
                }
                else
                {
                    lines.Add(line);
                }
                line = sr.ReadLine();
            }

            if (was_file_changed)
            {
                save_file(file_path, lines);
            }
        }

        private static void move_else_to_the_same_line_as_preceding_close_bracket(string file_path)
        {
            bool was_file_changed = false;
            string file_content = File.ReadAllText(file_path);
            List<string> lines = new List<string>();
            StringReader sr = new StringReader(file_content);
            string line = sr.ReadLine();
            while (line != null)
            {
                if (string.Copy(line).Trim().StartsWith("else"))
                {
                    if (lines.Count > 0 && string.Copy(lines[lines.Count - 1]).Trim() == "}")
                    {
                        lines[lines.Count - 1] += " " + line.Trim();
                        was_file_changed = true;
                    }
                }
                else
                {
                    lines.Add(line);
                }
                line = sr.ReadLine();
            }

            if (was_file_changed)
            {
                save_file(file_path, lines);
            }
        }

        private static void move_comments_at_2_spaces_from_code(string file_path)
        {
            bool was_file_changed = false;
            string file_content = File.ReadAllText(file_path);
            List<string> lines = new List<string>();
            StringReader sr = new StringReader(file_content);
            string line = sr.ReadLine();
            while (line != null)
            {
                int comment_index = string.Copy(line).IndexOf("//");
                if (0 <= comment_index && comment_index < line.Length)
                {
                    int spaces_count = 0;
                    int i = comment_index - 1;
                    while (i >= 0 && line[i] == ' ')
                    {
                        ++spaces_count;
                        --i;
                    }
                    if (spaces_count < 2 && i >= 0 && (i + 1 < line.Length))
                    {
                        string new_line = line.Substring(0, i + 1) + "  " +
                            line.Substring(comment_index);
                        line = new_line;
                        was_file_changed = true;
                    }
                }
                lines.Add(line);
                line = sr.ReadLine();
            }

            if (was_file_changed)
            {
                save_file(file_path, lines);
            }
        }

        private static List<string> generate_files_list(string dir_path)
        {
            return generate_files_list_req(dir_path);
        }

        private static List<string> generate_files_list_req(string dir_path)
        {
            List<string> ret = new List<string>();
            string[] file_paths = Directory.GetFiles(dir_path);
            foreach (string file_path in file_paths)
            {
                string extension = Path.GetExtension(file_path);
                if (extension == ".hpp" || extension == ".inl" || extension == ".cpp")
                {
                    ret.Add(file_path);
                }
            }
            string[] directory_paths = Directory.GetDirectories(dir_path);
            foreach (string dp in directory_paths)
            {
                ret.AddRange(generate_files_list_req(dp));
            }
            return ret;
        }

        private static void move_inl_includes_in_cpp_files_just_after_include_section(
            string file_path)
        {
            if (!file_path.Contains(".cpp"))
            {
                return;
            }

            List<string> lines = read_file(file_path);

            string inl_filename = Path.GetFileNameWithoutExtension(file_path) + ".inl";
            bool is_debug_section = false;
            bool was_inl_found = false;
            int debug_inl_section_start_index = -1;
            int debug_inl_section_end_index = -1;
            for (int li = 0; li < lines.Count; ++li)
            {
                string l = lines[li];
                if (l.Contains("_DEBUG"))
                {
                    is_debug_section = true;
                    debug_inl_section_start_index = li;
                }
                else if (l.Contains("#endif"))
                {
                    is_debug_section = false;
                    if (was_inl_found)
                    {
                        debug_inl_section_end_index = li;
                        break;
                    }
                    else
                    {
                        debug_inl_section_start_index = -1;
                        debug_inl_section_end_index = -1;
                    }
                }
                else
                {
                    if (is_debug_section && l.Contains(inl_filename))
                    {
                        was_inl_found = true;
                    }
                }
            }

            if (debug_inl_section_start_index == -1 || debug_inl_section_end_index == -1)
            {
                return;
            }

            List<string> inl_include_section = new List<string>();
            for (int li = debug_inl_section_start_index; li <= debug_inl_section_end_index; ++li)
            {
                string l = lines[li];
                if (!l.Equals(""))
                {
                    inl_include_section.Add(l);
                }
            }

            int lines_to_remove_after_end_index = debug_inl_section_end_index + 1;
            while (lines_to_remove_after_end_index < lines.Count
                && lines[lines_to_remove_after_end_index].Equals(""))
            {
                ++lines_to_remove_after_end_index;
            }

            lines.RemoveRange(debug_inl_section_start_index,
                lines_to_remove_after_end_index - debug_inl_section_start_index);

            int include_section_start_index = 0;
            for (int li = 0; li < lines.Count; ++li)
            {
                string l = lines[li];
                if (l.StartsWith("#include"))
                {
                    include_section_start_index = li;
                    break;
                }
            }

            int include_section_end_index = include_section_start_index;
            while (include_section_end_index < lines.Count
                && (lines[include_section_end_index].StartsWith("#include")
                || lines[include_section_end_index].Equals("")))
            {
                ++include_section_end_index;
            }

            if (include_section_end_index >= debug_inl_section_end_index)
            {
                return;
            }

            int last_include_index = include_section_end_index - 1;

            while (last_include_index >= 0
                && !lines[last_include_index].StartsWith("#include"))
            {
                --last_include_index;
            }

            if (include_section_end_index - (last_include_index + 1) > 0)
            {
                lines.RemoveRange(last_include_index + 1,
                    include_section_end_index - (last_include_index + 1));
            }

            lines.Insert(last_include_index + 1, "");
            lines.InsertRange(last_include_index + 2, inl_include_section);
            lines.Insert(last_include_index + 2 + inl_include_section.Count, "");

            save_file(file_path, lines);
        }

        private static void save_file(string file_path, List<string> lines)
        {
            StringBuilder result_content = new StringBuilder();
            int li = 0;
            for (li = 0; li < lines.Count - 1; ++li)
            {
                result_content.AppendLine(lines[li]);
            }
            if (li < lines.Count)
            {
                string l = lines[li];
                if (!lines[li].Equals(""))
                {
                    result_content.AppendLine(l);
                }
            }
            string result = result_content.ToString();
            File.WriteAllText(file_path, result);
        }

        private static List<string> read_file(string file_path)
        {
            string file_content = File.ReadAllText(file_path);
            List<string> lines = new List<string>();
            StringReader sr = new StringReader(file_content);
            string line = sr.ReadLine();
            while (line != null)
            {
                lines.Add(line);
                line = sr.ReadLine();
            }
            return lines;
        }

        private static void add_new_line_at_the_end(string file_path)
        {
            List<string> lines = read_file(file_path);
            int li = lines.Count - 1;
            while (li >= 0 && lines[li].Equals(""))
            {
                --li;
            }
            ++li;
            lines.RemoveRange(li, lines.Count - li);
            save_file(file_path, lines);
        }

        private string dir_path_ = null;
        private static readonly string COPYRIGHT_INFO_TEXT =
            "// Copyright 2017 Tomasz Kosek. All rights reserved.";
    }
}
