﻿using System;
using System.Drawing;
using System.IO;
using System.Text;

namespace textures_generator
{
    class Program
    {
        static Random random_generator = new Random();
        static void Main(string[] args)
        {
            KnownColor[] randomized_colors = generate_randomized_colors();
            const uint TEXTURES_COUNT = 100;
            for (int i = 0; i < TEXTURES_COUNT; ++i)
            {
                Size size = new Size(512, 512);
                Bitmap b = new Bitmap(size.Width, size.Height,
                    System.Drawing.Imaging.PixelFormat.Format24bppRgb);

                Graphics g = Graphics.FromImage(b);
                g.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.AntiAlias;
                g.InterpolationMode =
                    System.Drawing.Drawing2D.InterpolationMode.HighQualityBicubic;
                g.PixelOffsetMode = System.Drawing.Drawing2D.PixelOffsetMode.HighQuality;
                Color color = Color.FromKnownColor(randomized_colors[i % randomized_colors.Length]);
                Color inverted_color = Color.FromArgb(color.ToArgb() ^ 0xffffff);
                string str = i.ToString();
                g.FillRectangle(
                    new SolidBrush(color), 0, 0, size.Width, size.Height);
                Font font = new Font("curier", 80);
                SizeF str_size = g.MeasureString(str, font);
                RectangleF string_rect = new RectangleF(
                    size.Width / 2 - str_size.Width / 2, size.Height / 2 - str_size.Height / 2,
                    str_size.Width, str_size.Height);
                g.DrawString(str, font, new SolidBrush(inverted_color),
                    string_rect);
                g.Flush();

                b.Save(generate_texture_file_name(i), System.Drawing.Imaging.ImageFormat.Jpeg);
            }
            {
                StringBuilder sb = new StringBuilder();
                for (int i = 0; i < TEXTURES_COUNT; ++i)
                {
                    sb.AppendLine("ENGINE_DECLARE_UNIQUE_NAME_OF_TYPE(engine::assets3::Texture::Name,");
                    sb.AppendLine("    " + generate_texture_asset_id(i) + ");");
                }
                StreamWriter sw = File.CreateText("Textures_names.hpp");
                sw.Write(sb.ToString());
                sw.Flush();
                sw.Close();
            }
            {
                StringBuilder sb = new StringBuilder();
                for (int i = 0; i < TEXTURES_COUNT; ++i)
                {
                    sb.AppendLine("ENGINE_REGISTER_UNIQUE_NAME_OF_TYPE_AS_PATH(engine::assets3::Texture::Name,");
                    sb.AppendLine("    " + generate_texture_asset_id(i) + ");");
                }
                StreamWriter sw = File.CreateText("Textures_names.cpp");
                sw.Write(sb.ToString());
                sw.Flush();
                sw.Close();
            }
            {
                StringBuilder sb = new StringBuilder();
                sb.AppendLine("INLINE std::vector<engine::assets::Asset::Ptr<engine::assets3::Texture>>");
                sb.AppendLine("Game_main_script::Body::load_textures(ID3D11Device* device) {");
                sb.AppendLine("    std::vector<engine::assets::Asset::Ptr<engine::assets3::Texture>> ret(" + TEXTURES_COUNT.ToString() + ");");
                for (int i = 0; i < TEXTURES_COUNT; ++i)
                {
                    sb.AppendLine("    ret[" + i.ToString() + "] = engine::assets::Asset::Ptr<engine::assets3::Texture>::make_asset(");
                    sb.AppendLine("        " + generate_texture_asset_id(i) + ");");
                    sb.AppendLine("    ret[" + i.ToString() + "]->load(device);");
                }
                sb.AppendLine("    return ret;");
                sb.AppendLine("}");
                StreamWriter sw = File.CreateText("load_textures_method.cpp");
                sw.Write(sb.ToString());
                sw.Flush();
                sw.Close();
            }
        }

        private static string generate_texture_file_name(int texture_id)
        {
            return "texture" + texture_id.ToString() + ".jpg";
        }

        private static string generate_texture_asset_id(int texture_id)
        {
            return "GAME_ASSETS_TEXTURES_" +
                generate_texture_file_name(texture_id).ToUpper().Replace(".", "_");
        }

        private static KnownColor[] generate_randomized_colors() {
            KnownColor[] names = (KnownColor[])Enum.GetValues(typeof(KnownColor));
            for (int i = 0; i < names.Length; ++i)
            {
                names[i] = names[random_generator.Next(1024 * 1024) % names.Length];
            }
            return names;
        }

        private static Color pick_random_color()
        {
            return Color.FromArgb(
                random_generator.Next(1024 * 1024) % 256,
                random_generator.Next(1024 * 1024) % 256,
                random_generator.Next(1024 * 1024) % 256);
        }
    }
}
