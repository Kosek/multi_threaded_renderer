1. enter git bash
2. call: python cpplint.py --linelength=100 --counting=detailed --headers=hpp $( find ./multi_threaded_renderer -name *.hpp -or -name *.cpp -or -name *.inl )
3. call: python cpplint.py --linelength=100 --counting=detailed --headers=hpp $( find ./multi_threaded_renderer_tests/engine -name *.hpp -or -name *.cpp -or -name *.inl )