﻿using System;
using System.Text;

namespace code_generator
{
    class Program
    {
        static void Main(string[] args)
        {
            string pixel_shader_switch_case_for_textures_array =
                generate_pixel_shader_switch_case_for_textures_array(128);
            string textures_copy_stateements = generate_statements_to_copy_textures(100);
        }

        private static string generate_pixel_shader_switch_case_for_textures_array(
            uint max_textures_count)
        {
            const uint MAX_SIZE_OF_TEXTURE_ARRAY = 128;
            StringBuilder sb = new StringBuilder();
            sb.AppendLine("[forcecase] switch (input.instance_id) {");
            uint texture_array_no = 0;
            while (max_textures_count > 0)
            {
                uint switch_cases_count = Math.Min(max_textures_count, MAX_SIZE_OF_TEXTURE_ARRAY);
                for (uint i = 0; i < switch_cases_count; ++i)
                {
                    uint instance_id = texture_array_no * MAX_SIZE_OF_TEXTURE_ARRAY + i;
                    sb.AppendLine("    case " + instance_id + ":");
                    sb.AppendLine("        return textures_" + texture_array_no + "[" + i +
                        "].Sample(linear_sample, input.tex_coord);");
                }
                ++texture_array_no;
                max_textures_count -= switch_cases_count;
            }
            sb.AppendLine("    default:");
            sb.AppendLine("        return 0;");
            sb.AppendLine("}");
            return sb.ToString();
        }

        private static string generate_statements_to_copy_textures(uint textures_count)
        {
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < textures_count; ++i)
            {
                sb.AppendLine("    <Content Include=\"game\\assets\\textures\\texture" + i + ".jpg\">");
                sb.AppendLine("      <CopyToOutputDirectory>PreserveNewest</CopyToOutputDirectory>");
                sb.AppendLine("    </Content>");
            }
            return sb.ToString();
        }
    }
}
