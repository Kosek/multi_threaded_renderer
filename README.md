# multi_threaded_renderer

Sample of multithreaded 3D rendering. Code hereby allocates one thread for frame simulation and one thread for renderer. Synchronization between those two is done in a lock-free manner using cache coherent reads. Frame simulator has inset that simulates load overhead.

Scene consists of 14 x 14 x 14 cubes greed (2744 cubes, 16464 faces). Each cube's face has 392 triangles. Each cube rotates and each cube's face takes random texture, from 100 textures set, every 1 sec. All is rendered in 132 DrawIndexedInstanced calls.

## Measurements

### Results

#### Singlethreaded rendering
>FRAPS: Frames: 2303 - Time: 60000ms - Avg: 38.383 - Min: 36 - Max: 43

>Triangles: 244 M / second

>Instances: 0.62 M / second

#### Multithreaded rendering
>FRAPS: Frames: 3774 - Time: 60000ms - Avg: 62.900 - Min: 57 - Max: 66

>Triangles: 406 M / second

>Instances: 1 M / second

### Hardware

* Intel Core i5-6200U 2.30GHz, 8 GB RAM, Intel HD Graphics 520
* Windows 7 Professional x64

## Screenshots

![](https://gitlab.com/Kosek/multi_threaded_renderer/raw/master/doc/screenshots/multithreaded_renderer_1.jpg)

![](https://gitlab.com/Kosek/multi_threaded_renderer/raw/master/doc/screenshots/multithreaded_renderer_2.jpg)

![](https://gitlab.com/Kosek/multi_threaded_renderer/raw/master/doc/screenshots/multithreaded_renderer_3.jpg)

## Code guide

### Synchronization of renderer and simulation threads

```
void Renderer::render() {
...
#if !defined SINGLE_THREADED_RENDERING
    synchronize_with_context_if_needed(main_context);
#endif
...
#if !defined SINGLE_THREADED_RENDERING
    engine::threading::sync::Binary_point::Consumer_guard consumer_guard(
        main_context->get_sync_point_for_frame_simulation());
#endif
...
}
...
void Context::update() {
...
#if !defined SINGLE_THREADED_RENDERING
    engine::threading::sync::Binary_point::Producer_guard producer_guard(
        get_sync_point_for_frame_simulation(), [this]() {
        process_renderer_synchronization();
        return is_renderer_synchronized();
    });
#endif
...
}

```

### Load simulation

```
#define SIMULATE_LOAD_OVERHEAD_FOR_SIMULATION_THREAD
...
void Load_overhead_simulation_script::update(engine::game::Context* context) {
#if defined SIMULATE_LOAD_OVERHEAD_FOR_SIMULATION_THREAD
    std::this_thread::sleep_for(
        std::chrono::duration<float, std::milli>((1000.0f / 60.0f) * 0.7f));
#endif
}
```

### Thread synchronization mechanism

```
class Binary_point final
```

### Scene creation entry point

```
void Game_main_script::Body::start(engine::game::Context* context)
```

### Geometry grouping and drawing

```
void Renderer::render()
class Geometry_to_render_selector final
class Geometry_to_render_grouper final
class Geometry_drawer final
```

## Compilation

### Shader version

* Pixel Shader v. 4.0
* Vertex Shader v. 4.0

### Target architecture

* x64

### IDE

* Microsoft Visual Studio Community 2015

### Dependencies

* Microsoft DirectX SDK (June 2010)