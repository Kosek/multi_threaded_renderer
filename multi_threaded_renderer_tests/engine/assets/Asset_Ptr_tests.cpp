// Copyright 2017 Tomasz Kosek. All rights reserved.

#include <stdafx.h>

#include <memory>
#include <utility>

#include "gtest/gtest.h"

#include <engine\assets\Asset.hpp>
#include <engine\game\Context.hpp>
#include <engine\threading\Local_storage.hpp>

namespace {
class Some_asset : public engine::assets::Asset {
 public:
    class Name final : public Asset::Name {
        using Asset::Name::Name;
    };
    explicit Some_asset(const Name& name) : Asset(name) {
    }
    unsigned int get_refs_count() const {
        return Asset::get_refs_count();
    }
};

class Derived_asset final : public Some_asset {
 public:
    using Some_asset::Some_asset;
};

ENGINE_REGISTER_UNIQUE_NAME_OF_TYPE(engine::game::Context::Name, MAIN_CONTEXT);
ENGINE_REGISTER_UNIQUE_NAME_OF_TYPE(Some_asset::Name, SOME_ASSET_1);
ENGINE_REGISTER_UNIQUE_NAME_OF_TYPE(Some_asset::Name, SOME_ASSET_2);

engine::game::Context main_context(MAIN_CONTEXT);

class Asset_Ptr : public ::testing::Test {
 public:
    Asset_Ptr() : mcs_(main_context) {
    }
 private:
    engine::threading::Local_storage::Main_context_setter mcs_;
};

TEST_F(Asset_Ptr, should_be_initialized_with_default_values) {
    engine::assets::Asset::Ptr<Some_asset> sut;
    EXPECT_EQ(nullptr, sut.get());
    EXPECT_FALSE(sut);
}
TEST_F(Asset_Ptr, should_allow_to_assign_an_asset_instance) {
    engine::assets::Asset::Ptr<Some_asset> sut =
        engine::assets::Asset::Ptr<Some_asset>::make_asset(SOME_ASSET_1);
    EXPECT_NE(nullptr, sut.get());
    EXPECT_TRUE(sut);
    EXPECT_EQ(0, SOME_ASSET_1.compare(sut->get_name()));
    EXPECT_EQ(1, sut->get_refs_count());
}
TEST_F(Asset_Ptr, should_allow_to_reset) {
    engine::assets::Asset::Ptr<Some_asset> sut =
      engine::assets::Asset::Ptr<Some_asset>::make_asset(SOME_ASSET_1);
    sut.reset();
    EXPECT_EQ(nullptr, sut.get());
    EXPECT_FALSE(sut);
}
TEST_F(Asset_Ptr, should_allow_to_point_to_one_asset_by_multiple_ptr_instances) {
    engine::assets::Asset::Ptr<Some_asset> sut1 =
        engine::assets::Asset::Ptr<Some_asset>::make_asset(SOME_ASSET_1);
    engine::assets::Asset::Ptr<Some_asset> sut2 = sut1;
    engine::assets::Asset::Ptr<Some_asset> sut3;
    sut3 = sut1;
    EXPECT_NE(nullptr, sut1.get());
    EXPECT_NE(nullptr, sut2.get());
    EXPECT_NE(nullptr, sut3.get());
    EXPECT_TRUE(sut1);
    EXPECT_TRUE(sut2);
    EXPECT_TRUE(sut3);
    EXPECT_EQ(3, sut1->get_refs_count());
    EXPECT_EQ(3, sut2->get_refs_count());
    EXPECT_EQ(3, sut3->get_refs_count());
    EXPECT_EQ(sut1.get(), sut2.get());
    EXPECT_EQ(sut2.get(), sut3.get());
}
TEST_F(Asset_Ptr, should_allow_to_overwrite_reference_to_asset_by_other_asset) {
    engine::assets::Asset::Ptr<Some_asset> sut1 =
        engine::assets::Asset::Ptr<Some_asset>::make_asset(SOME_ASSET_1);
    engine::assets::Asset::Ptr<Some_asset> sut2 =
        engine::assets::Asset::Ptr<Some_asset>::make_asset(SOME_ASSET_2);
    sut1 = sut2;
    EXPECT_NE(nullptr, sut1.get());
    EXPECT_NE(nullptr, sut2.get());
    EXPECT_TRUE(sut1);
    EXPECT_TRUE(sut2);
    EXPECT_EQ(2, sut1->get_refs_count());
    EXPECT_EQ(2, sut2->get_refs_count());
    EXPECT_EQ(sut1.get(), sut2.get());
}
TEST_F(Asset_Ptr,
    should_allow_to_overwrite_reference_to_asset_pointed_by_multiple_ptrs_by_other_asset) {
    engine::assets::Asset::Ptr<Some_asset> sut1_1 =
        engine::assets::Asset::Ptr<Some_asset>::make_asset(SOME_ASSET_1);
    engine::assets::Asset::Ptr<Some_asset> sut1_2(sut1_1);
    engine::assets::Asset::Ptr<Some_asset> sut2 =
        engine::assets::Asset::Ptr<Some_asset>::make_asset(SOME_ASSET_2);
    sut1_2 = sut2;
    EXPECT_NE(nullptr, sut1_1.get());
    EXPECT_NE(nullptr, sut1_2.get());
    EXPECT_NE(nullptr, sut2.get());
    EXPECT_TRUE(sut1_1);
    EXPECT_TRUE(sut1_2);
    EXPECT_TRUE(sut2);
    EXPECT_EQ(1, sut1_1->get_refs_count());
    EXPECT_EQ(2, sut2->get_refs_count());
    EXPECT_EQ(sut1_2.get(), sut2.get());
    EXPECT_NE(sut1_1.get(), sut2.get());
}
TEST_F(Asset_Ptr, should_allow_to_decrement_multiple_references_to_zero) {
    engine::assets::Asset::Ptr<Some_asset> sut1 =
        engine::assets::Asset::Ptr<Some_asset>::make_asset(SOME_ASSET_1);
    engine::assets::Asset::Ptr<Some_asset> sut2 = sut1;
    engine::assets::Asset::Ptr<Some_asset> sut3 = sut2;
    EXPECT_EQ(sut1.get(), sut2.get());
    EXPECT_EQ(sut2.get(), sut3.get());
    EXPECT_EQ(3, sut3->get_refs_count());
    sut1.reset();
    EXPECT_EQ(2, sut3->get_refs_count());
    sut2.reset();
    EXPECT_EQ(1, sut3->get_refs_count());
    sut3.reset();
    EXPECT_EQ(nullptr, sut1.get());
    EXPECT_EQ(nullptr, sut2.get());
    EXPECT_EQ(nullptr, sut3.get());
}
TEST_F(Asset_Ptr, should_not_decrement_refs_more_than_one_with_multiple_reset_called) {
    engine::assets::Asset::Ptr<Some_asset> sut1 =
        engine::assets::Asset::Ptr<Some_asset>::make_asset(SOME_ASSET_1);
    engine::assets::Asset::Ptr<Some_asset> sut2 = sut1;
    engine::assets::Asset::Ptr<Some_asset> sut3 = sut2;
    EXPECT_EQ(sut1.get(), sut2.get());
    EXPECT_EQ(sut2.get(), sut3.get());
    EXPECT_EQ(3, sut3->get_refs_count());
    sut1.reset();
    sut1.reset();
    sut1.reset();
    sut1.reset();
    sut1.reset();
    EXPECT_EQ(2, sut3->get_refs_count());
    EXPECT_EQ(nullptr, sut1.get());
    EXPECT_NE(nullptr, sut2.get());
    EXPECT_NE(nullptr, sut3.get());
}
TEST_F(Asset_Ptr, should_decrement_reference_in_case_it_was_assigned_with_empty_ptr) {
    engine::assets::Asset::Ptr<Some_asset> sut1 =
        engine::assets::Asset::Ptr<Some_asset>::make_asset(SOME_ASSET_1);
    engine::assets::Asset::Ptr<Some_asset> sut2 = sut1;
    engine::assets::Asset::Ptr<Some_asset> sut3 = sut2;
    engine::assets::Asset::Ptr<Some_asset> sut_empty;
    EXPECT_EQ(3, sut3->get_refs_count());
    sut1 = sut_empty;
    EXPECT_EQ(2, sut3->get_refs_count());
    EXPECT_EQ(nullptr, sut1.get());
    EXPECT_NE(nullptr, sut2.get());
    EXPECT_NE(nullptr, sut3.get());
    EXPECT_EQ(nullptr, sut_empty.get());
}
TEST_F(Asset_Ptr, should_allow_to_make_asset_from_unique_ptr) {
    std::unique_ptr<Some_asset> asset = std::make_unique<Some_asset>(SOME_ASSET_1);
    EXPECT_EQ(0, asset->get_refs_count());
    engine::assets::Asset::Ptr<Some_asset> sut =
        engine::assets::Asset::Ptr<Some_asset>::make_from_pointer(std::move(asset));
    EXPECT_EQ(1, sut->get_refs_count());
    EXPECT_NE(nullptr, sut.get());
    EXPECT_TRUE(sut);
}
TEST_F(Asset_Ptr, should_allow_to_make_asset_from_raw_ptr) {
    std::unique_ptr<Some_asset> asset = std::make_unique<Some_asset>(SOME_ASSET_1);
    EXPECT_EQ(0, asset->get_refs_count());
    engine::assets::Asset::Ptr<Some_asset> sut =
        engine::assets::Asset::Ptr<Some_asset>::make_from_pointer(asset.release());
    EXPECT_EQ(1, sut->get_refs_count());
    EXPECT_NE(nullptr, sut.get());
    EXPECT_TRUE(sut);
}
TEST_F(Asset_Ptr, should_allow_to_reassign_once_cleared_ptr) {
    engine::assets::Asset::Ptr<Some_asset> sut =
        engine::assets::Asset::Ptr<Some_asset>::make_asset(SOME_ASSET_1);
    sut.reset();
    sut = engine::assets::Asset::Ptr<Some_asset>::make_asset(SOME_ASSET_2);
    EXPECT_EQ(1, sut->get_refs_count());
    EXPECT_NE(nullptr, sut.get());
    EXPECT_TRUE(sut);
    EXPECT_EQ(0, SOME_ASSET_2.compare(sut->get_name()));
}
TEST_F(Asset_Ptr, should_allow_to_assign_derived_asset_to_base_asset_ptr) {
    engine::assets::Asset::Ptr<Some_asset> sut =
        engine::assets::Asset::Ptr<Derived_asset>::make_asset(SOME_ASSET_1);
    EXPECT_EQ(1, sut->get_refs_count());
    EXPECT_NE(nullptr, sut.get());
    EXPECT_TRUE(sut);
    EXPECT_EQ(0, SOME_ASSET_1.compare(sut->get_name()));
}
}  // namespace
