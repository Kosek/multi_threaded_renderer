// Copyright 2017 Tomasz Kosek. All rights reserved.

#include <stdafx.h>

#include <utility>

#include "gtest/gtest.h"

#include <engine\data_structures\Name_index.hpp>

namespace {
class Some_type final {
 public:
    explicit Some_type(const engine::naming::Name& name) : name_(name) {
    }
    const engine::naming::Name& get_name() const {
        return name_;
    }
 private:
    const engine::naming::Name name_;
};

ENGINE_REGISTER_UNIQUE_NAME_OF_TYPE(engine::naming::Name, SOME_TYPE_INSTANCE_1);
ENGINE_REGISTER_UNIQUE_NAME_OF_TYPE(engine::naming::Name, SOME_TYPE_INSTANCE_2);
ENGINE_REGISTER_UNIQUE_NAME_OF_TYPE(engine::naming::Name, SOME_TYPE_INSTANCE_3);
ENGINE_REGISTER_UNIQUE_NAME_OF_TYPE(engine::naming::Name, SOME_TYPE_INSTANCE_4);

TEST(Name_index, should_allow_to_add_elements) {
    engine::data_structures::Name_index<Some_type> sut;
    Some_type t1(SOME_TYPE_INSTANCE_1);
    Some_type t2(SOME_TYPE_INSTANCE_2);
    Some_type t3(SOME_TYPE_INSTANCE_3);
    sut.add(&t2);
    sut.add(&t3);
    sut.add(&t1);
    EXPECT_TRUE(sut.contains(SOME_TYPE_INSTANCE_1));
    EXPECT_TRUE(sut.contains(SOME_TYPE_INSTANCE_2));
    EXPECT_TRUE(sut.contains(SOME_TYPE_INSTANCE_3));
    EXPECT_FALSE(sut.contains(SOME_TYPE_INSTANCE_4));
}
TEST(Name_index, should_allow_to_add_one_element_only) {
    engine::data_structures::Name_index<Some_type> sut;
    Some_type t1(SOME_TYPE_INSTANCE_1);
    sut.add(&t1);
    EXPECT_TRUE(sut.contains(SOME_TYPE_INSTANCE_1));
    EXPECT_FALSE(sut.contains(SOME_TYPE_INSTANCE_2));
    EXPECT_FALSE(sut.contains(SOME_TYPE_INSTANCE_3));
    EXPECT_FALSE(sut.contains(SOME_TYPE_INSTANCE_4));
}
TEST(Name_index, should_allow_to_add_one_element_remove_it_and_add_it_again) {
    engine::data_structures::Name_index<Some_type> sut;
    Some_type t1(SOME_TYPE_INSTANCE_1);
    sut.add(&t1);
    sut.remove(&t1);
    sut.add(&t1);
    EXPECT_TRUE(sut.contains(SOME_TYPE_INSTANCE_1));
    EXPECT_FALSE(sut.contains(SOME_TYPE_INSTANCE_2));
    EXPECT_FALSE(sut.contains(SOME_TYPE_INSTANCE_3));
    EXPECT_FALSE(sut.contains(SOME_TYPE_INSTANCE_4));
}
TEST(Name_index, should_allow_to_add_one_element_remove_it_and_add_other_element) {
    engine::data_structures::Name_index<Some_type> sut;
    Some_type t1(SOME_TYPE_INSTANCE_1);
    sut.add(&t1);
    sut.remove(&t1);
    Some_type t2(SOME_TYPE_INSTANCE_2);
    sut.add(&t2);
    EXPECT_FALSE(sut.contains(SOME_TYPE_INSTANCE_1));
    EXPECT_TRUE(sut.contains(SOME_TYPE_INSTANCE_2));
    EXPECT_FALSE(sut.contains(SOME_TYPE_INSTANCE_3));
    EXPECT_FALSE(sut.contains(SOME_TYPE_INSTANCE_4));
}
TEST(Name_index, should_allow_to_add_one_element_and_remove_it_by_its_name) {
    engine::data_structures::Name_index<Some_type> sut;
    Some_type t3(SOME_TYPE_INSTANCE_3);
    sut.add(&t3);
    sut.remove(&t3);
    EXPECT_FALSE(sut.contains(SOME_TYPE_INSTANCE_1));
    EXPECT_FALSE(sut.contains(SOME_TYPE_INSTANCE_2));
    EXPECT_FALSE(sut.contains(SOME_TYPE_INSTANCE_3));
    EXPECT_FALSE(sut.contains(SOME_TYPE_INSTANCE_4));
}
TEST(Name_index, should_allow_to_query_for_elements) {
    engine::data_structures::Name_index<Some_type> sut;
    Some_type t1(SOME_TYPE_INSTANCE_1);
    Some_type t2(SOME_TYPE_INSTANCE_2);
    Some_type t3(SOME_TYPE_INSTANCE_3);
    sut.add(&t2);
    sut.add(&t3);
    sut.add(&t1);
    EXPECT_EQ(&t1, sut.get(SOME_TYPE_INSTANCE_1));
    EXPECT_EQ(&t2, sut.get(SOME_TYPE_INSTANCE_2));
    EXPECT_EQ(&t3, sut.get(SOME_TYPE_INSTANCE_3));
    EXPECT_EQ(nullptr, sut.get(SOME_TYPE_INSTANCE_4));
}
TEST(Name_index, should_allow_to_query_for_elements_via_const_accesor) {
    using NI = engine::data_structures::Name_index<Some_type>;
    NI sut;
    Some_type t1(SOME_TYPE_INSTANCE_1);
    Some_type t2(SOME_TYPE_INSTANCE_2);
    Some_type t3(SOME_TYPE_INSTANCE_3);
    sut.add(&t2);
    sut.add(&t3);
    sut.add(&t1);
    EXPECT_EQ(&t1, static_cast<const NI&>(sut).get(SOME_TYPE_INSTANCE_1));
    EXPECT_EQ(&t2, static_cast<const NI&>(sut).get(SOME_TYPE_INSTANCE_2));
    EXPECT_EQ(&t3, static_cast<const NI&>(sut).get(SOME_TYPE_INSTANCE_3));
    EXPECT_EQ(nullptr, static_cast<const NI&>(sut).get(SOME_TYPE_INSTANCE_4));
}
}  // namespace
