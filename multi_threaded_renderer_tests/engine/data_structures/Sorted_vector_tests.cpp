// Copyright 2017 Tomasz Kosek. All rights reserved.

#include <stdafx.h>

#include <string>
#include <utility>
#include <vector>

#include "gtest/gtest.h"

#include <engine\data_structures\Sorted_vector.hpp>

namespace {
class Comparator final {
 public:
    INLINE int cmp(const std::string& key, const int& e) const {
        return key.compare(get_key(e));
    }
    INLINE const std::string get_key(const int& e) const {
        return std::to_string(e);
    }
};
TEST(Sorted_vector, should_be_initialized_with_default_values) {
    engine::data_structures::Sorted_vector<std::string, int, Comparator> sut;
    EXPECT_EQ(0, sut.get_count());
    EXPECT_TRUE(sut.is_empty());
}
TEST(Sorted_vector, should_allow_to_add_elements) {
    engine::data_structures::Sorted_vector<std::string, int, Comparator> sut;
    sut.add(3);
    sut.add(1);
    const int element = 2;
    sut.add(element);
    int i = 0;
    for (auto it = sut.cbegin(); it != sut.cend(); ++it) {
        EXPECT_EQ(++i, *it);
    }
    EXPECT_EQ(3, i);
}
TEST(Sorted_vector, should_allow_to_remove_elements_one_after_another) {
    engine::data_structures::Sorted_vector<std::string, int, Comparator> sut;
    sut.add(2);
    sut.add(3);
    sut.add(1);
    sut.remove(std::string("3"));
    int i = 0;
    for (auto it = sut.cbegin(); it != sut.cend(); ++it) {
        EXPECT_EQ(++i, *it);
    }
    sut.remove(std::string("1"));
    i = 1;
    for (auto it = sut.cbegin(); it != sut.cend(); ++it) {
        EXPECT_EQ(++i, *it);
    }
    sut.remove(std::string("2"));
    for (auto it = sut.cbegin(); it != sut.cend(); ++it) {
        FAIL();
    }
    EXPECT_EQ(0, sut.get_count());
    EXPECT_TRUE(sut.is_empty());
}
TEST(Sorted_vector, should_allow_to_add_one_element_and_remove_it) {
    engine::data_structures::Sorted_vector<std::string, int, Comparator> sut;
    sut.add(2);
    sut.remove(std::string("2"));
    for (auto it = sut.cbegin(); it != sut.cend(); ++it) {
        FAIL();
    }
    EXPECT_EQ(0, sut.get_count());
    EXPECT_TRUE(sut.is_empty());
}
TEST(Sorted_vector, should_not_change_order_of_elements_if_those_are_inserted_in_ascending_order) {
    engine::data_structures::Sorted_vector<std::string, int, Comparator> sut;
    sut.add(1);
    sut.add(2);
    sut.add(3);
    int i = 0;
    for (auto it = sut.cbegin(); it != sut.cend(); ++it) {
        EXPECT_EQ(++i, *it);
    }
}
TEST(Sorted_vector, should_change_order_of_elements_if_those_are_inserted_in_descending_order) {
    engine::data_structures::Sorted_vector<std::string, int, Comparator> sut;
    sut.add(3);
    sut.add(2);
    sut.add(1);
    int i = 0;
    for (auto it = sut.cbegin(); it != sut.cend(); ++it) {
        EXPECT_EQ(++i, *it);
    }
}
TEST(Sorted_vector, should_allow_to_remove_all_elements_at_once) {
    engine::data_structures::Sorted_vector<std::string, int, Comparator> sut;
    sut.add(3);
    sut.add(1);
    sut.add(2);
    sut.remove_all();
    for (auto it = sut.cbegin(); it != sut.cend(); ++it) {
        FAIL();
    }
    EXPECT_EQ(0, sut.get_count());
    EXPECT_TRUE(sut.is_empty());
}
TEST(Sorted_vector, should_allow_to_remove_all_elements_at_once_if_there_is_only_one_element) {
    engine::data_structures::Sorted_vector<std::string, int, Comparator> sut;
    sut.add(3);
    sut.remove_all();
    for (auto it = sut.cbegin(); it != sut.cend(); ++it) {
        FAIL();
    }
    EXPECT_EQ(0, sut.get_count());
    EXPECT_TRUE(sut.is_empty());
}
TEST(Sorted_vector, should_allow_to_memory_allocation_before_elements_addition) {
    engine::data_structures::Sorted_vector<std::string, int, Comparator> sut;
    sut.reserve(188);
    for (auto it = sut.cbegin(); it != sut.cend(); ++it) {
        FAIL();
    }
    EXPECT_EQ(0, sut.get_count());
    EXPECT_TRUE(sut.is_empty());
}
TEST(Sorted_vector, should_allow_to_get_particular_element) {
    using Sorted_vector_of_integers =
        engine::data_structures::Sorted_vector<std::string, int, Comparator>;
    Sorted_vector_of_integers sut;
    sut.add(188);
    sut.add(123);
    sut.add(913);
    sut.add(1);
    sut.add(1024);
    EXPECT_EQ(1, sut.get(std::string("1")));
    EXPECT_EQ(123, sut.get(std::string("123")));
    EXPECT_EQ(188, static_cast<const Sorted_vector_of_integers&>(sut).get(std::string("188")));
    EXPECT_EQ(913, sut.get(std::string("913")));
    EXPECT_EQ(1024, sut.get(std::string("1024")));
}
}  // namespace
