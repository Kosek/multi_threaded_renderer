// Copyright 2017 Tomasz Kosek. All rights reserved.

#include <stdafx.h>

#include <utility>

#include "gtest/gtest.h"

#include <engine\data_structures\Embedded_list.hpp>

namespace {
extern const char NULL_NOT_ALLOWED[] = { "Element must not be null!" };
extern const char ELEMENT_ALREADY_IN_LIST[] = { "Element already added as child node!" };
extern const char ELEMENT_NOT_IN_CURRENT_LIST[] = { "Element is not child of current parent!" };

class List_owner;

class List_element final {
 public:
    class Some_embeded_list;

    class Some_embeded_list_element final : public engine::data_structures::Embedded_list<
        Some_embeded_list,
        List_element,
        Some_embeded_list_element,
        NULL_NOT_ALLOWED,
        ELEMENT_ALREADY_IN_LIST,
        ELEMENT_NOT_IN_CURRENT_LIST>::Element {
     public:
        using Type = engine::data_structures::Embedded_list<
            Some_embeded_list,
            List_element,
            Some_embeded_list_element,
            NULL_NOT_ALLOWED,
            ELEMENT_ALREADY_IN_LIST,
            ELEMENT_NOT_IN_CURRENT_LIST>::Element;

        static Type& get_embedded_list_element(List_element* e) {
            return e->embeded_list_element_;
        }
        static const Type& get_embedded_list_element(const List_element* e) {
            return e->embeded_list_element_;
        }
    };
    class Some_embeded_list final : public engine::data_structures::Embedded_list<
        Some_embeded_list,
        List_element,
        Some_embeded_list_element,
        NULL_NOT_ALLOWED,
        ELEMENT_ALREADY_IN_LIST,
        ELEMENT_NOT_IN_CURRENT_LIST> {
     public:
        explicit Some_embeded_list(List_owner* owner) : owner_(owner) {
        }
        List_owner* get_owner() const {
            return owner_;
        }

     private:
        engine::utilities::memory::Raw_ptr<List_owner> owner_;
    };

    Some_embeded_list_element embeded_list_element_;
};

class List_owner final {
 public:
    List_owner() : sut_(this) {}

    List_element::Some_embeded_list sut_;
};

TEST(Embedded_list, should_be_initialized_with_default_values) {
    List_owner o;
    EXPECT_TRUE(o.sut_.is_empty());
    EXPECT_EQ(&o, o.sut_.get_owner());
}
TEST(Embedded_list, should_allow_to_add_elements) {
    List_owner o;
    List_element first;
    o.sut_.add(&first);
    EXPECT_FALSE(o.sut_.is_empty());
    List_element last;
    o.sut_.add(&last);
    EXPECT_FALSE(o.sut_.is_empty());
}
TEST(Embedded_list, should_allow_to_remove_elements_from_beginning) {
    List_owner o;
    List_element first;
    o.sut_.add(&first);
    EXPECT_FALSE(o.sut_.is_empty());
    List_element middle;
    o.sut_.add(&middle);
    EXPECT_FALSE(o.sut_.is_empty());
    List_element last;
    o.sut_.add(&last);
    EXPECT_FALSE(o.sut_.is_empty());
    o.sut_.remove(&first);
    EXPECT_FALSE(o.sut_.is_empty());
    o.sut_.remove(&middle);
    EXPECT_FALSE(o.sut_.is_empty());
    o.sut_.remove(&last);
    EXPECT_TRUE(o.sut_.is_empty());
}
TEST(Embedded_list, should_allow_to_remove_elements_from_end) {
    List_owner o;
    List_element first;
    o.sut_.add(&first);
    EXPECT_FALSE(o.sut_.is_empty());
    List_element middle;
    o.sut_.add(&middle);
    EXPECT_FALSE(o.sut_.is_empty());
    List_element last;
    o.sut_.add(&last);
    EXPECT_FALSE(o.sut_.is_empty());
    o.sut_.remove(&last);
    EXPECT_FALSE(o.sut_.is_empty());
    o.sut_.remove(&middle);
    EXPECT_FALSE(o.sut_.is_empty());
    o.sut_.remove(&first);
    EXPECT_TRUE(o.sut_.is_empty());
}
TEST(Embedded_list, should_allow_to_remove_all_elements) {
    List_owner o;
    List_element first;
    o.sut_.add(&first);
    EXPECT_FALSE(o.sut_.is_empty());
    List_element middle;
    o.sut_.add(&middle);
    EXPECT_FALSE(o.sut_.is_empty());
    List_element last;
    o.sut_.add(&last);
    o.sut_.remove_all();
    EXPECT_TRUE(o.sut_.is_empty());
}
TEST(Embedded_list, should_allow_to_remove_all_elements_if_empty) {
    List_owner o;
    o.sut_.remove_all();
    EXPECT_TRUE(o.sut_.is_empty());
}
TEST(Embedded_list, should_allow_to_remove_all_elements_with_callback_per_each_element) {
    List_owner o;
    List_element first;
    o.sut_.add(&first);
    EXPECT_FALSE(o.sut_.is_empty());
    List_element middle;
    o.sut_.add(&middle);
    EXPECT_FALSE(o.sut_.is_empty());
    List_element last;
    o.sut_.add(&last);
    int i = 0;
    o.sut_.remove_all([&i, &first, &middle, &last](List_element* e) {
        if (i == 0) {
            EXPECT_EQ(&last, e);
        } else if (i == 1) {
            EXPECT_EQ(&middle, e);
        } else if (i == 2) {
            EXPECT_EQ(&first, e);
        }
        ++i;
    });
    EXPECT_TRUE(o.sut_.is_empty());
    EXPECT_EQ(3, i);
}
TEST(Embedded_list, should_allow_to_iterate_over_all_elements) {
    List_owner o;
    List_element first;
    o.sut_.add(&first);
    EXPECT_FALSE(o.sut_.is_empty());
    List_element middle;
    o.sut_.add(&middle);
    EXPECT_FALSE(o.sut_.is_empty());
    List_element last;
    o.sut_.add(&last);
    int i = 0;
    o.sut_.get_iterator().visit_all([&i, &first, &middle, &last](const List_element* e) {
        if (i == 0) {
            EXPECT_EQ(&last, e);
        } else if (i == 1) {
            EXPECT_EQ(&middle, e);
        } else if (i == 2) {
            EXPECT_EQ(&first, e);
        }
        ++i;
    });
    EXPECT_EQ(3, i);
}
TEST(Embedded_list, should_return_no_element_if_its_empty_during_iteration_over_all_elements) {
    List_owner o;
    int i = 0;
    o.sut_.get_iterator().visit_all([&i](const List_element* e) {
        FAIL();
        ++i;
    });
    EXPECT_EQ(0, i);
}
TEST(Embedded_list, should_set_a_list_ptr_to_each_added_element) {
    List_owner o;
    List_element first;
    o.sut_.add(&first);
    List_element middle;
    o.sut_.add(&middle);
    List_element last;
    o.sut_.add(&last);
    o.sut_.get_iterator().visit_all([&o](const List_element* e) {
        EXPECT_EQ(&o.sut_, e->embeded_list_element_.get_list());
    });
}
TEST(Embedded_list_element, should_return_true_for_is_in_list_call_if_its_in_list) {
    List_owner o;
    List_element first;
    o.sut_.add(&first);
    List_element middle;
    o.sut_.add(&middle);
    List_element last;
    o.sut_.add(&last);
    o.sut_.get_iterator().visit_all([&o](const List_element* e) {
        EXPECT_TRUE(e->embeded_list_element_.is_in_list());
    });
    List_element not_added_element;
    EXPECT_FALSE(not_added_element.embeded_list_element_.is_in_list());
}
TEST(Embedded_list_element, should_return_nullptr_list_reference_if_it_was_removed_from_list) {
    List_owner o;
    List_element e;
    o.sut_.add(&e);
    o.sut_.remove_all();
    EXPECT_FALSE(e.embeded_list_element_.is_in_list());
}
TEST(Embedded_list_element, should_be_initialized_with_default_values) {
    List_element e;
    EXPECT_EQ(nullptr, e.embeded_list_element_.get_list());
    EXPECT_FALSE(e.embeded_list_element_.is_in_list());
}
}  // namespace
