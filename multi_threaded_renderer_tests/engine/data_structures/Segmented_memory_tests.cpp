// Copyright 2017 Tomasz Kosek. All rights reserved.

#include <stdafx.h>

#include "gtest/gtest.h"

#include <engine\data_structures\Segmented_memory.hpp>

namespace {

template <unsigned int ELEMENTS_COUNT_IN_SEGMENT>
class Elements_in_segment_provider final {
 public:
     constexpr static int COUNT = ELEMENTS_COUNT_IN_SEGMENT;
};
template <unsigned int ELEMENTS_COUNT_IN_SEGMENT>
int Elements_in_segment_provider<ELEMENTS_COUNT_IN_SEGMENT>::COUNT = ELEMENTS_COUNT_IN_SEGMENT;

template <typename ELEMENTS_COUNT_IN_SEGMENT_PROVIDER>
class Segmented_memory_test : public ::testing::Test {
 public:
    engine::data_structures::Segmented_memory<int, ELEMENTS_COUNT_IN_SEGMENT_PROVIDER::COUNT> sut;
};

typedef ::testing::Types<Elements_in_segment_provider<1>, Elements_in_segment_provider<2>,
    Elements_in_segment_provider<3>, Elements_in_segment_provider<4>,
    Elements_in_segment_provider<5>, Elements_in_segment_provider<128>,
    Elements_in_segment_provider<512>, Elements_in_segment_provider<1024>>
    Variants_of_elements_count_in_segment;
TYPED_TEST_CASE(Segmented_memory_test, Variants_of_elements_count_in_segment);

TYPED_TEST(Segmented_memory_test, should_be_initialized_with_default_values) {
    sut.get_iterator().visit_all([](const int& v) {
        FAIL();
    });
}
TYPED_TEST(Segmented_memory_test, should_iterate_corretly_if_elements_count_equals_1) {
    sut.push_back(1);
    int i = 0;
    sut.get_iterator().visit_all([&i](const int& v) {
        EXPECT_EQ(++i, v);
    });
    EXPECT_EQ(1, i);
}
TYPED_TEST(Segmented_memory_test,
    should_iterate_corretly_if_elements_count_is_less_than_segment_size_but_more_than_one) {
    const int elements_count = gtest_TypeParam_::COUNT - 1;
    for (unsigned int element_no = 1; element_no <= elements_count; ++element_no) {
        sut.push_back(element_no);
    }
    int i = 0;
    sut.get_iterator().visit_all([&i](const int& v) {
        EXPECT_EQ(++i, v);
    });
    EXPECT_EQ(elements_count, i);
}
TYPED_TEST(Segmented_memory_test, should_iterate_corretly_if_elements_count_equals_segment_size) {
    const int elements_count = gtest_TypeParam_::COUNT;
    for (unsigned int element_no = 1; element_no <= elements_count; ++element_no) {
        sut.push_back(element_no);
    }
    int i = 0;
    sut.get_iterator().visit_all([&i](const int& v) {
        EXPECT_EQ(++i, v);
    });
    EXPECT_EQ(elements_count, i);
}
TYPED_TEST(Segmented_memory_test, should_iterate_corretly_if_elements_count_exceeds_segment_size) {
    const int elements_count = gtest_TypeParam_::COUNT + 1;
    for (unsigned int element_no = 1; element_no <= elements_count; ++element_no) {
        sut.push_back(element_no);
    }
    int i = 0;
    sut.get_iterator().visit_all([&i](const int& v) {
        EXPECT_EQ(++i, v);
    });
    EXPECT_EQ(elements_count, i);
}
TYPED_TEST(Segmented_memory_test,
    should_iterate_corretly_if_elements_count_equals_two_segment_sizes) {
    const int elements_count = 2 * gtest_TypeParam_::COUNT;
    for (unsigned int element_no = 1; element_no <= elements_count; ++element_no) {
        sut.push_back(element_no);
    }
    int i = 0;
    sut.get_iterator().visit_all([&i](const int& v) {
        EXPECT_EQ(++i, v);
    });
    EXPECT_EQ(elements_count, i);
}
TYPED_TEST(Segmented_memory_test, should_allow_to_reset_elements_count) {
    sut.push_back(1);
    sut.push_back(2);
    sut.reset_elements_count();
    sut.get_iterator().visit_all([](const int& v) {
        FAIL();
    });
}
TYPED_TEST(Segmented_memory_test,
    should_allow_to_reset_elements_count_and_allow_to_add_new_elements) {
    sut.push_back(1);
    sut.push_back(2);
    sut.reset_elements_count();
    const int elements_count = 2 * gtest_TypeParam_::COUNT + 1;
    for (unsigned int element_no = 1; element_no <= elements_count; ++element_no) {
        sut.push_back(element_no + 2);
    }
    int i = 0;
    sut.get_iterator().visit_all([&i](const int& v) {
        EXPECT_EQ(++i + 2, v);
    });
    EXPECT_EQ(elements_count, i);
}
}  // namespace
