// Copyright 2017 Tomasz Kosek. All rights reserved.

#include <stdafx.h>

#include <utility>

#include "gtest/gtest.h"

#include <engine\data_structures\Array.hpp>

namespace {
class Some_type final {
 public:
    Some_type() = default;
    explicit Some_type(int value) : value_(value) {
    }
    int get_value() const {
        return value_;
    }
    void set_value(int value) {
        value_ = value;
    }
    bool operator==(const Some_type& st) const {
        return st.value_ == value_;
    }
    static Some_type default_value() {
        return Some_type();
    }
 private:
    int value_ = 0;
};
TEST(Array, should_be_initialized_with_default_values) {
    engine::data_structures::Array<int, 7> sut;
    EXPECT_TRUE(sut.is_empty());
    EXPECT_EQ(0, sut.get_count());
    EXPECT_NE(nullptr, sut.get_data());
    EXPECT_EQ(7, sut.get_max_size());
}
TEST(Array, should_let_to_add_elements_and_return_valid_number_of_elements) {
    engine::data_structures::Array<Some_type, 3> sut;
    Some_type instance_1(1);
    sut.push_back(instance_1);
    EXPECT_EQ(1, sut.get_count());
    EXPECT_EQ(3, sut.get_max_size());
    sut.push_back(Some_type(2));
    EXPECT_EQ(2, sut.get_count());
    EXPECT_EQ(3, sut.get_max_size());
    Some_type instance_3(3);
    sut.push_back(std::move(instance_3));
    EXPECT_EQ(3, sut.get_count());
    EXPECT_EQ(3, sut.get_max_size());
}
TEST(Array, should_let_to_get_elements) {
    engine::data_structures::Array<Some_type, 3> sut;
    sut.push_back(Some_type(1));
    sut.push_back(Some_type(2));
    sut.push_back(Some_type(3));
    EXPECT_EQ(1, sut.get(0).get_value());
    EXPECT_EQ(3, sut.get(2).get_value());
    EXPECT_EQ(2, sut.get(1).get_value());
}
TEST(Array, should_let_to_update_inserted_element) {
    engine::data_structures::Array<Some_type, 3> sut;
    sut.push_back(Some_type(1));
    sut.push_back(Some_type(2));
    sut.push_back(Some_type(3));
    sut.get(1).set_value(188);
    EXPECT_EQ(1, sut.get(0).get_value());
    EXPECT_EQ(188, sut.get(1).get_value());
    EXPECT_EQ(3, sut.get(2).get_value());
}
TEST(Array, should_answer_whether_its_empty) {
    engine::data_structures::Array<Some_type, 3> sut;
    EXPECT_TRUE(sut.is_empty());
    sut.push_back(Some_type(1));
    sut.push_back(Some_type(2));
    sut.push_back(Some_type(3));
    EXPECT_FALSE(sut.is_empty());
}
TEST(Array, should_allow_to_remove_all_elements_if_empty) {
    engine::data_structures::Array<Some_type, 4> sut;
    sut.remove_all();
    EXPECT_EQ(0, sut.get_count());
    EXPECT_EQ(4, sut.get_max_size());
    EXPECT_EQ(Some_type::default_value(), *(sut.get_data() + 0));
    EXPECT_EQ(Some_type::default_value(), *(sut.get_data() + 1));
    EXPECT_EQ(Some_type::default_value(), *(sut.get_data() + 2));
    EXPECT_EQ(Some_type::default_value(), *(sut.get_data() + 3));
}
TEST(Array, should_allow_to_remove_all_elements) {
    engine::data_structures::Array<Some_type, 4> sut;
    sut.push_back(Some_type(1));
    sut.push_back(Some_type(2));
    sut.push_back(Some_type(3));
    sut.push_back(Some_type(188));
    sut.remove_all();
    EXPECT_EQ(0, sut.get_count());
    EXPECT_EQ(4, sut.get_max_size());
    EXPECT_EQ(Some_type::default_value(), *(sut.get_data() + 0));
    EXPECT_EQ(Some_type::default_value(), *(sut.get_data() + 1));
    EXPECT_EQ(Some_type::default_value(), *(sut.get_data() + 2));
    EXPECT_EQ(Some_type::default_value(), *(sut.get_data() + 3));
}
TEST(Array, should_allow_to_overwrite_element) {
    engine::data_structures::Array<Some_type, 4> sut;
    sut.push_back(Some_type(1));
    sut.push_back(Some_type(2));
    sut.push_back(Some_type(3));
    sut.push_back(Some_type(188));
    sut.set(1, Some_type(22));
    sut.set(3, Some_type(4));
    EXPECT_EQ(1, sut.get(0).get_value());
    EXPECT_EQ(3, sut.get(2).get_value());
    EXPECT_EQ(22, sut.get(1).get_value());
    EXPECT_EQ(4, sut.get(3).get_value());
    EXPECT_EQ(4, sut.get_count());
}
TEST(Array, should_allow_to_set_not_first_element_in_case_its_empty) {
    engine::data_structures::Array<Some_type, 4> sut;
    sut.set(2, Some_type(4));
    EXPECT_EQ(Some_type::default_value(), *(sut.get_data() + 0));
    EXPECT_EQ(Some_type::default_value(), *(sut.get_data() + 1));
    EXPECT_EQ(Some_type(4), *(sut.get_data() + 2));
    EXPECT_EQ(Some_type::default_value(), *(sut.get_data() + 3));
    EXPECT_EQ(3, sut.get_count());
    EXPECT_EQ(4, sut.get_max_size());
}
TEST(Array, should_allow_to_set_first_element_in_case_its_empty) {
    engine::data_structures::Array<Some_type, 4> sut;
    sut.set(0, Some_type(189));
    EXPECT_EQ(Some_type(189), *(sut.get_data() + 0));
    EXPECT_EQ(Some_type::default_value(), *(sut.get_data() + 1));
    EXPECT_EQ(Some_type::default_value(), *(sut.get_data() + 2));
    EXPECT_EQ(Some_type::default_value(), *(sut.get_data() + 3));
    EXPECT_EQ(1, sut.get_count());
    EXPECT_EQ(4, sut.get_max_size());
}
TEST(Array, should_allow_to_set_element_in_case_its_not_empty_but_max_size_was_not_reached) {
    engine::data_structures::Array<Some_type, 4> sut;
    sut.push_back(Some_type(1));
    sut.push_back(Some_type(2));
    sut.set(3, Some_type(4));
    EXPECT_EQ(Some_type(1), *(sut.get_data() + 0));
    EXPECT_EQ(Some_type(2), *(sut.get_data() + 1));
    EXPECT_EQ(Some_type::default_value(), *(sut.get_data() + 2));
    EXPECT_EQ(Some_type(4), *(sut.get_data() + 3));
}
TEST(Array, should_allow_to_iterate_over_all_elements) {
    engine::data_structures::Array<Some_type, 4> sut;
    sut.push_back(Some_type(1));
    sut.push_back(Some_type(2));
    sut.push_back(Some_type(3));
    sut.push_back(Some_type(4));
    int i = 0;
    sut.get_iterator().visit_all([&i](const Some_type& st) {
        EXPECT_EQ(Some_type(++i), st);
    });
    EXPECT_EQ(4, i);
}
TEST(Array, should_return_no_element_if_its_empty_during_iteration_over_all_elements) {
    engine::data_structures::Array<Some_type, 4> sut;
    int i = 0;
    sut.get_iterator().visit_all([&i](const Some_type& st) {
        FAIL();
    });
    EXPECT_EQ(0, i);
}
TEST(Array, should_allow_to_return_const_reference) {
    using Array_of_some_type = engine::data_structures::Array<Some_type, 4>;
    Array_of_some_type sut;
    sut.push_back(Some_type(1));
    sut.push_back(Some_type(2));
    sut.push_back(Some_type(3));
    sut.push_back(Some_type(4));
    EXPECT_EQ(Some_type(1), static_cast<const Array_of_some_type&>(sut).get(0));
}
}  // namespace
