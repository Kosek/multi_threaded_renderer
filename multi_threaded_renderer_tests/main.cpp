// Copyright 2017 Tomasz Kosek. All rights reserved.

#include <stdafx.h>

#include "gmock/gmock.h"

int main(int argc, char** argv) {
	// The following line must be executed to initialize Google Mock
	// (and Google Test) before running the tests.
	::testing::InitGoogleMock(&argc, argv);
	return RUN_ALL_TESTS();
}