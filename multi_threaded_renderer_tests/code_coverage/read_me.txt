Download OpenCppCoverag from https://opencppcoverage.codeplex.com/.

Installation sequence for version 0.9.6.1:
1. OpenCppCoverageSetup-x86-0.9.6.1.exe
2. OpenCppCoverageSetup-x64-0.9.6.1.exe
3. OpenCppCoverage-0.9.4.0.vsix

In Visual Studio 2015 press Ctrl-R, Ctrl-C.